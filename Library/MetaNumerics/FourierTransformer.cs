﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> An engine for performing Fourier transforms on complex series. </summary>
    /// <remarks>
    /// <para>A Fourier transform decomposes a function into a sum of different frequency components.
    /// This is useful for a wide array of applications.</para>
    /// <para>Mathematically, the DFT is an N-dimensional linear transformation
    /// with coefficients that are the Nth complex roots of unity.</para>
    /// <img src="../images/Fourier.png" />
    /// <para>An instance of the FourierTransformer class performs DFTs on series of a particular
    /// length, given by its <see cref="FourierTransformer.Length" /> property. This specialization
    /// allows certain parts of the DFT calculation, which are independent of the transformed series
    /// but dependent on the length of the series, to be performed only once and then re-used for all
    /// transforms of that length. This saves time and improves performance. If you need to perform
    /// DFTs on series with different lengths, simply create a separate instance of the
    /// FourierTransform class for each required length.</para>
    /// <para>Many simple DFT implementations require that the series length be a power of two (2, 4,
    /// 8, 16, etc.). Meta.Numerics supports DFTs of any length. Our DFT implementation is fast --
    /// order O(N log N) -- for all lengths, including lengths that have large prime factors.</para>
    /// &lt;para&gt;
    /// </remarks>
    /// <seealso href="http://en.wikipedia.org/wiki/Discrete-time_Fourier_transform"/>
    public sealed class FourierTransformer
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the Fourier transformer. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="size"> The series length of the transformer, which must be positive. </param>
        public FourierTransformer( int size ) : this( size, FourierSign.Negative, FourierNormalization.None )
        {
        }

        /// <summary>
        /// Initializes a new instance of the Fourier transformer with the given sign and normalization
        /// conventions.
        /// </summary>
        /// <remarks>
        /// <para>There are multiple conventions for both the sign of the exponent and the overall
        /// normalization of Fourier transforms. The default conventions for some widely used software
        /// packages are summarized in the following table.</para>
        /// <table>
        /// <tr><th>Software</th><th>Sign</th><th>Normalization</th></tr>
        /// <tr><td>Meta.Numerics</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
        /// <tr><td>Matlab</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
        /// <tr><td>Mathematica</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.Unitary"/></td></tr>
        /// <tr><td>Numerical
        /// Recipes</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.None"/></td></tr>
        /// </table>
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="size">                    The series length of the transformer, which must be
        /// positive. </param>
        /// <param name="signConvention">          The sign convention of the transformer. </param>
        /// <param name="normalizationConvention"> The normalization convention of the transformer. </param>
        public FourierTransformer( int size, FourierSign signConvention, FourierNormalization normalizationConvention )
        {
            if ( size < 1 )
                throw new ArgumentOutOfRangeException( nameof( size ) );
            this.Length = size;
            this.SignConvention = signConvention;
            this.NormalizationConvention = normalizationConvention;

            // pre-compute the Nth complex roots of unity
            this.Roots = FourierAlgorithms.ComputeRoots( size, +1 );

            // decompose the size into prime factors
            this.Factors = AdvancedMath.Factor( size );

            // store a plan for the transform based on the prime factorization
            this.Plan = new System.Collections.Generic.List<Transformlet>();
            foreach ( Factor factor in this.Factors )
            {
                Transformlet t;
                switch ( factor.Value )
                {
                    // use a radix-specialized Transformlet when available
                    case 2:
                        {
                            t = new RadixTwoTransformlet( size, this.Roots );
                            break;
                        }

                    case 3:
                        {
                            t = new RadixThreeTransformlet( size, this.Roots );
                            break;
                        }
                    // eventually, we should make an optimized radix-4 transform
                    case 5:
                        {
                            t = new RadixFiveTransformlet( size, this.Roots );
                            break;
                        }

                    case 7:
                        {
                            t = new RadixSevenTransformlet( size, this.Roots );
                            break;
                        }

                    case 11:
                    case 13:
                        {
                            // the base Transformlet is R^2, but when R is small, this can still be faster than the Bluestein algorithm
                            // timing measurements appear to indicate that this is the case for radix 11 and 13
                            // eventually, we should make optimized Winograd transformlets for these factors
                            t = new Transformlet( factor.Value, size, this.Roots );
                            break;
                        }

                    default:
                        {
                            // for large factors with no available specialized Transformlet, use the Bluestein algorithm
                            t = new BluesteinTransformlet( factor.Value, size, this.Roots );
                            break;
                        }
                }

                t.Multiplicity = factor.Multiplicity;

                // if ((factor.Value == 2) && (factor.Multiplicity % 2 == 0)) {
                // t = new RadixFourTransformlet(size, roots);
                // t.Multiplicity = factor.Multiplicity / 2;}

                this.Plan.Add( t );
            }
        }

        #endregion

        #region "MEMBRS "

        /// <summary> The factors. </summary>
        /// <value> The factors. </value>
        private System.Collections.Generic.List<Factor> Factors { get; set; }

        /// <summary> The plan. </summary>
        /// <value> The plan. </value>
        private System.Collections.Generic.List<Transformlet> Plan { get; set; }

        /// <summary> The roots. </summary>
        /// <value> The roots. </value>
        private Complex[] Roots { get; set; }

        /// <summary> The series length for which the transformer is specialized. </summary>
        /// <value> The length. </value>
        public int Length { get; private set; }

        /// <summary> Gets or sets the normalization convention used by the transformer. </summary>
        /// <value> The normalization convention. </value>
        public FourierNormalization NormalizationConvention { get; private set; }

        /// <summary> Gets or sets the normalization convention used by the transformer. </summary>
        /// <value> The sign convention. </value>
        public FourierSign SignConvention { get; private set; }

        /// <summary> Gets the sign. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The sign. </returns>
        private int GetSign()
        {
            return this.SignConvention == FourierSign.Positive ? +1 : -1;
        }

        #endregion

        #region "COMPLEX "

        /// <summary> Normalizes the specified x. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The x. </param>
        /// <param name="f"> The f. </param>
        private static void Normalize( Complex[] x, double f )
        {
            for ( int i = 0, loopTo = x.Length - 1; i <= loopTo; i++ )
                x[i] = new Complex( f * x[i].Real, f * x[i].Imaginary );
        }

        /// <summary> Transforms the specified x. </summary>
        /// <remarks>
        /// This is an internal transform method that does not do checking, modifies the input array, and
        /// requires you to give it a scratch array. x is the input array, which is overwritten by the
        /// output array, and y is a scratch array of the same length. The transform works by carrying
        /// out each Transformlet in the plan, with input from x and output to y, then switching y and x
        /// so that the input is in x for the next Transformlet.
        /// </remarks>
        /// <param name="x">    [in,out] The x. </param>
        /// <param name="y">    [in,out] The y. </param>
        /// <param name="sign"> The sign. </param>
        internal void Transform( ref Complex[] x, ref Complex[] y, int sign )
        {
            int ns = 1;
            foreach ( Transformlet t in this.Plan )
            {
                for ( int k = 0, loopTo = t.Multiplicity - 1; k <= loopTo; k++ )
                {
                    t.FftPass( x, y, ns, sign );
                    // we avoid element-by-element copying by just switching the arrays referenced by x and y
                    // this is why x and y must be passed in with the ref keyword
                    var temp = x;
                    x = y;
                    y = temp;
                    ns *= t.Radix;
                }
            }
        }

        /// <summary> Computes the Fourier transform of the given series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The series to transform. </param>
        /// <returns> The discrete Fourier transform of the series. </returns>
        public Complex[] Transform( System.Collections.Generic.IList<Complex> values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Count != this.Length )
                throw new ArgumentOutOfRangeException( nameof( values ), $"Data Size {values.Count} does not match expected size of {this.Length}" );

            // copy the original values into a new array
            var x = new Complex[this.Length];
            values.CopyTo( x, 0 );

            // normalize the copy appropriately
            if ( this.NormalizationConvention == FourierNormalization.Unitary )
            {
                Normalize( x, 1.0d / Math.Sqrt( this.Length ) );
            }
            else if ( this.NormalizationConvention == FourierNormalization.Inverse )
            {
                Normalize( x, 1.0d / this.Length );
            }

            // create a scratch array
            var y = new Complex[this.Length];

            // do the FFT
            this.Transform( ref x, ref y, this.GetSign() );
            return x;
        }

        /// <summary> Computes the Fourier transform of the given series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The series to transform. </param>
        /// <returns> The discrete Fourier transform of the series. </returns>
        public Complex[] Transform( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length != this.Length )
                throw new ArgumentOutOfRangeException( nameof( values ), $"Data Size {values.Length} does not match expected size of {this.Length}" );

            // normalize the copy appropriately
            if ( this.NormalizationConvention == FourierNormalization.Unitary )
            {
                Normalize( values, 1.0d / Math.Sqrt( this.Length ) );
            }
            else if ( this.NormalizationConvention == FourierNormalization.Inverse )
            {
                Normalize( values, 1.0d / this.Length );
            }

            // create a scratch array
            var y = new Complex[this.Length];

            // do the FFT
            this.Transform( ref values, ref y, this.GetSign() );
            return values;
        }

        /// <summary> Computes the inverse Fourier transform of the given series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The series to invert. </param>
        /// <returns> The inverse discrete Fourier transform of the series. </returns>
        public Complex[] InverseTransform( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length != this.Length )
                throw new ArgumentOutOfRangeException( nameof( values ), $"Data Size {values.Length} does not match expected size of {this.Length}" );

            // normalize the copy appropriately
            if ( this.NormalizationConvention == FourierNormalization.None )
            {
                Normalize( values, 1.0d / this.Length );
            }
            else if ( this.NormalizationConvention == FourierNormalization.Unitary )
            {
                Normalize( values, 1.0d / Math.Sqrt( this.Length ) );
            }

            // create a scratch array
            var y = new Complex[this.Length];

            // do the FFT
            this.Transform( ref values, ref y, -this.GetSign() );
            return values;
        }

        #endregion

        #region " DOUBLE "

        /// <summary> Computes the Fourier transform of the given series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        /// <returns> The discrete Fourier transform of the series. </returns>
        public Complex[] Transform( double[] reals, double[] imaginaries )
        {
            return this.Transform( ComplexArrays.ToComplex( reals, imaginaries ) );
        }

        #endregion

        #region " SINGLE "

        /// <summary> Computes the Fourier transform of the given series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        /// <returns> The discrete Fourier transform of the series. </returns>
        public Complex[] Transform( float[] reals, float[] imaginaries )
        {
            return this.Transform( ComplexArrays.ToComplex( reals, imaginaries ) );
        }

        #endregion

    }

    /// <summary>
    /// Specifies the normalization convention to be used in a forward Fourier transform.
    /// </summary>
    /// <remarks>
    /// <para>The most common convention in signal processing applications is
    /// <see cref="FourierNormalization.None"/>.</para>
    /// </remarks>
    public enum FourierNormalization
    {

        /// <summary> The series is not normalized. </summary>
        None,

        /// <summary> The series is multiplied by 1/N<sup>1/2</sup>. </summary>
        Unitary,

        /// <summary> The series is multiplied by 1/N. </summary>
        Inverse
    }

    /// <summary>
    /// Specifies the sign convention to be used in the exponent of a forward Fourier transform.
    /// </summary>
    /// <remarks>
    /// <para>The most common convention in signal processing applications is
    /// <see cref="FourierSign.Negative"/>.</para>
    /// </remarks>
    public enum FourierSign
    {

        /// <summary> The exponent has positive imaginary values. </summary>
        Positive,

        /// <summary> The exponent has negative imaginary values. </summary>
        Negative
    }
}