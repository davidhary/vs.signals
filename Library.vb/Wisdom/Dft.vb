Imports System.Numerics

Namespace Wisdom

    ''' <summary> Calculates the Fast Fourier Transform using the FFTW algorithm. </summary>
    ''' <remarks>
    ''' Includes procedures for calculating the forward and inverse Fourier transform. FFTW is best
    ''' at handling sizes of the form 2^a x 3^b x 5^c 7^d x 11^e x 13^f, where e + f is either 0 or 1,
    ''' and the other exponents are arbitrary. Other sizes are computed by means of a slow, general-
    ''' purpose algorithm (which nevertheless retains O(n log n)
    ''' performance even for prime sizes). It is possible to customize FFTW for different array sizes;
    ''' see Chapter 8 [Installation and Customization], page 57. Transforms whose sizes are powers of
    ''' 2 are especially fast.
    ''' <para>
    ''' FFTW computes an unnormalized transform: computing a forward followed by a backward transform
    ''' (or vice versa)
    ''' will result in the original data multiplied by the size of the transform (the product of the
    ''' dimensions).
    ''' </para> <para>
    ''' David, 09/01/07, 2.0.2800 </para><para>
    ''' (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    ''' Licensed under The MIT License. </para>
    ''' </remarks>
    Public Class Dft
        Inherits FourierTransformBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Public Sub New()

            ' instantiate the base class
            MyBase.New(FourierTransformType.Wisdom)

        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed Then
                    If disposing Then
                        ' Free managed resources when explicitly called
                    End If
                    ' Free shared unmanaged resources
                    If Me._ComplexDataF IsNot Nothing Then
                        Me._ComplexDataF.Dispose()
                        Me._ComplexDataF = Nothing
                    End If
                    If Me._ComplexDataR IsNot Nothing Then
                        Me._ComplexDataR.Dispose()
                        Me._ComplexDataR = Nothing
                    End If
                    If Me._DftPlanF IsNot Nothing Then
                        Me._DftPlanF.Dispose()
                        Me._DftPlanF = Nothing
                    End If
                    If Me._DftPlanR IsNot Nothing Then
                        Me._DftPlanR.Dispose()
                        Me._DftPlanR = Nothing
                    End If
                End If
            Finally
                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " PROPERTIES "

#End Region

#Region " COMPLEX "

        ''' <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="values"> The values. </param>
        Public Overrides Sub Forward(ByVal values() As Complex)
            If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))
            If values.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(values), "Array must be longer than 1")
            Dim reals As Double() = New Double(values.Length - 1) {}
            Dim imaginaries As Double() = New Double(values.Length - 1) {}
            ComplexArrays.CopyTo(values, reals, imaginaries)
            Me.Forward(reals, imaginaries)
            ComplexArrays.Copy(reals, imaginaries, values)
        End Sub

#End Region

#Region " DOUBLE "

        ''' <summary>
        ''' Gets or sets reference to the DFT plan for Double DFT.
        ''' </summary>
        Private _DftPlanR As PlanR

        ''' <summary>
        ''' Holds the input and output data.
        ''' </summary>
        Private _ComplexDataR As ComplexArrayR

        ''' <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        ''' <param name="reals">       Holds the real values. </param>
        ''' <param name="imaginaries"> Holds the imaginary values. </param>
        Public Overrides Sub Forward(ByVal reals() As Double, ByVal imaginaries() As Double)

            ' Initializes
            MyBase.Forward(reals, imaginaries)

            ' execute the DFT
            Me._DftPlanR.Execute()

            ' get the data
            Me._ComplexDataR.CopyTo(reals, imaginaries)

        End Sub

        ''' <summary> Initializes the transform. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reals">       Holds the real values. </param>
        ''' <param name="imaginaries"> Holds the imaginary values. </param>
        Protected Overrides Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))

            ' set the element count
            Me.TimeSeriesLength = reals.Length

            ' allocate the data and copy it to the complex array.
            Me._ComplexDataR = New ComplexArrayR(reals, imaginaries, False)

            ' get a new plan.
            Me._DftPlanR = Planner.Create(reals.Length, Me._ComplexDataR, Me._ComplexDataR, TransformDirection.Forward, PlannerOptions.Estimate)

        End Sub

#End Region

#Region " SINGLE "

        ''' <summary>
        ''' Gets or sets reference to the DFT plan for Double DFT.
        ''' </summary>
        Private _DftPlanF As PlanF

        ''' <summary>
        ''' Holds the input and output data.
        ''' </summary>
        Private _ComplexDataF As ComplexArrayF

        ''' <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        ''' <param name="reals">       Holds the real values. </param>
        ''' <param name="imaginaries"> Holds the imaginary values. </param>
        Public Overrides Sub Forward(ByVal reals() As Single, ByVal imaginaries() As Single)

            ' copy data to the Complex Data Array
            MyBase.Forward(reals, imaginaries)

            ' execute the DFT
            Me._DftPlanF.Execute()

            ' get the data
            Me._ComplexDataF.CopyTo(reals, imaginaries)

        End Sub

        ''' <summary> Initializes the transform. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reals">       Holds the real values. </param>
        ''' <param name="imaginaries"> Holds the imaginary values. </param>
        Protected Overrides Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))

            ' set the element count
            Me.TimeSeriesLength = reals.Length

            ' allocate the data and copy it to the complex array.
            Me._ComplexDataF = New ComplexArrayF(reals, imaginaries, False)

            ' get a new plan.
            Me._DftPlanF = Planner.Create(Me.TimeSeriesLength, Me._ComplexDataF, Me._ComplexDataF, TransformDirection.Forward, PlannerOptions.Estimate)

        End Sub

#End Region

    End Class

End Namespace

