
Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Signals + &H1

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Algorithms Signals Forms"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Algorithms Signals Forms"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Algorithms.Signals.Forms"

    End Class

End Namespace

