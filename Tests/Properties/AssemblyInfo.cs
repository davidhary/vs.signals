﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Signals Tests")]
[assembly: AssemblyDescription("Signals Unit Tests Library")]
[assembly: AssemblyProduct("isr.Algorithms.Signals.Tests")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
