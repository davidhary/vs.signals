using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using isr.Algorithms.Signals.Forms.ExceptionExtensions;
using isr.Core.EnumExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Algorithms.Signals.Forms
{

    /// <summary>
    /// Includes test program for calculating the spectrum using DFT, FFTW, and sliding Fourier
    /// transform algorithms.
    /// </summary>
    /// <remarks>
    /// David, 2005-10-11, 1.0.2110. Convert to .NET <para>
    /// Launch this form by calling its Show or ShowDialog method from its default instance.
    /// </para><para>
    /// (c) 2005 Integrated Scientific Resources, Inc.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public partial class WisdomSpectrumView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public WisdomSpectrumView() : base()
        {
            this.InitializingComponents = true;

            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            this.InitializingComponents = false;
            this._NotesMessageList.CommenceUpdates();
            this.__PointsToDisplayTextBox.Name = "_PointsToDisplayTextBox";
            this.__SignalChartPanel.Name = "_SignalChartPanel";
            this.__PointsTextBox.Name = "_PointsTextBox";
            this.__PhaseTextBox.Name = "_PhaseTextBox";
            this.__CyclesTextBox.Name = "_CyclesTextBox";
            this.__SpectrumChartPanel.Name = "_SpectrumChartPanel";
            this.__StartStopCheckBox.Name = "_StartStopCheckBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.InitializingComponents = true;
                if ( disposing )
                {
                    this._NotesMessageList.SuspendUpdatesReleaseIndicators();

                    // Free managed resources when explicitly called
                    if ( this._SignalChartPane is object )
                    {
                        this._SignalChartPane.Dispose();
                        this._SignalChartPane = null;
                    }

                    if ( this._SpectrumChartPan is object )
                    {
                        this._SpectrumChartPan.Dispose();
                        this._SpectrumChartPan = null;
                    }

                    if ( this.components is object )
                    {
                        this.components.Dispose();
                    }
                }
            }
            // Free shared unmanaged resources
            // onDisposeUnManagedResources()

            finally
            {
                this.InitializingComponents = false;
                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the selected example. </summary>
        /// <value> The selected example. </value>
        private Example SelectedExample => ( Example ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ExampleComboBox.SelectedItem).Key );

        /// <summary> Gets or sets the status message. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private string _StatusMessage = string.Empty;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // Initialize and set the user interface
                this.InitializeUserInterface();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " USER INTERFACE "

        /// <summary> Shows the status. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="message"> The message. </param>
        private void ShowStatus( string message )
        {
            this._StatusMessage = message;
            _ = this._NotesMessageList.AddMessage( message );
            this._StatusStatusBarPanel.Text = message;
        }

        /// <summary>Gets or sets the data format</summary>
        private const string _ListFormat = "{0:0.000000000000000}{1}{2:0.000000000000000}";

        /// <summary> Enumerates the action options. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private enum Example
        {

            /// <summary> An enum constant representing the discrete fourier transform option. </summary>
            [System.ComponentModel.Description( "Discrete Fourier Transform" )]
            DiscreteFourierTransform,

            /// <summary> An enum constant representing the sliding FFT option. </summary>
            [System.ComponentModel.Description( "Sliding FFT" )]
            SlidingFFT,

            /// <summary> An enum constant representing the wisdom FFT option. </summary>
            [System.ComponentModel.Description( "Wisdom FFT" )]
            WisdomFFT
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {

            // tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
            // tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

            // Set initial values defining the signal
            this._PointsTextBox.Text = "16"; // "1000"
            this._CyclesTextBox.Text = "1"; // "11.5"
            this._PhaseTextBox.Text = "0"; // "45"
            this._PointsToDisplayTextBox.Text = "100";
            this._SignalDurationTextBox.Text = "1";

            // set the default sample
            this.PopulateExampleComboBox();
            this._ExampleComboBox.SelectedIndex = 0;

            // create the two charts.
            this.CreateSpectrumChart();
            this.CreateSignalChart();

            // plot the default sign wave
            _ = this.SineWaveDouble();
        }

        #endregion

        #region " SIGNALS "

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> . </param>
        /// <param name="phase">     . </param>
        /// <param name="elements">  . </param>
        /// <returns> A Double() </returns>
        private float[] SineWave( float frequency, float phase, int elements )
        {

            // get the signal
            var signal = Signal.Sine( frequency, phase, elements );

            // Plot the Signal 
            this.ChartSignal( Signal.Ramp( float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / elements, elements ), signal );
            return signal;
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> . </param>
        /// <param name="phase">     . </param>
        /// <param name="elements">  . </param>
        /// <returns> A Double() </returns>
        private double[] SineWave( double frequency, double phase, int elements )
        {

            // get the signal
            var signal = Signal.Sine( frequency, phase, elements );

            // Plot the Signal 
            this.ChartSignal( Signal.Ramp( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / elements, elements ), signal );
            return signal;
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Double() </returns>
        private double[] SineWaveDouble()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            double signalCycles = double.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            double signalPhase = Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture ) );
            return this.SineWave( signalCycles, signalPhase, signalPoints );
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Single() </returns>
        private float[] SineWaveSingle()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            float signalCycles = float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            float signalPhase = Convert.ToSingle( Signal.ToRadians( float.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) ) );
            return this.SineWave( signalCycles, signalPhase, signalPoints );
        }

        /// <summary> The copy signal format. </summary>
        private const string _CopySigFormat = "    Copy Signal:  {0:0.###} ms ";

        /// <summary> The FFT initialize format. </summary>
        private const string _FftInitFormat = " FFT Initialize:  {0:0} ms ";

        /// <summary> The FFT calculate format. </summary>
        private const string _FftCalcFormat = "  FFT Calculate:  {0:0.###} ms ";

        /// <summary> The frq calculate format. </summary>
        private const string _FrqCalcFormat = "FFT Frequencies:  {0:0.###} ms ";

        /// <summary> The magnitude calculate format. </summary>
        private const string _MagCalcFormat = "  FFT Magnitude:  {0:0.###} ms ";

        /// <summary> The inverse calculate format. </summary>
        private const string _InvCalcFormat = "    Inverse FFT:  {0:0.###} ms ";

        /// <summary> The charting format. </summary>
        private const string _ChartingFormat = "  Charting time:  {0:0} ms ";

        #endregion

        #region " SPECTRUM: DOUBLE "

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumDouble( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingBuilder = new System.Text.StringBuilder();
            _ = timingBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating double-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the sine wave.
            var signal = this.SineWaveDouble();

            // Allocate array for the Real part of the DFT.
            double[] real;
            real = new double[fftPoints];

            // Allocate array for the imaginary part of the DFT.
            double[] imaginary;
            imaginary = new double[fftPoints];

            // ------------ SELECT PROPERTIES ---------------

            // select the spectrum type
            Spectrum spectrum;
            FourierTransformBase fft;
            switch ( algorithm )
            {
                case Example.DiscreteFourierTransform:
                    {
                        fft = new DiscreteFourierTransform();
                        break;
                    }

                case Example.WisdomFFT:
                    {
                        fft = new Wisdom.Dft();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }

            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            spectrum = new Spectrum( fft ) {
                IsScaleFft = true,
                IsRemoveMean = this._RemoveMeanCheckBox.Checked
            };

            // Use Taper Window as selected
            spectrum.TaperWindow = this._TaperWindowCheckBox.Checked ? new BlackmanTaperWindow() : null;

            // Initialize the FFT.
            timeKeeper.Start();
            spectrum.Initialize( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ CREATE TEH SIGNAL ---------------

            timeKeeper.Start();
            signal.CopyTo( real, 0 );
            Array.Clear( imaginary, 0, imaginary.Length );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _CopySigFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Compute the FFT.
            timeKeeper.Start();
            spectrum.Calculate( real, imaginary );
            duration = timeKeeper.Elapsed;
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";
            _ = timingBuilder.AppendFormat( _FftCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------- Calculate FFT outcomes ---------

            // You must re-map the FFT utilities if using more than one time series at a time
            // isr.Algorithms.Signals.Helper.Map(real, imaginary)

            // Get the FFT magnitudes
            double[] magnitudes;
            _ = real.Magnitudes( imaginary );
            timeKeeper.Start();
            magnitudes = real.Magnitudes( imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _MagCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Get the Frequency
            double[] frequencies;
            timeKeeper.Start();
            frequencies = Signal.Frequencies( 1.0d, fftPoints );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FrqCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Compute the inverse transform.
            timeKeeper.Start();
            fft.Inverse( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _InvCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Display the forward and inverse data.
            this.DisplayCalculationAccuracy( signal, real );

            // ------------ Plot FFT outcome ---------------
            timeKeeper.Start();
            this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _ChartingFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();
            this._TimingTextBox.Text = timingBuilder.ToString();
        }

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumSingle( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingBuilder = new System.Text.StringBuilder();
            _ = timingBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating single-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the sine wave.
            var signal = this.SineWaveSingle();

            // Allocate array for the Real part of the DFT.
            float[] real;
            real = new float[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            float[] imaginary;
            imaginary = new float[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            Spectrum spectrum;
            FourierTransformBase fft;
            switch ( algorithm )
            {
                case Example.DiscreteFourierTransform:
                    {
                        fft = new DiscreteFourierTransform();
                        break;
                    }

                case Example.WisdomFFT:
                    {
                        fft = new Wisdom.Dft();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }
            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            spectrum = new Spectrum( fft ) {
                IsScaleFft = true,
                IsRemoveMean = this._RemoveMeanCheckBox.Checked
            };

            // Use Taper Window as selected
            spectrum.TaperWindow = this._TaperWindowCheckBox.Checked ? new BlackmanTaperWindow() : null;

            // Initialize the FFT.
            timeKeeper.Start();
            spectrum.Initialize( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Compute the FFT.
            timeKeeper.Start();
            spectrum.Calculate( real, imaginary );
            duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms ";

            // ------- Calculate FFT outcomes ---------

            // You must re-map the FFT utilities if using more than one time series at a time
            // isr.Algorithms.Signals.Helper.Map(real, imaginary)

            // Get the FFT magnitudes
            float[] magnitudes;
            magnitudes = real.Magnitudes( imaginary );

            // Get the Frequency
            float[] frequencies;
            frequencies = Signal.Frequencies( 1.0f, fftPoints );

            // Compute the inverse transform.
            fft.Inverse( real, imaginary );

            // Display the forward and inverse data.
            this.DisplayCalculationAccuracy( signal, real );

            // ------------ Plot FFT outcome ---------------
            this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1f );
        }

        /// <summary>
        /// Executes a single-precision FFT using the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void CalculateSpectrumSingle2( Example algorithm )
        {
            this.ShowStatus( $"Calculating single-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveSingle();

            // Allocate array for the Real part of the DFT.
            float[] real;
            real = new float[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            float[] imaginary;
            imaginary = new float[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the FFTW class
            var fft = new Wisdom.Dft();
            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            var spectrum = new Spectrum( fft ) {
                IsScaleFft = true,
                IsRemoveMean = this._RemoveMeanCheckBox.Checked
            };

            // Use Window as selected
            spectrum.TaperWindow = this._TaperWindowCheckBox.Checked ? new BlackmanTaperWindow() : null;

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Start();
            spectrum.Calculate( real, imaginary );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // ------- Calculate FFT outcomes ---------

            // You must re-map the FFT utilities if using more than one time series at a time
            // isr.Algorithms.Signals.Helper.Map(real, imaginary)

            // Get the FFT magnitudes
            float[] magnitudes;
            magnitudes = real.Magnitudes( imaginary );

            // Get the Frequency
            float[] frequencies;
            frequencies = Signal.Frequencies( 1.0f, fftPoints );

            // Compute the inverse transform.
            fft.Inverse( real, imaginary );

            // Display the forward and inverse data.
            this.DisplayCalculationAccuracy( signal, real );

            // ------------ Plot FFT outcome ---------------

            this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1f );
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal"> . </param>
        /// <param name="real">   . </param>
        private void DisplayCalculationAccuracy( double[] signal, double[] real )
        {
            if ( signal is null )
            {
                return;
            }

            if ( real is null )
            {
                return;
            }

            int elementCount = Math.Min( real.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._FftListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - real[i];
                totalError += value * value;
                _ = this._FftListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], ControlChars.Tab, real[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}";
            this._MainStatusBar.Invalidate();
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal"> . </param>
        /// <param name="real">   . </param>
        private void DisplayCalculationAccuracy( float[] signal, float[] real )
        {
            if ( signal is null )
            {
                return;
            }

            if ( real is null )
            {
                return;
            }

            int elementCount = Math.Min( real.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._FftListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - real[i];
                totalError += value * value;
                _ = this._FftListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], ControlChars.Tab, real[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}";
            this._MainStatusBar.Invalidate();
        }

        /// <summary> Populates the list of options in the action combo box. </summary>
        /// <remarks>
        /// David, 2004-11-08. Created <para>
        /// It seems that out enumerated list does not work very well with this list. </para>
        /// </remarks>
        private void PopulateExampleComboBox()
        {

            // set the action list
            this._ExampleComboBox.Items.Clear();
            this._ExampleComboBox.DataSource = typeof( Example ).ValueDescriptionPairs().ToList();
            this._ExampleComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._ExampleComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( double noiseFigure )
        {
            this.ShowStatus( "Calculating double-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveDouble();

            // Allocate array for the Real part of the DFT.
            double[] real;
            real = new double[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            double[] imaginary;
            imaginary = new double[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the FFTW class
            var fft = new Wisdom.Dft();

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Start();
            fft.Forward( real, imaginary );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._FftListBox.Items.Clear();

            // Clear the error value
            this._ErrorStatusBarPanel.Text = string.Empty;

            // Gets the FFT magnitudes
            double[] magnitudes;

            // Gets the Frequency
            double[] frequencies;

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            double deltaPhase = 2d * Math.PI * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / Convert.ToSingle( fftPoints );

            // get the signal phase of the last point
            double signalPhase = Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2d;

            // First element in the previous Real part of the time series
            double oldReal;

            // First element in the previous imaginary part of the time series.
            double oldImaginary;

            // New Real part of Signal
            double newReal;

            // New imaginary part of Signal
            double newImaginary;

            // Create a new instance of the sliding fft class
            var slidingFft = new SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( real, imaginary );

            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // You must re-map the FFT utilities if using more than one time series at a time
                // isr.Algorithms.Signals.Helper.Map(real, imaginary)

                // Get the FFT magnitudes
                magnitudes = real.Magnitudes( imaginary );

                // Get the Frequency
                frequencies = Signal.Frequencies( 1.0d, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( Signal.Ramp( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );

                // Update the previous values of the signal
                oldReal = signal[firstSignalPoint];
                oldImaginary = 0d;

                // Get new signal values.
                signalPhase += deltaPhase;
                newReal = Math.Sin( signalPhase );
                newImaginary = 0d;

                // Add some random noise to make it interesting.
                newReal += noiseFigure * 2d * (VBMath.Rnd() - 0.5d);

                // Update the signal itself.

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newReal;

                // You must re-map the FFT utilities if using more than one time series at a time
                // isr.Algorithms.Signals.Helper.Map(real, imaginary)

                // Calculate the sliding FFT coefficients.
                _ = slidingFft.Update( newReal, newImaginary, oldReal, oldImaginary );
            }
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( float noiseFigure )
        {
            this.ShowStatus( "Calculating single-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveSingle();

            // Allocate array for the Real part of the DFT.
            float[] real;
            real = new float[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            float[] imaginary;
            imaginary = new float[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the FFTW class
            var fft = new Wisdom.Dft();

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Start();
            fft.Forward( real, imaginary );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._FftListBox.Items.Clear();
            this._ErrorStatusBarPanel.Text = string.Empty;

            // Gets the FFT magnitudes
            float[] magnitudes;

            // Gets the Frequency
            float[] frequencies;

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            float deltaPhase = 2.0f * Convert.ToSingle( Math.PI ) * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints;

            // get the signal phase of the last point
            float signalPhase = Convert.ToSingle( Signal.ToRadians( float.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2f;
            float oldReal; // First element in the previous
                           // real part of the time series.

            float oldImaginary; // First element in the previous
                                // imaginary time series.

            float newReal; // New Real part of Signal
            float newImaginary; // New imaginary part of Signal

            // Create a new instance of the sliding fft class
            var slidingFft = new SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( real, imaginary );

            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // You must re-map the FFT utilities if using more than one time series at a time
                // isr.Algorithms.Signals.Helper.Map(real, imaginary)

                // Get the FFT magnitudes
                magnitudes = real.Magnitudes( imaginary );

                // Get the Frequency
                frequencies = Signal.Frequencies( 1.0f, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( Signal.Ramp( float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1f );

                // Update the previous values of the signal
                oldReal = signal[firstSignalPoint];
                oldImaginary = 0f;

                // Get new signal values.
                signalPhase += deltaPhase;
                newReal = Convert.ToSingle( Math.Sin( signalPhase ) );
                newImaginary = 0f;

                // Add some random noise to make it interesting.
                newReal += noiseFigure * 2.0f * (VBMath.Rnd() - 0.5f);

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newReal;

                // You must re-map the FFT utilities if using more than one time series at a time
                // isr.Algorithms.Signals.Helper.Map(real, imaginary)

                // Calculate the sliding FFT coefficients.
                _ = slidingFft.Update( newReal, newImaginary, oldReal, oldImaginary );
            }
        }

        #endregion

        #region " CHARTING "

        #region " AMPLITUDE SPECTRUM CHART "

        /// <summary>Gets or sets reference to the amplitude spectrum curve.</summary>
        private Visuals.Curve _AmplitudeSpectrumCurve;

        /// <summary>Gets or sets reference to the spectrum amplitude axis.</summary>
        private Visuals.Axis _AmplitudeSpectrumAxis;

        /// <summary>Gets or sets reference to the spectrum frequency axis.</summary>
        private Visuals.Axis _FrequencyAxis;

        /// <summary>Charts spectrum.</summary>
        private Visuals.ChartPane _SpectrumChartPan;

        /// <summary> Creates the chart for displaying the amplitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateSpectrumChart()
        {

            // set chart area and titles.
            this._SpectrumChartPan = new Visuals.ChartPane() { PaneArea = new RectangleF( 10f, 10f, 10f, 10f ) };
            this._SpectrumChartPan.Title.Caption = "Spectrum";
            this._SpectrumChartPan.Legend.Visible = false;
            this._FrequencyAxis = this._SpectrumChartPan.AddAxis( "Frequency, Hz", Visuals.AxisType.X );
            this._FrequencyAxis.Max = new Visuals.AutoValueR( 100d, false );
            this._FrequencyAxis.Min = new Visuals.AutoValueR( 1d, false );
            this._FrequencyAxis.Grid.Visible = true;
            this._FrequencyAxis.Grid.LineColor = Color.DarkGray;
            this._FrequencyAxis.TickLabels.Visible = true;
            this._FrequencyAxis.TickLabels.DecimalPlaces = new Visuals.AutoValue( 1, true );
            // frequencyAxis.TickLabels.Appearance.Angle = 60.0F

            this._AmplitudeSpectrumAxis = this._SpectrumChartPan.AddAxis( "Amplitude, Volts", Visuals.AxisType.Y );
            this._AmplitudeSpectrumAxis.CoordinateScale = new Visuals.CoordinateScale( Visuals.CoordinateScaleType.Linear );
            this._AmplitudeSpectrumAxis.Title.Visible = true;
            this._AmplitudeSpectrumAxis.Max = new Visuals.AutoValueR( 1d, true );
            this._AmplitudeSpectrumAxis.Min = new Visuals.AutoValueR( 0d, true );
            this._AmplitudeSpectrumAxis.Grid.Visible = true;
            this._AmplitudeSpectrumAxis.Grid.LineColor = Color.DarkGray;
            this._AmplitudeSpectrumAxis.TickLabels.Visible = true;
            this._AmplitudeSpectrumAxis.TickLabels.DecimalPlaces = new Visuals.AutoValue( 0, true );
            this._SpectrumChartPan.AxisFrame.FillColor = Color.WhiteSmoke; // color.FromArgb(232, 236, 245) '  Color.LightGoldenrodYellow
            this._SpectrumChartPan.SetSize( this._SpectrumChartPanel.ClientRectangle );
            this._AmplitudeSpectrumCurve = this._SpectrumChartPan.AddCurve( Visuals.CurveType.XY, "Amplitude Spectrum", this._FrequencyAxis, this._AmplitudeSpectrumAxis );
            this._AmplitudeSpectrumCurve.Cord.LineColor = Color.Red;
            this._AmplitudeSpectrumCurve.Cord.CordType = Visuals.CordType.Linear;
            this._AmplitudeSpectrumCurve.Symbol.Visible = false;
            this._AmplitudeSpectrumCurve.Cord.LineWidth = 1.0f;
            this._SpectrumChartPan.Rescale();
        }

        /// <summary>
        /// Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequencies"> Holds the spectrum frequencies. </param>
        /// <param name="magnitudes">  Holds the spectrum amplitudes. </param>
        /// <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
        /// plotting data that was not scaled. </param>
        private void ChartAmplitudeSpectrum( float[] frequencies, float[] magnitudes, float scaleFactor )
        {

            // copy the amplitudes to a double array
            var amplitudes = new double[frequencies.Length];
            magnitudes.CopyTo( amplitudes, 0 );

            // copy the frequencies to a double array.
            var freq = new double[frequencies.Length];
            frequencies.CopyTo( freq, 0 );
            this.ChartAmplitudeSpectrum( freq, amplitudes, scaleFactor );
        }

        /// <summary>
        /// Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequencies"> Holds the spectrum frequencies. </param>
        /// <param name="magnitudes">  Holds the spectrum amplitudes. </param>
        /// <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
        /// plotting data that was not scaled. </param>
        private void ChartAmplitudeSpectrum( double[] frequencies, double[] magnitudes, double scaleFactor )
        {

            // adjust abscissa scale.
            this._FrequencyAxis.Min = new Visuals.AutoValueR( frequencies[frequencies.GetLowerBound( 0 )], false );
            this._FrequencyAxis.Max = new Visuals.AutoValueR( frequencies[frequencies.GetUpperBound( 0 )], false );

            // adjust ordinate scale.
            this._AmplitudeSpectrumAxis.Max = new Visuals.AutoValueR( 1d, true );
            this._AmplitudeSpectrumAxis.Min = new Visuals.AutoValueR( 0d, true );

            // scale the magnitudes
            var amplitudes = new double[frequencies.Length];
            for ( int i = frequencies.GetLowerBound( 0 ), loopTo = frequencies.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                amplitudes[i] = scaleFactor * magnitudes[i];
            }

            this._AmplitudeSpectrumCurve.UpdateData( frequencies, amplitudes );
            this._SpectrumChartPan.Rescale();
            this._SpectrumChartPanel.Invalidate();
        }

        /// <summary> Redraws the amplitude spectrum Chart. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void SpectrumChartPanel_Paint( object sender, PaintEventArgs e )
        {
            if ( this._SpectrumChartPan is object )
            {
                e.Graphics.FillRectangle( new SolidBrush( Color.Gray ), this.ClientRectangle );
                this._SpectrumChartPan.Draw( e.Graphics );
            }
        }

        /// <summary> Redraws the amplitude spectrum chart when the form is resized. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of the
        /// chart panel
        /// <see cref="System.Windows.Forms.Panel"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void SpectrumChartPanel_Resize( object sender, EventArgs e )
        {
            if ( this._SpectrumChartPan is object )
            {
                this._SpectrumChartPan.SetSize( this._SpectrumChartPanel.ClientRectangle );
            }
        }

        #endregion

        #region " SIGNAL CHART "

        /// <summary>Gets or sets reference to the instantaneous DP curve.</summary>
        private Visuals.Curve _SignalCurve;

        /// <summary>Gets or sets reference to the instantaneous DP amplitude axis.</summary>
        private Visuals.Axis _SignalAxis;

        /// <summary>Gets or sets reference to the instantaneous DP time axis.</summary>
        private Visuals.Axis _TimeAxis;

        /// <summary>Charts spectrum.</summary>
        private Visuals.ChartPane _SignalChartPane;

        /// <summary> Creates the Scope for display of voltages during the epoch. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateSignalChart()
        {

            // set chart area and titles.
            this._SignalChartPane = new Visuals.ChartPane() { PaneArea = new RectangleF( 10f, 10f, 10f, 10f ) };
            this._SignalChartPane.Title.Caption = "Voltage versus Time";
            this._SignalChartPane.Legend.Visible = false;
            this._TimeAxis = this._SignalChartPane.AddAxis( "Time, Seconds", Visuals.AxisType.X );
            this._TimeAxis.Max = new Visuals.AutoValueR( 1d, false );
            this._TimeAxis.Min = new Visuals.AutoValueR( 0d, false );
            this._TimeAxis.Grid.Visible = true;
            this._TimeAxis.Grid.LineColor = Color.DarkGray;
            this._TimeAxis.TickLabels.Visible = true;
            this._TimeAxis.TickLabels.DecimalPlaces = new Visuals.AutoValue( 1, true );
            // timeAxis.TickLabels.Appearance.Angle = 60.0F

            this._SignalAxis = this._SignalChartPane.AddAxis( "Volts", Visuals.AxisType.Y );
            this._SignalAxis.CoordinateScale = new Visuals.CoordinateScale( Visuals.CoordinateScaleType.Linear );
            this._SignalAxis.Title.Visible = true;
            this._SignalAxis.Max = new Visuals.AutoValueR( 1d, true );
            this._SignalAxis.Min = new Visuals.AutoValueR( -1, true );
            this._SignalAxis.Grid.Visible = true;
            this._SignalAxis.Grid.LineColor = Color.DarkGray;
            this._SignalAxis.TickLabels.Visible = true;
            this._SignalAxis.TickLabels.DecimalPlaces = new Visuals.AutoValue( 0, true );
            this._SignalChartPane.AxisFrame.FillColor = Color.WhiteSmoke; // Color.FromArgb(232, 236, 245) ' Color.LightGoldenrodYellow
            this._SignalChartPane.SetSize( this._SignalChartPanel.ClientRectangle );
            this._SignalCurve = this._SignalChartPane.AddCurve( Visuals.CurveType.XY, "TimeSeries", this._TimeAxis, this._SignalAxis );
            this._SignalCurve.Cord.LineColor = Color.Red;
            this._SignalCurve.Cord.CordType = Visuals.CordType.Linear;
            this._SignalCurve.Symbol.Visible = false;
            this._SignalCurve.Cord.LineWidth = 1.0f;
            this._SignalChartPane.Rescale();
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( float[] times, float[] amplitudes )
        {

            // plot all but last, which was plotted above
            var newApms = new double[times.Length];
            var newTimes = new double[times.Length];
            for ( int i = times.GetLowerBound( 0 ), loopTo = times.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                newApms[i] = amplitudes[i];
                newTimes[i] = times[i];
            }

            this.ChartSignal( newTimes, newApms );
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( double[] times, double[] amplitudes )
        {
            this._TimeAxis.Max = new Visuals.AutoValueR( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ), false ); // times(UBound(times)), False)
            this._TimeAxis.Min = new Visuals.AutoValueR( 0d, false );  // times(LBound(times)), False)
            this._SignalAxis.Max = new Visuals.AutoValueR( 1d, true );
            this._SignalAxis.Min = new Visuals.AutoValueR( -1, true );
            this._SignalCurve.UpdateData( times, amplitudes );
            this._SignalChartPane.Rescale();
            this._SignalChartPanel.Invalidate();
        }

        /// <summary> Redraws the Scope Chart. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void ScopeChartPanel_Paint( object sender, PaintEventArgs e )
        {
            if ( this._SignalChartPane is object )
            {
                e.Graphics.FillRectangle( new SolidBrush( Color.Gray ), this.ClientRectangle );
                this._SignalChartPane.Draw( e.Graphics );
            }
        }

        /// <summary> Redraws the Scope chart when the form is resized. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of the
        /// chart panel
        /// <see cref="System.Windows.Forms.Panel"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void ScopeChartPanel_Resize( object sender, EventArgs e )
        {
            if ( this._SignalChartPane is object )
            {
                this._SignalChartPane.SetSize( this._SignalChartPanel.ClientRectangle );
            }
        }


        /// <summary>
        /// Includes sample code for special chart elements.  We can use the arrow and label to point to
        /// a potential resonance.  This could be turned on or off from the chart context menu.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signalChartPane"> Charts spectrum. </param>
        /// <param name="timeAxis">        Gets or sets reference to the instantaneous DP time axis. </param>
        /// <param name="signalAxis">      Gets or sets reference to the instantaneous DP amplitude
        /// axis. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void AddSpecialChartElements( Visuals.ChartPane signalChartPane, Visuals.Axis timeAxis, Visuals.Axis signalAxis )
        {
            Visuals.Curve curve;
            var x = new[] { 72d, 200d, 300d, 400d, 500d, 600d, 700d, 800d, 900d, 1000d };
            var y = new[] { 20d, 10d, 50d, 40d, 35d, 60d, 90d, 25d, 48d, 75d };
            curve = signalChartPane.AddCurve( "Larry", x, y, Color.Red, Visuals.ShapeType.Circle, timeAxis, signalAxis );
            curve.Symbol.Size = 14f;
            curve.Cord.LineWidth = 2.0f;
            var x2 = new[] { 300d, 400d, 500d, 600d, 700d, 800d, 900d };
            var y2 = new[] { 75d, 43d, 27d, 62d, 89d, 73d, 12d };
            curve = signalChartPane.AddCurve( "Moe", x2, y2, Color.Blue, Visuals.ShapeType.Diamond, timeAxis, signalAxis );
            curve.Cord.LineWidth = 2.0f;
            curve.Cord.Visible = false;
            curve.Symbol.Filled = true;
            curve.Symbol.Size = 14f;
            var x3 = new[] { 150d, 250d, 400d, 520d, 780d, 940d };
            var y3 = new[] { 5.2d, 49.0d, 33.8d, 88.57d, 99.9d, 36.8d };
            curve = signalChartPane.AddCurve( "Curly", x3, y3, Color.Green, Visuals.ShapeType.Triangle, timeAxis, signalAxis );
            curve.Symbol.Size = 14f;
            curve.Cord.LineWidth = 2.0f;
            curve.Symbol.Filled = true;
            var text = signalChartPane.AddTextBox( $"First Prod {Environment.NewLine} 21-Oct-99", 100.0f, 50.0f );
            text.Appearance.Italic = true;
            text.Alignment = new Visuals.Alignment( Visuals.HorizontalAlignment.Center, Visuals.VerticalAlignment.Bottom );
            text.Appearance.Frame.FillColor = Color.LightBlue;
            var arrow = signalChartPane.AddArrow( Color.Black, 12.0f, 100.0f, 47.0f, 72.0f, 25.0f );
            arrow.CoordinateFrame = Visuals.CoordinateFrameType.AxisXYScale;
            text = signalChartPane.AddTextBox( "Upgrade", 700.0f, 50.0f );
            text.CoordinateFrame = Visuals.CoordinateFrameType.AxisXYScale;
            text.Appearance.Angle = 90f;
            text.Appearance.FontColor = Color.Black;
            text.Appearance.Frame.Filled = true;
            text.Appearance.Frame.FillColor = Color.WhiteSmoke; // Color.FromArgb(232, 236, 245) ' Color.LightGoldenrodYellow
            text.Appearance.Frame.Visible = false;
            text.Alignment = new Visuals.Alignment( Visuals.HorizontalAlignment.Right, Visuals.VerticalAlignment.Center );
            arrow = signalChartPane.AddArrow( Color.Black, 15f, 700f, 53f, 700f, 80f );
            arrow.CoordinateFrame = Visuals.CoordinateFrameType.AxisXYScale;
            arrow.LineWidth = 2.0f;
            text = signalChartPane.AddTextBox( "Confidential", 0.8f, -0.03f );
            text.CoordinateFrame = Visuals.CoordinateFrameType.AxisFraction;
            text.Appearance.Angle = 15.0f;
            text.Appearance.FontColor = Color.Red;
            text.Appearance.Bold = true;
            text.Appearance.FontSize = 16f;
            text.Appearance.Frame.Visible = true;
            text.Appearance.Frame.LineColor = Color.Red;
            text.Alignment = new Visuals.Alignment( Visuals.HorizontalAlignment.Left, Visuals.VerticalAlignment.Bottom );
        }

        #endregion

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Cycles text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void CyclesTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._CyclesTextBox, string.Empty );
            if ( int.TryParse( this._CyclesTextBox.Text, out int value ) )
            {
                if ( value < 1 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._CyclesTextBox, "Must exceed 1" );
                }
                else
                {
                    _ = this.SineWaveDouble();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._CyclesTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Phase text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PhaseTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PhaseTextBox, string.Empty );
            if ( int.TryParse( this._PhaseTextBox.Text, out _ ) )
            {
                _ = this.SineWaveDouble();
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PhaseTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Points text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PointsTextBox, string.Empty );
            if ( int.TryParse( this._PointsTextBox.Text, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._PointsTextBox, "Must exceed 2" );
                }
                else
                {
                    // Set initial values defining the signal
                    if ( value < int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                    }

                    _ = this.SineWaveDouble();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PointsTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Points to display text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsToDisplayTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, string.Empty );
            if ( int.TryParse( this._PointsToDisplayTextBox.Text, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, "Must exceed 2" );
                }
                // Set initial values defining the signal
                else if ( int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) < value )
                {
                    this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Starts stop check box checked changed. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void StartStopCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._StartStopCheckBox.Text = this._StartStopCheckBox.Checked ? "&Stop" : "&Start";
            if ( this._StartStopCheckBox.Enabled && this._StartStopCheckBox.Checked )
            {
                var algorithm = this.SelectedExample;
                switch ( algorithm )
                {
                    case Example.DiscreteFourierTransform:
                    case Example.WisdomFFT:
                        {
                            this._CountStatusBarPanel.Text = "0";
                            do
                            {
                                if ( this._DoubleRadioButton.Checked )
                                {
                                    this.CalculateSpectrumDouble( algorithm );
                                }
                                else
                                {
                                    this.CalculateSpectrumSingle( algorithm );
                                }

                                Application.DoEvents();
                                int count = int.Parse( this._CountStatusBarPanel.Text, System.Globalization.CultureInfo.CurrentCulture ) + 1;
                                this._CountStatusBarPanel.Text = count.ToString( System.Globalization.CultureInfo.CurrentCulture );
                            }
                            while ( this._StartStopCheckBox.Checked & false );
                            break;
                        }

                    case Example.SlidingFFT:
                        {
                            if ( this._DoubleRadioButton.Checked )
                            {
                                this.SlidingFft( 0.5d );
                            }
                            else
                            {
                                this.SlidingFft( 0.5f );
                            }

                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                if ( this._StartStopCheckBox.Checked )
                {
                    this._StartStopCheckBox.Enabled = false;
                    this._StartStopCheckBox.Checked = false;
                    this._StartStopCheckBox.Enabled = true;
                }
            }
            else
            {
                this._CountStatusBarPanel.Text = "0";
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
