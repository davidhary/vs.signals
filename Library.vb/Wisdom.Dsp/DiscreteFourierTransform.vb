Namespace Wisdom

    ''' <summary>Calculates the Discrete Fourier Transform.</summary>
    ''' <remarks>Includes procedures for calculating the forward and inverse DFT.<p>
    ''' Benchmarks:</p><p>
    ''' Data    Data  Speed</p><p>
    ''' Points  Type   (ms)</p><p>
    '''   256   Double  less than 2</p><p>
    '''   512   Double  4-5</p><p>
    '''  1000   Double  9-10</p><p>
    '''  1024   Double 10-11</p><p>
    '''  2000   Double 19-20</p><p>
    '''  2048   Double 26</p>
    ''' (c) 1998 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
    ''' David, 06/03/98, 1.0.00. <para>
    ''' David, 09/27/05, 1.0.2096. Create based on FFT pro </para></remarks>
    Public Class DiscreteFourierTransform
        Inherits FourierTransform

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New(FourierTransformType.Dft)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " DOUBLE "

        ''' <summary>Calculates the Discrete Fourier Transform.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <remarks>Computes the DFT directly without using any of the special rotation 
        '''   and permutation algorithms that are part of the Fast Fourier Transform (FFT)
        '''   literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        ''' </remarks>
        Public Overrides Sub Forward(ByVal reals() As Double, ByVal imaginaries() As Double)
            MyBase.Forward(reals, imaginaries)
        End Sub

        ''' <summary>Initializes the transform.</summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        Protected Overrides Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' build the DFT tables
            MyBase.BuildDftTables(reals.Length)

            ' Set element count
            MyBase.ElementCount = reals.Length

            ' save initial values to the cache
            MyBase.CopyToCache(reals, imaginaries)

            Dim realValue As Double
            Dim imaginaryValue As Double
            Dim k As Integer
            For j As Integer = 0 To MyBase.ElementCount - 1
                realValue = MyBase.RealCache(0)
                imaginaryValue = MyBase.ImaginaryCache(0)
                k = 0
                For i As Integer = 1 To MyBase.ElementCount - 1
                    k += j
                    If k >= MyBase.ElementCount Then
                        k -= MyBase.ElementCount
                    End If
                    realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) _
                          + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                    imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) _
                          - MyBase.SineTable(k) * MyBase.RealCache(i)
                Next i
                reals(j) = realValue
                imaginaries(j) = imaginaryValue
            Next j
        End Sub

#End Region

#Region " SINGLE "

        ''' <summary>Calculates the Discrete Fourier Transform.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <remarks>Computes the DFT directly without using any of the special rotation 
        '''   and permutation algorithms that are part of the Fast Fourier Transform (FFT)
        '''   literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        ''' </remarks>
        Public Overrides Sub Forward(ByVal reals() As Single, ByVal imaginaries() As Single)
            MyBase.Forward(reals, imaginaries)
        End Sub

        ''' <summary>Initializes the transform.</summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        Protected Overrides Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)

            ' build the DFT tables
            MyBase.BuildDftTables(reals.Length)

            ' Set element count
            MyBase.ElementCount = reals.Length

            ' save initial values to the cache
            MyBase.CopyToCache(reals, imaginaries)

            Dim realValue As Double
            Dim imaginaryValue As Double
            Dim k As Integer
            For j As Integer = 0 To MyBase.ElementCount - 1
                realValue = MyBase.RealCache(0)
                imaginaryValue = MyBase.ImaginaryCache(0)
                k = 0
                For i As Integer = 1 To MyBase.ElementCount - 1
                    k += j
                    If k >= MyBase.ElementCount Then
                        k -= MyBase.ElementCount
                    End If
                    realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                    imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) - MyBase.SineTable(k) * MyBase.RealCache(i)
                Next i
                reals(j) = CSng(realValue)
                imaginaries(j) = CSng(imaginaryValue)
            Next j

        End Sub

#End Region

    End Class
End Namespace
