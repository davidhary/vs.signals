Imports isr.Algorithms.Signals.ExceptionExtensions

''' <summary> Form for viewing the spectrum panels. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/6/2019 </para>
''' </remarks>
Public Class MyForm
    Inherits isr.Core.Forma.ConsoleForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Forms.MyForm IsNot Nothing AndAlso Not My.Forms.MyForm.IsDisposed
        End Get
    End Property

    ''' <summary> Gets or sets the model view base. </summary>
    ''' <value> The model view base. </value>
    Private Property ModelViewBase As isr.Core.Forma.ModelViewTalkerBase = Nothing

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Dispose() : Me.ModelViewBase = Nothing
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding spectrum view"
            Select Case CommandLineInfo.UserInterface
                Case UserInterface.Spectrum
                    Me.ModelViewBase = New SpectrumView
                Case UserInterface.Wisdom
                    Me.ModelViewBase = New WisdomSpectrumView
                Case Else
                    Me.ModelViewBase = New SpectrumView
            End Select
            Me.AddTalkerControl($"{CommandLineInfo.UserInterface}", Me.ModelViewBase, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.Application.Logger)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await selection task enabled. </summary>
    ''' <value> The await selection task enabled. </value>
    Protected Property AwaitSelectionTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.ModelViewBase.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    #Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    #End Region

End Class
