
Partial Public NotInheritable Class TaperFilter

#Region " SHARED "

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                          ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        Return TaperFilter.HighPassTaper(frequency, 0, transitionBankCount, reals, imaginaries)
    End Function

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                          ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        Return TaperFilter.HighPassTaper(frequency, 0, transitionBankCount, reals, imaginaries)
    End Function

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="fromIndex">           The first frequency index form which to apply the high
    '''                                    pass taper. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal fromIndex As Integer,
                                          ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If fromIndex + transitionBankCount \ 2 > frequency Then
            Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Frequency must exceed the minimum frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If frequency - transitionBankCount \ 2 + transitionBankCount > elementCount \ 2 Then
            Throw New ArgumentOutOfRangeException(NameOf(frequency), frequency, "Filter frequency plus half transition band must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        If fromIndex = 0 Then
            reals(fromIndex) = 0
            imaginaries(fromIndex) = 0
            fromIndex += 1
        End If

        ' zero out low frequency amplitudes
        frequencyIndex = fromIndex
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < (frequency - transitionBankCount \ 2)

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band from the data
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= taperFactor
            imaginaries(frequencyIndex) *= taperFactor
            reals(conjugateFrequencyIndex) *= taperFactor
            imaginaries(conjugateFrequencyIndex) *= taperFactor

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary> Tapers the spectrum using a High Pass taper. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="frequency">           The high pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="fromIndex">           The first frequency index form which to apply the high
    '''                                    pass taper. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                          ByVal fromIndex As Integer, ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If fromIndex + transitionBankCount \ 2 > frequency Then
            Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Frequency must exceed the minimum frequency plus half the transition band.")
        End If
        Dim elementCount As Integer = reals.Length
        If frequency - transitionBankCount \ 2 + transitionBankCount > elementCount \ 2 Then
            Throw New ArgumentOutOfRangeException(NameOf(frequency), frequency, "Filter frequency plus half transition band must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        If fromIndex = 0 Then
            reals(fromIndex) = 0
            imaginaries(fromIndex) = 0
            fromIndex += 1
        End If

        ' zero out low frequency amplitudes
        frequencyIndex = fromIndex
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < (frequency - transitionBankCount \ 2)

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= CSng(taperFactor)
            imaginaries(frequencyIndex) *= CSng(taperFactor)
            reals(conjugateFrequencyIndex) *= CSng(taperFactor)
            imaginaries(conjugateFrequencyIndex) *= CSng(taperFactor)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary>
    ''' Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                        ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        Return TaperFilter.LowPassTaper(frequency, transitionBankCount, reals.Length \ 2, reals, imaginaries)
    End Function

    ''' <summary>
    ''' Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                        ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        Return TaperFilter.LowPassTaper(frequency, transitionBankCount, reals.Length \ 2, reals, imaginaries)
    End Function

    ''' <summary>
    ''' Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="toIndex">             The last frequency to apply the low pass filter.  This
    '''                                    allows using this method to apply the low past filter in
    '''                                    case of a band reject filter where the spectrum can be
    '''                                    zeroed only within a specific range. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal toIndex As Integer,
                                        ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException(NameOf(frequency), frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If
        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If
        Dim elementCount As Integer = reals.Length

        If toIndex > (elementCount \ 2) Then
            Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Maximum frequency must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        frequencyIndex = frequency - transitionBankCount \ 2
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= taperFactor
            imaginaries(frequencyIndex) *= taperFactor
            reals(conjugateFrequencyIndex) *= taperFactor
            imaginaries(conjugateFrequencyIndex) *= taperFactor

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Do While frequencyIndex < toIndex

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary>
    ''' Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="frequency">           The low pass frequency index. </param>
    ''' <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
    ''' <param name="toIndex">             The last frequency to apply the low pass filter.  This
    '''                                    allows using this method to apply the low past filter in
    '''                                    case of a band reject filter where the spectrum can be
    '''                                    zeroed only within a specific range. </param>
    ''' <param name="reals">               Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if filtered. </returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal toIndex As Integer,
                                        ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException(NameOf(frequency), frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If
        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If toIndex > (elementCount \ 2) Then
            Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Maximum frequency must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        frequencyIndex = frequency - transitionBankCount \ 2
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= CSng(taperFactor)
            imaginaries(frequencyIndex) *= CSng(taperFactor)
            reals(conjugateFrequencyIndex) *= CSng(taperFactor)
            imaginaries(conjugateFrequencyIndex) *= CSng(taperFactor)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Do While frequencyIndex < toIndex

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

#End Region

#Region " METHODS "

    ''' <summary> Taper the frequency band for filtering the signal in the frequency domain. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="samplingRate"> The spectrum sampling rate. </param>
    ''' <param name="reals">        Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">  Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Taper(ByVal samplingRate As Double, ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
        Me._SamplingRate = samplingRate
        Me._TimeSeriesLength = reals.Length

        Dim details As String = String.Empty
        If Not Me.ValidateFilterFrequencies(details) Then
            Throw New InvalidOperationException(details)
        End If

        Select Case Me._FilterType

            Case TaperFilterType.BandPass

                ' high pass at the low frequency
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.LowFrequency),
            Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

                ' low pass at the high frequency.
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.HighFrequency),
            Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

            Case TaperFilterType.BandReject

                Dim midFrequency As Double = (Me.LowFrequency + Me.HighFrequency) / 2

                ' low pass at the low frequency but up to the mid band
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency),
            Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
            reals, imaginaries)

                ' high pass at the high frequency but only from the mid band
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency),
            Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
            reals, imaginaries)

            Case TaperFilterType.HighPass

                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand),
            reals, imaginaries)

            Case TaperFilterType.LowPass

                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand),
            reals, imaginaries)

            Case TaperFilterType.None

                Return True

        End Select

    End Function

    ''' <summary> Taper the frequency band for filtering the signal in the frequency domain. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="samplingRate"> The spectrum sampling rate. </param>
    ''' <param name="reals">        Holds the real values of the spectrum. </param>
    ''' <param name="imaginaries">  Holds the imaginary values of the spectrum. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function Taper(ByVal samplingRate As Double, ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))

        Me._SamplingRate = samplingRate
        Me._TimeSeriesLength = reals.Length

        Select Case Me._FilterType

            Case TaperFilterType.BandPass

                ' high pass at the low frequency
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.LowFrequency),
            Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

                ' low pass at the high frequency.
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.HighFrequency),
            Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

            Case TaperFilterType.BandReject

                Dim midFrequency As Double = (Me.LowFrequency + Me.HighFrequency) / 2

                ' low pass at the low frequency but up to the mid band
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency),
            Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
            reals, imaginaries)

                ' high pass at the high frequency but only from the mid band
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency),
            Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
            reals, imaginaries)

            Case TaperFilterType.HighPass

                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand),
            reals, imaginaries)

            Case TaperFilterType.LowPass

                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand),
            reals, imaginaries)

            Case TaperFilterType.None

                Return True

        End Select

    End Function

#End Region

End Class
