﻿using System;

namespace isr.Algorithms.Signals
{

    /// <summary> Single value extensions. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public static class SingleExtensions
    {

        #region " SINGLE ARRAYS "

        /// <summary> Adds a value to the Single array elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        public static void Add( this float[] values, float value )
        {
            if ( values is object && values.Length > 0 )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                    values[i] += value;
            }
        }

        /// <summary> Copy values between arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fromValues"> From values. </param>
        /// <param name="toValues">   To values. </param>
        public static void Copy( this float[] fromValues, float[] toValues )
        {
            if ( fromValues is null )
                throw new ArgumentNullException( nameof( fromValues ) );
            if ( toValues is null )
                throw new ArgumentNullException( nameof( toValues ) );
            if ( fromValues.Length > 0 && toValues.Length > 0 )
            {
                for ( int i = Math.Max( fromValues.GetLowerBound( 0 ), toValues.GetLowerBound( 0 ) ), loopTo = Math.Min( fromValues.GetUpperBound( 0 ), toValues.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    toValues[i] = fromValues[i];
            }
        }

        /// <summary> Copy values between arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fromValues"> From values. </param>
        /// <param name="toValues">   To values. </param>
        public static void Copy( this float[] fromValues, double[] toValues )
        {
            if ( fromValues is null )
                throw new ArgumentNullException( nameof( fromValues ) );
            if ( toValues is null )
                throw new ArgumentNullException( nameof( toValues ) );
            if ( fromValues.Length > 0 && toValues.Length > 0 )
            {
                for ( int i = Math.Max( fromValues.GetLowerBound( 0 ), toValues.GetLowerBound( 0 ) ), loopTo = Math.Min( fromValues.GetUpperBound( 0 ), toValues.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    toValues[i] = fromValues[i];
            }
        }

        /// <summary> Returns the index of the first maximum of given array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The index of the first maximum of given array. </returns>
        public static int IndexFirstMaximum( this float[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                int maxIndex = values.GetLowerBound( 0 );
                float max = values[maxIndex];
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    float abs = values[i];
                    if ( max < abs )
                    {
                        max = abs;
                        maxIndex = i;
                    }
                }

                return maxIndex;
            }
            else
            {
                return -1;
            }
        }

        /// <summary> Calculates the mean of the array elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The mean of the array elements. </returns>
        public static double Mean( this float[] values )
        {
            return values is object && values.Length > 0 ? values.Sum() / values.Length : 0d;
        }

        /// <summary> Removes the mean from the values array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        public static void RemoveMean( this float[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                values.Add( ( float ) -values.Mean() );
            }
        }

        /// <summary> Multiplies the array elements by scalar elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this float[] values, float[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= scalars[i];
                }
            }
        }

        /// <summary> Multiplies the array elements by scalar elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this float[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= ( float ) scalars[i];
                }
            }
        }

        /// <summary> Multiplies the array elements by a scalar. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="scalar"> The scalar. </param>
        public static void Scale( this float[] values, float scalar )
        {
            if ( values is object && values.Length > 0 )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                    values[i] *= scalar;
            }
        }

        /// <summary> Swaps values between two array locations. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="data">       The data. </param>
        /// <param name="leftIndex">  Zero-based index of the left. </param>
        /// <param name="rightIndex"> Zero-based index of the right. </param>
        public static void Swap( this float[] data, int leftIndex, int rightIndex )
        {
            if ( data is object )
            {
                float cache = data[leftIndex];
                data[leftIndex] = data[rightIndex];
                data[rightIndex] = cache;
            }
        }

        /// <summary> Calculates the sum of the array elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The sum of the array elements. </returns>
        public static double Sum( this float[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                float cache = 0f;
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                    cache += values[i];
                return cache;
            }
            else
            {
                return 0d;
            }
        }

        /// <summary> Calculates the sum of the array elements squared. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The sum of the array elements squared. </returns>
        public static double SumSquares( this float[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                float cache = 0f;
                float value;
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    value = values[i];
                    cache += value * value;
                }

                return cache;
            }
            else
            {
                return 0d;
            }
        }

        /// <summary> Calculates the root mean square (RMS) value of the array elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The root mean square (RMS) value of the array elements. </returns>
        public static double RootMeanSquare( this float[] values )
        {
            return values is object && values.Length > 0 ? Math.Sqrt( values.SumSquares() / values.Length ) : 0d;
        }

        /// <summary> Returns the magnitude of a complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        /// <returns> A Single() </returns>
        public static float[] Magnitudes( this float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            var result = new float[imaginaries.Length];

            // calculate the magnitude
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                double realValue = reals[i];
                double imaginaryValue = imaginaries[i];
                result[i] = ( float ) Math.Sqrt( realValue * realValue + imaginaryValue * imaginaryValue );
            }

            return result;
        }

        #endregion


    }
}