using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using isr.Algorithms.Signals.Wisdom;

namespace isr.Algorithms.Signals
{

    /// <summary> Tests the Wisdom FFT using Safe Native Methods. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public partial class WisdomNativeTest : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public WisdomNativeTest() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> True if disposed. </summary>
        private bool _Disposed;

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The disposed. </value>
        protected bool Disposed
        {
            get => this._Disposed;

            private set => this._Disposed = value;
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        // Free managed resources when explicitly called
                    }
                    // Free shared unmanaged resources
                    if ( this._Pin != ( IntPtr ) 0 )
                        Wisdom.FftwF.SafeNativeMethods.Free( this._Pin );
                    if ( this._Pout != ( IntPtr ) 0 )
                        Wisdom.FftwF.SafeNativeMethods.Free( this._Pout );
                    if ( this._Fplan1 != ( IntPtr ) 0 )
                        Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan1 );
                    if ( this._Fplan2 != ( IntPtr ) 0 )
                        Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan2 );
                    if ( this._Fplan3 != ( IntPtr ) 0 )
                        Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan3 );
                    this._Hin.Free();
                    this._Hout.Free();
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.Disposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        ~WisdomNativeTest()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        /// <summary>
        /// managed arrays
        /// </summary>
        private float[] _Fin, _Fout;

        /// <summary>
        /// handles to managed arrays, keeps them pinned in memory.
        /// </summary>
        private GCHandle _Hin, _Hout;

        /// <summary> Initializes FFTW and all arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="logicalSize"> Logical size (number of elements) of the transform. </param>
        public void InitFftw( int logicalSize )
        {

            // create two unmanaged arrays, properly aligned
            this._Pin = Wisdom.FftwF.SafeNativeMethods.Malloc( logicalSize * 8 );
            this._Pout = Wisdom.FftwF.SafeNativeMethods.Malloc( logicalSize * 8 );

            // create two managed arrays, possibly misaligned
            // n*2 because we are dealing with complex numbers
            this._Fin = new float[(logicalSize * 2)];
            this._Fout = new float[(logicalSize * 2)];

            // get handles and pin arrays so the GC doesn't move them
            this._Hin = GCHandle.Alloc( this._Fin, GCHandleType.Pinned );
            this._Hout = GCHandle.Alloc( this._Fout, GCHandleType.Pinned );

            // fill our arrays with a sawtooth signal
            for ( int i = 0, loopTo = logicalSize * 2 - 1; i <= loopTo; i++ )
                this._Fin[i] = i % 50;
            for ( int i = 0, loopTo1 = logicalSize * 2 - 1; i <= loopTo1; i++ )
                this._Fout[i] = i % 50;

            // copy managed arrays to unmanaged arrays
            Marshal.Copy( this._Fin, 0, this._Pin, logicalSize * 2 );
            Marshal.Copy( this._Fout, 0, this._Pout, logicalSize * 2 );

            // create a few test transforms
            this._Fplan1 = Wisdom.FftwF.SafeNativeMethods.Dft_1d( logicalSize, this._Pin, this._Pout, ( int ) TransformDirection.Forward, ( uint ) PlannerOptions.Estimate );
            this._Fplan2 = Wisdom.FftwF.SafeNativeMethods.Dft_1d( logicalSize, this._Hin.AddrOfPinnedObject(), this._Hout.AddrOfPinnedObject(), ( int ) TransformDirection.Forward, ( uint ) PlannerOptions.Estimate );
            this._Fplan3 = Wisdom.FftwF.SafeNativeMethods.Dft_1d( logicalSize, this._Hout.AddrOfPinnedObject(), this._Pin, ( int ) TransformDirection.Backward, ( uint ) PlannerOptions.Measure );
        }

        /// <summary> Tests all plans. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void TestAll()
        {
            TestPlan( this._Fplan1 );
            TestPlan( this._Fplan2 );
            TestPlan( this._Fplan3 );
        }

        /// <summary> Tests a single plan, displaying results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="plan"> Pointer to plan to test. </param>
        public static void TestPlan( IntPtr plan )
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Wisdom.FftwF.SafeNativeMethods.Execute( plan );
            Console.WriteLine( "Time: {0} ms", stopwatch.Elapsed.TotalMilliseconds );

            // a: adds, b: m.u.l.s, c: f.m.a.s
            double a = 0d;
            double b = 0d;
            double c = 0d;
            Wisdom.FftwF.SafeNativeMethods.Flops( plan, ref a, ref b, ref c );
            Console.WriteLine( "Approx. flops: {0}", a + b + 2d * c );
        }

        /// <summary> Releases all memory used by FFTW/C#. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void FreeFftw()
        {
            // it is essential that you call these after finishing
            // may want to put the initializers in the constructor
            // and these in the destructor
            if ( this._Pin != ( IntPtr ) 0 )
                Wisdom.FftwF.SafeNativeMethods.Free( this._Pin );
            if ( this._Pout != ( IntPtr ) 0 )
                Wisdom.FftwF.SafeNativeMethods.Free( this._Pout );
            if ( this._Fplan1 != ( IntPtr ) 0 )
                Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan1 );
            if ( this._Fplan2 != ( IntPtr ) 0 )
                Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan2 );
            if ( this._Fplan3 != ( IntPtr ) 0 )
                Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._Fplan3 );
            this._Hin.Free();
            this._Hout.Free();
        }
    }
}
