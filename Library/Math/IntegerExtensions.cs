﻿
namespace isr.Algorithms.Signals
{

    /// <summary> Integer value extensions.</summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public static class IntegerExtensions
    {

        #region " INTEGER ARRAYS "

        /// <summary> Returns the index of the first maximum of given array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The index of the first maximum of given array. </returns>
        public static int IndexFirstMaximum( this int[] values )
        {
            if ( values is null || values.Length == 0 )
            {
                return -1;
            }

            int maxIndex = values.GetLowerBound( 0 );
            int max = values[maxIndex];
            for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                int candidate = values[i];
                if ( max < candidate )
                {
                    max = candidate;
                    maxIndex = i;
                }
            }

            return maxIndex;
        }

        #endregion

    }
}