Imports System.Numerics

''' <summary> Calculate the Sliding Fourier Transform. </summary>
''' <remarks>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 8/7/2019 </para>
''' </remarks>
Public Class SlidingFourierTransform
    Inherits FourierTransformBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New(FourierTransformType.Sliding)
    End Sub

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overrides Sub Forward(ByVal values() As Complex)
        MyBase.Forward(values)
    End Sub

    ''' <summary>
    ''' Initializes the sine and cosine tables for calculating the sliding Fourier transform.
    ''' </summary>
    ''' <remarks>
    ''' Allocates and calculates sine and cosine tables for the sliding Fourier transform and maps
    ''' the data arrays to the internal memory space of the sliding Fourier transform.
    ''' </remarks>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    Protected Overrides Sub Initialize(ByVal values() As Complex)
        MyBase.Initialize(values)
        ' save the real- and imaginary-parts into the cache
        MyBase.CopyToCache(values)
        ' Allocate the sine tables.
        MyBase.BuildDftTable(values.Length)
    End Sub

    ''' <summary> Calculates the sliding Fast Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="newValue"> The new value to use in calculating the sliding Fourier transform. </param>
    ''' <param name="oldValue"> The first real value in the time series previously used to calculate
    '''                         the Fourier transform. </param>
    Public Sub Update(ByVal newValue As Complex, ByVal oldValue As Complex)
        ' get the difference term
        Dim delta As Complex = newValue - oldValue
        ' Update the sliding Fourier transform
        For i As Integer = 0 To MyBase.DftCache.Length - 1
            MyBase.DftCache(i) = MyBase.DftTable(i) * (MyBase.DftCache(i) + delta)
        Next i
    End Sub

#End Region

#Region " DOUBLE "

    ''' <summary>
    ''' Initializes the sliding FFT; Use <see cref="Update">update</see> to add values.
    ''' </summary>
    ''' <remarks>
    ''' Computes the DFT directly without using any of the special rotation and permutation
    ''' algorithms that are part of the Fast Fourier Transform (FFT)
    ''' literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals() As Double, ByVal imaginaries() As Double)
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary>
    ''' Initializes the sine and cosine tables for calculating the sliding Fourier transform.
    ''' </summary>
    ''' <remarks>
    ''' Allocates and calculates sine and cosine tables for the sliding Fourier transform and maps
    ''' the data arrays to the internal memory space of the sliding Fourier transform.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
        End If

        ' save the real- and imaginary-parts into the cache
        MyBase.CopyToCache(reals, imaginaries)

        ' Allocate the sine tables.
        MyBase.BuildDftTables(reals.Length)

    End Sub

    ''' <summary> Calculates the sliding Fast Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="newReal">      The new real value to use in calculating the sliding Fourier
    '''                             transform. </param>
    ''' <param name="newImaginary"> The new imaginary value to use in calculating the sliding Fourier
    '''                             transform. </param>
    ''' <param name="oldReal">      The first real value in the time series previously used to
    '''                             calculate the Fourier transform. </param>
    ''' <param name="oldImaginary"> The first imaginary value in the time series previously used to
    '''                             calculate the Fourier transform. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function Update(ByVal newReal As Double, ByVal newImaginary As Double,
                               ByVal oldReal As Double, ByVal oldImaginary As Double) As Boolean
        ' get the difference term
        Dim realDelta As Double = newReal - oldReal
        Dim imagDelta As Double = newImaginary - oldImaginary

        ' Update the sliding Fourier transform
        For i As Integer = 0 To MyBase.RealCache.Length - 1
            newReal = MyBase.RealCache(i) + realDelta
            newImaginary = MyBase.ImaginaryCache(i) + imagDelta
            MyBase.RealCache(i) = MyBase.CosineTable(i) * newReal - MyBase.SineTable(i) * newImaginary
            MyBase.ImaginaryCache(i) = MyBase.CosineTable(i) * newImaginary + MyBase.SineTable(i) * newReal
        Next i
    End Function

#End Region

#Region " SINGLE "

    ''' <summary>
    ''' Initializes the sliding FFT; Use <see cref="Update">update</see> to add values.
    ''' </summary>
    ''' <remarks>
    ''' Computes the DFT directly without using any of the special rotation and permutation
    ''' algorithms that are part of the Fast Fourier Transform (FFT)
    ''' literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals() As Single, ByVal imaginaries() As Single)
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary>
    ''' Initializes the sine and cosine tables for calculating the sliding Fourier transform.
    ''' </summary>
    ''' <remarks>
    ''' Allocates and calculates sine and cosine tables for the sliding Fourier transform and  maps
    ''' the data arrays to the internal memory space of the sliding Fourier transform.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
        End If

        ' save the real- and imaginary-parts into the cache
        MyBase.CopyToCache(reals, imaginaries)

        ' Allocate the sine tables.
        MyBase.BuildDftTables(reals.Length)

    End Sub

    ''' <summary> Calculates the sliding Fast Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="newReal">      The new real value to use in calculating the sliding Fourier
    '''                             transform. </param>
    ''' <param name="newImaginary"> The new imaginary value to use in calculating the sliding Fourier
    '''                             transform. </param>
    ''' <param name="oldReal">      The first real value in the time series previously used to
    '''                             calculate the Fourier transform. </param>
    ''' <param name="oldImaginary"> The first imaginary value in the time series previously used to
    '''                             calculate the Fourier transform. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    ''' <example>
    ''' Sub Form_Click
    '''   Dim fft As New isr.Dsp.MixedRadixFft Dim sliding As New isr.Dsp.SlidingFft Dim signalLength
    '''   As integer   ' Signal points Dim reals() As Single         ' real-part of Fourier transform
    '''   Dim imaginaries() As Single   ' imaginary-part of Fourier transform Dim magnitude() As
    '''   Single     ' Fourier transform magnitude Dim samplingRate As Single Dim frequencyAxis () As
    '''   Single  ' Frequency Dim newReal As Single Dim newImaginary As Single Dim oldReal As Single
    '''   Dim oldImaginary As Single ' Set signal points signalLength = 250 ' Create 10 cycles of the
    '''   sine wave with 0 phase Dim signal() as Single = isr.Dsp.Helper.Sine(10 / signalLength, 0)
    '''   ' Allocate arrays for real and imaginary-parts ReDim reals(signalLength - 1)
    '''   ReDim imaginaries(signalLength - 1)
    '''   ' Place signal in the real-part for DFT. signal.CopyTo(reals(),0)
    '''   ' Set options fft.IsRemoveMean False fft.TaperWindow = 0 ' Compute the Mixed Radix Fourier
    '''   transform ' to initialize the sliding Fourier transform fft.Forward ( reals(),
    '''   imaginaries())
    '''   ' set the most recent values of the signal dim firstPoint as integer = 0 newReal =
    '''   signal(signalLength-1)
    '''   newImaginary = 0 oldReal = signal(firstPoint)
    '''   oldImaginary = 0 sliding.Initialize(reals(), imaginaries())
    '''   ' Get the Previous values of the signal ' Get new values of the signal newReal = 0.99
    '''   newImaginary = 0 signal(firstPoint) = newReal
    '''   ....
    '''   ' Compute the sliding Fourier transform. fft.Update (newReal, newImaginary, oldReal,
    '''   oldImaginary)
    '''   ....
    '''   ' increment firstPoint += 1 oldReal = signal(firstPoint)
    '''   oldImaginary = 0 newReal = 1.01 newImaginary = 0 signal(firstPoint) = newReal ' Compute the
    '''   sliding Fourier transform. fft.Update (newReal, newImaginary, oldReal, oldImaginary)
    '''   ....
    '''   ' Compute the magnitude Dim magnitudes() = isr.Dsp.Helper.Magnitude(reals(), Imaginaries())
    ''' End Sub
    ''' </example>
    Public Function Update(ByVal newReal As Single, ByVal newImaginary As Single,
                               ByVal oldReal As Single, ByVal oldImaginary As Single) As Boolean

        ' get the difference term
        Dim realDelta As Single = newReal - oldReal
        Dim imagDelta As Single = newImaginary - oldImaginary

        ' Update the sliding Fourier transform
        For i As Integer = 0 To MyBase.RealCache.Length - 1
            newReal = CSng(MyBase.RealCache(i) + realDelta)
            newImaginary = CSng(MyBase.ImaginaryCache(i) + imagDelta)
            MyBase.RealCache(i) = MyBase.CosineTable(i) * newReal - MyBase.SineTable(i) * newImaginary
            MyBase.ImaginaryCache(i) = MyBase.CosineTable(i) * newImaginary + MyBase.SineTable(i) * newReal
        Next i

    End Function

#End Region


End Class
