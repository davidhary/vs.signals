﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Algorithms.Signals.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Algorithms.Signals.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Algorithms.Signals.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
