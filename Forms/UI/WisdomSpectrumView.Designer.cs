﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Algorithms.Signals.Forms
{
    [DesignerGenerated()]
    public partial class WisdomSpectrumView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __PointsToDisplayTextBox = new TextBox();
            __PointsToDisplayTextBox.Validating += new System.ComponentModel.CancelEventHandler(PointsToDisplayTextBox_Validating);
            _FftListBox = new ListBox();
            _DataTabPage = new TabPage();
            _TimingListBoxLabel = new Label();
            _PointsToDisplayTextBoxLabel = new Label();
            _FftListBoxLabel = new Label();
            _DoubleRadioButton = new RadioButton();
            _RemoveMeanCheckBox = new CheckBox();
            _ExampleComboBox = new ComboBox();
            _TaperWindowCheckBox = new CheckBox();
            _FftTimeStatusBarPanel = new StatusBarPanel();
            _ErrorStatusBarPanel = new StatusBarPanel();
            _MainTabControl = new TabControl();
            _SignalTabPage = new TabPage();
            __SignalChartPanel = new Panel();
            __SignalChartPanel.Paint += new PaintEventHandler(ScopeChartPanel_Paint);
            __SignalChartPanel.Resize += new EventHandler(ScopeChartPanel_Resize);
            _SignalOptionsPanel = new Panel();
            _SignalDurationTextBox = new TextBox();
            _SignalDurationTextBoxLabel = new Label();
            __PointsTextBox = new TextBox();
            __PointsTextBox.Validating += new System.ComponentModel.CancelEventHandler(PointsTextBox_Validating);
            __PhaseTextBox = new TextBox();
            __PhaseTextBox.Validating += new System.ComponentModel.CancelEventHandler(PhaseTextBox_Validating);
            __CyclesTextBox = new TextBox();
            __CyclesTextBox.Validating += new System.ComponentModel.CancelEventHandler(CyclesTextBox_Validating);
            _PointsTextBoxLabel = new Label();
            _PhaseTextBoxLabel = new Label();
            _CyclesTextBoxLabel = new Label();
            _SpectrumTabPage = new TabPage();
            __SpectrumChartPanel = new Panel();
            __SpectrumChartPanel.Paint += new PaintEventHandler(SpectrumChartPanel_Paint);
            __SpectrumChartPanel.Resize += new EventHandler(SpectrumChartPanel_Resize);
            _SpectrumOptionsPanel = new Panel();
            _ExampleComboBoxLabel = new Label();
            __StartStopCheckBox = new CheckBox();
            __StartStopCheckBox.CheckedChanged += new EventHandler(StartStopCheckBox_CheckedChanged);
            _SingleRadioButton = new RadioButton();
            _MessagesTabPage = new TabPage();
            _NotesMessageList = new Core.Forma.MessagesBox();
            _ErrorErrorProvider = new ErrorProvider(components);
            _CountStatusBarPanel = new StatusBarPanel();
            _StatusStatusBarPanel = new StatusBarPanel();
            _MainStatusBar = new StatusBar();
            _TimingTextBox = new TextBox();
            _DataTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_FftTimeStatusBarPanel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorStatusBarPanel).BeginInit();
            _MainTabControl.SuspendLayout();
            _SignalTabPage.SuspendLayout();
            _SignalOptionsPanel.SuspendLayout();
            _SpectrumTabPage.SuspendLayout();
            _SpectrumOptionsPanel.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorErrorProvider).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_CountStatusBarPanel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_StatusStatusBarPanel).BeginInit();
            SuspendLayout();
            // 
            // _PointsToDisplayTextBox
            // 
            __PointsToDisplayTextBox.AcceptsReturn = true;
            __PointsToDisplayTextBox.BackColor = SystemColors.Window;
            __PointsToDisplayTextBox.Cursor = Cursors.IBeam;
            __PointsToDisplayTextBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __PointsToDisplayTextBox.ForeColor = SystemColors.WindowText;
            __PointsToDisplayTextBox.Location = new Point(128, 16);
            __PointsToDisplayTextBox.MaxLength = 0;
            __PointsToDisplayTextBox.Name = "__PointsToDisplayTextBox";
            __PointsToDisplayTextBox.RightToLeft = RightToLeft.No;
            __PointsToDisplayTextBox.Size = new Size(65, 20);
            __PointsToDisplayTextBox.TabIndex = 17;
            // 
            // _FftListBox
            // 
            _FftListBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _FftListBox.BackColor = SystemColors.Window;
            _FftListBox.Cursor = Cursors.Default;
            _FftListBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FftListBox.ForeColor = SystemColors.WindowText;
            _FftListBox.Location = new Point(272, 24);
            _FftListBox.Name = "_FftListBox";
            _FftListBox.RightToLeft = RightToLeft.No;
            _FftListBox.Size = new Size(269, 303);
            _FftListBox.TabIndex = 15;
            // 
            // _DataTabPage
            // 
            _DataTabPage.Controls.Add(_TimingTextBox);
            _DataTabPage.Controls.Add(_TimingListBoxLabel);
            _DataTabPage.Controls.Add(__PointsToDisplayTextBox);
            _DataTabPage.Controls.Add(_FftListBox);
            _DataTabPage.Controls.Add(_PointsToDisplayTextBoxLabel);
            _DataTabPage.Controls.Add(_FftListBoxLabel);
            _DataTabPage.Location = new Point(4, 22);
            _DataTabPage.Name = "_DataTabPage";
            _DataTabPage.Size = new Size(552, 342);
            _DataTabPage.TabIndex = 2;
            _DataTabPage.Text = "Data";
            // 
            // _TimingListBoxLabel
            // 
            _TimingListBoxLabel.AutoSize = true;
            _TimingListBoxLabel.Location = new Point(16, 54);
            _TimingListBoxLabel.Name = "_TimingListBoxLabel";
            _TimingListBoxLabel.Size = new Size(70, 13);
            _TimingListBoxLabel.TabIndex = 20;
            _TimingListBoxLabel.Text = "Timing Data: ";
            // 
            // _PointsToDisplayTextBoxLabel
            // 
            _PointsToDisplayTextBoxLabel.BackColor = SystemColors.Control;
            _PointsToDisplayTextBoxLabel.Cursor = Cursors.Default;
            _PointsToDisplayTextBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _PointsToDisplayTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PointsToDisplayTextBoxLabel.Location = new Point(16, 20);
            _PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel";
            _PointsToDisplayTextBoxLabel.RightToLeft = RightToLeft.No;
            _PointsToDisplayTextBoxLabel.Size = new Size(104, 16);
            _PointsToDisplayTextBoxLabel.TabIndex = 18;
            _PointsToDisplayTextBoxLabel.Text = "Points to Display: ";
            _PointsToDisplayTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _FftListBoxLabel
            // 
            _FftListBoxLabel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _FftListBoxLabel.BackColor = SystemColors.Control;
            _FftListBoxLabel.Cursor = Cursors.Default;
            _FftListBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FftListBoxLabel.ForeColor = SystemColors.ControlText;
            _FftListBoxLabel.Location = new Point(272, 8);
            _FftListBoxLabel.Name = "_FftListBoxLabel";
            _FftListBoxLabel.RightToLeft = RightToLeft.No;
            _FftListBoxLabel.Size = new Size(269, 16);
            _FftListBoxLabel.TabIndex = 16;
            _FftListBoxLabel.Text = "Signal                       IFFT{ FFT{ Signal}}";
            // 
            // _DoubleRadioButton
            // 
            _DoubleRadioButton.Checked = true;
            _DoubleRadioButton.Location = new Point(112, 5);
            _DoubleRadioButton.Name = "_DoubleRadioButton";
            _DoubleRadioButton.Size = new Size(120, 17);
            _DoubleRadioButton.TabIndex = 27;
            _DoubleRadioButton.TabStop = true;
            _DoubleRadioButton.Text = "Double Precision";
            // 
            // _RemoveMeanCheckBox
            // 
            _RemoveMeanCheckBox.BackColor = SystemColors.Control;
            _RemoveMeanCheckBox.Cursor = Cursors.Default;
            _RemoveMeanCheckBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _RemoveMeanCheckBox.ForeColor = SystemColors.ControlText;
            _RemoveMeanCheckBox.Location = new Point(8, 4);
            _RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox";
            _RemoveMeanCheckBox.RightToLeft = RightToLeft.No;
            _RemoveMeanCheckBox.Size = new Size(104, 18);
            _RemoveMeanCheckBox.TabIndex = 23;
            _RemoveMeanCheckBox.Text = "Remove Mean";
            _RemoveMeanCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ExampleComboBox
            // 
            _ExampleComboBox.BackColor = SystemColors.Window;
            _ExampleComboBox.Cursor = Cursors.Default;
            _ExampleComboBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ExampleComboBox.ForeColor = SystemColors.WindowText;
            _ExampleComboBox.Items.AddRange(new object[] { "Mixed Radix FFT", "Sliding FFT" });
            _ExampleComboBox.Location = new Point(304, 27);
            _ExampleComboBox.Name = "_ExampleComboBox";
            _ExampleComboBox.RightToLeft = RightToLeft.No;
            _ExampleComboBox.Size = new Size(168, 21);
            _ExampleComboBox.TabIndex = 25;
            _ExampleComboBox.Text = "Mixed Radix FFT";
            // 
            // _TaperWindowCheckBox
            // 
            _TaperWindowCheckBox.BackColor = SystemColors.Control;
            _TaperWindowCheckBox.Cursor = Cursors.Default;
            _TaperWindowCheckBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TaperWindowCheckBox.ForeColor = SystemColors.ControlText;
            _TaperWindowCheckBox.Location = new Point(8, 24);
            _TaperWindowCheckBox.Name = "_TaperWindowCheckBox";
            _TaperWindowCheckBox.RightToLeft = RightToLeft.No;
            _TaperWindowCheckBox.Size = new Size(104, 18);
            _TaperWindowCheckBox.TabIndex = 24;
            _TaperWindowCheckBox.Text = "Taper Window";
            _TaperWindowCheckBox.UseVisualStyleBackColor = false;
            // 
            // _FftTimeStatusBarPanel
            // 
            _FftTimeStatusBarPanel.Alignment = HorizontalAlignment.Center;
            _FftTimeStatusBarPanel.AutoSize = StatusBarPanelAutoSize.Contents;
            _FftTimeStatusBarPanel.MinWidth = 80;
            _FftTimeStatusBarPanel.Name = "_FftTimeStatusBarPanel";
            _FftTimeStatusBarPanel.Text = "0.000 ms";
            _FftTimeStatusBarPanel.ToolTipText = "Time to calculate an FFT";
            _FftTimeStatusBarPanel.Width = 80;
            // 
            // _ErrorStatusBarPanel
            // 
            _ErrorStatusBarPanel.Alignment = HorizontalAlignment.Center;
            _ErrorStatusBarPanel.AutoSize = StatusBarPanelAutoSize.Contents;
            _ErrorStatusBarPanel.MinWidth = 60;
            _ErrorStatusBarPanel.Name = "_ErrorStatusBarPanel";
            _ErrorStatusBarPanel.Text = "0.000";
            _ErrorStatusBarPanel.ToolTipText = "RMS difference between signal and inverse FFT";
            _ErrorStatusBarPanel.Width = 60;
            // 
            // _MainTabControl
            // 
            _MainTabControl.Controls.Add(_SignalTabPage);
            _MainTabControl.Controls.Add(_SpectrumTabPage);
            _MainTabControl.Controls.Add(_DataTabPage);
            _MainTabControl.Controls.Add(_MessagesTabPage);
            _MainTabControl.Dock = DockStyle.Fill;
            _MainTabControl.Location = new Point(0, 0);
            _MainTabControl.Name = "_MainTabControl";
            _MainTabControl.SelectedIndex = 0;
            _MainTabControl.Size = new Size(560, 368);
            _MainTabControl.TabIndex = 2;
            // 
            // _SignalTabPage
            // 
            _SignalTabPage.Controls.Add(__SignalChartPanel);
            _SignalTabPage.Controls.Add(_SignalOptionsPanel);
            _SignalTabPage.Location = new Point(4, 22);
            _SignalTabPage.Name = "_SignalTabPage";
            _SignalTabPage.Size = new Size(552, 342);
            _SignalTabPage.TabIndex = 0;
            _SignalTabPage.Text = "Signal";
            // 
            // _SignalChartPanel
            // 
            __SignalChartPanel.Dock = DockStyle.Fill;
            __SignalChartPanel.Location = new Point(0, 56);
            __SignalChartPanel.Name = "__SignalChartPanel";
            __SignalChartPanel.Size = new Size(552, 286);
            __SignalChartPanel.TabIndex = 30;
            // 
            // _SignalOptionsPanel
            // 
            _SignalOptionsPanel.Controls.Add(_SignalDurationTextBox);
            _SignalOptionsPanel.Controls.Add(_SignalDurationTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(__PointsTextBox);
            _SignalOptionsPanel.Controls.Add(__PhaseTextBox);
            _SignalOptionsPanel.Controls.Add(__CyclesTextBox);
            _SignalOptionsPanel.Controls.Add(_PointsTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(_PhaseTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(_CyclesTextBoxLabel);
            _SignalOptionsPanel.Dock = DockStyle.Top;
            _SignalOptionsPanel.Location = new Point(0, 0);
            _SignalOptionsPanel.Name = "_SignalOptionsPanel";
            _SignalOptionsPanel.Size = new Size(552, 56);
            _SignalOptionsPanel.TabIndex = 29;
            // 
            // _SignalDurationTextBox
            // 
            _SignalDurationTextBox.AcceptsReturn = true;
            _SignalDurationTextBox.BackColor = SystemColors.Window;
            _SignalDurationTextBox.Cursor = Cursors.IBeam;
            _SignalDurationTextBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _SignalDurationTextBox.ForeColor = SystemColors.WindowText;
            _SignalDurationTextBox.Location = new Point(90, 30);
            _SignalDurationTextBox.MaxLength = 0;
            _SignalDurationTextBox.Name = "_SignalDurationTextBox";
            _SignalDurationTextBox.RightToLeft = RightToLeft.No;
            _SignalDurationTextBox.Size = new Size(46, 20);
            _SignalDurationTextBox.TabIndex = 23;
            // 
            // _SignalDurationTextBoxLabel
            // 
            _SignalDurationTextBoxLabel.BackColor = SystemColors.Control;
            _SignalDurationTextBoxLabel.Cursor = Cursors.Default;
            _SignalDurationTextBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _SignalDurationTextBoxLabel.ForeColor = SystemColors.ControlText;
            _SignalDurationTextBoxLabel.Location = new Point(8, 32);
            _SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel";
            _SignalDurationTextBoxLabel.RightToLeft = RightToLeft.No;
            _SignalDurationTextBoxLabel.Size = new Size(80, 16);
            _SignalDurationTextBoxLabel.TabIndex = 24;
            _SignalDurationTextBoxLabel.Text = "Duration [Sec]: ";
            _SignalDurationTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _PointsTextBox
            // 
            __PointsTextBox.AcceptsReturn = true;
            __PointsTextBox.BackColor = SystemColors.Window;
            __PointsTextBox.Cursor = Cursors.IBeam;
            __PointsTextBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __PointsTextBox.ForeColor = SystemColors.WindowText;
            __PointsTextBox.Location = new Point(90, 4);
            __PointsTextBox.MaxLength = 0;
            __PointsTextBox.Name = "__PointsTextBox";
            __PointsTextBox.RightToLeft = RightToLeft.No;
            __PointsTextBox.Size = new Size(46, 20);
            __PointsTextBox.TabIndex = 22;
            __PointsTextBox.Text = "1000";
            // 
            // _PhaseTextBox
            // 
            __PhaseTextBox.AcceptsReturn = true;
            __PhaseTextBox.BackColor = SystemColors.Window;
            __PhaseTextBox.Cursor = Cursors.IBeam;
            __PhaseTextBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __PhaseTextBox.ForeColor = SystemColors.WindowText;
            __PhaseTextBox.Location = new Point(240, 30);
            __PhaseTextBox.MaxLength = 0;
            __PhaseTextBox.Name = "__PhaseTextBox";
            __PhaseTextBox.RightToLeft = RightToLeft.No;
            __PhaseTextBox.Size = new Size(48, 20);
            __PhaseTextBox.TabIndex = 19;
            // 
            // _CyclesTextBox
            // 
            __CyclesTextBox.AcceptsReturn = true;
            __CyclesTextBox.BackColor = SystemColors.Window;
            __CyclesTextBox.Cursor = Cursors.IBeam;
            __CyclesTextBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __CyclesTextBox.ForeColor = SystemColors.WindowText;
            __CyclesTextBox.Location = new Point(240, 4);
            __CyclesTextBox.MaxLength = 0;
            __CyclesTextBox.Name = "__CyclesTextBox";
            __CyclesTextBox.RightToLeft = RightToLeft.No;
            __CyclesTextBox.Size = new Size(48, 20);
            __CyclesTextBox.TabIndex = 17;
            // 
            // _PointsTextBoxLabel
            // 
            _PointsTextBoxLabel.BackColor = SystemColors.Control;
            _PointsTextBoxLabel.Cursor = Cursors.Default;
            _PointsTextBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _PointsTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PointsTextBoxLabel.Location = new Point(40, 6);
            _PointsTextBoxLabel.Name = "_PointsTextBoxLabel";
            _PointsTextBoxLabel.RightToLeft = RightToLeft.No;
            _PointsTextBoxLabel.Size = new Size(48, 16);
            _PointsTextBoxLabel.TabIndex = 21;
            _PointsTextBoxLabel.Text = "Points: ";
            _PointsTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _PhaseTextBoxLabel
            // 
            _PhaseTextBoxLabel.BackColor = SystemColors.Control;
            _PhaseTextBoxLabel.Cursor = Cursors.Default;
            _PhaseTextBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _PhaseTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PhaseTextBoxLabel.Location = new Point(160, 32);
            _PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel";
            _PhaseTextBoxLabel.RightToLeft = RightToLeft.No;
            _PhaseTextBoxLabel.Size = new Size(80, 16);
            _PhaseTextBoxLabel.TabIndex = 20;
            _PhaseTextBoxLabel.Text = "Phase [Deg]: ";
            _PhaseTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _CyclesTextBoxLabel
            // 
            _CyclesTextBoxLabel.BackColor = SystemColors.Control;
            _CyclesTextBoxLabel.Cursor = Cursors.Default;
            _CyclesTextBoxLabel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _CyclesTextBoxLabel.ForeColor = SystemColors.ControlText;
            _CyclesTextBoxLabel.Location = new Point(152, 6);
            _CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel";
            _CyclesTextBoxLabel.RightToLeft = RightToLeft.No;
            _CyclesTextBoxLabel.Size = new Size(87, 16);
            _CyclesTextBoxLabel.TabIndex = 18;
            _CyclesTextBoxLabel.Text = "Frequency [Hz]: ";
            _CyclesTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _SpectrumTabPage
            // 
            _SpectrumTabPage.Controls.Add(__SpectrumChartPanel);
            _SpectrumTabPage.Controls.Add(_SpectrumOptionsPanel);
            _SpectrumTabPage.Location = new Point(4, 22);
            _SpectrumTabPage.Name = "_SpectrumTabPage";
            _SpectrumTabPage.Size = new Size(552, 342);
            _SpectrumTabPage.TabIndex = 1;
            _SpectrumTabPage.Text = "Spectrum";
            // 
            // _SpectrumChartPanel
            // 
            __SpectrumChartPanel.Dock = DockStyle.Fill;
            __SpectrumChartPanel.Location = new Point(0, 51);
            __SpectrumChartPanel.Name = "_spectrumChartPanel";
            __SpectrumChartPanel.Size = new Size(552, 291);
            __SpectrumChartPanel.TabIndex = 28;
            // 
            // _SpectrumOptionsPanel
            // 
            _SpectrumOptionsPanel.Controls.Add(_ExampleComboBoxLabel);
            _SpectrumOptionsPanel.Controls.Add(__StartStopCheckBox);
            _SpectrumOptionsPanel.Controls.Add(_SingleRadioButton);
            _SpectrumOptionsPanel.Controls.Add(_DoubleRadioButton);
            _SpectrumOptionsPanel.Controls.Add(_RemoveMeanCheckBox);
            _SpectrumOptionsPanel.Controls.Add(_ExampleComboBox);
            _SpectrumOptionsPanel.Controls.Add(_TaperWindowCheckBox);
            _SpectrumOptionsPanel.Dock = DockStyle.Top;
            _SpectrumOptionsPanel.Location = new Point(0, 0);
            _SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel";
            _SpectrumOptionsPanel.Size = new Size(552, 51);
            _SpectrumOptionsPanel.TabIndex = 27;
            // 
            // _ExampleComboBoxLabel
            // 
            _ExampleComboBoxLabel.Location = new Point(232, 29);
            _ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel";
            _ExampleComboBoxLabel.Size = new Size(64, 16);
            _ExampleComboBoxLabel.TabIndex = 32;
            _ExampleComboBoxLabel.Text = "Calculate: ";
            _ExampleComboBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _StartStopCheckBox
            // 
            __StartStopCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __StartStopCheckBox.Appearance = Appearance.Button;
            __StartStopCheckBox.Location = new Point(484, 25);
            __StartStopCheckBox.Name = "__StartStopCheckBox";
            __StartStopCheckBox.Size = new Size(64, 24);
            __StartStopCheckBox.TabIndex = 31;
            __StartStopCheckBox.Text = "&Start";
            __StartStopCheckBox.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // _SingleRadioButton
            // 
            _SingleRadioButton.Location = new Point(112, 25);
            _SingleRadioButton.Name = "_SingleRadioButton";
            _SingleRadioButton.Size = new Size(104, 17);
            _SingleRadioButton.TabIndex = 28;
            _SingleRadioButton.Text = "Single Precision";
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(_NotesMessageList);
            _MessagesTabPage.Location = new Point(4, 22);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new Size(552, 342);
            _MessagesTabPage.TabIndex = 3;
            _MessagesTabPage.Text = "Messages";
            // 
            // _NotesMessageList
            // 
            _NotesMessageList.Dock = DockStyle.Fill;
            _NotesMessageList.Location = new Point(0, 0);
            _NotesMessageList.Multiline = true;
            _NotesMessageList.Name = "_NotesMessageList";
            _NotesMessageList.PresetCount = 50;
            _NotesMessageList.ReadOnly = true;
            _NotesMessageList.ResetCount = 100;
            _NotesMessageList.ScrollBars = ScrollBars.Both;
            _NotesMessageList.Size = new Size(552, 342);
            _NotesMessageList.TabIndex = 4;
            // 
            // _ErrorErrorProvider
            // 
            _ErrorErrorProvider.ContainerControl = this;
            // 
            // _CountStatusBarPanel
            // 
            _CountStatusBarPanel.Alignment = HorizontalAlignment.Center;
            _CountStatusBarPanel.AutoSize = StatusBarPanelAutoSize.Contents;
            _CountStatusBarPanel.MinWidth = 20;
            _CountStatusBarPanel.Name = "_CountStatusBarPanel";
            _CountStatusBarPanel.Text = "0";
            _CountStatusBarPanel.ToolTipText = "FFT counter";
            _CountStatusBarPanel.Width = 20;
            // 
            // _StatusStatusBarPanel
            // 
            _StatusStatusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            _StatusStatusBarPanel.Name = "_StatusStatusBarPanel";
            _StatusStatusBarPanel.Text = "<status>";
            _StatusStatusBarPanel.ToolTipText = "Status";
            _StatusStatusBarPanel.Width = 383;
            // 
            // _MainStatusBar
            // 
            _MainStatusBar.Location = new Point(0, 368);
            _MainStatusBar.Name = "_MainStatusBar";
            _MainStatusBar.Panels.AddRange(new StatusBarPanel[] { _StatusStatusBarPanel, _CountStatusBarPanel, _ErrorStatusBarPanel, _FftTimeStatusBarPanel });
            _MainStatusBar.ShowPanels = true;
            _MainStatusBar.Size = new Size(560, 22);
            _MainStatusBar.TabIndex = 3;
            _MainStatusBar.Text = "Main Status Bar";
            // 
            // _TimingTextBox
            // 
            _TimingTextBox.Font = new Font("Courier New", 8.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TimingTextBox.Location = new Point(20, 70);
            _TimingTextBox.Multiline = true;
            _TimingTextBox.Name = "_TimingTextBox";
            _TimingTextBox.Size = new Size(246, 257);
            _TimingTextBox.TabIndex = 21;
            // 
            // SpectrumPanel
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(560, 390);
            Controls.Add(_MainTabControl);
            Controls.Add(_MainStatusBar);
            Name = "WisdomSpectrumView";
            Text = "Wisdom Spectrum View";
            _DataTabPage.ResumeLayout(false);
            _DataTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_FftTimeStatusBarPanel).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorStatusBarPanel).EndInit();
            _MainTabControl.ResumeLayout(false);
            _SignalTabPage.ResumeLayout(false);
            _SignalOptionsPanel.ResumeLayout(false);
            _SignalOptionsPanel.PerformLayout();
            _SpectrumTabPage.ResumeLayout(false);
            _SpectrumOptionsPanel.ResumeLayout(false);
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorErrorProvider).EndInit();
            ((System.ComponentModel.ISupportInitialize)_CountStatusBarPanel).EndInit();
            ((System.ComponentModel.ISupportInitialize)_StatusStatusBarPanel).EndInit();
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
        }

        private TextBox __PointsToDisplayTextBox;

        private TextBox _PointsToDisplayTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PointsToDisplayTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PointsToDisplayTextBox != null)
                {
                    __PointsToDisplayTextBox.Validating -= PointsToDisplayTextBox_Validating;
                }

                __PointsToDisplayTextBox = value;
                if (__PointsToDisplayTextBox != null)
                {
                    __PointsToDisplayTextBox.Validating += PointsToDisplayTextBox_Validating;
                }
            }
        }

        private ListBox _FftListBox;
        private TabPage _DataTabPage;
        private Label _PointsToDisplayTextBoxLabel;
        private Label _FftListBoxLabel;
        private RadioButton _DoubleRadioButton;
        private CheckBox _RemoveMeanCheckBox;
        private ComboBox _ExampleComboBox;
        private CheckBox _TaperWindowCheckBox;
        private StatusBarPanel _FftTimeStatusBarPanel;
        private StatusBarPanel _ErrorStatusBarPanel;
        private TabControl _MainTabControl;
        private TabPage _SignalTabPage;
        private Panel __SignalChartPanel;

        private Panel _SignalChartPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SignalChartPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SignalChartPanel != null)
                {
                    __SignalChartPanel.Paint -= ScopeChartPanel_Paint;
                    __SignalChartPanel.Resize -= ScopeChartPanel_Resize;
                }

                __SignalChartPanel = value;
                if (__SignalChartPanel != null)
                {
                    __SignalChartPanel.Paint += ScopeChartPanel_Paint;
                    __SignalChartPanel.Resize += ScopeChartPanel_Resize;
                }
            }
        }

        private Panel _SignalOptionsPanel;
        private TextBox _SignalDurationTextBox;
        private Label _SignalDurationTextBoxLabel;
        private TextBox __PointsTextBox;

        private TextBox _PointsTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PointsTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PointsTextBox != null)
                {
                    __PointsTextBox.Validating -= PointsTextBox_Validating;
                }

                __PointsTextBox = value;
                if (__PointsTextBox != null)
                {
                    __PointsTextBox.Validating += PointsTextBox_Validating;
                }
            }
        }

        private TextBox __PhaseTextBox;

        private TextBox _PhaseTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PhaseTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PhaseTextBox != null)
                {
                    __PhaseTextBox.Validating -= PhaseTextBox_Validating;
                }

                __PhaseTextBox = value;
                if (__PhaseTextBox != null)
                {
                    __PhaseTextBox.Validating += PhaseTextBox_Validating;
                }
            }
        }

        private TextBox __CyclesTextBox;

        private TextBox _CyclesTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CyclesTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CyclesTextBox != null)
                {
                    __CyclesTextBox.Validating -= CyclesTextBox_Validating;
                }

                __CyclesTextBox = value;
                if (__CyclesTextBox != null)
                {
                    __CyclesTextBox.Validating += CyclesTextBox_Validating;
                }
            }
        }

        private Label _PointsTextBoxLabel;
        private Label _PhaseTextBoxLabel;
        private Label _CyclesTextBoxLabel;
        private TabPage _SpectrumTabPage;
        private Panel __SpectrumChartPanel;

        private Panel _SpectrumChartPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SpectrumChartPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SpectrumChartPanel != null)
                {
                    __SpectrumChartPanel.Paint -= SpectrumChartPanel_Paint;
                    __SpectrumChartPanel.Resize -= SpectrumChartPanel_Resize;
                }

                __SpectrumChartPanel = value;
                if (__SpectrumChartPanel != null)
                {
                    __SpectrumChartPanel.Paint += SpectrumChartPanel_Paint;
                    __SpectrumChartPanel.Resize += SpectrumChartPanel_Resize;
                }
            }
        }

        private Panel _SpectrumOptionsPanel;
        private Label _ExampleComboBoxLabel;
        private CheckBox __StartStopCheckBox;

        private CheckBox _StartStopCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartStopCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartStopCheckBox != null)
                {
                    __StartStopCheckBox.CheckedChanged -= StartStopCheckBox_CheckedChanged;
                }

                __StartStopCheckBox = value;
                if (__StartStopCheckBox != null)
                {
                    __StartStopCheckBox.CheckedChanged += StartStopCheckBox_CheckedChanged;
                }
            }
        }

        private RadioButton _SingleRadioButton;
        private TabPage _MessagesTabPage;
        private Core.Forma.MessagesBox _NotesMessageList;
        private ErrorProvider _ErrorErrorProvider;
        private StatusBar _MainStatusBar;
        private StatusBarPanel _StatusStatusBarPanel;
        private StatusBarPanel _CountStatusBarPanel;
        private Label _TimingListBoxLabel;
        private TextBox _TimingTextBox;
    }
}