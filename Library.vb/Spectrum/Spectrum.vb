Imports System.Numerics

''' <summary>
''' Spectrum base class for calculating the spectrum using the Fourier Transform.  Includes pre-
''' processing fro removing mean and applying a taper data window. Includes post-processing
''' methods for high- pass and low-pass filtering in the frequency domain.
''' </summary>
''' <remarks>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Spectrum
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
    '''                                 calculating the spectrum. </param>
    Public Sub New(ByVal fourierTransform As isr.Algorithms.Signals.FourierTransformBase)
        ' instantiate the base class
        MyBase.New()
        ' set reference to the Fourier Transform class
        Me._FourierTransform = fourierTransform
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make This method Overridable (virtual) because a derived class should not be able to
    ''' override This method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.
        ' this disposes all child classes.
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          False if This method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' Free managed resources when explicitly called
                    ' remove the reference to the external objects.
                End If
                ' Free shared unmanaged resources
                Me._FourierTransform = Nothing
                Me._TaperWindow = Nothing
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " COMMON MEMBERS "

    ''' <summary> Gets reference to the Fourier Transform used for calculating the spectrum. </summary>
    ''' <value> The Fourier transform. </value>
    Protected ReadOnly Property FourierTransform() As isr.Algorithms.Signals.FourierTransformBase

    ''' <summary> The number of elements in half the spectrum including the DC point. </summary>
    ''' <value> The length of the half spectrum. </value>
    Public ReadOnly Property HalfSpectrumLength() As Integer
        Get
            Return Me.FourierTransform.HalfSpectrumLength
        End Get
    End Property

    ''' <summary> Gets the length of the full spectrum. </summary>
    ''' <value> The length of the full spectrum. </value>
    Public ReadOnly Property FullSpectrumLength() As Integer
        Get
            Return Me.FourierTransform.FullSpectrumLength
        End Get
    End Property

    ''' <summary>
    ''' Gets or Sets the condition as True scale the Spectrum.  It is useful to set This to false
    ''' whenever averaging transforms so that the scaling is done at the end.
    ''' </summary>
    ''' <value> The is scale FFT. </value>
    Public Property IsScaleFft() As Boolean

    ''' <summary>
    ''' Gets or Sets the condition as True remove the mean before computing the Spectrum.
    ''' </summary>
    ''' <value> The is remove mean. </value>
    Public Property IsRemoveMean() As Boolean

    ''' <summary>
    ''' Returns the power (average sum of squares of amplitudes) of the taper window.
    ''' </summary>
    ''' <remarks>
    ''' Represents the average amount by which the power of the signal was attenuated due to the
    ''' tapering effect of the taper data window.
    ''' </remarks>
    ''' <value> The taper window power. </value>
    Public ReadOnly Property TaperWindowPower() As Double
        Get
            Return If(Me.TaperWindow Is Nothing, 1.0R, Me.TaperWindow.Power())
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the number of elements in the time series sample that is processed to compute
    ''' the Spectrum.
    ''' </summary>
    ''' <remarks>
    ''' This must be set before setting the filter frequencies. The size of the Fourier transform is
    ''' set when calling the Fourier Transform. That call is used to update the time series length.
    ''' This value is allowed to be set independently in case the spectrum is read from a file.
    ''' </remarks>
    ''' <value> The length of the time series. </value>
    Public Property TimeSeriesLength() As Integer
        Get
            Return Me.FourierTransform.TimeSeriesLength
        End Get
        Set(value As Integer)
            Me.FourierTransform.TimeSeriesLength = value
        End Set
    End Property

    ''' <summary> Populates the spectrum frequency array. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="halfSpectrum"> True to half spectrum. </param>
    ''' <returns> A Double() </returns>
    Public Function PopulateFrequencies(ByVal halfSpectrum As Boolean) As Double()
        Dim frequencyResolution As Double = Me.SamplingRate / Me.FullSpectrumLength
        Return If(halfSpectrum,
            isr.Algorithms.Signals.Signal.Frequencies(frequencyResolution, Me.HalfSpectrumLength),
            isr.Algorithms.Signals.Signal.Frequencies(frequencyResolution, Me.FullSpectrumLength))
    End Function

    ''' <summary>
    ''' Gets the sampling rate for use when setting the <see cref="TaperFilter"/> properties. Must be
    ''' set before setting the filter frequencies.
    ''' </summary>
    ''' <value> The sampling rate. </value>
    Public Property SamplingRate() As Double

    ''' <summary> Gets the frequency resolution. </summary>
    ''' <value> The frequency resolution. </value>
    Public ReadOnly Property FrequencyResolution As Double
        Get
            Return Me.SamplingRate / Me.FullSpectrumLength
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperFilter">taper
    ''' Filter</see>.
    ''' </summary>
    ''' <value> The taper filter. </value>
    Public Property TaperFilter() As isr.Algorithms.Signals.TaperFilter

    ''' <summary>
    ''' Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperWindow">taper
    ''' window</see>
    ''' used for reducing side lobes.
    ''' </summary>
    ''' <value> The taper window. </value>
    Public Property TaperWindow() As isr.Algorithms.Signals.TaperWindow

    ''' <summary>
    ''' Returns the FFT scale factor taking into account the effect of the taper window.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The FFT scale factor taking into account the effect of the taper window. </returns>
    Public Function FftScaleFactor() As Double
        Return 1.0 / (System.Math.Sqrt(Me.TaperWindowPower) * Me.TimeSeriesLength)
    End Function

#End Region

#Region " COMPLEX "

    ''' <summary> Initializes the spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Overridable Sub Initialize(ByVal timeSeries() As Complex)
        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        If timeSeries.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(timeSeries), "Time series must be longer than 1")
        ' initializes the time series length
        Me.TimeSeriesLength = timeSeries.Length
        ' Create the taper window
        If Me.TaperWindow IsNot Nothing Then Me.TaperWindow.Create(timeSeries.Length)
    End Sub

    ''' <summary> Applies the filter described by values. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The input time series where the spectrum is returned. </param>
    Public Sub ApplyFilter(ByVal values As Complex())
        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then
            Me.TaperFilter.Taper(values, Me.SamplingRate)
        End If
    End Sub

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub ApplyTaperWindow(ByVal timeSeries As Complex())
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(timeSeries)
        End If
    End Sub

    ''' <summary> Removes the mean. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub RemoveMean(ByVal timeSeries As Complex())
        If Me.IsRemoveMean Then
            timeSeries.RemoveRealMean()
        End If
    End Sub

    ''' <summary>
    ''' Scales the specified spectrum if <see cref="IsScaleFft">option</see> is set.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="spectrum"> The spectrum. </param>
    Public Sub ScaleFft(ByVal spectrum As Complex())
        ' Scale the Spectrum if the scale option is set
        If spectrum IsNot Nothing AndAlso Me.IsScaleFft Then spectrum.Scale(Me.FftScaleFactor())
    End Sub

    ''' <summary>
    ''' Calculates the spectrum using the Fourier Transform with pre- and post-processing.
    ''' </summary>
    ''' <remarks>
    ''' Applies filter, removes mean, calculates the spectrum and appends to the previous spectrum.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The input time series where the spectrum is returned. </param>
    Public Overridable Sub Calculate(ByVal values As Complex())

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException(NameOf(values), "Time series must have more than 1 value")
        End If

        ' Remove the mean if the mean option is set
        Me.RemoveMean(values)

        ' Apply the taper window
        Me.ApplyTaperWindow(values)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(values)

        ' scale if required
        Me.ScaleFft(values)

        ' apply the filter as necessary
        Me.ApplyFilter(values)

    End Sub

    ''' <summary>
    ''' Filters the real valued signal using low pass and high pass taper frequency windows.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub Filter(ByVal timeSeries() As Complex)

        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        If timeSeries.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException(NameOf(timeSeries), "Time series must be longer than 1")
        End If

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(timeSeries)

        ' Scale the Spectrum.
        timeSeries.Scale(1.0R / timeSeries.Length)

        ' apply the filter as necessary
        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then
            Me.ApplyFilter(timeSeries)
        End If

        ' inverse transform
        Me.FourierTransform.Inverse(timeSeries)

    End Sub

#End Region

#Region " DOUBLE "

    ''' <summary> Initializes the spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")

        ' Create the taper window
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Create(reals.Length)
        End If

        ' apply the transform
        Me.FourierTransform.Forward(reals, imaginaries)

    End Sub

    ''' <summary> Applies the filter to the spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub ApplyFilter(ByVal reals() As Double, ByVal imaginaries() As Double)
        If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        Me.TaperFilter.Taper(Me.SamplingRate, reals, imaginaries)
    End Sub

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub ApplyTaperWindow(ByVal timeSeries As Double())
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(timeSeries)
        End If
    End Sub

    ''' <summary> Removes the mean. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub RemoveMean(ByVal timeSeries As Double())
        If Me.IsRemoveMean Then
            timeSeries.RemoveMean()
        End If
    End Sub

    ''' <summary>
    ''' Calculates the spectrum using the Fourier Transform with pre and post processing.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    ''' <example> Sub Form_Click End Sub </example>
    Public Sub Calculate(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")
        Me.RemoveMean(reals)

        Me.ApplyTaperWindow(reals)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Check if the scale option is set
        If Me.IsScaleFft Then

            ' Set the scale factor
            Dim scaleFactor As Double = 1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length)

            ' Scale the spectrum.
            DoubleExtensions.Scale(reals, scaleFactor)
            DoubleExtensions.Scale(imaginaries, scaleFactor)

        End If

        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

    End Sub

    ''' <summary>
    ''' Filters the real valued signal using low pass and high pass taper frequency windows.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals"> Holds the real values. </param>
    ''' <example> Sub Form_Click End Sub </example>
    Public Sub Filter(ByVal reals() As Double)

        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

        ' Initialize imaginary array.
        Dim imaginaries() As Double = New Double(reals.Length - 1) {}

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Set the scale factor
        Dim scaleFactor As Double = 1.0R / reals.Length

        ' Scale the Spectrum.
        DoubleExtensions.Scale(reals, scaleFactor)
        DoubleExtensions.Scale(imaginaries, scaleFactor)

        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

        ' inverse transform
        Me.FourierTransform.Inverse(reals, imaginaries)

    End Sub


#End Region

#Region " SINGLE "

    ''' <summary> Initializes the spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")
        ' Create the taper window
        If Not Me.TaperWindow Is Nothing Then Me.TaperWindow.Create(reals.Length)
        ' apply the transform
        Me.FourierTransform.Forward(reals, imaginaries)
    End Sub

    ''' <summary> Applies the filter to the spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub ApplyFilter(ByVal reals() As Single, ByVal imaginaries() As Single)
        If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        Me.TaperFilter.Taper(Me.SamplingRate, reals, imaginaries)
    End Sub

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub ApplyTaperWindow(ByVal timeSeries As Single())
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(timeSeries)
        End If
    End Sub

    ''' <summary> Removes the mean. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub RemoveMean(ByVal timeSeries As Single())
        If Me.IsRemoveMean Then
            timeSeries.RemoveMean()
        End If
    End Sub

    ''' <summary>
    ''' Calculates the spectrum using the Fourier Transform with pre and post processing.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub Calculate(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")

        Me.RemoveMean(reals)

        Me.ApplyTaperWindow(reals)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Check if the scale option is set
        If Me.IsScaleFft Then

            ' Set the scale factor
            Dim scaleFactor As Single = CSng(1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length))

            ' Scale the Spectrum.
            SingleExtensions.Scale(reals, scaleFactor)
            SingleExtensions.Scale(imaginaries, scaleFactor)

        End If

        If Me._TaperFilter IsNot Nothing AndAlso Me._TaperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

    End Sub

    ''' <summary>
    ''' Filters the real valued signal using low pass and high pass taper frequency windows.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals"> Holds the real values. </param>
    ''' <example> Sub Form_Click End Sub </example>
    <CodeAnalysis.SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification:="<Pending>")>
    Public Sub Filter(ByVal reals() As Single)

        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

        ' Initialize imaginary array.
        Dim imaginaries() As Single = Array.Empty(Of Single)()
        ReDim imaginaries(reals.Length - 1)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Set the scale factor
        Dim scaleFactor As Single = CSng(1.0R / reals.Length)

        ' Scale the Spectrum.
        SingleExtensions.Scale(reals, scaleFactor)
        SingleExtensions.Scale(imaginaries, scaleFactor)

        If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

        ' inverse transform
        Me.FourierTransform.Inverse(reals, imaginaries)

    End Sub

#End Region

End Class

