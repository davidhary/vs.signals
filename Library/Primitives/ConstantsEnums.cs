﻿#region " TYPES "

/// <summary> Enumerates the filter types. </summary>
/// <remarks> David, 2020-10-26. </remarks>

namespace isr.Algorithms.Signals
{
    public enum TaperFilterType
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary> An enum constant representing the low pass option. </summary>
        [System.ComponentModel.Description( "Low Pass" )]
        LowPass,

        /// <summary> An enum constant representing the high pass option. </summary>
        [System.ComponentModel.Description( "High Pass" )]
        HighPass,

        /// <summary> An enum constant representing the band pass option. </summary>
        [System.ComponentModel.Description( "Band Pass" )]
        BandPass,

        /// <summary> An enum constant representing the band reject option. </summary>
        [System.ComponentModel.Description( "Band Reject" )]
        BandReject
    }

    /// <summary> Enumerates the FFT Types. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public enum FourierTransformType
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary> An enum constant representing the dft option. </summary>
        [System.ComponentModel.Description( "DFT" )]
        Dft,

        /// <summary> An enum constant representing the sliding option. </summary>
        [System.ComponentModel.Description( "Sliding" )]
        Sliding,

        /// <summary> An enum constant representing the mixed radix option. </summary>
        [System.ComponentModel.Description( "Mixed Radix" )]
        MixedRadix,

        /// <summary> An enum constant representing the wisdom option. </summary>
        [System.ComponentModel.Description( "Wisdom" )]
        Wisdom
    }
}

#endregion

