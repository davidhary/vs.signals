﻿
using System;

namespace isr.Algorithms.Signals
{
    public sealed partial class TaperFilter
    {

        #region " SHARED "

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        private static bool HighPassTaper( int frequency, int transitionBankCount, double[] reals, double[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : HighPassTaper( frequency, 0, transitionBankCount, reals, imaginaries );
        }

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        private static bool HighPassTaper( int frequency, int transitionBankCount, float[] reals, float[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : HighPassTaper( frequency, 0, transitionBankCount, reals, imaginaries );
        }

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="fromIndex">           The first frequency index form which to apply the high
        /// pass taper. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        private static bool HighPassTaper( int frequency, int transitionBankCount, int fromIndex, double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( fromIndex + transitionBankCount / 2 > frequency )
            {
                throw new ArgumentOutOfRangeException( nameof( fromIndex ), fromIndex, "Frequency must exceed the minimum frequency plus half the transition band." );
            }

            int elementCount = reals.Length;
            if ( frequency - transitionBankCount / 2 + transitionBankCount > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency plus half transition band must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;
            if ( fromIndex == 0 )
            {
                reals[fromIndex] = 0d;
                imaginaries[fromIndex] = 0d;
                fromIndex += 1;
            }

            // zero out low frequency amplitudes
            frequencyIndex = fromIndex;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 )
            {
                reals[frequencyIndex] = 0d;
                imaginaries[frequencyIndex] = 0d;
                reals[conjugateFrequencyIndex] = 0d;
                imaginaries[conjugateFrequencyIndex] = 0d;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band from the data
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                reals[frequencyIndex] *= taperFactor;
                imaginaries[frequencyIndex] *= taperFactor;
                reals[conjugateFrequencyIndex] *= taperFactor;
                imaginaries[conjugateFrequencyIndex] *= taperFactor;

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            return true;
        }

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="fromIndex">           The first frequency index form which to apply the high
        /// pass taper. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        private static bool HighPassTaper( int frequency, int transitionBankCount, int fromIndex, float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( fromIndex + transitionBankCount / 2 > frequency )
            {
                throw new ArgumentOutOfRangeException( nameof( fromIndex ), fromIndex, "Frequency must exceed the minimum frequency plus half the transition band." );
            }

            int elementCount = reals.Length;
            if ( frequency - transitionBankCount / 2 + transitionBankCount > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency plus half transition band must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;
            if ( fromIndex == 0 )
            {
                reals[fromIndex] = 0f;
                imaginaries[fromIndex] = 0f;
                fromIndex += 1;
            }

            // zero out low frequency amplitudes
            frequencyIndex = fromIndex;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 )
            {
                reals[frequencyIndex] = 0f;
                imaginaries[frequencyIndex] = 0f;
                reals[conjugateFrequencyIndex] = 0f;
                imaginaries[conjugateFrequencyIndex] = 0f;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band band from the data
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                reals[frequencyIndex] *= ( float ) taperFactor;
                imaginaries[frequencyIndex] *= ( float ) taperFactor;
                reals[conjugateFrequencyIndex] *= ( float ) taperFactor;
                imaginaries[conjugateFrequencyIndex] *= ( float ) taperFactor;

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            return true;
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        public static bool LowPassTaper( int frequency, int transitionBankCount, double[] reals, double[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : LowPassTaper( frequency, transitionBankCount, reals.Length / 2, reals, imaginaries );
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        public static bool LowPassTaper( int frequency, int transitionBankCount, float[] reals, float[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : LowPassTaper( frequency, transitionBankCount, reals.Length / 2, reals, imaginaries );
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="toIndex">             The last frequency to apply the low pass filter.  This
        /// allows using this method to apply the low past filter in
        /// case of a band reject filter where the spectrum can be
        /// zeroed only within a specific range. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        public static bool LowPassTaper( int frequency, int transitionBankCount, int toIndex, double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( frequency - transitionBankCount / 2 <= 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency minus half the transition band must exceed zero." );
            }

            if ( frequency - transitionBankCount / 2 + transitionBankCount > toIndex )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band." );
            }

            int elementCount = reals.Length;
            if ( toIndex > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band band from the data
            frequencyIndex = frequency - transitionBankCount / 2;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                reals[frequencyIndex] *= taperFactor;
                imaginaries[frequencyIndex] *= taperFactor;
                reals[conjugateFrequencyIndex] *= taperFactor;
                imaginaries[conjugateFrequencyIndex] *= taperFactor;

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            while ( frequencyIndex < toIndex )
            {
                reals[frequencyIndex] = 0d;
                imaginaries[frequencyIndex] = 0d;
                reals[conjugateFrequencyIndex] = 0d;
                imaginaries[conjugateFrequencyIndex] = 0d;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            return true;
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="toIndex">             The last frequency to apply the low pass filter.  This
        /// allows using this method to apply the low past filter in
        /// case of a band reject filter where the spectrum can be
        /// zeroed only within a specific range. </param>
        /// <param name="reals">               Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">         Holds the imaginary values of the spectrum. </param>
        /// <returns> True if filtered. </returns>
        public static bool LowPassTaper( int frequency, int transitionBankCount, int toIndex, float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( frequency - transitionBankCount / 2 <= 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency minus half the transition band must exceed zero." );
            }

            if ( frequency - transitionBankCount / 2 + transitionBankCount > toIndex )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band." );
            }

            int elementCount = reals.Length;
            if ( toIndex > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band band from the data
            frequencyIndex = frequency - transitionBankCount / 2;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                reals[frequencyIndex] *= ( float ) taperFactor;
                imaginaries[frequencyIndex] *= ( float ) taperFactor;
                reals[conjugateFrequencyIndex] *= ( float ) taperFactor;
                imaginaries[conjugateFrequencyIndex] *= ( float ) taperFactor;

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            while ( frequencyIndex < toIndex )
            {
                reals[frequencyIndex] = 0f;
                imaginaries[frequencyIndex] = 0f;
                reals[conjugateFrequencyIndex] = 0f;
                imaginaries[conjugateFrequencyIndex] = 0f;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            return true;
        }

        #endregion

        #region " METHODS "

        /// <summary> Taper the frequency band for filtering the signal in the frequency domain. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="samplingRate"> The spectrum sampling rate. </param>
        /// <param name="reals">        Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">  Holds the imaginary values of the spectrum. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Taper( double samplingRate, double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            this.SamplingRate = samplingRate;
            this.TimeSeriesLength = reals.Length;
            string details = string.Empty;
            if ( !this.ValidateFilterFrequencies( ref details ) )
            {
                throw new InvalidOperationException( details );
            }

            switch ( this.FilterType )
            {
                case TaperFilterType.BandPass:
                    {

                        // high pass at the low frequency
                        _ = HighPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );

                        // low pass at the high frequency.
                        _ = LowPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.BandReject:
                    {
                        double midFrequency = (this.LowFrequency + this.HighFrequency) / 2d;

                        // low pass at the low frequency but up to the mid band
                        _ = LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ), reals, imaginaries );

                        // high pass at the high frequency but only from the mid band
                        _ = HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.HighPass:
                    {
                        _ = HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.LowPass:
                    {
                        _ = LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.None:
                    {
                        return true;
                    }
            }

            return default;
        }

        /// <summary> Taper the frequency band for filtering the signal in the frequency domain. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="samplingRate"> The spectrum sampling rate. </param>
        /// <param name="reals">        Holds the real values of the spectrum. </param>
        /// <param name="imaginaries">  Holds the imaginary values of the spectrum. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Taper( double samplingRate, float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            this.SamplingRate = samplingRate;
            this.TimeSeriesLength = reals.Length;
            switch ( this.FilterType )
            {
                case TaperFilterType.BandPass:
                    {

                        // high pass at the low frequency
                        _ = HighPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );

                        // low pass at the high frequency.
                        _ = LowPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.BandReject:
                    {
                        double midFrequency = (this.LowFrequency + this.HighFrequency) / 2d;

                        // low pass at the low frequency but up to the mid band
                        _ = LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ), reals, imaginaries );

                        // high pass at the high frequency but only from the mid band
                        _ = HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.HighPass:
                    {
                        _ = HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.LowPass:
                    {
                        _ = LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), reals, imaginaries );
                        break;
                    }

                case TaperFilterType.None:
                    {
                        return true;
                    }
            }

            return default;
        }

        #endregion

    }
}