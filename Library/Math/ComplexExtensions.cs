﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Provides simple functions of complex arguments. </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para></remarks>
    public static class ComplexExtensions
    {

        /// <summary> Computes the phase of a complex number. </summary>
        /// <remarks>
        /// <para>The phase of a complex number is the angle between the line joining it to the origin
        /// and the real axis of the complex plane.</para>
        /// <para>The phase of complex numbers in the upper complex plane lies between 0 and &#x3C0;. The
        /// phase of complex numbers in the lower complex plane lies between 0 and -&#x3C0;. The phase of
        /// a real number is zero.</para>
        /// </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The complex value. </returns>
        public static double Arg( this Complex value )
        {
            // returns 0 to PI in the upper complex plane (Imaginary part>=0),
            // 0 to -PI in the lower complex plane (Imaginary part<0)
            return Math.Atan2( value.Imaginary, value.Real );
        }

        /// <summary> Computes the absolute value of a complex number. </summary>
        /// <remarks>
        /// <para>The absolute value of a complex number is the distance of the number from the origin in
        /// the complex plane. This is a compatible generalization of the definition of the absolute
        /// value of a real number.</para>
        /// </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of |z|. </returns>
        /// <seealso cref="System.Math.Abs"/>
        public static double Abs( this Complex value )
        {
            return Hypotenuse( value.Real, value.Imaginary );
        }

        /// <summary> Gets the complex conjugate of the complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The complex conjugate of the complex number. </returns>
        public static Complex Conjugate( this Complex value )
        {
            return new Complex( value.Real, -value.Imaginary );
        }

        /// <summary> Computes the cosine of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of Cos(value). </returns>
        public static Complex Cos( this Complex value )
        {
            double p = Math.Exp( value.Imaginary );
            double q = 1d / p;
            double sinwH = (p - q) / 2.0d;
            double cosineH = (p + q) / 2.0d;
            return new Complex( Math.Cos( value.Real ) * cosineH, -Math.Sin( value.Real ) * sinwH );
        }

        /// <summary> Computes the hyperbolic cosine of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of CosH(value). </returns>
        public static Complex Cosh( this Complex value )
        {
            return new Complex( -value.Imaginary, value.Real ).Cos();
        }

        /// <summary> Computes e raised to the power of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of e<sup>value</sup>. </returns>
        public static Complex Exp( this Complex value )
        {
            double m = Math.Exp( value.Real );
            return new Complex( m * AdvancedMath.Cos( value.Imaginary, 0.0d ), m * AdvancedMath.Sin( value.Imaginary, 0.0d ) );
        }

        /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns>
        /// The length of the hypotenuse, SQRT(real<sup>2</sup> + imaginary<sup>2</sup>) =
        /// |value|.
        /// </returns>
        public static double Hypotenuse( this Complex value )
        {
            return Hypotenuse( value.Real, value.Imaginary );
        }

        /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <param name="value">      The length of one side. </param>
        /// <param name="orthogonal"> The length of orthogonal side. </param>
        /// <returns> The length of the hypotenuse, sqrt(x<sup>2</sup> + y<sup>2</sup>). </returns>
        public static double Hypotenuse( double value, double orthogonal )
        {
            if ( value == 0.0d && orthogonal == 0.0d )
            {
                return 0.0d;
            }
            else
            {
                double ax = Math.Abs( value );
                double ay = Math.Abs( orthogonal );
                if ( ax > ay )
                {
                    double r = orthogonal / value;
                    return ax * Math.Sqrt( 1.0d + r * r );
                }
                else
                {
                    double r = value / orthogonal;
                    return ay * Math.Sqrt( 1.0d + r * r );
                }
            }
        }

        /// <summary> Gets the unit imaginary number I. </summary>
        /// <value> The i. </value>
        public static Complex I => new Complex( 0.0d, 1.0d );

        /// <summary> Computes the natural logarithm of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of natural log of value. </returns>
        public static Complex Log( this Complex value )
        {
            return new Complex( Math.Log( value.Abs() ), value.Arg() );
        }

        /// <summary> Raises a complex number to an arbitrary real power. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <param name="power"> The power. </param>
        /// <returns> The value of value<sup>power</sup>. </returns>
        public static Complex Pow( this Complex value, double power )
        {
            double m = Math.Pow( value.Abs(), power );
            double t = value.Arg() * power;
            return new Complex( m * Math.Cos( t ), m * Math.Sin( t ) );
        }

        /// <summary> Raises a real number to an arbitrary complex power. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="value"> The real base, which must be non-negative. </param>
        /// <param name="power"> The complex exponent. </param>
        /// <returns> The value of value<sup>power</sup>. </returns>
        public static Complex Pow( this double value, Complex power )
        {
            if ( value < 0.0d )
                throw new ArgumentOutOfRangeException( nameof( value ) );
            if ( power == 0.0d )
                return 1.0d;
            if ( value == 0.0d )
                return 0.0d;
            double m = Math.Pow( value, power.Real );
            double t = Math.Log( value ) * power.Imaginary;
            return new Complex( m * Math.Cos( t ), m * Math.Sin( t ) );
        }

        /// <summary> Raises a complex number to an integer power. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <param name="power"> The power. </param>
        /// <returns> The value of value<sup>power</sup>. </returns>
        public static Complex Pow( this Complex value, int power )
        {

            // this is a straight-up copy of MoreMath.Pow with x -> z, double -> Complex

            if ( power < 0 )
            {
                return 1.0d / value.Pow( -power );
            }

            switch ( power )
            {
                case 0:
                    {
                        // we follow convention that 0^0 = 1
                        return 1.0d;
                    }

                case 1:
                    {
                        return value;
                    }

                case 2:
                    {
                        // 1 multiply
                        return value * value;
                    }

                case 3:
                    {
                        // 2 multiplies
                        return value * value * value;
                    }

                case 4:
                    {
                        // 2 multiplies
                        var z2 = value * value;
                        return z2 * z2;
                    }

                case 5:
                    {
                        // 3 multiplies
                        var z2 = value * value;
                        return z2 * z2 * value;
                    }

                case 6:
                    {
                        // 3 multiplies
                        var z2 = value * value;
                        return z2 * z2 * z2;
                    }

                case 7:
                    {
                        // 4 multiplies
                        var z3 = value * value * value;
                        return z3 * z3 * value;
                    }

                case 8:
                    {
                        // 3 multiplies
                        var z2 = value * value;
                        var z4 = z2 * z2;
                        return z4 * z4;
                    }

                case 9:
                    {
                        // 4 multiplies
                        var z3 = value * value * value;
                        return z3 * z3 * z3;
                    }

                case 10:
                    {
                        // 4 multiplies
                        var z2 = value * value;
                        var z4 = z2 * z2;
                        return z4 * z4 * z2;
                    }

                default:
                    {
                        return value.Pow( ( double ) power );
                    }
            }
        }

        /// <summary> Computes the sine of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of sin(value). </returns>
        public static Complex Sin( this Complex value )
        {
            double p = Math.Exp( value.Imaginary );
            double q = 1d / p;
            double sineH = (p - q) / 2.0d;
            double cosineH = (p + q) / 2.0d;
            return new Complex( Math.Sin( value.Real ) * cosineH, Math.Cos( value.Real ) * sineH );
        }

        /// <summary> Computes the hyperbolic sine of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of SinH(value). </returns>
        public static Complex Sinh( this Complex value )
        {
            // SinH(z) = -i sin(i z)
            var sineH = new Complex( -value.Imaginary, value.Real ).Sin();
            return new Complex( sineH.Imaginary, -sineH.Real );
        }

        /// <summary> Computes the square root of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The square root of the argument. </returns>
        public static Complex Sqrt( this Complex value )
        {
            return value.Imaginary == 0d ? Math.Sqrt( value.Real ) : value.Pow( 0.5d );
        }

        /// <summary> Gets the complex value swapping imaginary and real. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The complex value swapping imaginary and real. </returns>
        public static Complex Swap( this Complex value )
        {
            return new Complex( value.Imaginary, value.Real );
        }

        /// <summary> Computes the tangent of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of tan(value). </returns>
        public static Complex Tan( this Complex value )
        {
            // tan z = [sin(2x) + I SinH(2y)]/[Cos(2x) + I CosH(2y)]
            double x2 = 2.0d * value.Real;
            double y2 = 2.0d * value.Imaginary;
            double p = Math.Exp( y2 );
            double q = 1d / p;
            double cosineH = (p + q) / 2.0d;
            if ( Math.Abs( value.Imaginary ) < 4.0d )
            {
                double sineH = (p - q) / 2.0d;
                double d = Math.Cos( x2 ) + cosineH;
                return new Complex( Math.Sin( x2 ) / d, sineH / d );
            }
            else
            {
                // when Imaginary(z) gets too large, SinH and CosH individually blow up
                // but ratio is still ~1, so rearrange to use TanH instead
                double f = 1.0d + Math.Cos( x2 ) / cosineH;
                return new Complex( Math.Sin( x2 ) / cosineH / f, Math.Tanh( y2 ) / f );
            }
        }

        /// <summary> Computes the hyperbolic tangent of a complex number. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The argument. </param>
        /// <returns> The value of TanH(value). </returns>
        public static Complex Tanh( this Complex value )
        {
            return value.Sinh() / value.Cosh();
        }
    }
}