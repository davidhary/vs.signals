using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Algorithms.Signals.Forms.ExceptionExtensions;

namespace isr.Algorithms.Signals.Forms
{

    /// <summary> Form for viewing the spectrum panels. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-06 </para>
    /// </remarks>
    public class MyForm : Core.Forma.ConsoleForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public MyForm() : base()
        {
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        internal static bool Instantiated => My.MyProject.Forms.m_MyForm is object && !My.MyProject.Forms.MyForm.IsDisposed;

        /// <summary> Gets or sets the model view base. </summary>
        /// <value> The model view base. </value>
        private Core.Forma.ModelViewTalkerBase ModelViewBase { get; set; } = null;

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.ModelViewBase is object )
            {
                this.ModelViewBase.Dispose();
                this.ModelViewBase = null;
            }

            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding spectrum view";
                switch ( CommandLineInfo.UserInterface )
                {
                    case UserInterface.Spectrum:
                        {
                            this.ModelViewBase = new SpectrumView();
                            break;
                        }

                    case UserInterface.Wisdom:
                        {
                            this.ModelViewBase = new WisdomSpectrumView();
                            break;
                        }

                    default:
                        {
                            this.ModelViewBase = new SpectrumView();
                            break;
                        }
                }

                this.AddTalkerControl( $"{CommandLineInfo.UserInterface}", this.ModelViewBase, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyProject.Application.Logger );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets or sets the await selection task enabled. </summary>
        /// <value> The await selection task enabled. </value>
        protected bool AwaitSelectionTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ModelViewBase.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.ModelViewBase is object )
                    this.ModelViewBase.Cursor = Cursors.Default;
            }
        }

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
