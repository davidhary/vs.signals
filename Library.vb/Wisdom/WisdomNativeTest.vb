Imports System.Runtime.InteropServices

Imports isr.Algorithms.Signals.Wisdom

''' <summary> Tests the Wisdom FFT using Safe Native Methods. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public Class WisdomNativeTest

    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The disposed. </value>
    Protected Property Disposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          False if this method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    ' Free managed resources when explicitly called
                End If
                ' Free shared unmanaged resources
                If Me._Pin <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Free(Me._Pin)
                If Me._Pout <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Free(Me._Pout)
                If Me._Fplan1 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan1)
                If Me._Fplan2 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan2)
                If Me._Fplan3 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan3)
                Me._Hin.Free()
                Me._Hout.Free()
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.Disposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

    ''' <summary>
    ''' managed arrays
    ''' </summary>
    Private _Fin, _Fout As Single()

    ''' <summary>
    ''' handles to managed arrays, keeps them pinned in memory.
    ''' </summary>
    Private _Hin, _Hout As GCHandle

    ''' <summary> Initializes FFTW and all arrays. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="logicalSize"> Logical size (number of elements) of the transform. </param>
    Public Sub InitFftw(ByVal logicalSize As Integer)

        ' create two unmanaged arrays, properly aligned
        Me._Pin = FftwF.SafeNativeMethods.Malloc(logicalSize * 8)
        Me._Pout = FftwF.SafeNativeMethods.Malloc(logicalSize * 8)

        ' create two managed arrays, possibly misaligned
        ' n*2 because we are dealing with complex numbers
        Me._Fin = New Single(logicalSize * 2 - 1) {}
        Me._Fout = New Single(logicalSize * 2 - 1) {}

        ' get handles and pin arrays so the GC doesn't move them
        Me._Hin = GCHandle.Alloc(Me._Fin, GCHandleType.Pinned)
        Me._Hout = GCHandle.Alloc(Me._Fout, GCHandleType.Pinned)

        ' fill our arrays with a sawtooth signal
        For i As Integer = 0 To logicalSize * 2 - 1
            Me._Fin(i) = i Mod 50
        Next i
        For i As Integer = 0 To logicalSize * 2 - 1
            Me._Fout(i) = i Mod 50
        Next i

        ' copy managed arrays to unmanaged arrays
        Marshal.Copy(Me._Fin, 0, Me._Pin, logicalSize * 2)
        Marshal.Copy(Me._Fout, 0, Me._Pout, logicalSize * 2)

        ' create a few test transforms
        Me._Fplan1 = FftwF.SafeNativeMethods.Dft_1d(logicalSize, Me._Pin, Me._Pout, TransformDirection.Forward, CUInt(PlannerOptions.Estimate))
        Me._Fplan2 = FftwF.SafeNativeMethods.Dft_1d(logicalSize, Me._Hin.AddrOfPinnedObject(), Me._Hout.AddrOfPinnedObject(), TransformDirection.Forward, CUInt(PlannerOptions.Estimate))
        Me._Fplan3 = FftwF.SafeNativeMethods.Dft_1d(logicalSize, Me._Hout.AddrOfPinnedObject(), Me._Pin, TransformDirection.Backward, CUInt(PlannerOptions.Measure))

    End Sub

    ''' <summary> Tests all plans. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub TestAll()
        TestPlan(Me._Fplan1)
        TestPlan(Me._Fplan2)
        TestPlan(Me._Fplan3)
    End Sub

    ''' <summary> Tests a single plan, displaying results. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="plan"> Pointer to plan to test. </param>
    Public Shared Sub TestPlan(ByVal plan As IntPtr)

        Dim stopwatch As New System.Diagnostics.Stopwatch
        stopwatch.Start()
        FftwF.SafeNativeMethods.Execute(plan)
        Console.WriteLine("Time: {0} ms", stopwatch.Elapsed.TotalMilliseconds)

        ' a: adds, b: m.u.l.s, c: f.m.a.s
        Dim a As Double = 0, b As Double = 0, c As Double = 0
        FftwF.SafeNativeMethods.Flops(plan, a, b, c)
        Console.WriteLine("Approx. flops: {0}", (a + b + 2 * c))

    End Sub

    ''' <summary> Releases all memory used by FFTW/C#. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub FreeFftw()
        ' it is essential that you call these after finishing
        ' may want to put the initializers in the constructor
        ' and these in the destructor
        If Me._Pin <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Free(Me._Pin)
        If Me._Pout <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Free(Me._Pout)
        If Me._Fplan1 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan1)
        If Me._Fplan2 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan2)
        If Me._Fplan3 <> CType(0, IntPtr) Then FftwF.SafeNativeMethods.Destroy_plan(Me._Fplan3)
        Me._Hin.Free()
        Me._Hout.Free()
    End Sub

End Class

