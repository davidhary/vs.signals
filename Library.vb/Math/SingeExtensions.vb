Imports System.Runtime.CompilerServices

''' <summary> Single value extensions. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Module SingleExtensions

#Region " SINGLE ARRAYS "

    ''' <summary> Adds a value to the Single array elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    <Extension()>
    Public Sub Add(ByVal values() As Single, ByVal value As Single)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) += value
            Next
        End If

    End Sub

    ''' <summary> Copy values between arrays. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fromValues"> From values. </param>
    ''' <param name="toValues">   To values. </param>
    <Extension()>
    Public Sub Copy(ByVal fromValues() As Single, ByVal toValues() As Single)
        If fromValues Is Nothing Then Throw New ArgumentNullException(NameOf(fromValues))
        If toValues Is Nothing Then Throw New ArgumentNullException(NameOf(toValues))
        If fromValues.Length > 0 AndAlso toValues.Length > 0 Then
            For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                        toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                               toValues.GetUpperBound(0))
                toValues(i) = fromValues(i)
            Next
        End If
    End Sub

    ''' <summary> Copy values between arrays. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fromValues"> From values. </param>
    ''' <param name="toValues">   To values. </param>
    <Extension()>
    Public Sub Copy(ByVal fromValues() As Single, ByVal toValues() As Double)

        If fromValues Is Nothing Then Throw New ArgumentNullException(NameOf(fromValues))
        If toValues Is Nothing Then Throw New ArgumentNullException(NameOf(toValues))

        If fromValues.Length > 0 AndAlso toValues.Length > 0 Then
            For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                        toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                               toValues.GetUpperBound(0))
                toValues(i) = CSng(fromValues(i))
            Next
        End If

    End Sub

    ''' <summary> Returns the index of the first maximum of given array. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The index of the first maximum of given array. </returns>
    <Extension()>
    Public Function IndexFirstMaximum(ByVal values() As Single) As Integer

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim maxIndex As Integer = values.GetLowerBound(0)
            Dim max As Single = values(maxIndex)
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                Dim abs As Single = values(i)
                If max < abs Then
                    max = abs
                    maxIndex = i
                End If
            Next
            Return maxIndex
        Else
            Return -1
        End If

    End Function

    ''' <summary> Calculates the mean of the array elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The mean of the array elements. </returns>
    <Extension()>
    Public Function Mean(ByVal values() As Single) As Double

        Return If(values IsNot Nothing AndAlso values.Length > 0, Sum(values) / values.Length, 0)

    End Function

    ''' <summary> Removes the mean from the values array. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    <Extension()>
    Public Sub RemoveMean(ByVal values() As Single)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Add(values, CSng(-Mean(values)))
        End If

    End Sub

    ''' <summary> Multiplies the array elements by scalar elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values">  The values. </param>
    ''' <param name="scalars"> The scalars. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Single, ByVal scalars() As Single)
        If values IsNot Nothing AndAlso values.Length > 0 AndAlso
            scalars IsNot Nothing AndAlso scalars.Length > 0 Then

            If values.GetLowerBound(0) <> scalars.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(values), values, "The arrays of values and scalars must have the same lower bound")
            ElseIf values.GetUpperBound(0) <> scalars.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(values), values, "The arrays of values and scalars must have the same upper bound")
            Else
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    values(i) *= scalars(i)
                Next
            End If
        End If
    End Sub

    ''' <summary> Multiplies the array elements by scalar elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values">  The values. </param>
    ''' <param name="scalars"> The scalars. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Single, ByVal scalars() As Double)
        If values IsNot Nothing AndAlso values.Length > 0 AndAlso
            scalars IsNot Nothing AndAlso scalars.Length > 0 Then

            If values.GetLowerBound(0) <> scalars.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(values), values, "The arrays of values and scalars must have the same lower bound")
            ElseIf values.GetUpperBound(0) <> scalars.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(values), values, "The arrays of values and scalars must have the same upper bound")
            Else
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    values(i) *= CSng(scalars(i))
                Next
            End If
        End If
    End Sub

    ''' <summary> Multiplies the array elements by a scalar. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <param name="scalar"> The scalar. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Single, ByVal scalar As Single)
        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) *= scalar
            Next
        End If
    End Sub

    ''' <summary> Swaps values between two array locations. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="data">       The data. </param>
    ''' <param name="leftIndex">  Zero-based index of the left. </param>
    ''' <param name="rightIndex"> Zero-based index of the right. </param>
    <Extension()>
    Public Sub Swap(ByVal data() As Single, ByVal leftIndex As Integer, ByVal rightIndex As Integer)
        If Not data Is Nothing Then
            Dim cache As Single = data(leftIndex)
            data(leftIndex) = data(rightIndex)
            data(rightIndex) = cache
        End If
    End Sub

    ''' <summary> Calculates the sum of the array elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the array elements. </returns>
    <Extension()>
    Public Function Sum(ByVal values() As Single) As Double
        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Single = 0
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                cache += values(i)
            Next
            Return cache
        Else
            Return 0
        End If
    End Function

    ''' <summary> Calculates the sum of the array elements squared. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the array elements squared. </returns>
    <Extension()>
    Public Function SumSquares(ByVal values() As Single) As Double
        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Single = 0
            Dim value As Single
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                value = values(i)
                cache += value * value
            Next
            Return cache
        Else
            Return 0
        End If
    End Function

    ''' <summary> Calculates the root mean square (RMS) value of the array elements. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The root mean square (RMS) value of the array elements. </returns>
    <Extension()>
    Public Function RootMeanSquare(ByVal values() As Single) As Double
        Return If(values IsNot Nothing AndAlso values.Length > 0, Math.Sqrt(SumSquares(values) / values.Length), 0)
    End Function

    ''' <summary> Returns the magnitude of a complex array. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    ''' <returns> A Single() </returns>
    <Extension>
    Public Function Magnitudes(ByVal reals() As Single, ByVal imaginaries() As Single) As Single()

        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
        End If

        Dim result(imaginaries.Length - 1) As Single

        ' calculate the magnitude
        For i As Integer = 0 To reals.Length - 1

            Dim realValue As Double = reals(i)
            Dim imaginaryValue As Double = imaginaries(i)
            result(i) = CSng(Math.Sqrt(realValue * realValue + imaginaryValue * imaginaryValue))

        Next i

        Return result

    End Function

#End Region


End Module
