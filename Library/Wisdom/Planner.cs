using System;
using System.Diagnostics.CodeAnalysis;

namespace isr.Algorithms.Signals.Wisdom
{

    /// <summary> Creates FFTW plans. </summary>
    /// <remarks>
    /// The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    /// fastest way to compute the transform on your machine.
    /// <para>
    /// The planner produces a data structure called a plan that contains this information.
    /// Subsequently, the plan is executed (<see cref="Plan.Execute">execute</see> or
    /// <see cref="Plan.Execute">execute</see>) to transform the array of input data as dictated by
    /// the plan.
    /// Once the plan has been created, it can be executed many times for transforms on the specified
    /// in/out arrays.
    /// </para>
    /// <para>
    /// If you want to transform a different array of the same size, you can create a new plan with
    /// and FFTW automatically reuses the information from the previous plan, if possible.
    /// Alternatively, with the “guru” interface you can apply a given plan to a different array, if
    /// you are careful. See Chapter 4 [FFTW Reference], page 21. When you are done with the plan,
    /// you deallocate it by calling <see cref="Plan.Free()">free</see>
    /// or <see cref="Plan.Free">free</see> methods.
    /// </para>
    /// <para>
    /// The DFT results are stored in-order in the output array, with the zero-frequency (DC)
    /// component in the first element (index 0). If using different input and output pointers, the
    /// transform is out-of-place and the input array is not modified. Otherwise, the input array is
    /// overwritten with the transform.
    /// </para>
    /// <para>
    /// FFTW computes an unnormalized DFT. Thus, computing a forward followed by a backward transform
    /// (or vice versa)
    /// results in the original array scaled by N. For the definition of the DFT, 'What FFTW Really
    /// Computes', see Section 4.7 page 39.
    /// </para>
    /// <para>
    /// One-Dimensional DFT of real-valued data.
    /// </para>
    /// <para>
    /// In many practical applications, the input data are purely real numbers, in which case the DFT
    /// output satisfies the “Hermitian” redundancy: out[i] is the conjugate of out[ni]. It is
    /// possible to take advantage of these circumstances in order to achieve roughly a factor of two
    /// improvement in both speed and memory usage. In exchange for these speed and space advantages,
    /// the user sacrifices some of the simplicity of FFTW’s complex transforms. First of all, the
    /// input and output arrays are of different sizes and types: the input is n real numbers, while
    /// the output is n/2+1 complex numbers (the non-redundant outputs); this also requires slight
    /// “padding” of the input array for in-place transforms. Second, the inverse transform (complex
    /// to real) has the side-effect of destroying its input array, by default. Neither of these
    /// inconveniences should pose a serious problem for users, but it is important to be aware of
    /// them.
    /// </para>
    /// </remarks>
    public sealed class Planner
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Prevents construction of this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private Planner()
        {
        }

        #endregion

        #region " SINGLE PRECISION  COMPLEX <--> COMPLEX PLANS "

        // <SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification:="Match safe native methods."), _
        // SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Justification:="Match safe native methods."), _
        // SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification:="Match safe native methods.")> 

        /// <summary> Creates a plan for a 1-dimensional complex-to-complex DFT. </summary>
        /// <remarks>
        /// having equal <paramref name="input">input</paramref> and
        /// <paramref name="output">output</paramref> pointers indicates a transform in place.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">      The logical size (number of elements) of the transform. The size can
        /// be any positive integer, but sizes that are products of small factors
        /// are transformed most efficiently. Prime sizes still use the direct
        /// DFT algorithm of O(n log n). </param>
        /// <param name="input">     A <see cref="ComplexArrayF">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size, ComplexArrayF input, ComplexArrayF output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_1d( size, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">     The logical size (number of elements) of the transform along the
        /// first dimension. </param>
        /// <param name="size2">     The logical size (number of elements) of the transform along the
        /// second dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayF">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, ComplexArrayF input, ComplexArrayF output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_2d( size1, size2, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">     The logical size (number of elements) of the transform along the
        /// first dimension. </param>
        /// <param name="size2">     The logical size (number of elements) of the transform along the
        /// second dimension. </param>
        /// <param name="size3">     The logical size (number of elements) of the transform along the
        /// third dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayF">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, int size3, ComplexArrayF input, ComplexArrayF output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_3d( size1, size2, size3, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">      Number of dimensions. </param>
        /// <param name="size">      Array containing the logical size (number of elements) along each
        /// dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayF">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int rank, int[] size, ComplexArrayF input, ComplexArrayF output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft( rank, size, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        #endregion

        #region " SINGLE PRECISION  REAL --> COMPLEX PLANS "

        /// <summary>
        /// Creates a plan for a 1-dimensional forward real-to-complex DFT
        /// <para>
        /// An alternative interface for one-dimensional real-to-complex and complex-to-real DFTs can be
        /// found in real-to-real interface (see Section 2.5.1 [The Halfcomplex-format DFT], page 10),
        /// with 'halfcomplex'-format output that is the same size (and type) as the input array. That
        /// interface, although it is not very useful for multi-dimensional transforms, may sometimes
        /// yield better performance.
        /// </para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of REAL (input) elements in the transform. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayF">complex array</see> receiving the FFT. It
        /// includes <paramref name="size">N</paramref>/2+1 complex numbers (the
        /// non-redundant outputs). This also requires slight “padding” of the
        /// <paramref name="input"/> array if the transform is computed in place.
        /// </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size, RealArrayF input, ComplexArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_r2c_1d( size, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (input) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (input) elements in the transform along the second
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, RealArrayF input, ComplexArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_r2c_2d( size1, size2, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (input) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (input) elements in the transform along the second
        /// dimension. </param>
        /// <param name="size3">   Number of REAL (input) elements in the transform along the third
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, int size3, RealArrayF input, ComplexArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_r2c_3d( size1, size2, size3, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of REAL (input) elements along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayF">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int rank, int[] size, RealArrayF input, ComplexArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_r2c( rank, size, input.Handle, output.Handle, ( uint ) options ) );
        }

        #endregion

        #region " SINGLE PRECISION  COMPLEX --> REAL PLANS "

        /// <summary>
        /// Creates a plan for a 1-dimensional inverse complex-to-real DFT. Destroys its input array even
        /// for out-of-place transforms unless using the
        /// <see cref="PlannerOptions.PreserveInput">preservve input</see>
        /// <paramref name="options">flag</paramref>
        /// with unfortunately some sacrifice in performance. This flag is also not currently supported
        /// for multi-dimensional real DFTs.
        /// <para>
        /// An alternative interface for one-dimensional real-to-complex and complex-to-real DFTs can be
        /// found in real-to-real interface (see Section 2.5.1 [The Halfcomplex-format DFT], page 10),
        /// with 'halfcomplex'-format output that is the same size (and type) as the input array. That
        /// interface, although it is not very useful for multi-dimensional transforms, may sometimes
        /// yield better performance.
        /// </para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of REAL (output) elements in the transform. </param>
        /// <param name="input">   A <see cref="ComplexArrayF">complex array</see> holding the input
        /// DFT. It includes <paramref name="size">N</paramref>/2+1 complex
        /// numbers (the non-redundant outputs). This also requires slight
        /// “padding” of the <paramref name="output"/> array if the transform is
        /// computed in place. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size, ComplexArrayF input, RealArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_c2r_1d( size, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (output) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (output) elements in the transform along the second
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayF">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, ComplexArrayF input, RealArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_c2r_2d( size1, size2, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (output) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (output) elements in the transform along the second
        /// dimension. </param>
        /// <param name="size3">   Number of REAL (output) elements in the transform along the third
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayF">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, int size3, ComplexArrayF input, RealArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_c2r_3d( size1, size2, size3, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of REAL (output) elements along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayF">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int rank, int[] size, ComplexArrayF input, RealArrayF output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.Dft_c2r( rank, size, input.Handle, output.Handle, ( uint ) options ) );
        }

        #endregion

        #region " SINGLE PRECISION  REAL <--> REAL PLANS "

        /// <summary> Creates a plan for a 1-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of elements in the transform. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind">    The kind of real-to-real transform to compute. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size, RealArrayF input, RealArrayF output, TransformKind kind, PlannerOptions options )
        {
            return input is null
                ? throw new ArgumentNullException( nameof( input ) )
                : output is null
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.R2r_1d( size, input.Handle, output.Handle, kind, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of elements in the transform along the first dimension. </param>
        /// <param name="size2">   Number of elements in the transform along the second dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind1">   The kind of real-to-real transform to compute along the first
        /// dimension. </param>
        /// <param name="kind2">   The kind of real-to-real transform to compute along the second
        /// dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, RealArrayF input, RealArrayF output, TransformKind kind1, TransformKind kind2, PlannerOptions options )
        {
            return input is null
                ? throw new ArgumentNullException( nameof( input ) )
                : output is null
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.R2r_2d( size1, size2, input.Handle, output.Handle, kind1, kind2, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of elements in the transform along the first dimension. </param>
        /// <param name="size2">   Number of elements in the transform along the second dimension. </param>
        /// <param name="size3">   Number of elements in the transform along the third dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind1">   The kind of real-to-real transform to compute along the first
        /// dimension. </param>
        /// <param name="kind2">   The kind of real-to-real transform to compute along the second
        /// dimension. </param>
        /// <param name="kind3">   The kind of real-to-real transform to compute along the third
        /// dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int size1, int size2, int size3, RealArrayF input, RealArrayF output, TransformKind kind1, TransformKind kind2, TransformKind kind3, PlannerOptions options )
        {
            return input is null
                ? throw new ArgumentNullException( nameof( input ) )
                : output is null
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.R2r_3d( size1, size2, size3, input.Handle, output.Handle, kind1, kind2, kind3, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of elements in the transform along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayF">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayF">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind">    An array containing the kind of real-to-real transform to compute
        /// along each dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanF. </returns>
        public static PlanF Create( int rank, int[] size, RealArrayF input, RealArrayF output, TransformKind[] kind, PlannerOptions options )
        {
            return input is null
                ? throw new ArgumentNullException( nameof( input ) )
                : output is null
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanF( FftwF.SafeNativeMethods.R2r( rank, size, input.Handle, output.Handle, kind, ( uint ) options ) );
        }

        #endregion

        #region " DOUBLE PRECISION COMPLEX <--> COMPLEX PLANS "

        /// <summary> Creates a plan for a 1-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">      The logical size (number of elements) of the transform. </param>
        /// <param name="input">     A <see cref="ComplexArrayR">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size, ComplexArrayR input, ComplexArrayR output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_1d( size, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">     The logical size (number of elements) of the transform along the
        /// first dimension. </param>
        /// <param name="size2">     The logical size (number of elements) of the transform along the
        /// second dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayR">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, ComplexArrayR input, ComplexArrayR output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_2d( size1, size2, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">     The logical size (number of elements) of the transform along the
        /// first dimension. </param>
        /// <param name="size2">     The logical size (number of elements) of the transform along the
        /// second dimension. </param>
        /// <param name="size3">     The logical size (number of elements) of the transform along the
        /// third dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayR">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, int size3, ComplexArrayR input, ComplexArrayR output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_3d( size1, size2, size3, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional complex-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">      Number of dimensions. </param>
        /// <param name="size">      Array containing the logical size (number of elements) along each
        /// dimension. </param>
        /// <param name="input">     A <see cref="ComplexArrayR">complex array</see> holding the input
        /// elements. </param>
        /// <param name="output">    A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="direction"> Specifies the direction of the transform. </param>
        /// <param name="options">   Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int rank, int[] size, ComplexArrayR input, ComplexArrayR output, TransformDirection direction, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft( rank, size, input.Handle, output.Handle, ( int ) direction, ( uint ) options ) );
        }

        #endregion

        #region " DOUBLE PRECISION REAL --> COMPLEX PLANS "

        /// <summary>
        /// Creates a plan for a 1-dimensional forward real-to-complex DFT
        /// <para>
        /// An alternative interface for one-dimensional real-to-complex and complex-to-real DFTs can be
        /// found in real-to-real interface (see Section 2.5.1 [The Halfcomplex-format DFT], page 10),
        /// with 'halfcomplex'-format output that is the same size (and type) as the input array. That
        /// interface, although it is not very useful for multi-dimensional transforms, may sometimes
        /// yield better performance.
        /// </para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of REAL (input) elements in the transform. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayR">complex array</see> receiving the FFT. It
        /// includes <paramref name="size">N</paramref>/2+1 complex numbers (the
        /// non-redundant outputs). This also requires slight “padding” of the
        /// <paramref name="input"/> array if the transform is computed in place.
        /// </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size, RealArrayR input, ComplexArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_r2c_1d( size, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (input) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (input) elements in the transform along the second
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, RealArrayR input, ComplexArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_r2c_2d( size1, size2, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (input) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (input) elements in the transform along the second
        /// dimension. </param>
        /// <param name="size3">   Number of REAL (input) elements in the transform along the third
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, int size3, RealArrayR input, ComplexArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_r2c_3d( size1, size2, size3, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional forward real-to-complex DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of REAL (input) elements along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="ComplexArrayR">complex array</see> receiving the FFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int rank, int[] size, RealArrayR input, ComplexArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_r2c( rank, size, input.Handle, output.Handle, ( uint ) options ) );
        }

        #endregion

        #region " DOUBLE PRECISION COMPLEX --> REAL PLANS "

        /// <summary>
        /// Creates a plan for a 1-dimensional inverse complex-to-real DFT Destroys its input array even
        /// for out-of-place transforms unless using the
        /// <see cref="PlannerOptions.PreserveInput">preservve input</see>
        /// <paramref name="options">flag</paramref>
        /// with unfortunately some sacrifice in performance. This flag is also not currently supported
        /// for multi-dimensional real DFTs.
        /// <para>
        /// An alternative interface for one-dimensional real-to-complex and complex-to-real DFTs can be
        /// found in real-to-real interface (see Section 2.5.1 [The Halfcomplex-format DFT], page 10),
        /// with 'halfcomplex'-format output that is the same size (and type) as the input array. That
        /// interface, although it is not very useful for multi-dimensional transforms, may sometimes
        /// yield better performance.
        /// </para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of REAL (output) elements in the transform. </param>
        /// <param name="input">   A <see cref="ComplexArrayR">complex array</see> holding the input
        /// DFT. It includes <paramref name="size">N</paramref>/2+1 complex
        /// numbers (the non-redundant outputs). This also requires slight
        /// “padding” of the <paramref name="output"/> array if the transform is
        /// computed in place. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size, ComplexArrayR input, RealArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_c2r_1d( size, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (output) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (output) elements in the transform along the second
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayR">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, ComplexArrayR input, RealArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_c2r_2d( size1, size2, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of REAL (output) elements in the transform along the first
        /// dimension. </param>
        /// <param name="size2">   Number of REAL (output) elements in the transform along the second
        /// dimension. </param>
        /// <param name="size3">   Number of REAL (output) elements in the transform along the third
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayR">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, int size3, ComplexArrayR input, RealArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_c2r_3d( size1, size2, size3, input.Handle, output.Handle, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional inverse complex-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of REAL (output) elements along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="ComplexArrayR">complex array</see> holding the input
        /// DFT. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// DFT. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int rank, int[] size, ComplexArrayR input, RealArrayR output, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.Dft_c2r( rank, size, input.Handle, output.Handle, ( uint ) options ) );
        }

        #endregion

        #region " DOUBLE PRECISION REAL <--> REAL PLANS "

        /// <summary> Creates a plan for a 1-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size">    Number of elements in the transform. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind">    The kind of real-to-real transform to compute. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size, RealArrayR input, RealArrayR output, TransformKind kind, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.R2r_1d( size, input.Handle, output.Handle, kind, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 2-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of elements in the transform along the first dimension. </param>
        /// <param name="size2">   Number of elements in the transform along the second dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind1">   The kind of real-to-real transform to compute along the first
        /// dimension. </param>
        /// <param name="kind2">   The kind of real-to-real transform to compute along the second
        /// dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, RealArrayR input, RealArrayR output, TransformKind kind1, TransformKind kind2, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.R2r_2d( size1, size2, input.Handle, output.Handle, kind1, kind2, ( uint ) options ) );
        }

        /// <summary> Creates a plan for a 3-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="size1">   Number of elements in the transform along the first dimension. </param>
        /// <param name="size2">   Number of elements in the transform along the second dimension. </param>
        /// <param name="size3">   Number of elements in the transform along the third dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind1">   The kind of real-to-real transform to compute along the first
        /// dimension. </param>
        /// <param name="kind2">   The kind of real-to-real transform to compute along the second
        /// dimension. </param>
        /// <param name="kind3">   The kind of real-to-real transform to compute along the third
        /// dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int size1, int size2, int size3, RealArrayR input, RealArrayR output, TransformKind kind1, TransformKind kind2, TransformKind kind3, PlannerOptions options )
        {
            return (input is not object)
                ? throw new ArgumentNullException( nameof( input ) )
                : (output is not object)
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.R2r_3d( size1, size2, size3, input.Handle, output.Handle, kind1, kind2, kind3, ( uint ) options ) );
        }

        /// <summary> Creates a plan for an n-dimensional real-to-real DFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rank">    Number of dimensions. </param>
        /// <param name="size">    Array containing the number of elements in the transform along each
        /// dimension. </param>
        /// <param name="input">   A <see cref="RealArrayR">real-parts array</see> holding the input
        /// elements. </param>
        /// <param name="output">  A <see cref="RealArrayR">real-parts array</see> receiving the output
        /// elements. </param>
        /// <param name="kind">    An array containing the kind of real-to-real transform to compute
        /// along each dimension. </param>
        /// <param name="options"> Flags that specify the behavior of the planner. </param>
        /// <returns> A PlanR. </returns>
        public static PlanR Create( int rank, int[] size, RealArrayR input, RealArrayR output, TransformKind[] kind, PlannerOptions options )
        {
            return (input is not object )
                ? throw new ArgumentNullException( nameof( input ) )
                : ( output is not object )
                ? throw new ArgumentNullException( nameof( output ) )
                : new PlanR( FftwR.SafeNativeMethods.R2r( rank, size, input.Handle, output.Handle, kind, ( uint ) options ) );
        }

        #endregion

    }
}
