﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Complex extensions for taper filter. </summary>
    internal static class ComplexTaperFilter
    {

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="spectrum">            The spectrum. </param>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        public static void HighPassTaper( this Complex[] spectrum, int frequency, int transitionBankCount )
        {
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            spectrum.HighPassTaper( frequency, transitionBankCount, 0 );
        }

        /// <summary> Tapers the spectrum using a High Pass taper. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="spectrum">            The spectrum. </param>
        /// <param name="frequency">           The high pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="fromIndex">           The first frequency index form which to apply the high
        /// pass taper. </param>
        public static void HighPassTaper( this Complex[] spectrum, int frequency, int transitionBankCount, int fromIndex )
        {
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            if ( spectrum.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( spectrum ), "The spectrum must have more than a one element." );
            }

            if ( fromIndex + transitionBankCount / 2 > frequency )
            {
                throw new ArgumentOutOfRangeException( nameof( fromIndex ), fromIndex, "Frequency must exceed the minimum frequency plus half the transition band." );
            }

            int elementCount = spectrum.Length;
            if ( frequency - transitionBankCount / 2 + transitionBankCount > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency plus half transition band must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;
            if ( fromIndex == 0 )
            {
                spectrum[fromIndex] = 0;
                fromIndex += 1;
            }

            // zero out low frequency amplitudes
            frequencyIndex = fromIndex;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 )
            {
                spectrum[frequencyIndex] = 0;
                spectrum[conjugateFrequencyIndex] = 0;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band from the data
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                spectrum[frequencyIndex] = taperFactor * spectrum[frequencyIndex];
                spectrum[conjugateFrequencyIndex] = taperFactor * spectrum[conjugateFrequencyIndex];

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="spectrum">            The spectrum. </param>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        public static void LowPassTaper( this Complex[] spectrum, int frequency, int transitionBankCount )
        {
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            spectrum.LowPassTaper( frequency, transitionBankCount, spectrum.Length / 2 );
        }

        /// <summary>
        /// Applies a Low Pass Filter in the frequency domain by tapering the signal spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="spectrum">            The spectrum. </param>
        /// <param name="frequency">           The low pass frequency index. </param>
        /// <param name="transitionBankCount"> The range of indexes over which the filter is tapered. </param>
        /// <param name="toIndex">             Zero-based index of to. </param>
        public static void LowPassTaper( this Complex[] spectrum, int frequency, int transitionBankCount, int toIndex )
        {
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            if ( spectrum.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( spectrum ), "The spectrum must have more than a one element." );
            }

            if ( frequency - transitionBankCount / 2 <= 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( frequency ), frequency, "Filter frequency minus half the transition band must exceed zero." );
            }

            if ( frequency - transitionBankCount / 2 + transitionBankCount > toIndex )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band." );
            }

            int elementCount = spectrum.Length;
            if ( toIndex > elementCount / 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Maximum frequency must be lower than half the spectrum." );
            }

            int frequencyIndex;
            int conjugateFrequencyIndex;
            double taperFactor;

            // The Cosine taper scale factor
            double deltaPhase;
            double taperPhase;

            // Set the cosine values  
            taperPhase = 0d;
            deltaPhase = 0.5d * Math.PI / Convert.ToSingle( transitionBankCount );

            // taper through the frequency transition band from the data
            frequencyIndex = frequency - transitionBankCount / 2;
            conjugateFrequencyIndex = elementCount - frequencyIndex;
            while ( frequencyIndex < frequency - transitionBankCount / 2 + transitionBankCount )
            {
                taperFactor = 0.5d * (1.0d + Math.Cos( taperPhase ));

                // Apply the taper factor      
                spectrum[frequencyIndex] = taperFactor * spectrum[frequencyIndex];
                spectrum[conjugateFrequencyIndex] = taperFactor * spectrum[conjugateFrequencyIndex];

                // Set the next point
                taperPhase += deltaPhase;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }

            while ( frequencyIndex <= toIndex )
            {
                spectrum[frequencyIndex] = 0;
                spectrum[conjugateFrequencyIndex] = 0;
                frequencyIndex += 1;
                conjugateFrequencyIndex -= 1;
            }
        }
    }
}