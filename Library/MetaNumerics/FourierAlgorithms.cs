﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Class Transformlet. </summary>
    /// <remarks>
    /// Given a length-N DFT, we decompose N = R1 R2 ... Rn into prime factors R. The total DFT can
    /// then be expressed as a series of length-R DFTs, where each length-R DFT is repeated N/R
    /// times. (Each R need not actually be prime, only co-prime to the other factors.)
    /// If a length-N DFT is O(N^2) and N = R1 R2, then a naive implementation would be order N^2 =
    /// R1^2 R2^2. But the decomposed work is order (N / R1) R1^2 + (N / R2) R2^2 = R1 R2 (R1 + R2),
    /// which is less. We handle large prime factors with the Bluestein algorithm. Each length-R DFT
    /// is handled by a Transformlet. We have a general transformlet for arbitrary R (the
    /// Transformlet class), specialized dedicated transformlets for small values of R
    /// (LengthTwoTransformlet, LengthThreeTransformlet, etc.) and a BluesteinTransformlet for larger
    /// R. (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class Transformlet
    {

        /// <summary> Initializes a new instance of the <see cref="Transformlet" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="radix">       The radix. </param>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The Nth complex roots of unity. </param>
        public Transformlet( int radix, int totalLength, Complex[] unitRoots ) : base()
        {
            this.Radix = radix;
            this.TotalLength = totalLength;
            this.UnityRoots = unitRoots;
        }

        /// <summary> Gets or sets the total length. </summary>
        /// <value> The total length. </value>
        protected int TotalLength { get; private set; }

        /// <summary> Gets or sets the Nth complex roots of unity. </summary>
        /// <value> The unity roots. </value>
        protected Complex[] UnityRoots { get; private set; }

        /// <summary> Gets or sets the radix. </summary>
        /// <value> The radix. </value>
        public int Radix { get; private set; }

        /// <summary> The multiplicity. </summary>
        private int _Multiplicity;

        /// <summary> Gets or sets the multiplicity. </summary>
        /// <remarks>
        /// we don't use the Multiplicity in any transformlet methods, but it is used to execute the
        /// whole plan, and since there is one per transformlet we save ourselves from creating an
        /// additional container class by storing it in the Transformlet.
        /// </remarks>
        /// <value> The multiplicity. </value>
        public int Multiplicity
        {
            get => this._Multiplicity;

            internal set => this._Multiplicity = value;
        }

        /// <summary> The FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The x. </param>
        /// <param name="y">    The y. </param>
        /// <param name="ns">   The ns. </param>
        /// <param name="sign"> The sign. </param>
        public virtual void FftPass( Complex[] x, Complex[] y, int ns, int sign )
        {
            var v = new Complex[this.Radix];
            int dx = this.TotalLength / this.Radix;
            for ( int j = 0, loopTo = dx - 1; j <= loopTo; j++ )
            {
                // note: the j-loop could be parallelized, if the v-buffer is not shared between threads
                int xi = j;
                int ui = 0;
                if ( sign < 0 )
                {
                    ui = this.TotalLength;
                }

                int du = dx / ns * (j % ns);
                if ( sign < 0 )
                {
                    du = -du;
                }
                // basically, we need to copy x[j + r * dx] * u[r * d.u] into v[r]
                // we do this in a complicated-looking way in order to avoid unnecessary multiplication when u = 1
                // such a complex multiply requires 6 flops which results in a no-op; we have shown this to be a measurable time-saver
                if ( false )
                {
                }
                // all u-factors are 1, so we can just reference x directly without copying into v
                // to do this, we need to change FftKernel to accept an offset and stride for x
                else
                {
                    v[0] = x[xi]; // the first u is guaranteed to be 1
                    for ( int r = 1, loopTo1 = this.Radix - 1; r <= loopTo1; r++ )
                    {
                        xi += dx;
                        ui += du;
                        v[r] = x[xi] * this.UnityRoots[ui];
                    }
                }

                int y0 = Expand( j, ns, this.Radix );
                this.FftKernel( v, y, y0, ns, sign );
            }
        }

        /// <summary> The FFT kernel. </summary>
        /// <remarks>
        /// This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1
        /// case to avoid unnecessary complex multiplications. For good performance, override this with a
        /// custom kernel for each radix. I am a little worried that this call is virtual. It's not in
        /// the innermost loop (which is inside it), but it is in the next loop out. But the whole
        /// transformlet architecture gives a significant performance boost over our last architecture,
        /// so it's a price I'm willing to pay for now.
        /// </remarks>
        /// <param name="x">    The source vector. </param>
        /// <param name="y">    The target vector. </param>
        /// <param name="y0">   The initial y index. </param>
        /// <param name="dy">   The stride. </param>
        /// <param name="sign"> The gives the sign of the Fourier transform in the exponent. </param>
        public virtual void FftKernel( Complex[] x, Complex[] y, int y0, int dy, int sign )
        {
            int dx = this.TotalLength / this.Radix;

            // y sub I is the y index we are currently computing; initialize it to y0
            int yi = y0;

            // the first index is the zero frequency component that just adds all the x's
            // encode this specially so we are not unnecessarily multiplying by complex 1 R times
            y[yi] = 0.0d;
            for ( int j = 0, loopTo = this.Radix - 1; j <= loopTo; j++ )
                y[yi] += x[j];

            // now do the higher index entries
            for ( int i = 1, loopTo1 = this.Radix - 1; i <= loopTo1; i++ )
            {
                yi += dy;
                y[yi] = x[0];
                int ui = 0;
                if ( sign < 0 )
                {
                    ui = this.TotalLength;
                }

                int du = dx * i;
                if ( sign < 0 )
                {
                    du = -du;
                }

                for ( int j = 1, loopTo2 = this.Radix - 1; j <= loopTo2; j++ )
                {
                    ui += du;
                    if ( ui >= this.TotalLength )
                    {
                        ui -= this.TotalLength;
                    }

                    if ( ui < 0 )
                    {
                        ui += this.TotalLength;
                    }

                    y[yi] += x[j] * this.UnityRoots[ui];
                }
            }
        }

        /// <summary> Expands the specified index L. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="index"> The index L. </param>
        /// <param name="n1">    The n1. </param>
        /// <param name="n2">    The n2. </param>
        /// <returns> The expanded index L. </returns>
        protected static int Expand( int index, int n1, int n2 )
        {
            return index / n1 * n1 * n2 + index % n1;
        }
    }

    /// <summary> Class RadixTwoTransformlet. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class RadixTwoTransformlet : Transformlet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RadixTwoTransformlet" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public RadixTwoTransformlet( int totalLength, Complex[] unitRoots ) : base( 2, totalLength, unitRoots )
        {
        }

        /// <summary> The pass FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The x. </param>
        /// <param name="y">    The y. </param>
        /// <param name="ns">   The ns. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftPass( Complex[] x, Complex[] y, int ns, int sign )
        {
            int dx = this.TotalLength / 2;
            for ( int j = 0, loopTo = dx - 1; j <= loopTo; j++ )
            {
                int du = dx / ns * (j % ns);
                int y0 = Expand( j, ns, 2 );
                if ( sign < 0 )
                {
                    FftKernel( x[j], x[j + dx] * this.UnityRoots[this.TotalLength - du], ref y[y0], ref y[y0 + ns] );
                }
                else
                {
                    FftKernel( x[j], x[j + dx] * this.UnityRoots[du], ref y[y0], ref y[y0 + ns] );
                }
            }
        }

        /// <summary> Performs a length-2 FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x0"> The x0. </param>
        /// <param name="x1"> The x1. </param>
        /// <param name="y0"> [in,out] The y0. </param>
        /// <param name="y1"> [in,out] The y1. </param>
        private static void FftKernel( Complex x0, Complex x1, ref Complex y0, ref Complex y1 )
        {
            double a0 = x0.Real;
            double b0 = x0.Imaginary;
            double a1 = x1.Real;
            double b1 = x1.Imaginary;
            y0 = new Complex( a0 + a1, b0 + b1 );
            y1 = new Complex( a0 - a1, b0 - b1 );
            // for some reason, this looks to be faster than using the complex add and subtract; i don't see why

            // this kernel has 4 flops, all adds/subs

            // the naive R=2 kernel has 1 complex multiply and 2 complex adds
            // a complex multiply requires 6 flops and complex add 2 ops
            // so the naive kernel has 6 * 1 + 2 * 2 = 6 + 4 = 10 flops
            // we have saved a factor 2.5

        }
    }

    /// <summary> Class RadixThreeTransformlet. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class RadixThreeTransformlet : Transformlet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RadixThreeTransformlet" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public RadixThreeTransformlet( int totalLength, Complex[] unitRoots ) : base( 3, totalLength, unitRoots )
        {
        }

        /// <summary> The FFT pass. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The x. </param>
        /// <param name="y">    The y. </param>
        /// <param name="ns">   The ns. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftPass( Complex[] x, Complex[] y, int ns, int sign )
        {
            int dx = this.TotalLength / 3;
            for ( int j = 0, loopTo = dx - 1; j <= loopTo; j++ )
            {
                int du = dx / ns * (j % ns);
                int y0 = Expand( j, ns, 3 );
                if ( sign < 0 )
                {
                    FftKernel( x[j], x[j + dx] * this.UnityRoots[this.TotalLength - du], x[j + 2 * dx] * this.UnityRoots[this.TotalLength - 2 * du], ref y[y0], ref y[y0 + ns], ref y[y0 + 2 * ns], -1 );
                }
                else
                {
                    FftKernel( x[j], x[j + dx] * this.UnityRoots[du], x[j + 2 * dx] * this.UnityRoots[2 * du], ref y[y0], ref y[y0 + ns], ref y[y0 + 2 * ns], 1 );
                }
            }
        }

        /// <summary> Performs a length-3 FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x0">   The x0. </param>
        /// <param name="x1">   The x1. </param>
        /// <param name="x2">   The x2. </param>
        /// <param name="y0">   [in,out] The y0. </param>
        /// <param name="y1">   [in,out] The y1. </param>
        /// <param name="y2">   [in,out] The y2. </param>
        /// <param name="sign"> The sign. </param>
        private static void FftKernel( Complex x0, Complex x1, Complex x2, ref Complex y0, ref Complex y1, ref Complex y2, int sign )
        {
            double a12p = x1.Real + x2.Real;
            double b12p = x1.Imaginary + x2.Imaginary;
            double sa = x0.Real + R31.Real * a12p;
            double sb = x0.Imaginary + R31.Real * b12p;
            double ta = R31.Imaginary * (x1.Real - x2.Real);
            double tb = R31.Imaginary * (x1.Imaginary - x2.Imaginary);
            if ( sign < 0 )
            {
                ta = -ta;
                tb = -tb;
            }

            y0 = new Complex( x0.Real + a12p, x0.Imaginary + b12p );
            y1 = new Complex( sa - tb, sb + ta );
            y2 = new Complex( sa + tb, sb - ta );

            // this kernel has 16 flops

            // the naive kernel for R=3 has 4 complex multiplies and 6 complex adds
            // a complex multiply requires 6 flops and a complex add 2 flops
            // so the naive kernel has 4 * 6 + 6 * 2 = 24 + 12 = 36 flops
            // we have saved a factor 2.25, actually a bit more since we have also removed index loop accounting
        }

        /// <summary> The first r 3. </summary>
        private static readonly Complex R31 = new Complex( -1.0d / 2.0d, Math.Sqrt( 3.0d ) / 2.0d );
    }

    /// <summary> Class RadixFourTransformlet. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class RadixFourTransformlet : Transformlet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RadixFourTransformlet" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public RadixFourTransformlet( int totalLength, Complex[] unitRoots ) : base( 4, totalLength, unitRoots )
        {
        }

        /// <summary> Performs a length-4 FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The source vector. </param>
        /// <param name="y">    The target vector. </param>
        /// <param name="y0">   The initial y index. </param>
        /// <param name="dy">   The stride. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftKernel( Complex[] x, Complex[] y, int y0, int dy, int sign )
        {
            double a02p = x[0].Real + x[2].Real;
            double b02p = x[0].Imaginary + x[2].Imaginary;
            double a02m = x[0].Real - x[2].Real;
            double b02m = x[0].Imaginary - x[2].Imaginary;
            double a13p = x[1].Real + x[3].Real;
            double b13p = x[1].Imaginary + x[3].Imaginary;
            double a13m = x[1].Real - x[1].Real;
            double b13m = x[1].Imaginary - x[3].Imaginary;
            y[y0] = new Complex( a02p + a13p, b02p + b13p );
            y[y0 + dy] = new Complex( a02m - b13m, b02m + a13m );
            y[y0 + 2 * dy] = new Complex( a02p - a13p, b02p - b13p );
            y[y0 + 3 * dy] = new Complex( a02m + b13m, b02m - a13m );
        }
    }

    /// <summary> Class RadixFiveTransformlet. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class RadixFiveTransformlet : Transformlet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RadixFiveTransformlet" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public RadixFiveTransformlet( int totalLength, Complex[] unitRoots ) : base( 5, totalLength, unitRoots )
        {
        }

        /// <summary> Performs a length-5 FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The source vector. </param>
        /// <param name="y">    The target vector. </param>
        /// <param name="y0">   The initial y index. </param>
        /// <param name="dy">   The stride. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftKernel( Complex[] x, Complex[] y, int y0, int dy, int sign )
        {
            // first set of combinations
            double a14p = x[1].Real + x[4].Real;
            double a14m = x[1].Real - x[4].Real;
            double a23p = x[2].Real + x[3].Real;
            double a23m = x[2].Real - x[3].Real;
            double b14p = x[1].Imaginary + x[4].Imaginary;
            double b14m = x[1].Imaginary - x[4].Imaginary;
            double b23p = x[2].Imaginary + x[3].Imaginary;
            double b23m = x[2].Imaginary - x[3].Imaginary;
            // second set of combinations, for v[1] and v[4]
            double s14a = x[0].Real + R51.Real * a14p + R52.Real * a23p;
            double s14b = x[0].Imaginary + R51.Real * b14p + R52.Real * b23p;
            double t14a = R51.Imaginary * a14m + R52.Imaginary * a23m;
            double t14b = R51.Imaginary * b14m + R52.Imaginary * b23m;
            // second set of combinations, for v[2] and v[3]
            double s23a = x[0].Real + R52.Real * a14p + R51.Real * a23p;
            double s23b = x[0].Imaginary + R52.Real * b14p + R51.Real * b23p;
            double t23a = R52.Imaginary * a14m - R51.Imaginary * a23m;
            double t23b = R52.Imaginary * b14m - R51.Imaginary * b23m;
            // take care of sign
            if ( sign < 0 )
            {
                t14a = -t14a;
                t14b = -t14b;
                t23a = -t23a;
                t23b = -t23b;
            }
            // bring together results
            y[y0] = new Complex( x[0].Real + a14p + a23p, x[0].Imaginary + b14p + b23p );
            y[y0 + dy] = new Complex( s14a - t14b, s14b + t14a );
            y[y0 + 2 * dy] = new Complex( s23a - t23b, s23b + t23a );
            y[y0 + 3 * dy] = new Complex( s23a + t23b, s23b - t23a );
            y[y0 + 4 * dy] = new Complex( s14a + t14b, s14b - t14a );
        }

        /// <summary> The fifth s. </summary>
        private static readonly double S5 = Math.Sqrt( 5.0d );

        /// <summary> The first r 5. </summary>
        private static readonly Complex R51 = new Complex( (S5 - 1.0d) / 4.0d, Math.Sqrt( (5.0d + S5) / 8.0d ) );

        /// <summary> The second r 5. </summary>
        private static readonly Complex R52 = new Complex( -(S5 + 1.0d) / 4.0d, Math.Sqrt( (5.0d - S5) / 8.0d ) );
    }

    /// <summary> Class RadixSevenTransformlet. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal class RadixSevenTransformlet : Transformlet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RadixSevenTransformlet" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public RadixSevenTransformlet( int totalLength, Complex[] unitRoots ) : base( 7, totalLength, unitRoots )
        {
        }

        /// <summary> Performs the length 7 FFT. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">    The source vector. </param>
        /// <param name="y">    The target vector. </param>
        /// <param name="y0">   The initial y index. </param>
        /// <param name="dy">   The stride. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftKernel( Complex[] x, Complex[] y, int y0, int dy, int sign )
        {
            // relevant sums and differences
            double a16p = x[1].Real + x[6].Real;
            double a16m = x[1].Real - x[6].Real;
            double a25p = x[2].Real + x[5].Real;
            double a25m = x[2].Real - x[5].Real;
            double a34p = x[3].Real + x[4].Real;
            double a34m = x[3].Real - x[4].Real;
            double b16p = x[1].Imaginary + x[6].Imaginary;
            double b16m = x[1].Imaginary - x[6].Imaginary;
            double b25p = x[2].Imaginary + x[5].Imaginary;
            double b25m = x[2].Imaginary - x[5].Imaginary;
            double b34p = x[3].Imaginary + x[4].Imaginary;
            double b34m = x[3].Imaginary - x[4].Imaginary;
            // combinations used in y[1] and y[6]
            double s16a = x[0].Real + R71.Real * a16p + R72.Real * a25p + R73.Real * a34p;
            double s16b = x[0].Imaginary + R71.Real * b16p + R72.Real * b25p + R73.Real * b34p;
            double t16a = R71.Imaginary * a16m + R72.Imaginary * a25m + R73.Imaginary * a34m;
            double t16b = R71.Imaginary * b16m + R72.Imaginary * b25m + R73.Imaginary * b34m;
            // combinations used in y[2] and y[5]
            double s25a = x[0].Real + R71.Real * a34p + R72.Real * a16p + R73.Real * a25p;
            double s25b = x[0].Imaginary + R71.Real * b34p + R72.Real * b16p + R73.Real * b25p;
            double t25a = R71.Imaginary * a34m - R72.Imaginary * a16m + R73.Imaginary * a25m;
            double t25b = R71.Imaginary * b34m - R72.Imaginary * b16m + R73.Imaginary * b25m;
            // combinations used in y[3] and y[4]
            double s34a = x[0].Real + R71.Real * a25p + R72.Real * a34p + R73.Real * a16p;
            double s34b = x[0].Imaginary + R71.Real * b25p + R72.Real * b34p + R73.Real * b16p;
            double t34a = R71.Imaginary * a25m - R72.Imaginary * a34m - R73.Imaginary * a16m;
            double t34b = R71.Imaginary * b25m - R72.Imaginary * b34m - R73.Imaginary * b16m;
            // if sign is negative, invert t's
            if ( sign < 0 )
            {
                t16a = -t16a;
                t16b = -t16b;
                t25a = -t25a;
                t25b = -t25b;
                t34a = -t34a;
                t34b = -t34b;
            }
            // combine to get results
            y[y0] = new Complex( x[0].Real + a16p + a25p + a34p, x[0].Imaginary + b16p + b25p + b34p );
            y[y0 + dy] = new Complex( s16a - t16b, s16b + t16a );
            y[y0 + 2 * dy] = new Complex( s25a + t25b, s25b - t25a );
            y[y0 + 3 * dy] = new Complex( s34a + t34b, s34b - t34a );
            y[y0 + 4 * dy] = new Complex( s34a - t34b, s34b + t34a );
            y[y0 + 5 * dy] = new Complex( s25a - t25b, s25b + t25a );
            y[y0 + 6 * dy] = new Complex( s16a + t16b, s16b - t16a );
        }

        // seventh roots of unity
        // a la Gauss, these are not expressible in closed form using rational values and rational roots

        /// <summary> The first r 7. </summary>
        private static readonly Complex R71 = new Complex( 0.62348980185873348d, 0.7818314824680298d );

        /// <summary> The second r 7. </summary>
        private static readonly Complex R72 = new Complex( -0.22252093395631439d, 0.97492791218182362d );

        /// <summary> The third r 7. </summary>
        private static readonly Complex R73 = new Complex( -0.90096886790241915d, 0.43388373911755812d );
    }

    /// <summary> Class BluesteinTransformlet. </summary>
    /// <remarks>
    /// The Bluestein technique works as follows: <para>
    /// Given the length-N FT</para><para>
    /// \tilde{x}_m = \sum_{n=0}^{N-1} x_n \exp{i \pm 2 \pi m n / N}</para><para>
    /// use m n = \FRAC{m^2 + n^2 - (m - n)^2}{2} to turn this into</para><para>
    /// \tilde{x}_m = \exp{i \pm \pi m^2 / N} \sum_{n=0}^{N-1} x_n \exp{i \pm \pi n^2 / N} \exp{i \mp
    /// \pi (m - n)^2 / N}</para><para>
    /// The summed expression is a convolution of</para><para>
    /// a_n = x_n \exp{i \pm \pi n^2 / N}</para><para>
    /// b_n = \exp{i \mp \pi n^2 / N}</para><para>
    /// A convolution can be done via an FT of any length larger than 2N-1. The 2N is necessary so
    /// that a_0 can be multiplied by b_{-N} and a_N can be multiplied by b_0. This thus the
    /// sequences to be convolved are</para><para>
    /// 0 0  0       0   0  a_0 a_1 a_2 ... a_n 0 0 0 0 0 b_n ... b_2 b_1 b_0 b_1 b_2 ... b_n 0 0 0
    /// Since this is a convolution, it doesn't matter how far out we zero-pad. We pick an M &gt;= 2N-
    /// 1 that is composed of small prime factors, so we won't need the Bluestein technique to do the
    /// convolution itself.</para> (c) 2012 David Wright (http://www.meta-numerics.net).<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    internal class BluesteinTransformlet : Transformlet
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="radix">       The radix. </param>
        /// <param name="totalLength"> The total length. </param>
        /// <param name="unitRoots">   The unit roots. </param>
        public BluesteinTransformlet( int radix, int totalLength, Complex[] unitRoots ) : base( radix, totalLength, unitRoots )
        {

            // figure out the right Bluestein length and create a transformer for it
            this.Nb = SetBluesteinLength( 2 * radix - 1 );
            this.Ft = new FourierTransformer( this.Nb );

            // compute the Bluestein coefficients and compute the FT of the filter based on them
            this.B = ComputeBluesteinCoefficients( radix );
            var c = new Complex[this.Nb];
            c[0] = 1.0d;
            for ( int i = 1, loopTo = radix - 1; i <= loopTo; i++ )
            {
                c[i] = this.B[i].Conjugate();
                c[this.Nb - i] = c[i];
            }

            this.Bt = this.Ft.Transform( c );
        }

        /// <summary> The Length of convolution transform. </summary>
        /// <value> The nb. </value>
        private int Nb { get; set; }

        /// <summary> The Fourier transform for convolution transform. </summary>
        /// <value> The ft. </value>
        private FourierTransformer Ft { get; set; }

        /// <summary> The R Bluestein coefficients. </summary>
        /// <value> The b. </value>
        private Complex[] B { get; set; }

        /// <summary>
        /// The Nb-length Fourier transform of the symmetric Bluestein coefficient filter.
        /// </summary>
        /// <value> The bt. </value>
        private Complex[] Bt { get; set; }

        /// <summary> Computes the Complex Bluestein coefficients. </summary>
        /// <remarks>
        /// This method computes b_n = \exp{i \pi n^2 / N}. If we do this naively, by computing sin and
        /// Cos of \pi n^2 / N, then the argument can get large, up to N \pi, and the inaccuracy of trig
        /// methods for large arguments will hit us To avoid this, note that the difference n^2 - (n-1)^2
        /// = 2n-1. So we can add 2n-1 each time and take the result mod 2N to keep the argument less
        /// than 2 \pi.
        /// </remarks>
        /// <param name="radix"> The radix. </param>
        /// <returns> The Complex he Bluestein coefficients. </returns>
        private static Complex[] ComputeBluesteinCoefficients( int radix )
        {
            var b = new Complex[radix];
            double t = Math.PI / radix;
            b[0] = 1.0d;
            int s = 0;
            int twoR = 2 * radix;
            for ( int i = 1, loopTo = radix - 1; i <= loopTo; i++ )
            {
                s += 2 * i - 1;
                if ( s >= twoR )
                {
                    s -= twoR;
                }

                double ts = t * s;
                b[i] = new Complex( Math.Cos( ts ), Math.Sin( ts ) );
            }

            return b;
        }

        /// <summary> The FFT kernel. </summary>
        /// <remarks>
        /// This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1
        /// case to avoid unnecessary complex multiplications. For good performance, override this with a
        /// custom kernel for each radix. I am a little worried that this call is virtual. It's not in
        /// the innermost loop (which is inside it), but it is in the next loop out. But the whole
        /// transformlet architecture gives a significant performance boost over our last architecture,
        /// so it's a price I'm willing to pay for now.
        /// </remarks>
        /// <param name="x">    The source vector. </param>
        /// <param name="y">    The target vector. </param>
        /// <param name="y0">   The initial y index. </param>
        /// <param name="dy">   The stride. </param>
        /// <param name="sign"> The sign. </param>
        public override void FftKernel( Complex[] x, Complex[] y, int y0, int dy, int sign )
        {

            // all we have to do here is convolve (b x) with b-star
            // to do this convolution, we need to multiply the DFT of (b x) with the DFT of b-star, the Inverse DFT the result back
            // we have already stored the DFT of b-star

            // create c = b x and transform it into Fourier space
            var c = new Complex[this.Nb];
            if ( sign > 0 )
            {
                for ( int i = 0, loopTo = this.Radix - 1; i <= loopTo; i++ )
                    c[i] = this.B[i] * x[i];
            }
            else
            {
                for ( int i = 0, loopTo1 = this.Radix - 1; i <= loopTo1; i++ )
                    c[i] = this.B[i] * x[i].Conjugate();
            }

            var ct = this.Ft.Transform( c );

            // multiply b-star and c = b x in Fourier space, and inverse transform the product back into configuration space
            for ( int i = 0, loopTo2 = this.Nb - 1; i <= loopTo2; i++ )
                ct[i] = ct[i] * this.Bt[i];
            c = this.Ft.InverseTransform( ct );

            // read off the result
            if ( sign > 0 )
            {
                for ( int i = 0, loopTo3 = this.Radix - 1; i <= loopTo3; i++ )
                    y[y0 + i * dy] = this.B[i] * c[i];
            }
            else
            {
                for ( int i = 0, loopTo4 = this.Radix - 1; i <= loopTo4; i++ )
                    y[y0 + i * dy] = this.B[i].Conjugate() * c[i].Conjugate();
            }

            // for the sign < 0 case, we have used the fact that the convolution of (b-star x) with b is
            // just the convolution of (b x-star) with b-star, starred

        }

        /// <summary> Sets the Bluestein length. </summary>
        /// <remarks>
        /// This is all about determining a good value to use for the Bluestein length. We choose a
        /// length based on powers of two and three, since those give very fast Fourier transforms. Our
        /// method is heuristic and not optimized.
        /// </remarks>
        /// <param name="totalLength"> The total length. </param>
        /// <returns> The Bluestein length. </returns>
        private static int SetBluesteinLength( int totalLength )
        {

            // try the next power of two
            int t = NextPowerOfBase( totalLength, 2 );
            int m = t;

            // see if replacing factors of 4 by 3, which shortens the length, will still be long enough
            while ( m % 4 == 0 )
            {
                t = m / 4 * 3;
                if ( t < totalLength )
                {
                    break;
                }

                if ( t < m )
                {
                    m = t;
                }
            }

            // try the next power of three
            t = NextPowerOfBase( totalLength, 3 );
            if ( t > 0 && t < m )
            {
                m = t;
            }

            return m;
        }

        /// <summary> Returns the next the power of base. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="n"> The n. </param>
        /// <param name="b"> The b. </param>
        /// <returns> The next the power of base. </returns>
        private static int NextPowerOfBase( int n, int b )
        {
            int m = b;
            while ( m <= int.MaxValue )
            {
                if ( m >= n )
                {
                    return m;
                }

                m *= b;
            }

            return -1;
        }
    }

    /// <summary> Class FourierAlgorithms. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    internal sealed class FourierAlgorithms
    {

        /// <summary>
        /// Prevents a default instance of the <see cref="FourierAlgorithms" /> class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private FourierAlgorithms()
        {
        }

        /// <summary>
        /// The 2*PI
        /// </summary>
        public const double TwoPI = 2.0d * Math.PI;

        /// <summary>
        /// Computes the Complex Nth roots of unity, which are the factors in a length-N Fourier
        /// transform.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="totalLength"> The total length (N). </param>
        /// <param name="sign">        The sign. </param>
        /// <returns>
        /// The Complex Nth roots of unity, which are the factors in a length-N Fourier transform.
        /// </returns>
        public static Complex[] ComputeRoots( int totalLength, int sign )
        {
            var u = new Complex[totalLength + 1];
            double t = sign * TwoPI / totalLength;
            u[0] = 1.0d;
            for ( int r = 1, loopTo = totalLength - 1; r <= loopTo; r++ )
            {
                double rt = r * t;
                u[r] = new Complex( Math.Cos( rt ), Math.Sin( rt ) );
            }

            u[totalLength] = 1.0d;
            return u;
        }
    }
}