﻿using System;

namespace isr.Algorithms.Signals
{
    public sealed partial class AdvancedMath
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private AdvancedMath()
        {
        }

        /// <summary> Computes the Greatest Common Denominator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="u"> The u. </param>
        /// <param name="v"> The v. </param>
        /// <returns> The Greatest Common Denominator. </returns>
        internal static int GCD( int u, int v )
        {
            while ( v != 0 )
            {
                int t = u % v;
                u = v;
                v = t;
            }

            return u;
        }

        /// <summary> Computes a power of an integer in modular arithmetic. </summary>
        /// <remarks>
        /// <para>Modular exponentiation is used in many number-theory applications, including prime
        /// value testing, prime factorization, and cryptography.</para>
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> <paramref name="base"/>,
        /// <paramref name="exponenet"/>, or
        /// <paramref name="modulus"/> is not positive.
        /// </exception>
        /// <param name="base">      The base, which must be positive. </param>
        /// <param name="exponenet"> The exponent, which must be positive. </param>
        /// <param name="modulus">   The modulus, which must be positive. </param>
        /// <returns> The value of b<sup>e</sup> mod m. </returns>
        /// <seealso href="http://en.wikipedia.org/wiki/Modular_exponentiation"/>
        public static int PowMod( int @base, int exponenet, int modulus )
        {
            if ( @base < 0 )
                throw new ArgumentOutOfRangeException( nameof( @base ) );
            if ( exponenet < 1 )
                throw new ArgumentOutOfRangeException( nameof( exponenet ) );
            if ( modulus < 1 )
                throw new ArgumentOutOfRangeException( nameof( modulus ) );

            // use long internally
            // since the "worst" we ever do before mod is to square, and since a long should
            // hold twice as many digits as an integer, this algorithm should not overflow
            long bb = Convert.ToInt64( @base );
            long mm = Convert.ToInt64( modulus );
            long rr = 1L;
            while ( exponenet > 0 )
            {
                if ( (exponenet & 1) == 1 )
                {
                    // There is no Visual Basic equivalent to 'checked' in this context:
                    // moreover, because of the module calculation, check is superfluous.
                    // ORIGINAL LINE: r = checked((r * b) Mod m);
                    rr = rr * bb % mm;
                }

                exponenet >>= 1;
                // There is no Visual Basic equivalent to 'checked' in this context:
                // moreover, because of the module calculation, check is superfluous.
                // ORIGINAL LINE: bb = checked((bb * bb) Mod mm);
                bb = bb * bb % mm;
            }

            return Convert.ToInt32( rr );
        }

        /// <summary> Factors the specified n. </summary>
        /// <remarks>
        /// Prime factorization. Leave this internal for now, until it is cleaned up. As currently
        /// implemented, it does not actually guarantee full prime factorization! Pollard's Rho method
        /// can yield non-prime factors, and this appears to occur for about 0.25% of all integers under
        /// 1,000,000. For example, "factors" of 1681 = 41 * 41, 6751 = 43 * 157, and 9167 = 89 *
        /// 103 are claimed. These composite "factors" are, however, still co-prime to the other factors,
        /// so the almost-factorization will still work for reduction of Fourier transforms, which is how
        /// we are currently using it.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> <paramref name="n"/> is not positive. </exception>
        /// <param name="n"> The n. </param>
        /// <returns> The Factors the specified n. </returns>
        internal static System.Collections.Generic.List<Factor> Factor( int n )
        {
            if ( n < 1 )
                throw new ArgumentOutOfRangeException( nameof( n ) );
            var factors = new System.Collections.Generic.List<Factor>();
            if ( n > 1 )
            {
                FactorByTrialDivision( factors, ref n );
            }

            if ( n > 1 )
            {
                FactorByPollardsRhoMethod( factors, ref n, 250 );
            }

            if ( n > 1 )
            {
                factors.Add( new Factor( n, 1 ) );
            }

            return factors;
        }

        /// <summary> The small primes. </summary>
        internal static readonly int[] SmallPrimes = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 };

        /// <summary> Factors the by trial division. </summary>
        /// <remarks>
        /// Trial division is the simplest prime factorization method. It consists of attempted to divide
        /// by known primes. It is a good way to eliminate known small prime factors before proceeding on
        /// to bigger and more difficult prime factors.
        /// </remarks>
        /// <param name="factors"> The factors. </param>
        /// <param name="n">       [in,out] The n. </param>
        private static void FactorByTrialDivision( System.Collections.Generic.List<Factor> factors, ref int n )
        {
            foreach ( int p in SmallPrimes )
            {
                int m = 0;
                while ( n % p == 0 )
                {
                    n /= p;
                    m += 1;
                }

                if ( m > 0 )
                {
                    factors.Add( new Factor( p, m ) );
                }

                if ( n == 1 )
                {
                    return;
                }
            }
        }

        /// <summary> Factors by Pollards Rho method. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="factors">           The factors. </param>
        /// <param name="n">                 [in,out] The n. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        private static void FactorByPollardsRhoMethod( System.Collections.Generic.List<Factor> factors, ref int n, int maximumIterations )
        {
            int x = 5;
            int y = 2;
            int k = 1;
            int l = 1;
            for ( int c = 0, loopTo = maximumIterations - 1; c <= loopTo; c++ )
            {
                // while (true) {
                int g = GCD( Math.Abs( y - x ), n );
                if ( g == n )
                {
                    // the factor n will repeat itself indefinitely; either n is prime or the method has failed
                    return;
                }
                else if ( g == 1 )
                {
                    k -= 1;
                    if ( k == 0 )
                    {
                        y = x;
                        l = 2 * l;
                        k = l;
                    }
                    // take x <- (x^2 + 1) mod n
                    x = PowMod( x, 2, n ) + 1;
                    if ( x == n )
                    {
                        x = 0;
                    }
                }
                else
                {
                    // g is a factor of n; in all likelihood, it is prime, although this isn't guaranteed
                    // for our current approximate-factoring purposes, we will assume it is prime
                    // it is at least co-prime to all other recognized factors
                    int m = 0;
                    while ( n % g == 0 )
                    {
                        n /= g;
                        x = x % n;
                        y = y % n;
                        m += 1;
                    }

                    factors.Add( new Factor( g, m ) );
                }
            }
        }
    }

    /// <summary> Structure Factor. </summary>
    /// <remarks>
    /// (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
    /// License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
    /// provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    /// </remarks>
    internal struct Factor
    {

        /// <summary> Initializes a new instance of the <see cref="Factor" /> structure. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="multiplicity"> The multiplicity. </param>
        public Factor( int value, int multiplicity )
        {
            this.Value = value;
            this.Multiplicity = multiplicity;
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public int Value { get; private set; }

        /// <summary> Gets or sets the multiplicity. </summary>
        /// <value> The multiplicity. </value>
        public int Multiplicity { get; private set; }
    }
}