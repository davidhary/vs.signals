## ISR Algorithms Signals Library<sub>&trade;</sub>: Digital Signal Processing Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.1.6969 01/30/19*  
Uses new core libraries.

*4.1.6667 04/03/18*  
2018 release.

*4.0.6166 11/18/16*  
Uses VS 2015.

*3.0.5133 01/20/14*  
Replaces invalid scale factor exception and null
spectrum densities exception and spectrum. Scales exception with Invalid
operation exception. Documents return values for functions. Removes
interfaces. Removes the validation outcome construct. Removes the
convergence exception. Replaces dimension mismatch with argument out of
range exception. Sets assembly product to 2014.

*3.0.4763 01/15/13*  
Adds array copy from double to single types.

*3.0.4711 11/24/12*  
Uses complex only functions.

*2.2.4707 11/20/12*  
Converted to VS10. Uses Mixed Radix FFT from the Meta
Numerics library.

*2.1.4706 11/19/12*  
Prepared for VS10.

*2.1.4232 08/03/11*  
Standardizes code elements and documentation.

*2.1.4213 07/15/11*  
Simplifies the assembly information.

*2.1.2961 02/09/08*  
Updates to .NET 3.5.

*2.0.2818 09/19/07*  
Removes Mixed-Radix FFT.

*2.0.2817 09/19/07*  
Updates to Visual Studio 8. Uses Wisdom FFT.

*1.0.2225 02/03/06*  
Modifies Taper Filter to specify type and filter
frequencies and transition band. Adds validate Outcome structure..

*1.0.2219 01/28/06*  
Removes Visual Basic import.

*1.0.2205 01/14/06*  
Adds new support and exceptions libraries. Uses INt32,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.2147 11/17/05 Converts FFT pro to .NET

*1.0 1998 FFT pro.

\(C\) 1998 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Signals Library](https://bitbucket.org/davidhary/vs.signals)  
[Meta Numerics](http://www.meta-numerics.net)  
[Wisdom FFT](http://www.fftw.org/)  
[Wisdom FFT C\#](http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html)
