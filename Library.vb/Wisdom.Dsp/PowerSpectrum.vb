Namespace Wisdom

    ''' <summary>The base class for the power spectrum.</summary>
    ''' <remarks> (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/03/98, 1.0.00.. Created as FFT pro <para>
    ''' David, 09/27/05, 1.0.2096. Created based on FFT pro </para></remarks>
    Public Class PowerSpectrum
        Inherits Spectrum

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs This class.</summary>
        ''' <param name="FourierTransform">Holds reference to the Fourier Transform class to use for 
        '''   calculating the spectrum.</param>
        Public Sub New(ByVal fourierTransform As FourierTransformBase)
            ' instantiate the base class
            MyBase.New(fourierTransform)
        End Sub

#End Region

#Region " COMMON "

        ''' <summary> Gets the number of averages. </summary>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <value> The number of averages. </value>
        Public ReadOnly Property AverageCount() As Integer

        ''' <summary>Clears and allocates the power spectrum densities.
        ''' </summary>
        ''' <param name="length">Specifies the length of the spectrum thus allowing for a Int16er
        '''   spectrum than is calculated. Specify 0 to let the program set the maximum
        '''   length.</param>
        Public Sub Clear(ByVal length As Integer)
            Me._Scaled = False
            Me._AverageCount = 0
            If length > 0 Then
                ReDim Me._Densities(length - 1)
            Else
                Me._Densities = Nothing
            End If
        End Sub

        ''' <summary>Returns the total power for the specified index range.
        ''' </summary>
        ''' <param name="fromIndex"></param>
        Public Function TotalPower(ByVal fromIndex As Integer) As Double
            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            Return TotalPower(fromIndex, _Densities.GetUpperBound(0))
        End Function

        ''' <summary>Returns the total power.</summary>
        Public Function TotalPower() As Double
            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            Return TotalPower(_Densities.GetLowerBound(0), _Densities.GetUpperBound(0))
        End Function

        ''' <summary>Returns the spectrum scale factor including the effect of the 
        '''   taper window power.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled
        '''   for displaying the half spectrum.</param>
        Public Function SpectrumScaleFactor(ByVal halfSpectrum As Boolean) As Double
            Dim factor As Double = 1 / (Convert.ToDouble(Me.AverageCount) * MyBase.TimeSeriesLength * MyBase.TimeSeriesLength)
            If halfSpectrum Then
                ' The factor of two ensure double the power for half spectrum.
                factor *= 2
            End If
            ' add taper window power.
            If Not MyBase.TaperWindow Is Nothing Then
                factor /= MyBase.TaperWindow.Power
            End If
            Return factor
        End Function

        ''' <summary>Returns the total power for the specified index range but using both
        '''   side of the spectrum and assuming the spectrum is for real valued time series, that
        '''   is, that the spectrum is symmetric.
        ''' </summary>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Function TotalPower(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' the DC spectrum is scaled by half because the Spectrum Scale Factor
            ' is derived for the half spectrum (twice as large).
            Dim total As Double = 0
            If fromIndex = 0 Then
                total += 0.5 * _Densities(0)
                fromIndex += 1
            End If
            ' go up to half spectrum
            For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)
                ' add the power
                total += _Densities(i)
            Next

            If Not _Scaled Then
                ' apply double the spectrum power factor to correct for the two-sided spectrum.
                total *= Me.SpectrumScaleFactor(True)
            Else
                ' apply double the spectrum power factor to correct for the two-sided spectrum.
                total *= 2
            End If

            Return total

        End Function

        ''' <summary>Returns the index of the spectrum peak.</summary>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Function PeakIndex(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Integer

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' Set Max to first value
            Dim maxDensityIndex As Integer = fromIndex
            Dim maxDensity As Single = CSng(_Densities(maxDensityIndex))
            Dim maxCandidate As Single
            For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)

                ' Get candidate
                maxCandidate = CSng(_Densities(i))

                ' Select a new maximum if lower than element.
                If maxCandidate > maxDensity Then
                    maxDensity = maxCandidate
                    maxDensityIndex = i
                End If

            Next i

            ' return the index
            Return maxDensityIndex

        End Function

#End Region

#Region " DOUBLE TIME SEREIS "

        ''' <summary>Appends the spectrum to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub Append(ByVal reals() As Double, ByVal imaginaries() As Double)
            If Me._Scaled Then
                Throw New InvalidOperationException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
            End If
            If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            Dim real As Double
            Dim imaginary As Double
            If Me.Densities Is Nothing Then
                ReDim _Densities(reals.Length - 1)
            End If
            For i As Integer = 0 To _densities.Length - 1
                real = reals(i)
                imaginary = imaginaries(i)
                Me.Densities(i) += real * real + imaginary * imaginary
            Next

            ' increment the average count
            Me._AverageCount += 1

        End Sub

#End Region

#Region " DOUBLE APPEND "

        ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Double)

            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            ' Initialize imaginary array.
            Dim imaginaries() As Double = {}
            ReDim imaginaries(reals.Length - 1)

            ' calculate the spectrum
            MyBase.Calculate(reals, imaginaries)

            ' add the spectrum
            Me.Append(reals, imaginaries)

        End Sub

        ''' <summary>Calculates the spectrum and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If

            ' calculate the spectrum
            MyBase.Calculate(reals, imaginaries)

            ' add the spectrum
            Me.Append(reals, imaginaries)

        End Sub

        ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="dataPoints">Specifies the number of data points to use.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Double, ByVal dataPoints As Integer)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If dataPoints = reals.Length Then
                Me.TransformAppend(reals)
            Else
                ' Get the data
                Dim realPart() As Double = {}
                ReDim realPart(dataPoints - 1)
                DoubleExtensions.Copy(reals, realPart)
                ' reals.CopyTo(realPart, 0)
                If reals.Length < realPart.Length Then
                    For i As Integer = reals.Length To realPart.Length - 1
                        realPart(i) = realPart(i - 1)
                    Next
                End If
                Me.TransformAppend(realPart)
            End If

        End Sub

#End Region

#Region " DOUBLE SPECTRUM "

        ''' <summary>Gets or sets the condition for the densities were scaled.  Once the densities
        '''   are scaled no more averages can be append until the spectrum
        '''   is <see cref="Restore">resotored</see></summary>
        Private _Scaled As Boolean

        ''' <summary>Returns the length of the densities array.</summary>
        Public ReadOnly Property DensitiesLength() As Integer
            Get
                If _Densities Is Nothing Then
                    Return 0
                Else
                    Return _Densities.Length
                End If
            End Get
        End Property

        ''' <summary>Copies the density and the number of averages.</summary>
        ''' <param name="values"></param>
        ''' <param name="averageCount">Specifies the number of averages used to calculate
        '''   the densities.</param>
        ''' <param name="spectrumCount">Specifies the number of points to copy.</param>
        Public Sub CopyToDensities(ByVal values() As Double, ByVal averageCount As Integer, ByVal spectrumCount As Integer)
            If values Is Nothing Then
                Me._Densities = Nothing
            Else
                Me._Scaled = True
                Me._AverageCount = averageCount
                ReDim Me._Densities(spectrumCount - 1)
                Array.Copy(values, Me._Densities, spectrumCount)
            End If
        End Sub

        ''' <summary>Returns the indexed density.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        ''' <param name="index"></param>
        Public Function Density(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double
            If Me._Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If index < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(index), index, "Index must exceed lower bound")
            End If
            If index > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(index), index, "Index must not exceed spectrum length")
            End If

            If Me._Scaled Then

                If halfSpectrum Then

                    If index = 0 Then
                        Return Me._Densities(index)
                    Else
                        Dim tempValue As Double = Me._Densities(index)
                        Return tempValue + tempValue
                    End If

                Else

                    Return Me._Densities(index)

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)
                If index = 0 And halfSpectrum Then
                    Return 0.5R * factor * Me._Densities(index)
                Else
                    Return factor * Me._Densities(index)
                End If

            End If

        End Function

        Private _Densities() As Double

        ''' <summary>Returns the unscaled (if not scaled) spectrum densities.
        ''' </summary>
        ''' <remarks>Unlike SRE5, the spectrum densities are scaled for full spectrum rather
        '''   than as a half spectrum.  This simplifies greatly the retrieval of 
        '''   spectrum magnitude or power for display.
        ''' </remarks>
        Public Overloads Function Densities() As Double()
            Return Me._Densities
        End Function

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        Public Overloads Function DensitiesDouble() As Double()

            If Me.Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            ' allocate the outcome array
            Dim output(Me._Densities.Length - 1) As Double

            If Me._Scaled Then

                ReDim output(Me._Densities.Length - 1)
                For i As Integer = 0 To Me._Densities.Length - 1
                    output(i) = Me._Densities(i)
                Next

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)
                For i As Integer = 0 To Me._Densities.Length - 1
                    output(i) = factor * Me._Densities(i)
                Next

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        Public Overloads Function DensitiesDouble(ByVal halfSpectrum As Boolean) As Double()

            If Me._Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            Dim output() As Double

            If _Scaled Then

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = _Densities(0)
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = _Densities(i)
                        output(i) = tempValue + tempValue
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = _Densities(i)
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = 0.5R * factor * _Densities(0)
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = factor * _Densities(i)
                        output(i) = tempValue + tempValue
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = factor * _Densities(i)
                    Next

                End If

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled
        '''   for displaying the half spectrum.</param>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Overloads Function DensitiesDouble(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' allocate outcome space
            Dim output(toIndex - fromIndex) As Double

            If _Scaled Then

                If halfSpectrum Then

                    Dim j As Integer = 0
                    If fromIndex = 0 Then
                        output(j) = _Densities(fromIndex)
                        fromIndex += 1
                        j += 1
                    End If
                    For i As Integer = fromIndex To toIndex
                        Dim tempValue As Double = _Densities(i)
                        output(j) = tempValue + tempValue
                        j += 1
                    Next

                Else

                    Dim j As Integer = 0
                    ReDim output(toIndex - fromIndex)
                    For i As Integer = fromIndex To toIndex
                        output(j) = _Densities(i)
                        j += 1
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

                Dim j As Integer = 0
                If fromIndex = 0 And halfSpectrum Then
                    output(j) = 0.5R * factor * _Densities(fromIndex)
                    fromIndex += 1
                    j += 1
                End If

                ' Scale the rest of the spectrum
                For i As Integer = fromIndex To toIndex
                    output(j) = factor * _Densities(i)
                    j += 1
                Next i

            End If

            Return output

        End Function

        ''' <summary>Returns the indexed magnitude.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        ''' <param name="index"></param>
        Public Function Magnitude(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If index < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(index), index, "Index must exceed lower bound")
            End If
            If index > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(index), index, "Index must not exceed spectrum length")
            End If

            Return Math.Sqrt(Me.Density(halfSpectrum, index))

        End Function

        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        Public Function MagnitudesDouble() As Double()
            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            ' allocate the outcome array
            Dim output(_Densities.Length - 1) As Double

            If _Scaled Then

                ReDim output(_Densities.Length - 1)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = Math.Sqrt(_Densities(i))
                Next

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = Math.Sqrt(factor * _Densities(i))
                Next

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        Public Function MagnitudesDouble(ByVal halfSpectrum As Boolean) As Double()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            Dim output() As Double

            If _Scaled Then

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = Math.Sqrt(_Densities(0))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = _Densities(i)
                        output(i) = Math.Sqrt(tempValue + tempValue)
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = Math.Sqrt(_Densities(i))
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = Math.Sqrt(0.5R * factor * _Densities(0))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = factor * _Densities(i)
                        output(i) = Math.Sqrt(tempValue + tempValue)
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = Math.Sqrt(factor * _Densities(i))
                    Next

                End If

            End If

            Return output

        End Function


        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled
        '''   for displaying the half spectrum.</param>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Function MagnitudesDouble(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' allocate outcome space
            Dim output(toIndex - fromIndex) As Double

            If _Scaled Then

                If halfSpectrum Then

                    Dim j As Integer = 0
                    If fromIndex = 0 Then
                        output(j) = Math.Sqrt(_Densities(fromIndex))
                        fromIndex += 1
                        j += 1
                    End If
                    For i As Integer = fromIndex To toIndex
                        Dim tempValue As Double = _Densities(i)
                        output(j) = Math.Sqrt(tempValue + tempValue)
                        j += 1
                    Next

                Else

                    Dim j As Integer = 0
                    ReDim output(toIndex - fromIndex)
                    For i As Integer = fromIndex To toIndex
                        output(j) = Math.Sqrt(_Densities(i))
                        j += 1
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

                Dim j As Integer = 0
                If fromIndex = 0 And halfSpectrum Then
                    output(j) = Math.Sqrt(0.5R * factor * _Densities(fromIndex))
                    fromIndex += 1
                    j += 1
                End If

                ' Scale the rest of the spectrum
                For i As Integer = fromIndex To toIndex
                    output(j) = Math.Sqrt(factor * _Densities(i))
                    j += 1
                Next i

            End If

            Return output

        End Function


        ''' <summary>Restore the densities to their pre-scaled values so that additional averages
        '''   can be added.</summary>
        Public Sub Restore()

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            If factor <= 0 Then
                Throw New InvalidOperationException("Restoration spectrum scale factor must be positive.")
            Else
                factor = 1.0R / factor
            End If

            ' un-scale the rest of the spectrum
            For i As Integer = 0 To _Densities.GetUpperBound(0)
                Me._Densities(i) *= factor
            Next i

            ' tag power spectrum as not scaled allowing adding spectrum averages.
            _Scaled = False

        End Sub


        ''' <summary>Scales the spectrum densities based on the number of averages.</summary>
        Public Sub Scale()

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            ' Scale the rest of the spectrum
            For i As Integer = 0 To _Densities.GetUpperBound(0)
                Me._Densities(i) *= factor
            Next i

            ' tag power spectrum as scaled.  This prevents adding new averages until
            ' the spectrum is cleared or restored.
            _Scaled = True

        End Sub

#End Region

#Region " SINGLE TIME SERIES "

        ''' <summary>Appends the spectrum to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub Append(ByVal reals() As Single, ByVal imaginaries() As Single)

            If Me._Scaled Then
                Throw New InvalidOperationException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
            End If
            If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            Dim real As Double
            Dim imaginary As Double
            If Me.Densities Is Nothing Then
                ReDim _Densities(reals.Length - 1)
            End If
            For i As Integer = 0 To _densities.Length - 1
                real = reals(i)
                imaginary = imaginaries(i)
                Me.Densities(i) += real * real + imaginary * imaginary
            Next

            ' increment the average count
            Me._AverageCount += 1

        End Sub

#End Region

#Region " SINGLE SPECTRUM "

        ''' <summary>Copies the density and the number of averages.</summary>
        ''' <param name="values"></param>
        ''' <param name="averageCount">Specifies the number of averages used to calculate
        '''   the densities.</param>
        ''' <param name="spectrumCount">Specifies the number of points to copy.</param>
        Public Sub CopyToDensities(ByVal values() As Single, ByVal averageCount As Integer, ByVal spectrumCount As Integer)

            If values Is Nothing Then
                Me._Densities = Nothing
            Else
                Me._Scaled = True
                Me._AverageCount = averageCount
                ReDim Me._Densities(spectrumCount - 1)
                Array.Copy(values, Me._Densities, spectrumCount)
            End If
        End Sub

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        Public Overloads Function DensitiesSingle(ByVal halfSpectrum As Boolean) As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            Dim output() As Single

            If _Scaled Then

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = CSng(Me._Densities(0))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = _Densities(i)
                        output(i) = CSng(tempValue + tempValue)
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = CSng(Me._Densities(i))
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = CSng(0.5R * factor * _Densities(0))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = factor * _Densities(i)
                        output(i) = CSng(tempValue + tempValue)
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = CSng(factor * _Densities(i))
                    Next

                End If

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        Public Overloads Function DensitiesSingle() As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            ' allocate the outcome array
            Dim output(_Densities.Length - 1) As Single

            If _Scaled Then

                ReDim output(_Densities.Length - 1)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = CSng(_Densities(i))
                Next

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = CSng(factor * _Densities(i))
                Next

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled spectrum densities.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled
        '''   for displaying the half spectrum.</param>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Overloads Function DensitiesSingle(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' allocate outcome space
            Dim output(toIndex - fromIndex) As Single

            If _Scaled Then

                If halfSpectrum Then

                    Dim j As Integer = 0
                    If fromIndex = 0 Then
                        output(j) = CSng(_Densities(fromIndex))
                        fromIndex += 1
                        j += 1
                    End If
                    For i As Integer = fromIndex To toIndex
                        Dim tempValue As Double = _Densities(i)
                        output(j) = CSng(tempValue + tempValue)
                        j += 1
                    Next

                Else

                    Dim j As Integer = 0
                    ReDim output(toIndex - fromIndex)
                    For i As Integer = fromIndex To toIndex
                        output(j) = CSng(_Densities(i))
                        j += 1
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

                Dim j As Integer = 0
                If fromIndex = 0 And halfSpectrum Then
                    output(j) = CSng(0.5R * factor * _Densities(fromIndex))
                    fromIndex += 1
                    j += 1
                End If

                ' Scale the rest of the spectrum
                For i As Integer = fromIndex To toIndex
                    output(j) = CSng(factor * _Densities(i))
                    j += 1
                Next i

            End If

            Return output

        End Function


        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        Public Function MagnitudesSingle() As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            ' allocate the outcome array
            Dim output(_Densities.Length - 1) As Single

            If _Scaled Then

                ReDim output(_Densities.Length - 1)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = CSng(Math.Sqrt(_Densities(i)))
                Next

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)
                For i As Integer = 0 To _Densities.Length - 1
                    output(i) = CSng(Math.Sqrt(factor * _Densities(i)))
                Next

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
        Public Function MagnitudesSingle(ByVal halfSpectrum As Boolean) As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            Dim output() As Single

            If _Scaled Then

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = CSng(Math.Sqrt(_Densities(0)))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = _Densities(i)
                        output(i) = CSng(Math.Sqrt(tempValue + tempValue))
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = CSng(Math.Sqrt(_Densities(i)))
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(False)

                If halfSpectrum Then

                    ReDim output(MyBase.HalfSpectrumLength - 1)
                    output(0) = CSng(Math.Sqrt(0.5R * factor * _Densities(0)))
                    For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                        Dim tempValue As Double = factor * _Densities(i)
                        output(i) = CSng(Math.Sqrt(tempValue + tempValue))
                    Next

                Else

                    ReDim output(_Densities.Length - 1)
                    For i As Integer = 0 To _Densities.Length - 1
                        output(i) = CSng(Math.Sqrt(factor * _Densities(i)))
                    Next

                End If

            End If

            Return output

        End Function

        ''' <summary>Returns the scaled magnitude spectrum.
        ''' </summary>
        ''' <param name="halfSpectrum">Specifies is the scaling is doubled
        '''   for displaying the half spectrum.</param>
        ''' <param name="fromIndex"></param>
        ''' <param name="toIndex"></param>
        Public Function MagnitudesSingle(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Single()

            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If
            If fromIndex < _Densities.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
            End If
            If fromIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
            End If
            If toIndex < fromIndex Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
            End If
            If toIndex > _Densities.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")
            End If

            ' allocate outcome space
            Dim output(toIndex - fromIndex) As Single

            If _Scaled Then

                If halfSpectrum Then

                    Dim j As Integer = 0
                    If fromIndex = 0 Then
                        output(j) = CSng(Math.Sqrt(_Densities(fromIndex)))
                        fromIndex += 1
                        j += 1
                    End If
                    For i As Integer = fromIndex To toIndex
                        Dim tempValue As Double = _Densities(i)
                        output(j) = CSng(Math.Sqrt(tempValue + tempValue))
                        j += 1
                    Next

                Else

                    Dim j As Integer = 0
                    ReDim output(toIndex - fromIndex)
                    For i As Integer = fromIndex To toIndex
                        output(j) = CSng(Math.Sqrt(_Densities(i)))
                        j += 1
                    Next

                End If

            Else

                Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

                Dim j As Integer = 0
                If fromIndex = 0 And halfSpectrum Then
                    output(j) = CSng(Math.Sqrt(0.5R * factor * _Densities(fromIndex)))
                    fromIndex += 1
                    j += 1
                End If

                ' Scale the rest of the spectrum
                For i As Integer = fromIndex To toIndex
                    output(j) = CSng(Math.Sqrt(factor * _Densities(i)))
                    j += 1
                Next i

            End If

            Return output

        End Function

#End Region

#Region " SINGLE APPEND "

        ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Single)

            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            ' Initialize imaginary array.
            Dim imaginaries() As Single = {}
            ReDim imaginaries(reals.Length - 1)

            ' calculate the spectrum
            MyBase.Calculate(reals, imaginaries)

            ' add the spectrum
            Me.Append(reals, imaginaries)

        End Sub

        ''' <summary>Calculates the spectrum and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Single, ByVal imaginaries() As Single)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If

            ' calculate the spectrum
            MyBase.Calculate(reals, imaginaries)

            ' add the spectrum
            Me.Append(reals, imaginaries)

        End Sub

        ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="dataPoints">Specifies the number of data points to use.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub TransformAppend(ByVal reals() As Single, ByVal dataPoints As Integer)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If dataPoints = reals.Length Then
                TransformAppend(reals)
            Else
                ' Get the data
                Dim realPart() As Single = {}
                ReDim realPart(dataPoints - 1)
                If reals.Length < realPart.Length Then
                    For i As Integer = reals.Length To realPart.Length - 1
                        realPart(i) = realPart(i - 1)
                    Next
                End If
                Me.TransformAppend(realPart)
            End If
        End Sub

#End Region

#Region " FILE I/O "

        ''' <summary>Reads spectrum densities from the .</summary>
        ''' <param name="reader">Specifies reference to an open binary reader.</param>
        ''' <remarks>Returns values as an array.</remarks>
        Public Overloads Sub ReadDensities(ByVal reader As IO.BinaryReader)

            If reader Is Nothing Then
                Throw New ArgumentNullException("reader")
            End If

            ' assume the densities were scaled.
            Me._Scaled = True

            ' read the densities parameters
            Me._AverageCount = reader.ReadInt32
            Dim elementCount As Integer = reader.ReadInt32

            ' allocate data array
            ReDim _Densities(elementCount - 1)

            ' Read the densities from the file
            For i As Integer = 0 To elementCount - 1
                _Densities(i) = reader.ReadDouble
            Next i

        End Sub


        ''' <summary>Writes the densities to a data file.  The densities are 
        '''   written as scaled.</summary>
        ''' <param name="writer">Specifies reference to an open binary writer.</param>
        Public Overloads Sub WriteDensities(ByVal writer As System.IO.BinaryWriter)

            If writer Is Nothing Then Throw New ArgumentNullException(NameOf(writer))
            If _Densities Is Nothing Then
                Throw New InvalidOperationException("Spectrum densities do not exist.")
            End If

            ' write the spectrum parameters
            writer.Write(Me._averageCount)

            Dim elementCount As Integer = Me._densities.Length
            writer.Write(elementCount)

            If _scaled Then
                ' write values to the file
                For i As Integer = 0 To elementCount - 1
                    writer.Write(_densities(i))
                Next i
            Else
                Dim factor As Double = Me.SpectrumScaleFactor(False)
                For i As Integer = 0 To elementCount - 1
                    writer.Write(factor * _densities(i))
                Next i
            End If

        End Sub

#End Region

    End Class

End Namespace
