﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Calculates the Fast Fourier Transform using the Meta Numerics Library. </summary>
    /// <remarks>
    /// Includes procedures for calculating the forward and inverse Fourier transform.  <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-01-20 </para>
    /// </remarks>
    public class MixedRadixFourierTransform : FourierTransformBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public MixedRadixFourierTransform() : base( FourierTransformType.MixedRadix )
        {
        }

        #endregion

        #region " COMPLEX "

        /// <summary> Calculates a Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="values"> Takes the time series and returns the spectrum values. </param>
        public override void Forward( Complex[] values )
        {
            base.Forward( values );
        }

        /// <summary> Initializes and performs the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the inputs and returns the outputs. </param>
        protected override void Initialize( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            var ft = new FourierTransformer( values.Length );
            ft.Transform( values ).CopyTo( values );
        }

        #endregion

        #region " DOUBLE "

        /// <summary> Calculates a Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( double[] reals, double[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary> Initializes and performs the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            var ft = new FourierTransformer( reals.Length );
            ft.Transform( reals, imaginaries ).CopyTo( reals, imaginaries );
        }

        #endregion

        #region " SINGLE "

        /// <summary> Calculates a Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( float[] reals, float[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary> Initializes and performs the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            var ft = new FourierTransformer( reals.Length );
            ft.Transform( reals, imaginaries ).CopyTo( reals, imaginaries );
        }

        #endregion


    }
}