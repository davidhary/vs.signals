Imports Microsoft.VisualStudio.TestTools.UnitTesting

''' <summary> Wisdom unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2017 </para>
''' </remarks>
<TestClass()>
Public Class WisdomTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Algorithms.Signals.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(SignalsTests.WisdomSettings.Get.Exists, $"{GetType(SignalsTests.WisdomSettings)} not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SPECTRUM TESTS "

    ''' <summary> Tests double precision spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fft">        The FFT. </param>
    ''' <param name="scaleFft">   True to scale FFT. </param>
    ''' <param name="removeMean"> True to remove mean. </param>
    Private Shared Sub DoublePrecisionSpectrumTest(ByVal fft As isr.Algorithms.Signals.FourierTransformBase, ByVal scaleFft As Boolean, ByVal removeMean As Boolean)
        ' scale the FFT by the Window power and data points
        ' Remove mean before calculating the fft
        Using spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft) With {.IsScaleFft = scaleFft, .IsRemoveMean = removeMean}
            spectrum.IsScaleFft = scaleFft
            spectrum.IsRemoveMean = removeMean
            Assert.AreEqual(scaleFft, spectrum.IsScaleFft, $"{NameOf(Signals.Spectrum.IsScaleFft)} should be {scaleFft}")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests double precision discrete fourier transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <TestMethod()>
    Public Sub DoublePrecisionDiscreteFourierTransformTest()
        Dim scaleFft As Boolean = True
        Dim removemean As Boolean = True
        Using fft As isr.Algorithms.Signals.FourierTransformBase = New isr.Algorithms.Signals.Wisdom.Dft
            WisdomTests.DoublePrecisionSpectrumTest(fft, scaleFft, removemean)
        End Using
    End Sub

#End Region

End Class
