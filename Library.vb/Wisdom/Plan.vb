Namespace Wisdom

    ''' <summary> Base class for storing and destroying FFTW plans. </summary>
    ''' <remarks>
    ''' David, 09/01/07, 2.0.2800. Created <para>
    ''' The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    ''' fastest way to compute the transform on your machine. The planner produces a data structure
    ''' called a plan that contains this information. Subsequently, the plan is executed to transform
    ''' the array of input data as dictated by the plan. </para><para>
    ''' (c) 2007 Integrated Scientific Resources, Inc. </para><para>
    ''' Licensed under The MIT License.</para><para>
    ''' </para>
    ''' </remarks>
    Public MustInherit Class Plan
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs a plan. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="plan"> A <see cref="IntPtr">handle</see> to the plan. </param>
        Protected Sub New(ByVal plan As IntPtr)
            MyBase.New()
            If plan = IntPtr.Zero Then Throw New ArgumentNullException(NameOf(plan), "Invalid memory reference")
            Me._Handle = plan
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if disposed. </summary>
        Private _Disposed As Boolean

        ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
        ''' <value> The disposed. </value>
        Protected Property Disposed() As Boolean
            Get
                Return Me._Disposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._Disposed = value
            End Set
        End Property

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.Disposed = True

            End Try

        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary> Gets or sets the handle to the plan. </summary>
        ''' <value> The handle. </value>
        Public ReadOnly Property Handle() As IntPtr

#End Region

    End Class

#Region " SINGLE PRECISION "

    ''' <summary> Stores and destroys FFTW plans. </summary>
    ''' <remarks>
    ''' The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    ''' fastest way to compute the transform on your machine. The planner produces a data structure
    ''' called a plan that contains this information. Subsequently, the plan is executed to transform
    ''' the array of input data as dictated by the plan. <para>
    ''' David, 09/01/07, 2.0.2800 </para><para>
    ''' (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    ''' Licensed under The MIT License. </para>
    ''' </remarks>
    Public Class PlanF
        Inherits Plan

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs a plan. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="plan"> A <see cref="IntPtr">handle</see> to the plan. </param>
        Public Sub New(ByVal plan As IntPtr)
            MyBase.New(plan)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.Disposed Then
                    If disposing Then
                        ' Free managed resources when explicitly called
                    End If
                    ' Free shared unmanaged resources
                    PlanF.Destroy(MyBase.Handle)
                End If
            Finally
                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary>
        ''' Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
        ''' free.
        ''' </summary>
        ''' <remarks>
        ''' After calling Cleanup, all existing plans become undefined, and you should not attempt to
        ''' execute them nor to destroy them. You can however create and execute/destroy new plans, in
        ''' which case FFTW starts accumulating wisdom information again. Cleanup does not deallocate
        ''' your plans; you should still call fftw_destroy_plan for this purpose.
        ''' </remarks>
        Public Shared Sub Cleanup()
            FftwF.SafeNativeMethods.Cleanup()
        End Sub

        ''' <summary> Deallocates an FFTW plan and all associated resources. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="plan"> Pointer to the plan to release. </param>
        Public Shared Sub Destroy(ByVal plan As IntPtr)
            If plan <> IntPtr.Zero Then
                FftwF.SafeNativeMethods.Destroy_plan(plan)
            End If
        End Sub

        ''' <summary>
        ''' Executes the FFTW plan, provided that the input and output arrays still exist.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Public Sub Execute()
            FftwF.SafeNativeMethods.Execute(MyBase.Handle)
        End Sub

        ''' <summary> The time limit. </summary>
        Private Shared _TimeLimit As Double

        ''' <summary> Gets or sets the maximum time that can be used by the planner. </summary>
        ''' <remarks>
        ''' Instructs FFTW to spend at most seconds seconds (approximately) in the planner. If seconds ==
        ''' -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW plans with a
        ''' progressively wider range of algorithms until the the given time limit is reached or the
        ''' given range of algorithms is explored, returning the best available plan. For example,
        ''' specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode, then
        ''' finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead, the
        ''' planner will further progress to Exhaustive mode.
        ''' </remarks>
        ''' <value> The time limit. </value>
        Public Shared Property TimeLimit() As Double
            Get
                Return _TimeLimit
            End Get
            Set(ByVal value As Double)
                _TimeLimit = value
                FftwF.SafeNativeMethods.Set_timelimit(value)
            End Set
        End Property

#End Region

    End Class

#End Region

#Region " DOUBLE PRECISION "

    ''' <summary> Stores and destroys FFTW plans. </summary>
    ''' <remarks>
    ''' David, 09/01/07, 2.0.2800. Create.  <para>
    ''' The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    ''' fastest way to compute the transform on your machine. The planner produces a data structure
    ''' called a plan that contains this information. Subsequently, the plan is executed to transform
    ''' the array of input data as dictated by the plan. (c) 2007 Integrated Scientific Resources,
    ''' Inc. </para><para>
    ''' Licensed under The MIT License. </para>
    ''' </remarks>
    Public Class PlanR

        Inherits Plan

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs a plan. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="plan"> A <see cref="IntPtr">handle</see> to the plan. </param>
        Public Sub New(ByVal plan As IntPtr)
            MyBase.New(plan)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    PlanR.Destroy(MyBase.Handle)

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary> Deallocates an FFTW plan and all associated resources. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="plan"> Pointer to the plan to release. </param>
        Public Shared Sub Destroy(ByVal plan As IntPtr)
            If plan <> IntPtr.Zero Then
                FftwR.SafeNativeMethods.Destroy_plan(plan)
            End If
        End Sub

        ''' <summary>
        ''' Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
        ''' free.
        ''' </summary>
        ''' <remarks>
        ''' After calling Cleanup, all existing plans become undefined, and you should not attempt to
        ''' execute them nor to destroy them. You can however create and execute/destroy new plans, in
        ''' which case FFTW starts accumulating wisdom information again. Cleanup does not deallocate
        ''' your plans; you should still call fftw_destroy_plan for this purpose.
        ''' </remarks>
        Public Shared Sub Cleanup()
            FftwR.SafeNativeMethods.Cleanup()
        End Sub

        ''' <summary>
        ''' Executes the FFTW plan, provided that the input and output arrays still exist.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Public Sub Execute()
            FftwR.SafeNativeMethods.Execute(MyBase.Handle)
        End Sub

        ''' <summary> The time limit. </summary>
        Private Shared _TimeLimit As Double

        ''' <summary> Gets or sets the maximum time that can be used by the planner. </summary>
        ''' <remarks>
        ''' Instructs FFTW to spend at most seconds seconds (approximately) in the planner. If seconds ==
        ''' -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW plans with a
        ''' progressively wider range of algorithms until the the given time limit is reached or the
        ''' given range of algorithms is explored, returning the best available plan. For example,
        ''' specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode, then
        ''' finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead, the
        ''' planner will further progress to Exhaustive mode.
        ''' </remarks>
        ''' <value> The time limit. </value>
        Public Shared Property TimeLimit() As Double
            Get
                Return _TimeLimit
            End Get
            Set(ByVal value As Double)
                _TimeLimit = value
                FftwF.SafeNativeMethods.Set_timelimit(value)
            End Set
        End Property

#End Region

    End Class

#End Region

End Namespace

