


''' <summary> Contains methods that compute advanced functions with real arguments. </summary>
''' <remarks>
''' Trig: internal utility trig functions that are accurate for large arguments Factor: Prime
''' factorization. (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Partial Public NotInheritable Class AdvancedMath

    ''' <summary> Computes the Sine for large arguments. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The x. </param>
    ''' <param name="y"> The y. </param>
    ''' <returns> The Sine for large arguments. </returns>
    Friend Shared Function Sin(ByVal x As Double, ByVal y As Double) As Double
        Return Math.Sin(Reduce(x, y))
    End Function

    ''' <summary> Computes the Cosine for large arguments. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The x. </param>
    ''' <param name="y"> The y. </param>
    ''' <returns> The Cosine for large arguments. </returns>
    Friend Shared Function Cos(ByVal x As Double, ByVal y As Double) As Double
        Return Math.Cos(Reduce(x, y))
    End Function

    ''' <summary> The maximum value. </summary>
    Private Shared ReadOnly MaxValue As Double = Convert.ToDouble(Decimal.MaxValue)

    ''' <summary> The two pi. </summary>
    Public Const TwoPI As Decimal = 2D * 3.1415926535897932384626433833D

    ''' <summary> Reduces an argument to its corresponding argument between -2Pi and 2Pi. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The x. </param>
    ''' <param name="y"> The y. </param>
    ''' <returns> The reduced argument to its corresponding argument between -2Pi and 2Pi. </returns>
    Friend Shared Function Reduce(ByVal x As Double, ByVal y As Double) As Double

        Dim t As Double = x + TwoPI * y
        If (Math.Abs(t) < 64.0) OrElse (Math.Abs(t) > MaxValue) Then
            ' if the argument is small we don't need the high accuracy reduction
            ' if the argument is too big, we can't do the high accuracy reduction because it would overflow a decimal variable
            Return t
        Else
            ' otherwise, convert to decimal, subtract a multiple of 2 Pi, and return

            ' reduce x by factors of 2 Pi
            Dim dx As Decimal = Convert.ToDecimal(x)
            Dim dn As Decimal = Decimal.Truncate(CDec(dx / TwoPI))
            dx -= CDec(dn * TwoPI)

            ' reduce y by factors of 1
            Dim dy As Decimal = Convert.ToDecimal(y)
            Dim dm As Decimal = Decimal.Truncate(dy / 1D)
            dy -= dm * 1D

            ' form the argument
            Dim dt As Decimal = CDec(dx + dy * TwoPI)
            Return Convert.ToDouble(dt)

        End If
    End Function

End Class
