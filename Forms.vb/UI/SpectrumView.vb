Imports System.Numerics
Imports isr.Core.EnumExtensions
Imports isr.Algorithms.Signals.ExceptionExtensions

''' <summary>
''' A user interface for calculating the spectrum using DFT, Mixed Radix, and sliding Fourier
''' transform algorithms.
''' </summary>
''' <remarks>
''' David, 11/17/2005. from FFT Pro. <para>
''' Launch this form by calling its Show or ShowDialog method from its default instance.
''' </para><para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class SpectrumView
    Inherits isr.Core.Forma.ModelViewTalkerBase

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        Me.InitializingComponents = False

        Me._MessagesList.CommenceUpdates()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            Me.InitializingComponents = True
            If disposing Then
                Me._MessagesList.SuspendUpdatesReleaseIndicators()

                ' Free managed resources when explicitly called
                If Me._SignalChartPane IsNot Nothing Then
                    Me._SignalChartPane.Dispose()
                    Me._SignalChartPane = Nothing
                End If

                If Me._SpectrumChartPan IsNot Nothing Then
                    Me._SpectrumChartPan.Dispose()
                    Me._SpectrumChartPan = Nothing
                End If

                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        Finally
            Me.InitializingComponents = False

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region

    #Region " PROPERTIES "

    ''' <summary> Gets the selected example. </summary>
    ''' <value> The selected example. </value>
    Private ReadOnly Property SelectedExample() As Example
        Get
            Return CType(CType(Me._ExampleComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, Example)
        End Get
    End Property

    ''' <summary> Gets the selected SignalType. </summary>
    ''' <value> The type of the selected signal. </value>
    Private ReadOnly Property SelectedSignalType() As SignalType
        Get
            Return CType(CType(Me._SignalComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, SignalType)
        End Get
    End Property

    ''' <summary> Gets the selected TaperFilterType. </summary>
    ''' <value> The type of the selected taper filter. </value>
    Private ReadOnly Property SelectedTaperFilterType() As isr.Algorithms.Signals.TaperFilterType
        Get
            Return CType(CType(Me._FilterComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, TaperFilterType)
        End Get
    End Property

    #End Region

    #Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown. This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SPECTRUM VIEW")

            ' Initialize and set the user interface
            Me.InitializeUserInterface()

            ' turn on the loaded flag
            ' loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    #End Region

    #Region " USER INTERFACE "

    ''' <summary> Shows the status. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="message"> The message. </param>
    Private Sub ShowStatus(ByVal message As String)

        Me._MessagesList.AddMessage(message)
        Me._StatusToolStripStatusLabel.Text = message

    End Sub

    ''' <summary>Gets or sets the data format</summary>
    Private Const _ListFormat As String = "{0:0.000000000000000}{1}{2:0.000000000000000}"

    ''' <summary> Enumerates the action options. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Enum Example

        ''' <summary> An enum constant representing the discrete Fourier transform option. </summary>
        <System.ComponentModel.Description("Discrete Fourier Transform")> DiscreteFourierTransform

        ''' <summary> An enum constant representing the sliding Fourier transform option. </summary>
        <System.ComponentModel.Description("Sliding FFT")> SlidingFFT

        ''' <summary> An enum constant representing the Mixed Radix Fourier transform option. </summary>
        <System.ComponentModel.Description("Mixed Radix FFT")> MixedRadixFFT

        ''' <summary> An enum constant representing the Wisdom (FFTW) transform option. </summary>
        <System.ComponentModel.Description("Wisdom FFT")> WisdomFFT
    End Enum

    ''' <summary> Enumerates the signal options. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Enum SignalType

        ''' <summary> An enum constant representing the sine wave option. </summary>
        <System.ComponentModel.Description("Sine")> SineWave

        ''' <summary> An enum constant representing the random option. </summary>
        <System.ComponentModel.Description("Random")> Random
    End Enum

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub InitializeUserInterface()

        ' tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        ' tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

        ' Set initial values defining the signal
        Me._PointsTextBox.Text = "1000"
        Me._CyclesTextBox.Text = "1" ' "11.5"
        Me._PhaseTextBox.Text = "0" ' "45"
        Me._PointsToDisplayTextBox.Text = "100"
        Me._SignalDurationTextBox.Text = "1"

        ' set the default sample
        Me.PopulateComboBoxs()

        ' create the two charts.
        Me.CreateSpectrumChart()
        Me.CreateSignalChart()

        ' plot the signal
        Me.UpdateSignal()

    End Sub

    #End Region

    #Region " SIGNALS "

    ''' <summary> Updates the signal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub UpdateSignal()
        If Me._DoubleRadioButton.Checked Then
            Me.UpdateSignalDouble()
        Else
            Me.UpdateSignalSingle()
        End If
    End Sub

    ''' <summary> Gets the signal Single. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The signal. </returns>
    Private Function GetSignalSingle() As Single()

        Dim signal As Single()
        Select Case Me.SelectedSignalType
            Case SignalType.Random
                signal = Me.RandomWaveSingle()
            Case Else
                signal = Me.SineWaveSingle()
        End Select
        Return signal

    End Function

    ''' <summary> Updates the signal single. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new signal. </returns>
    Private Function UpdateSignalSingle() As Single()

        Dim signal As Single() = Me.GetSignalSingle()
        ' Plot the Signal 
        Dim duration As Single = Single.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim frequences As Single() = isr.Algorithms.Signals.Signal.Ramp(duration / signal.Length, signal.Length)
        Me.ChartSignal(frequences, signal)
        Return signal

    End Function

    ''' <summary> Gets the signal double. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The signal. </returns>
    Private Function GetSignalDouble() As Double()

        Dim signal As Double()
        Select Case Me.SelectedSignalType
            Case SignalType.Random
                signal = Me.RandomWaveDouble()
            Case Else
                signal = Me.SineWaveDouble()
        End Select
        Return signal

    End Function

    ''' <summary> Updates the signal double. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The signal. </returns>
    Private Function UpdateSignalDouble() As Double()

        Dim signal As Double() = Me.GetSignalDouble()
        ' Plot the Signal 
        Dim duration As Double = Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim frequences As Double() = isr.Algorithms.Signals.Signal.Ramp(duration / signal.Length, signal.Length)
        Me.ChartSignal(frequences, signal)
        Return signal

    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The sine wave signal. </returns>
    Private Function SineWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Double = Double.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        ' get the signal
        Return isr.Algorithms.Signals.Signal.Sine(signalCycles, signalPhase, signalPoints)
    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The sine wave signal. </returns>
    Private Function SineWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Single = Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        ' get the signal
        Return isr.Algorithms.Signals.Signal.Sine(signalCycles, CSng(signalPhase), signalPoints)
    End Function

    ''' <summary> Calculates the Random wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The random signal. </returns>
    Private Function RandomWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim dataPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Return isr.Algorithms.Signals.Signal.Random(1.0R + DateTimeOffset.Now.Second, dataPoints)
    End Function

    ''' <summary> Calculates the Random wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The random signal. </returns>
    Private Function RandomWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim dataPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signal() As Single = isr.Algorithms.Signals.Signal.Random(1.0F + DateTimeOffset.Now.Second, dataPoints)
        Return signal

    End Function

    ''' <summary> The copy signal format. </summary>
    Private Const _CopySigFormat As String = "    Copy Signal:  {0:0.###} ms "

    ''' <summary> The FFT initialize format. </summary>
    Private Const _FftInitFormat As String = " FFT Initialize:  {0:0} ms "

    ''' <summary> The FFT calculate format. </summary>
    Private Const _FftCalcFormat As String = "  FFT Calculate:  {0:0.###} ms "

    ''' <summary> The frequency calculate format. </summary>
    Private Const _FrqCalcFormat As String = "FFT Frequencies:  {0:0.###} ms "

    ''' <summary> The magnitude calculate format. </summary>
    Private Const _MagCalcFormat As String = "  FFT Magnitude:  {0:0.###} ms "

    ''' <summary> The inverse calculate format. </summary>
    Private Const _InvCalcFormat As String = "    Inverse FFT:  {0:0.###} ms "

    ''' <summary> The charting format. </summary>
    Private Const _ChartingFormat As String = "  Charting time:  {0:0} ms "

    #End Region

    #Region " SPECTRUM "

    ''' <summary>
    ''' Calculates the Spectrum based on the selected algorithm and displays the results.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub CalculateSpectrumDouble(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Dim timingTextBuilder As New System.Text.StringBuilder()
        timingTextBuilder.AppendLine(algorithm.Description)

        Me.ShowStatus($"Calculating double-precision {algorithm.Description} FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Double = Me.UpdateSignalDouble

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ SELECT PROPERTIES ---------------

        ' select the spectrum type
        Dim fft As isr.Algorithms.Signals.FourierTransformBase = Nothing
        Try

            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Algorithms.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Algorithms.Signals.MixedRadixFourierTransform
                Case Example.WisdomFFT
                    fft = New isr.Algorithms.Signals.Wisdom.Dft
                Case Else
                    Return
            End Select
            Using spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft)

                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the FFT
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                spectrum.TaperWindow = If(Me._TaperWindowCheckBox.Checked, New isr.Algorithms.Signals.BlackmanTaperWindow, Nothing)

                Const transitionBand As Double = 0.01
                Const lowFreq As Double = 0.2
                Const highFreq As Double = 0.4
                spectrum.TaperFilter = New isr.Algorithms.Signals.TaperFilter(Me.SelectedTaperFilterType)
                spectrum.SamplingRate = fftPoints
                spectrum.TaperFilter.TimeSeriesLength = fftPoints
                spectrum.TaperFilter.LowFrequency = spectrum.SamplingRate * lowFreq
                spectrum.TaperFilter.HighFrequency = spectrum.SamplingRate * highFreq
                spectrum.TaperFilter.TransitionBand = spectrum.SamplingRate * transitionBand
                Select Case Me.SelectedTaperFilterType
                    Case TaperFilterType.BandPass
                    Case TaperFilterType.BandReject
                    Case TaperFilterType.HighPass
                    Case TaperFilterType.LowPass
                    Case Else
                End Select

                ' Initialize the FFT.
                timeKeeper.Restart()
                spectrum.Initialize(fftValues)
                duration = timeKeeper.Elapsed

                timingTextBuilder.AppendFormat(_FftInitFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' ------------ CREATE the SIGNAL ---------------

                timeKeeper.Restart()
                signal.CopyTo(fftValues)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_CopySigFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' ------------ Calculate Forward and Inverse FFT ---------------

                ' Compute the FFT.
                timeKeeper.Restart()
                spectrum.Calculate(fftValues)
                duration = timeKeeper.Elapsed
                Me._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms"
                timingTextBuilder.AppendFormat(_FftCalcFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Double
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                timeKeeper.Restart()
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_MagCalcFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' Get the Frequency
                Dim frequencies() As Double

                timeKeeper.Restart()
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_FrqCalcFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' Compute the inverse transform.
                timeKeeper.Restart()
                fft.Inverse(fftValues)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_InvCalcFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' Display the forward and inverse data.
                Me.DisplayCalculationAccuracy(signal, fftValues)

                ' ------------ Plot FFT outcome ---------------
                timeKeeper.Restart()
                Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_ChartingFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then
                fft.Dispose()
            End If
            Me._TimingTextBox.Text = timingTextBuilder.ToString()
        End Try

    End Sub

    ''' <summary>
    ''' Calculates the Spectrum based on the selected algorithm and displays the results.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub CalculateSpectrumSingle(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Dim timingTextBuilder As New System.Text.StringBuilder()
        timingTextBuilder.AppendLine(algorithm.Description)

        Me.ShowStatus($"Calculating single-precision {algorithm.Description} FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Single = Me.UpdateSignalSingle

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim fft As isr.Algorithms.Signals.FourierTransformBase = Nothing
        Try
            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Algorithms.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Algorithms.Signals.MixedRadixFourierTransform
                Case Example.WisdomFFT
                    fft = New isr.Algorithms.Signals.Wisdom.Dft
                Case Else
                    Return
            End Select

            Using spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft)

                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the FFT
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                spectrum.TaperWindow = If(Me._TaperWindowCheckBox.Checked, New isr.Algorithms.Signals.BlackmanTaperWindow, Nothing)

                ' Initialize the FFT.
                timeKeeper.Restart()
                spectrum.Initialize(fftValues)
                duration = timeKeeper.Elapsed
                timingTextBuilder.AppendFormat(_FftInitFormat, duration.TotalMilliseconds)
                timingTextBuilder.AppendLine()

                ' Compute the FFT.
                timeKeeper.Restart()
                spectrum.Calculate(fftValues)
                duration = timeKeeper.Elapsed

                ' Display time
                Me._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms "

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Double
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

                ' Get the Frequency
                Dim frequencies() As Double
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

                ' Compute the inverse transform.
                fft.Inverse(fftValues)

                ' Display the forward and inverse data.
                Me.DisplayCalculationAccuracy(signal, fftValues)

                ' ------------ Plot FFT outcome ---------------
                Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then fft.Dispose()
            Me._TimingTextBox.Text = timingTextBuilder.ToString()
        End Try

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="signal">     The signal. </param>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Double, ByVal timeSeries() As Complex)

        If signal Is Nothing Then
            Exit Sub
        End If
        If timeSeries Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(timeSeries.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - timeSeries(i).Real
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture, _ListFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, timeSeries(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorToolStripStatusLabel.Text = $"{totalError:0.###E+0}"
        Me._StatusStrip.Invalidate()

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="signal">     The signal. </param>
    ''' <param name="timeSeries"> The time series. </param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Single, ByVal timeSeries() As Complex)

        If signal Is Nothing Then
            Exit Sub
        End If
        If timeSeries Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(timeSeries.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - timeSeries(i).Real
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture, _ListFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, timeSeries(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorToolStripStatusLabel.Text = $"{totalError:0.###E+0}"
        Me._StatusStrip.Invalidate()

    End Sub

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
    Private Sub PopulateComboBoxs()

        ' set the action list
        Me._ExampleComboBox.Items.Clear()
        Me._ExampleComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(Example)).ToList
        Me._ExampleComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._ExampleComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._ExampleComboBox.SelectedIndex = 0

        Me._SignalComboBox.Items.Clear()
        Me._SignalComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(SignalType)).ToList
        Me._SignalComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._SignalComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._SignalComboBox.SelectedIndex = 0

        Me._FilterComboBox.Items.Clear()
        Me._FilterComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(isr.Algorithms.Signals.TaperFilterType)).ToList
        Me._FilterComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._FilterComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._FilterComboBox.SelectedIndex = 0

    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    '''                            observing the changes in the spectrum. </param>
    Private Sub SlidingFft(ByVal noiseFigure As Double)

        Me.ShowStatus("Calculating double-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim duration As TimeSpan = TimeSpan.Zero
        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Algorithms.Signals.MixedRadixFourierTransform = New isr.Algorithms.Signals.MixedRadixFourierTransform
            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Restart()
            fft.Forward(fftValues)
            duration = timeKeeper.Elapsed
        End Using

        ' Display time
        Me._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms"

        ' Clear the data
        Me._SignalListBox.Items.Clear()

        ' Clear the error value
        Me._ErrorToolStripStatusLabel.Text = String.Empty

        ' Get the FFT magnitudes
        Dim magnitudes() As Double
        magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

        ' Get the Frequency
        Dim frequencies() As Double
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

        ' Initialize the last point of the signal to the last point.
        Dim lastSignalPoint As Integer = fftPoints - 1

        ' Initialize the first point of the signal to the last point.
        Dim firstSignalPoint As Integer = 0

        ' Calculate the phase of the last point of the sine wave
        Dim deltaPhase As Double = 2 * Math.PI * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / Convert.ToSingle(fftPoints)

        ' get the signal phase of the last point
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        signalPhase += deltaPhase * lastSignalPoint

        ' Use a new frequency for the sine wave to see how
        ' the old is phased out and the new gets in
        deltaPhase *= 2

        ' First element in the previous value of the time series
        Dim oldValue As Complex

        ' New value from the Signal
        Dim newValue As Complex

        ' Create a new instance of the sliding FFT class
        Using slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

            ' Initialize sliding FFT coefficients.
            slidingFft.Forward(fftValues)

            ' Recalculate the sliding FFT
            Do While Me._StartStopCheckBox.Checked

                ' Allow other events to occur
                Windows.Forms.Application.DoEvents()

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(slidingFft.Dft)

                ' Get the Frequency
                frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

                ' ------------ Plot the Signal ---------------

                Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text,
                                                                             Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

                ' ------------ Plot FFT outcome ---------------

                Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

                ' Update the previous values of the signal
                oldValue = New Complex(signal(firstSignalPoint), 0)

                ' Add some random noise to make it interesting.
                Dim noise As Double = noiseFigure * 2 * (Microsoft.VisualBasic.Rnd() - 0.5)

                ' Get new signal values.
                signalPhase += deltaPhase
                newValue = New Complex(System.Math.Sin(signalPhase) + noise, 0)

                ' Update the signal itself.

                ' Update the last point.
                lastSignalPoint += 1
                If lastSignalPoint >= fftPoints Then
                    lastSignalPoint = 0
                End If

                ' Update the first point.
                firstSignalPoint += 1
                If firstSignalPoint >= fftPoints Then
                    firstSignalPoint = 0
                End If

                ' Place new data in the signal itself.
                signal(lastSignalPoint) = newValue.Real

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                ' Calculate the sliding FFT coefficients.
                slidingFft.Update(newValue, oldValue)

            Loop
        End Using


    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    '''                            observing the changes in the spectrum. </param>
    Private Sub SlidingFft(ByVal noiseFigure As Single)

        Me.ShowStatus("Calculating single-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the signal
        Dim signal() As Single = Me.UpdateSignalSingle

        ' Allocate the spectrum array.
        Dim fftValues(fftPoints - 1) As Complex
        signal.CopyTo(fftValues)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Algorithms.Signals.MixedRadixFourierTransform = New isr.Algorithms.Signals.MixedRadixFourierTransform

            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Restart()
            fft.Forward(fftValues)
            Dim duration As TimeSpan = timeKeeper.Elapsed

            ' Display time
            Me._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms"

            ' Clear the data
            Me._SignalListBox.Items.Clear()
            Me._ErrorToolStripStatusLabel.Text = String.Empty

            ' Get the FFT magnitudes
            Dim magnitudes() As Double
            magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(fftValues)

            ' Get the Frequency
            Dim frequencies() As Double
            frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0, fftPoints)

            ' Initialize the last point of the signal to the last point.
            Dim lastSignalPoint As Integer = fftPoints - 1

            ' Initialize the first point of the signal to the last point.
            Dim firstSignalPoint As Integer = 0

            ' Calculate the phase of the last point of the sine wave
            Dim deltaPhase As Single = 2.0F * Convert.ToSingle(Math.PI) *
                Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints

            ' get the signal phase of the last point
            Dim signalPhase As Single = Convert.ToSingle(isr.Algorithms.Signals.Signal.ToRadians(
                                Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
            signalPhase += deltaPhase * lastSignalPoint

            ' Use a new frequency for the sine wave to see how
            ' the old is phased out and the new gets in
            deltaPhase *= 2

            ' First element in the previous value of the time series
            Dim oldValue As Complex

            ' New value from the Signal
            Dim newValue As Complex

            ' Create a new instance of the sliding FFT class
            Using slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

                ' Initialize sliding FFT coefficients.
                slidingFft.Forward(fftValues)

                ' Recalculate the sliding FFT
                Do While Me._StartStopCheckBox.Checked

                    ' Allow other events to occur
                    Windows.Forms.Application.DoEvents()

                    ' ------- Calculate FFT outcomes ---------

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                    ' Get the FFT magnitudes
                    magnitudes = isr.Algorithms.Signals.ComplexArrays.Magnitudes(slidingFft.Dft)

                    ' Get the Frequency
                    frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0, fftPoints)

                    ' ------------ Plot the Signal ---------------

                    Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text,
                                                                                 Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints),
                                                                         signal)

                    ' ------------ Plot FFT outcome ---------------

                    Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

                    ' Update the previous values of the signal
                    oldValue = New Complex(signal(firstSignalPoint), 0)

                    ' Add some random noise to make it interesting.
                    Dim noise As Double = noiseFigure * 2.0F * (Microsoft.VisualBasic.Rnd() - 0.5F)

                    ' Get new signal values.
                    signalPhase += deltaPhase
                    newValue = New Complex(System.Math.Sin(signalPhase) + noise, 0)

                    ' Update the last point.
                    lastSignalPoint += 1
                    If lastSignalPoint >= fftPoints Then
                        lastSignalPoint = 0
                    End If

                    ' Update the first point.
                    firstSignalPoint += 1
                    If firstSignalPoint >= fftPoints Then
                        firstSignalPoint = 0
                    End If

                    ' Place new data in the signal itself.
                    signal(lastSignalPoint) = CSng(newValue.Real)

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

                    ' Calculate the sliding FFT coefficients.
                    slidingFft.Update(newValue, oldValue)
                Loop
            End Using
        End Using

    End Sub

    #End Region

    #Region " AMPLITUDE SPECTRUM CHART "

    ''' <summary>Gets or sets reference to the amplitude spectrum curve.</summary>
    Private _AmplitudeSpectrumCurve As isr.Visuals.Curve

    ''' <summary>Gets or sets reference to the spectrum amplitude axis.</summary>
    Private _AmplitudeSpectrumAxis As isr.Visuals.Axis

    ''' <summary>Gets or sets reference to the spectrum frequency axis.</summary>
    Private _FrequencyAxis As isr.Visuals.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _SpectrumChartPan As isr.Visuals.ChartPane

    ''' <summary> Creates the chart for displaying the amplitude spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub CreateSpectrumChart()

        ' set chart area and titles.
        Me._SpectrumChartPan = New isr.Visuals.ChartPane With {
            .PaneArea = New RectangleF(10, 10, 10, 10)
        }
        Me._SpectrumChartPan.Title.Caption = "Spectrum"
        Me._SpectrumChartPan.Legend.Visible = False

        Me._FrequencyAxis = Me._SpectrumChartPan.AddAxis("Frequency, Hz", isr.Visuals.AxisType.X)
        Me._FrequencyAxis.Max = New isr.Visuals.AutoValueR(100, False)
        Me._FrequencyAxis.Min = New isr.Visuals.AutoValueR(1, False)

        Me._FrequencyAxis.Grid.Visible = True
        Me._FrequencyAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._FrequencyAxis.TickLabels.Visible = True
        Me._FrequencyAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(1, True)
        ' frequencyAxis.TickLabels.Appearance.Angle = 60.0F

        Me._AmplitudeSpectrumAxis = Me._SpectrumChartPan.AddAxis("Amplitude, Volts", isr.Visuals.AxisType.Y)
        Me._AmplitudeSpectrumAxis.CoordinateScale = New isr.Visuals.CoordinateScale(isr.Visuals.CoordinateScaleType.Linear)

        Me._AmplitudeSpectrumAxis.Title.Visible = True

        Me._AmplitudeSpectrumAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._AmplitudeSpectrumAxis.Min = New isr.Visuals.AutoValueR(0, True)

        Me._AmplitudeSpectrumAxis.Grid.Visible = True
        Me._AmplitudeSpectrumAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._AmplitudeSpectrumAxis.TickLabels.Visible = True
        Me._AmplitudeSpectrumAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(0, True)

        Me._SpectrumChartPan.AxisFrame.FillColor = Color.WhiteSmoke

        Me._SpectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)

        Me._AmplitudeSpectrumCurve = Me._SpectrumChartPan.AddCurve(Visuals.CurveType.XY, "Amplitude Spectrum", Me._FrequencyAxis, Me._AmplitudeSpectrumAxis)
        Me._AmplitudeSpectrumCurve.Cord.LineColor = Color.Red
        Me._AmplitudeSpectrumCurve.Cord.CordType = Visuals.CordType.Linear
        Me._AmplitudeSpectrumCurve.Symbol.Visible = False
        Me._AmplitudeSpectrumCurve.Cord.LineWidth = 1.0F

        Me._SpectrumChartPan.Rescale()

    End Sub

    ''' <summary>
    ''' Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequencies"> Holds the spectrum frequencies. </param>
    ''' <param name="magnitudes">  Holds the spectrum amplitudes. </param>
    ''' <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
    '''                            plotting data that was not scaled. </param>
    Private Sub ChartAmplitudeSpectrum(ByVal frequencies() As Double, ByVal magnitudes() As Double,
                                       ByVal scaleFactor As Double)

        ' adjust abscissa scale.
        Me._FrequencyAxis.Min = New isr.Visuals.AutoValueR(frequencies(frequencies.GetLowerBound(0)), False)
        Me._FrequencyAxis.Max = New isr.Visuals.AutoValueR(frequencies(frequencies.GetUpperBound(0)), False)

        ' adjust ordinate scale.
        Me._AmplitudeSpectrumAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._AmplitudeSpectrumAxis.Min = New isr.Visuals.AutoValueR(0, True)

        ' Set initial value for frequency
        '    Dim currentFrequency As Double = Gopher.AmplitudeSpectrumChart.AbscissaMin.Value
        '   Dim deltaFrequency As Double = 1.0# / Gopher.Test.EpochDuration

        ' plot all but last, which was plotted above

        ' scale the magnitudes
        Dim amplitudes(frequencies.Length - 1) As Double
        For i As Integer = frequencies.GetLowerBound(0) To frequencies.GetUpperBound(0)

            amplitudes(i) = scaleFactor * magnitudes(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        Me._AmplitudeSpectrumCurve.UpdateData(frequencies, amplitudes)
        Me._SpectrumChartPan.Rescale()
        Me._SpectrumChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the amplitude spectrum Chart. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub SpectrumChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SpectrumChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                Me._SpectrumChartPan.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary> Redraws the amplitude spectrum chart when the form is resized. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the chart panel
    '''                                             <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub SpectrumChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SpectrumChartPanel.Resize
        If Not Me._SpectrumChartPan Is Nothing Then
            Me._SpectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)
        End If
    End Sub

    #End Region

    #Region " SIGNAL CHART "

    ''' <summary>Gets or sets reference to the instantaneous DP curve.</summary>
    Private _SignalCurve As isr.Visuals.Curve

    ''' <summary>Gets or sets reference to the instantaneous DP amplitude axis.</summary>
    Private _SignalAxis As isr.Visuals.Axis

    ''' <summary>Gets or sets reference to the instantaneous DP time axis.</summary>
    Private _TimeAxis As isr.Visuals.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _SignalChartPane As isr.Visuals.ChartPane

    ''' <summary> Creates the Scope for display of voltages during the epoch. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub CreateSignalChart()

        ' set chart area and titles.
        Me._SignalChartPane = New isr.Visuals.ChartPane With {
            .PaneArea = New RectangleF(10, 10, 10, 10)
        }
        Me._SignalChartPane.Title.Caption = "Voltage versus Time"
        Me._SignalChartPane.Legend.Visible = False

        Me._TimeAxis = Me._SignalChartPane.AddAxis("Time, Seconds", isr.Visuals.AxisType.X)
        Me._TimeAxis.Max = New isr.Visuals.AutoValueR(1, False)
        Me._TimeAxis.Min = New isr.Visuals.AutoValueR(0, False)

        Me._TimeAxis.Grid.Visible = True
        Me._TimeAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._TimeAxis.TickLabels.Visible = True
        Me._TimeAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(1, True)
        ' timeAxis.TickLabels.Appearance.Angle = 60.0F

        Me._SignalAxis = Me._SignalChartPane.AddAxis("Volts", isr.Visuals.AxisType.Y)
        Me._SignalAxis.CoordinateScale = New isr.Visuals.CoordinateScale(isr.Visuals.CoordinateScaleType.Linear)

        Me._SignalAxis.Title.Visible = True

        Me._SignalAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._SignalAxis.Min = New isr.Visuals.AutoValueR(-1, True)

        Me._SignalAxis.Grid.Visible = True
        Me._SignalAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._SignalAxis.TickLabels.Visible = True
        Me._SignalAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(0, True)

        Me._SignalChartPane.AxisFrame.FillColor = Color.WhiteSmoke
        Me._SignalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)

        Me._SignalCurve = Me._SignalChartPane.AddCurve(Visuals.CurveType.XY, "TimeSeries", Me._TimeAxis, Me._SignalAxis)
        Me._SignalCurve.Cord.LineColor = Color.Red
        Me._SignalCurve.Cord.CordType = Visuals.CordType.Linear
        Me._SignalCurve.Symbol.Visible = False
        Me._SignalCurve.Cord.LineWidth = 1.0F

        Me._SignalChartPane.Rescale()

    End Sub

    ''' <summary>
    ''' Plot the DP signal from the entire epoch data simulating a strip chart effect.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub ChartSignal(ByVal times() As Single, ByVal amplitudes() As Single)

        ' plot all but last, which was plotted above
        Dim newApms(times.Length - 1) As Double
        Dim newTimes(times.Length - 1) As Double
        For i As Integer = times.GetLowerBound(0) To times.GetUpperBound(0)

            newApms(i) = amplitudes(i)
            newTimes(i) = times(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        Me.ChartSignal(newTimes, newApms)

    End Sub

    ''' <summary>
    ''' Plot the DP signal from the entire epoch data simulating a strip chart effect.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub ChartSignal(ByVal times() As Double, ByVal amplitudes() As Double)

        Me._TimeAxis.Max = New isr.Visuals.AutoValueR(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture), False)
        Me._TimeAxis.Min = New isr.Visuals.AutoValueR(0, False)

        Me._SignalAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._SignalAxis.Min = New isr.Visuals.AutoValueR(-1, True)

        Me._SignalCurve.UpdateData(times, amplitudes)
        Me._SignalChartPane.Rescale()

        Me._SignalChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the Signal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub SignalChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SignalChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                Me._SignalChartPane.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary> Redraws the Signal chart when the form is resized. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the chart panel
    '''                                             <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub SignalChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChartPanel.Resize
        If Not Me._SignalChartPane Is Nothing Then
            Me._SignalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)
        End If
    End Sub

    #End Region

    #Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by _SignalComboBox for validated events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SignalComboBox_Validated(sender As Object, e As System.EventArgs) Handles _SignalComboBox.Validated
        Me.UpdateSignal()
    End Sub

    ''' <summary> Event handler. Called by _cyclesTextBox for validating events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub CyclesTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _CyclesTextBox.Validating

        Me._ErrorProvider.SetError(Me._CyclesTextBox, String.Empty)
        Dim value As Integer = 0
        If Integer.TryParse(Me._CyclesTextBox.Text, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, value) Then
            If value < 1 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._CyclesTextBox, "Must exceed 1")
            Else
                Me.UpdateSignal()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If
    End Sub

    ''' <summary> Event handler. Called by _PhaseTextBox for validating events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PhaseTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PhaseTextBox.Validating

        Me._ErrorProvider.SetError(Me._PhaseTextBox, String.Empty)
        Dim value As Integer = 0
        If Integer.TryParse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, value) Then
            Me.UpdateSignal()
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _pointsTextBox for validating events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PointsTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsTextBox, String.Empty)
        Me._ErrorProvider.SetError(Me._PhaseTextBox, String.Empty)
        Dim value As Integer = 0
        If Integer.TryParse(Me._PointsTextBox.Text, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, value) Then
            If value < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) <
                    Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
                Me.UpdateSignal()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If
    End Sub

    ''' <summary> Event handler. Called by _pointsToDisplayTextBox for validating events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PointsToDisplayTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsToDisplayTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, String.Empty)
        Dim value As Integer = 0
        If Integer.TryParse(Me._PointsToDisplayTextBox.Text, Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, value) Then
            If value < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) <
                    Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Event handler. Called by _startStopCheckBox for checked changed events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartStopCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopCheckBox.CheckedChanged

        Me._StartStopCheckBox.Text = If(Me._StartStopCheckBox.Checked, "&Stop", "&Start")

        If Me._StartStopCheckBox.Enabled AndAlso Me._StartStopCheckBox.Checked Then

            Dim algorithm As Example = Me.SelectedExample

            Select Case algorithm

                Case Example.DiscreteFourierTransform, Example.MixedRadixFFT, Example.WisdomFFT

                    Me._CountToolStripStatusLabel.Text = "0"
                    Do
                        If Me._DoubleRadioButton.Checked Then
                            Me.CalculateSpectrumDouble(algorithm)
                        Else
                            Me.CalculateSpectrumSingle(algorithm)
                        End If
                        Windows.Forms.Application.DoEvents()
                        Dim count As Integer = Integer.Parse(Me._CountToolStripStatusLabel.Text, Globalization.CultureInfo.CurrentCulture) + 1
                        Me._CountToolStripStatusLabel.Text = count.ToString(Globalization.CultureInfo.CurrentCulture)
                    Loop While Me._StartStopCheckBox.Checked And False

                Case Example.SlidingFFT

                    If Me._DoubleRadioButton.Checked Then
                        Me.SlidingFft(0.5R)
                    Else
                        Me.SlidingFft(0.5F)
                    End If

                Case Else

                    Me.ShowStatus($"Unknown algorithm: {algorithm}::{algorithm.Description}")

            End Select

            If Me._StartStopCheckBox.Checked Then
                Me._StartStopCheckBox.Enabled = False
                Me._StartStopCheckBox.Checked = False
                Me._StartStopCheckBox.Enabled = True
            End If

        Else

            Me._CountToolStripStatusLabel.Text = "0"

        End If

    End Sub

    #End Region

    #Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    #End Region

End Class
