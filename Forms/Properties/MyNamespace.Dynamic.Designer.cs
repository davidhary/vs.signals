using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Algorithms.Signals.Forms.My
{

    /// <summary>   my project. </summary>
        /// <remarks>   David, 2020-10-29. </remarks>
    internal static partial class MyProject
    {

        /// <summary>   my forms. </summary>
        /// <remarks>   David, 2020-10-29. </remarks>
        internal partial class MyForms
        {
            /// <summary>   my form. </summary>
            [EditorBrowsable( EditorBrowsableState.Never )]
            public MyForm m_MyForm;

            /// <summary>   Gets or sets my form. </summary>
            /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
            ///                                         illegal values. </exception>
            /// <value> my form. </value>
            public MyForm MyForm
            {
                [DebuggerHidden]
                get {
                    m_MyForm = Create__Instance__( m_MyForm );
                    return m_MyForm;
                }

                [DebuggerHidden]
                set {
                    if ( ReferenceEquals( value, m_MyForm ) )
                        return;
                    if ( value is object )
                        throw new ArgumentException( "Property can only be set to Nothing" );
                    Dispose__Instance__( ref m_MyForm );
                }
            }

            /// <summary>   my splash screen. </summary>
            [EditorBrowsable( EditorBrowsableState.Never )]
            public MySplashScreen m_MySplashScreen;

            /// <summary>   Gets or sets my splash screen. </summary>
            /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
            ///                                         illegal values. </exception>
            /// <value> my splash screen. </value>
            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get {
                    m_MySplashScreen = Create__Instance__( m_MySplashScreen );
                    return m_MySplashScreen;
                }

                [DebuggerHidden]
                set {
                    if ( ReferenceEquals( value, m_MySplashScreen ) )
                        return;
                    if ( value is object )
                        throw new ArgumentException( "Property can only be set to Nothing" );
                    Dispose__Instance__( ref m_MySplashScreen );
                }
            }
        }
    }
}
