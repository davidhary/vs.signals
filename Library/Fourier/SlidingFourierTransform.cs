using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Calculate the Sliding Fourier Transform. </summary>
    /// <remarks>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-07 </para>
    /// </remarks>
    public class SlidingFourierTransform : FourierTransformBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public SlidingFourierTransform() : base( FourierTransformType.Sliding )
        {
        }

        #endregion

        #region " COMPLEX "

        /// <summary> Calculates the Forward Fourier Transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="values"> Takes the time series and returns the spectrum values. </param>
        public override void Forward( Complex[] values )
        {
            base.Forward( values );
        }

        /// <summary>
        /// Initializes the sine and cosine tables for calculating the sliding Fourier transform.
        /// </summary>
        /// <remarks>
        /// Allocates and calculates sine and cosine tables for the sliding Fourier transform and maps
        /// the data arrays to the internal memory space of the sliding Fourier transform.
        /// </remarks>
        /// <param name="values"> Holds the inputs and returns the outputs. </param>
        protected override void Initialize( Complex[] values )
        {
            base.Initialize( values );
            // save the real- and imaginary-parts into the cache
            this.CopyToCache( values );
            // Allocate the sine tables.
            this.BuildDftTable( values.Length );
        }

        /// <summary> Calculates the sliding Fast Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="newValue"> The new value to use in calculating the sliding Fourier transform. </param>
        /// <param name="oldValue"> The first real value in the time series previously used to calculate
        /// the Fourier transform. </param>
        public void Update( Complex newValue, Complex oldValue )
        {
            // get the difference term
            var delta = newValue - oldValue;
            // Update the sliding Fourier transform
            for ( int i = 0, loopTo = this.DftCache.Length - 1; i <= loopTo; i++ )
                this.DftCache[i] = this.DftTable[i] * (this.DftCache[i] + delta);
        }

        /// <summary>   Gets the dft. </summary>
        /// <value> The dft. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        public Complex[] Dft => base.DftCache;
        #endregion

        #region " DOUBLE "

        /// <summary>
        /// Initializes the sliding FFT; Use <see cref="Update">update</see> to add values.
        /// </summary>
        /// <remarks>
        /// Computes the DFT directly without using any of the special rotation and permutation
        /// algorithms that are part of the Fast Fourier Transform (FFT)
        /// literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        /// </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( double[] reals, double[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary>
        /// Initializes the sine and cosine tables for calculating the sliding Fourier transform.
        /// </summary>
        /// <remarks>
        /// Allocates and calculates sine and cosine tables for the sliding Fourier transform and maps
        /// the data arrays to the internal memory space of the sliding Fourier transform.
        /// </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            // save the real- and imaginary-parts into the cache
            this.CopyToCache( reals, imaginaries );

            // Allocate the sine tables.
            _ = this.BuildDftTables( reals.Length );
        }

        /// <summary> Calculates the sliding Fast Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="newReal">      The new real value to use in calculating the sliding Fourier
        /// transform. </param>
        /// <param name="newImaginary"> The new imaginary value to use in calculating the sliding Fourier
        /// transform. </param>
        /// <param name="oldReal">      The first real value in the time series previously used to
        /// calculate the Fourier transform. </param>
        /// <param name="oldImaginary"> The first imaginary value in the time series previously used to
        /// calculate the Fourier transform. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool Update( double newReal, double newImaginary, double oldReal, double oldImaginary )
        {
            // get the difference term
            double realDelta = newReal - oldReal;
            double imagDelta = newImaginary - oldImaginary;

            // Update the sliding Fourier transform
            for ( int i = 0, loopTo = this.RealCache.Length - 1; i <= loopTo; i++ )
            {
                newReal = this.RealCache[i] + realDelta;
                newImaginary = this.ImaginaryCache[i] + imagDelta;
                this.RealCache[i] = this.CosineTable[i] * newReal - this.SineTable[i] * newImaginary;
                this.ImaginaryCache[i] = this.CosineTable[i] * newImaginary + this.SineTable[i] * newReal;
            }

            return default;
        }

        /// <summary>   Gets the Real Values of the dft. </summary>
        /// <value> The Real values of the dft. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        public Double[] RealValues => base.RealCache;

        /// <summary>   Gets the Imaginary Values of the dft. </summary>
        /// <value> The Imaginary values of the dft. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        public Double[] ImaginaryValues => base.ImaginaryCache;

        #endregion

        #region " SINGLE "

        /// <summary>
        /// Initializes the sliding FFT; Use <see cref="Update">update</see> to add values.
        /// </summary>
        /// <remarks>
        /// Computes the DFT directly without using any of the special rotation and permutation
        /// algorithms that are part of the Fast Fourier Transform (FFT)
        /// literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        /// </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( float[] reals, float[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary>
        /// Initializes the sine and cosine tables for calculating the sliding Fourier transform.
        /// </summary>
        /// <remarks>
        /// Allocates and calculates sine and cosine tables for the sliding Fourier transform and  maps
        /// the data arrays to the internal memory space of the sliding Fourier transform.
        /// </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            // save the real- and imaginary-parts into the cache
            this.CopyToCache( reals, imaginaries );

            // Allocate the sine tables.
            _ = this.BuildDftTables( reals.Length );
        }

        /// <summary> Calculates the sliding Fast Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="newReal">      The new real value to use in calculating the sliding Fourier
        /// transform. </param>
        /// <param name="newImaginary"> The new imaginary value to use in calculating the sliding Fourier
        /// transform. </param>
        /// <param name="oldReal">      The first real value in the time series previously used to
        /// calculate the Fourier transform. </param>
        /// <param name="oldImaginary"> The first imaginary value in the time series previously used to
        /// calculate the Fourier transform. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        /// <example>
        /// Sub Form_Click
        /// Dim fft As New isr.Dsp.MixedRadixFft Dim sliding As New isr.Dsp.SlidingFft Dim signalLength
        /// As integer   ' Signal points Dim reals() As Single         ' real-part of Fourier transform
        /// Dim imaginaries() As Single   ' imaginary-part of Fourier transform Dim magnitude() As
        /// Single     ' Fourier transform magnitude Dim samplingRate As Single Dim frequencyAxis () As
        /// Single  ' Frequency Dim newReal As Single Dim newImaginary As Single Dim oldReal As Single
        /// Dim oldImaginary As Single ' Set signal points signalLength = 250 ' Create 10 cycles of the
        /// sine wave with 0 phase Dim signal() as Single = isr.Dsp.Helper.Sine(10 / signalLength, 0)
        /// ' Allocate arrays for real and imaginary-parts ReDim reals(signalLength - 1)
        /// ReDim imaginaries(signalLength - 1)
        /// ' Place signal in the real-part for DFT. signal.CopyTo(reals(),0)
        /// ' Set options fft.IsRemoveMean False fft.TaperWindow = 0 ' Compute the Mixed Radix Fourier
        /// transform ' to initialize the sliding Fourier transform fft.Forward ( reals(),
        /// imaginaries())
        /// ' set the most recent values of the signal dim firstPoint as integer = 0 newReal =
        /// signal(signalLength-1)
        /// newImaginary = 0 oldReal = signal(firstPoint)
        /// oldImaginary = 0 sliding.Initialize(reals(), imaginaries())
        /// ' Get the Previous values of the signal ' Get new values of the signal newReal = 0.99
        /// newImaginary = 0 signal(firstPoint) = newReal
        /// ....
        /// ' Compute the sliding Fourier transform. fft.Update (newReal, newImaginary, oldReal,
        /// oldImaginary)
        /// ....
        /// ' increment firstPoint += 1 oldReal = signal(firstPoint)
        /// oldImaginary = 0 newReal = 1.01 newImaginary = 0 signal(firstPoint) = newReal ' Compute the
        /// sliding Fourier transform. fft.Update (newReal, newImaginary, oldReal, oldImaginary)
        /// ....
        /// ' Compute the magnitude Dim magnitudes() = isr.Dsp.Helper.Magnitude(reals(), Imaginaries())
        /// End Sub
        /// </example>
        public bool Update( float newReal, float newImaginary, float oldReal, float oldImaginary )
        {

            // get the difference term
            float realDelta = newReal - oldReal;
            float imagDelta = newImaginary - oldImaginary;

            // Update the sliding Fourier transform
            for ( int i = 0, loopTo = this.RealCache.Length - 1; i <= loopTo; i++ )
            {
                newReal = ( float ) (this.RealCache[i] + realDelta);
                newImaginary = ( float ) (this.ImaginaryCache[i] + imagDelta);
                this.RealCache[i] = this.CosineTable[i] * newReal - this.SineTable[i] * newImaginary;
                this.ImaginaryCache[i] = this.CosineTable[i] * newImaginary + this.SineTable[i] * newReal;
            }

            return default;
        }

        #endregion


    }
}
