Imports System.Numerics

''' <summary> The base class for the power spectrum. </summary>
''' <remarks>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class PowerSpectrum
    Inherits Spectrum

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
    '''                                 calculating the spectrum. </param>
    Public Sub New(ByVal fourierTransform As isr.Algorithms.Signals.FourierTransformBase)
        ' instantiate the base class
        MyBase.New(fourierTransform)
    End Sub

#End Region

#Region " WELCH METHOD COMMON "

    ''' <summary> Gets the average count. </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The average count. </value>
    Public ReadOnly Property AverageCount() As Integer

#End Region

#Region " DENSITIES "

    ''' <summary> Clears and allocates the power spectrum densities. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="length"> Specifies the length of the spectrum thus allowing for a Int16er spectrum
    '''                       than is calculated. Specify 0 to let the program set the maximum
    '''                       length. </param>
    Public Sub Clear(ByVal length As Integer)
        Me.Scaled = False
        Me._AverageCount = 0
        If length > 0 Then
            ReDim Me._Densities(length - 1)
        Else
            Me._Densities = Nothing
        End If
    End Sub

    ''' <summary> Copies the density and the number of averages. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values">        The values. </param>
    ''' <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
    ''' <param name="spectrumCount"> Specifies the number of points to copy. </param>
    ''' <param name="scaled">        The scaled sentinel. </param>
    Public Sub CopyToDensities(ByVal values() As Double, ByVal averageCount As Integer, ByVal spectrumCount As Integer, ByVal scaled As Boolean)
        If values Is Nothing Then
            Me.Scaled = False
            Me._Densities = Nothing
        Else
            Me.Scaled = scaled
            Me._AverageCount = averageCount
            ReDim Me._Densities(spectrumCount - 1)
            Array.Copy(values, Me._Densities, spectrumCount)
        End If
    End Sub

    ''' <summary> Copies the density and the number of averages. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values">        The values. </param>
    ''' <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
    ''' <param name="spectrumCount"> Specifies the number of points to copy. </param>
    ''' <param name="scaled">        The scaled sentinel. </param>
    Public Sub CopyToDensities(ByVal values() As Single, ByVal averageCount As Integer, ByVal spectrumCount As Integer, ByVal scaled As Boolean)
        If values Is Nothing Then
            Me.Scaled = False
            Me._Densities = Nothing
        Else
            Me.Scaled = scaled
            Me._AverageCount = averageCount
            ReDim Me._Densities(spectrumCount - 1)
            Array.Copy(values, Me._Densities, spectrumCount)
        End If
    End Sub

    ''' <summary> Returns the indexed density. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <param name="index">        The index. </param>
    ''' <returns> The indexed density. </returns>
    Public Function Density(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If index < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(index), index,
                                                                $"{NameOf(index)} {index} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If index > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(index), index,
                                                                $"{NameOf(index)} {index} Index must not exceed upper bound {Me.Densities.GetLowerBound(0)}")
        If Me.Scaled Then
            If halfSpectrum Then
                If index = 0 Then
                    Return Me.Densities(index)
                Else
                    Dim tempValue As Double = Me.Densities(index)
                    Return tempValue + tempValue
                End If
            Else
                Return Me.Densities(index)
            End If
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)
            Return If(index = 0 And halfSpectrum, 0.5R * factor * Me.Densities(index), factor * Me.Densities(index))
        End If

    End Function

    ''' <summary> The power spectrum densities. </summary>
    Private _Densities() As Double

    ''' <summary> Returns the spectrum densities. </summary>
    ''' <remarks>
    ''' Unless<see cref="Scale()"/> is applied and not <see cref="Restore()"/>, the spectrum
    ''' densities are not scaled, which allows adding spectrum segments for Welch method power
    ''' spectrum calculation. Use <see cref="Restore()"/> in case the densities were scaled and new
    ''' spectra need to be appended.
    ''' </remarks>
    ''' <returns> The power spectrum densities. </returns>
    Public Overloads Function Densities() As Double()
        Return Me._Densities
    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <returns> The scaled densities. </returns>
    Public Overloads Function ScaledDensities() As Double()

        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")

        ' allocate the outcome array
        Dim output(Me.Densities.Length - 1) As Double

        If Me.Scaled Then
            ReDim output(Me.Densities.Length - 1)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Me.Densities(i)
            Next
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = factor * Me.Densities(i)
            Next
        End If
        Return output

    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <returns> The scaled spectrum densities. </returns>
    Public Overloads Function ScaledDensities(ByVal halfSpectrum As Boolean) As Double()

        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")

        Dim output() As Double

        If Me.Scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Me.Densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me.Densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Me.Densities(i)
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = 0.5R * factor * Me.Densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me.Densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = factor * Me.Densities(i)
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled spectrum densities. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="halfSpectrum">  Specifies is the scaling is doubled for displaying the half
    '''                              spectrum. </param>
    ''' <param name="startingIndex"> From index. </param>
    ''' <param name="endingIndex">   To index. </param>
    ''' <returns> The scaled spectrum densities. </returns>
    Public Overloads Function ScaledDensities(ByVal halfSpectrum As Boolean, ByVal startingIndex As Integer, ByVal endingIndex As Integer) As Double()

        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If startingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If startingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} fromIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} endingIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")


        ' allocate outcome space
        Dim output(endingIndex - startingIndex) As Double

        If Me.Scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If startingIndex = 0 Then
                    output(j) = Me.Densities(startingIndex)
                    startingIndex += 1
                    j += 1
                End If
                For i As Integer = startingIndex To endingIndex
                    Dim tempValue As Double = Me.Densities(i)
                    output(j) = tempValue + tempValue
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(endingIndex - startingIndex)
                For i As Integer = startingIndex To endingIndex
                    output(j) = Me.Densities(i)
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If startingIndex = 0 And halfSpectrum Then
                output(j) = 0.5R * factor * Me.Densities(startingIndex)
                startingIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = startingIndex To endingIndex
                output(j) = factor * Me.Densities(i)
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary> Returns the length of the densities array. </summary>
    ''' <value> The length of the densities. </value>
    Public ReadOnly Property DensitiesLength() As Integer
        Get
            Return If(Me.Densities Is Nothing, 0, Me.Densities.Length)
        End Get
    End Property

#End Region

#Region " MAGNITUDES "

    ''' <summary> Returns the indexed magnitude. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <param name="index">        The index. </param>
    ''' <returns> The indexed magnitude. </returns>
    Public Function Magnitude(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If index < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(index), index,
                                                                $"{NameOf(index)} {index} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If index > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(index), index,
                                                                $"{NameOf(index)} {index} Index must not exceed upper bound {Me.Densities.GetLowerBound(0)}")
        Return Math.Sqrt(Me.Density(halfSpectrum, index))
    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes() As Double()
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")

        ' allocate the outcome array
        Dim output(Me.Densities.Length - 1) As Double
        If Me.Scaled Then
            ReDim output(Me.Densities.Length - 1)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Math.Sqrt(Me.Densities(i))
            Next
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me.Densities.Length - 1
                output(i) = Math.Sqrt(factor * Me.Densities(i))
            Next
        End If
        Return output

    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes(ByVal halfSpectrum As Boolean) As Double()
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")

        Dim output() As Double

        If Me.Scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(Me.Densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me.Densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Math.Sqrt(Me.Densities(i))
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(0.5R * factor * Me.Densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me.Densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me.Densities.Length - 1)
                For i As Integer = 0 To Me.Densities.Length - 1
                    output(i) = Math.Sqrt(factor * Me.Densities(i))
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary> Returns the scaled magnitude spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="halfSpectrum">  Specifies is the scaling is doubled for displaying the half
    '''                              spectrum. </param>
    ''' <param name="startingIndex"> From index. </param>
    ''' <param name="endingIndex">   To index. </param>
    ''' <returns> The scaled magnitude spectrum. </returns>
    Public Function ScaledMagnitudes(ByVal halfSpectrum As Boolean, ByVal startingIndex As Integer, ByVal endingIndex As Integer) As Double()

        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If startingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If startingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} fromIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} endingIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")

        ' allocate outcome space
        Dim output(endingIndex - startingIndex) As Double

        If Me.Scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If startingIndex = 0 Then
                    output(j) = Math.Sqrt(Me.Densities(startingIndex))
                    startingIndex += 1
                    j += 1
                End If
                For i As Integer = startingIndex To endingIndex
                    Dim tempValue As Double = Me.Densities(i)
                    output(j) = Math.Sqrt(tempValue + tempValue)
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(endingIndex - startingIndex)
                For i As Integer = startingIndex To endingIndex
                    output(j) = Math.Sqrt(Me.Densities(i))
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If startingIndex = 0 And halfSpectrum Then
                output(j) = Math.Sqrt(0.5R * factor * Me.Densities(startingIndex))
                startingIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = startingIndex To endingIndex
                output(j) = Math.Sqrt(factor * Me.Densities(i))
                j += 1
            Next i

        End If

        Return output

    End Function

#End Region

#Region " POWER "

    ''' <summary> Returns the index of the spectrum peak. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="fromIndex"> From index. </param>
    ''' <param name="toIndex">   To index. </param>
    ''' <returns> The index of the spectrum peak. </returns>
    Public Function PeakIndex(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Integer

        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If fromIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must exceed lower bound")
        If fromIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(fromIndex), fromIndex, "Starting index must not exceed spectrum length")
        If toIndex < fromIndex Then Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must exceed starting index")
        If toIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(toIndex), toIndex, "Ending index must not exceed spectrum length")

        ' Set Max to first value
        Dim maxDensityIndex As Integer = fromIndex
        Dim maxDensity As Single = CSng(Me.Densities(maxDensityIndex))
        Dim maxCandidate As Single
        For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)

            ' Get candidate
            maxCandidate = CSng(Me.Densities(i))

            ' Select a new maximum if lower than element.
            If maxCandidate > maxDensity Then
                maxDensity = maxCandidate
                maxDensityIndex = i
            End If

        Next i

        ' return the index
        Return maxDensityIndex

    End Function

    ''' <summary> Searches for the half power indexes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="spectrumPeakIndex"> Zero-based index of the spectrum peak. </param>
    ''' <returns> The found half power indexes. </returns>
    Public Function FindHalfPowerPoints(ByVal spectrumPeakIndex As Integer) As (Lower As Double, Upper As Double)

        ' Step down to get low frequency.
        Dim searchIndex As Integer = spectrumPeakIndex
        Dim lowerBound As Integer = 0
        Dim upperBound As Integer = Me.Densities.GetUpperBound(0)
        Dim targetDensity As Double = Me.Densities(spectrumPeakIndex)
        ' find the first power lower than half power on the low frequency end.
        Do While (Me.Densities(searchIndex) > targetDensity) And (searchIndex > lowerBound)
            searchIndex -= 1
        Loop

        ' look for the peak lower frequency
        Dim lowerIndex As Double = lowerBound
        If searchIndex > lowerBound Then
            lowerIndex = searchIndex
            ' check if we need to interpolate
            Dim psd2 As Double = Me.Densities(searchIndex + 1)
            Dim psd1 As Double = Me.Densities(searchIndex)
            If psd2 <> psd1 Then
                lowerIndex += (targetDensity - psd1) / (psd2 - psd1)
            End If
        End If

        ' Step up to get high frequency.
        searchIndex = spectrumPeakIndex
        ' find the first power lower than half power on the high frequency end.
        Do While (Me.Densities(searchIndex) > targetDensity) And (searchIndex < upperBound)
            searchIndex += 1
        Loop

        ' Find the high frequency value.
        Dim upperIndex As Double = upperBound
        If searchIndex < upperBound Then
            upperIndex = searchIndex
            ' check if we need to interpolate
            Dim psd2 As Double = Me.Densities(searchIndex - 1)
            Dim psd1 As Double = Me.Densities(searchIndex)
            If psd2 <> psd1 Then
                upperIndex += (psd1 - targetDensity) / (psd2 - psd1)
            End If
        End If
        Return (lowerIndex, upperIndex)

    End Function

    ''' <summary>
    ''' Returns the total power for the specified index range but using both side of the spectrum and
    ''' assuming the spectrum is for real valued time series, that is, that the spectrum is symmetric.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid because spectrum densities were not
    '''                                                created. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="startingIndex"> From index. </param>
    ''' <param name="endingIndex">   To index. </param>
    ''' <returns>
    ''' The total power for the specified index range but using both side of the spectrum and
    ''' assuming the spectrum is for real valued time series, that is, that the spectrum is symmetric.
    ''' </returns>
    Public Function TotalPower(ByVal startingIndex As Integer, ByVal endingIndex As Integer) As Double
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        If startingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If startingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(startingIndex), startingIndex,
                                                                $"{NameOf(startingIndex)} {startingIndex} fromIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex < Me.Densities.GetLowerBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} must exceed lower bound {Me.Densities.GetLowerBound(0)}")
        If endingIndex > Me.Densities.GetUpperBound(0) Then Throw New ArgumentOutOfRangeException(NameOf(endingIndex), endingIndex,
                                                                $"{NameOf(endingIndex)} {endingIndex} endingIndex must not exceed upper bound {Me.Densities.GetLowerBound(0)}")

        ' the DC spectrum is scaled by half because the Spectrum Scale Factor
        ' is derived for the half spectrum (twice as large).
        Dim total As Double = 0
        If startingIndex = 0 Then
            total += 0.5 * Me.Densities(0)
            startingIndex += 1
        End If
        ' go up to half spectrum
        For i As Integer = startingIndex To Math.Min(endingIndex, Me.HalfSpectrumLength - 1)
            ' add the power
            total += Me.Densities(i)
        Next

        If Not Me.Scaled Then
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= Me.SpectrumScaleFactor(True)
        Else
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= 2
        End If

        Return total

    End Function

    ''' <summary> Returns the total power for the specified index range. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <param name="fromIndex"> From index. </param>
    ''' <returns> The total power for the specified index range. </returns>
    Public Function TotalPower(ByVal fromIndex As Integer) As Double
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        Return Me.TotalPower(fromIndex, Me.Densities.GetUpperBound(0))
    End Function

    ''' <summary> Returns the total power. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <returns> The total power. </returns>
    Public Function TotalPower() As Double
        If Me.Densities Is Nothing Then Throw New InvalidOperationException("Spectrum densities where not created.")
        Return Me.TotalPower(Me.Densities.GetLowerBound(0), Me.Densities.GetUpperBound(0))
    End Function

#End Region

#Region " SCALE "

    ''' <summary>
    ''' Restore the densities to their pre-scaled values so that additional averages can be added.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub Restore()

        Dim factor As Double = Me.SpectrumScaleFactor(False)
        If factor <= Single.Epsilon Then
            Throw New InvalidOperationException("Invalid scale factor not caught by the Spectrum Scale Factor function--Contact Developer.")
        Else
            factor = 1.0R / factor
        End If

        ' un-scale the rest of the spectrum
        For i As Integer = 0 To Me.Densities.GetUpperBound(0)
            Me.Densities(i) *= factor
        Next i

        ' un-tag power spectrum as not scaled allowing adding spectrum averages.
        Me.Scaled = False

    End Sub

    ''' <summary> Scales the spectrum densities based on the number of averages. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Scale()

        Dim factor As Double = Me.SpectrumScaleFactor(False)

        ' Scale the rest of the spectrum
        For i As Integer = 0 To Me.Densities.GetUpperBound(0)
            Me.Densities(i) *= factor
        Next i

        ' tag power spectrum as scaled.  This prevents adding new averages until
        ' the spectrum is cleared or restored.
        Me.Scaled = True

    End Sub

    ''' <summary>
    ''' Returns the spectrum scale factor including the effect of the taper window power.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="halfSpectrum"> Specifies is the scaling is doubled for displaying the half
    '''                             spectrum. </param>
    ''' <returns> The spectrum scale factor including the effect of the taper window power. </returns>
    Public Function SpectrumScaleFactor(ByVal halfSpectrum As Boolean) As Double
        If MyBase.TimeSeriesLength = 0 Then
            Throw New InvalidOperationException("Invalid scale factor--The spectrum time series is set to zero.")
        ElseIf Me.AverageCount = 0 Then
            Throw New InvalidOperationException("Invalid scale factor--Average count is zero.")
        End If
        Dim factor As Double = 1 / (Convert.ToDouble(Me.AverageCount) * MyBase.TimeSeriesLength * MyBase.TimeSeriesLength)
        If halfSpectrum Then
            ' The factor of two ensure double the power for half spectrum.
            factor *= 2
        End If
        ' add taper window power.
        If Not MyBase.TaperWindow Is Nothing Then
            If MyBase.TaperWindow.Power <= Single.Epsilon Then
                Throw New InvalidOperationException("Invalid scale factor--Taper window has zero power.")
            End If
            factor /= MyBase.TaperWindow.Power
        End If
        Return factor
    End Function

    ''' <summary>
    ''' Gets or sets the condition for the densities were scaled.  Once the densities are scaled no
    ''' more averages can be appended until the spectrum is
    ''' <see cref="Restore">restored</see>
    ''' </summary>
    ''' <value> The scaled sentinel. </value>
    Private Property Scaled As Boolean

#End Region

#Region " WELCH METHOD: COMPLEX "

    ''' <summary> Appends the spectrum to the existing spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="spectrum"> The spectrum. </param>
    Public Sub Append(ByVal spectrum() As Complex)

        If Me.Scaled Then Throw New InvalidOperationException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
        If spectrum Is Nothing Then Throw New System.ArgumentNullException(NameOf(spectrum))
        If spectrum.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(spectrum), "Spectrum must be longer than 1")

        If Me.Densities Is Nothing OrElse Me.Densities.Length <> spectrum.Length Then
            ReDim Me._Densities(spectrum.Length - 1)
        End If
        For i As Integer = 0 To Me.Densities.Length - 1
            Dim abs As Double = spectrum(i).Abs
            Me.Densities(i) += abs * abs
        Next

        ' increment the average count
        Me._AverageCount += 1

    End Sub

    ''' <summary>
    ''' Calculates the spectrum for complex data and appends to the existing spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Complex)
        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        If timeSeries.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(timeSeries), "Time series must be longer than 1")
        ' calculate the spectrum
        MyBase.Calculate(timeSeries)
        ' add the spectrum
        Me.Append(timeSeries)
    End Sub

    ''' <summary>
    ''' Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="dataPoints"> Specifies the number of data points to use. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Complex, ByVal dataPoints As Integer)

        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        If dataPoints = timeSeries.Length Then
            Me.TransformAppend(timeSeries)
        Else
            ' Get the time series data
            Dim ts(dataPoints - 1) As Complex
            Array.Copy(timeSeries, ts, Math.Min(ts.Length, timeSeries.Length))
            Me.TransformAppend(ts)
        End If

    End Sub

#End Region

#Region " WELCH METHOD: DOUBLE "

    ''' <summary>
    ''' Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="dataPoints"> Specifies the number of data points to use. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Double, ByVal dataPoints As Integer)
        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        Dim ts(dataPoints - 1) As Complex
        timeSeries.CopyTo(ts)
        Me.TransformAppend(ts)
    End Sub

    ''' <summary> Calculates the spectrum for real data segment by segment. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries">            The time series. </param>
    ''' <param name="fourierTransformSize">  Size of the Fourier transform. </param>
    ''' <param name="timeSeriesSegmentStep"> The time series segment step. </param>
    ''' <returns> The number of segments. </returns>
    Public Function Transform(ByVal timeSeries As Double(), ByVal fourierTransformSize As Integer, ByVal timeSeriesSegmentStep As Integer) As Integer
        If timeSeries Is Nothing Then Throw New ArgumentNullException(NameOf(timeSeries))
        Dim spectrumValues(fourierTransformSize - 1) As Complex
        Dim spectrumSegmentsCount As Integer = 0
        Dim iSegmentStart As Integer = 0
        Do Until iSegmentStart + fourierTransformSize > timeSeries.Length

            ' increment the number of segments
            spectrumSegmentsCount += 1

            ' populate the complex array.
            timeSeries.CopyTo(iSegmentStart, spectrumValues)

            Me.TransformAppend(spectrumValues)

            ' increment the start of segment sample
            iSegmentStart += timeSeriesSegmentStep

            My.MyLibrary.DoEvents()

        Loop
        Return spectrumSegmentsCount
    End Function

#End Region

#Region " WELCH METHOD: SINGLE "

    ''' <summary>
    ''' Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="dataPoints"> Specifies the number of data points to use. </param>
    Public Sub TransformAppend(ByVal timeSeries() As Single, ByVal dataPoints As Integer)
        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        Dim ts(dataPoints - 1) As Complex
        timeSeries.CopyTo(ts)
        Me.TransformAppend(ts)
    End Sub

    ''' <summary> Calculates the spectrum for real data segment by segment. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeSeries">            The time series. </param>
    ''' <param name="fourierTransformSize">  Size of the Fourier transform. </param>
    ''' <param name="timeSeriesSegmentStep"> The time series segment step. </param>
    ''' <returns> The number of segments. </returns>
    Public Function Transform(ByVal timeSeries As Single(), ByVal fourierTransformSize As Integer, ByVal timeSeriesSegmentStep As Integer) As Integer
        If timeSeries Is Nothing Then Throw New System.ArgumentNullException(NameOf(timeSeries))
        Dim spectrumValues(fourierTransformSize - 1) As Complex
        Dim spectrumSegmentsCount As Integer = 0
        Dim iSegmentStart As Integer = 0
        Do Until iSegmentStart + fourierTransformSize > timeSeries.Length

            ' increment the number of segments
            spectrumSegmentsCount += 1

            ' populate the complex array.
            timeSeries.CopyTo(iSegmentStart, spectrumValues)

            Me.TransformAppend(spectrumValues)

            ' increment the start of segment sample
            iSegmentStart += timeSeriesSegmentStep

            My.MyLibrary.DoEvents()

        Loop
        Return spectrumSegmentsCount
    End Function

#End Region

#Region " FILE I/O "

    ''' <summary> Reads spectrum densities from the . </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reader"> Specifies reference to an open binary reader. </param>
    Public Overloads Sub ReadDensities(ByVal reader As IO.BinaryReader)

        If reader Is Nothing Then Throw New ArgumentNullException(NameOf(reader))

        ' assume the densities were scaled.
        Me.Scaled = True

        ' read the densities parameters
        Me._AverageCount = reader.ReadInt32
        Dim elementCount As Integer = reader.ReadInt32

        ' allocate data array
        ReDim Me._Densities(elementCount - 1)

        ' Read the densities from the file
        For i As Integer = 0 To elementCount - 1
            Me.Densities(i) = reader.ReadDouble
        Next i

    End Sub

    ''' <summary> Writes the densities to a data file.  The densities are written as scaled. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
    '''                                              because spectrum densities were not created. 
    ''' </exception>
    ''' <param name="writer"> Specifies reference to an open binary writer. </param>
    Public Overloads Sub WriteDensities(ByVal writer As System.IO.BinaryWriter)

        If writer Is Nothing Then Throw New ArgumentNullException(NameOf(writer))

        If Me.Densities Is Nothing Then
            Throw New InvalidOperationException("Spectrum densities where not created.")
        End If

        ' write the spectrum parameters
        writer.Write(Me.AverageCount)

        Dim elementCount As Integer = Me.Densities.Length
        writer.Write(elementCount)

        If Me.Scaled Then
            ' write values to the file
            For i As Integer = 0 To elementCount - 1
                writer.Write(Me.Densities(i))
            Next i
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To elementCount - 1
                writer.Write(factor * Me.Densities(i))
            Next i
        End If

    End Sub

#End Region

End Class

