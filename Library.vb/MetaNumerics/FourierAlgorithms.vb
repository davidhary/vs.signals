Imports System.Numerics

''' <summary> Class Transformlet. </summary>
''' <remarks>
''' Given a length-N DFT, we decompose N = R1 R2 ... Rn into prime factors R. The total DFT can
''' then be expressed as a series of length-R DFTs, where each length-R DFT is repeated N/R
''' times. (Each R need not actually be prime, only co-prime to the other factors.)
''' If a length-N DFT is O(N^2) and N = R1 R2, then a naive implementation would be order N^2 =
''' R1^2 R2^2. But the decomposed work is order (N / R1) R1^2 + (N / R2) R2^2 = R1 R2 (R1 + R2),
''' which is less. We handle large prime factors with the Bluestein algorithm. Each length-R DFT
''' is handled by a Transformlet. We have a general transformlet for arbitrary R (the
''' Transformlet class), specialized dedicated transformlets for small values of R
''' (LengthTwoTransformlet, LengthThreeTransformlet, etc.) and a BluesteinTransformlet for larger
''' R. (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class Transformlet

    ''' <summary> Initializes a new instance of the <see cref="Transformlet" /> class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="radix">       The radix. </param>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The Nth complex roots of unity. </param>
    Public Sub New(ByVal radix As Integer, ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New()
        Me._Radix = radix
        Me._TotalLength = totalLength
        Me._UnityRoots = unitRoots
    End Sub

    ''' <summary> Gets or sets the total length. </summary>
    ''' <value> The total length. </value>
    Protected ReadOnly Property TotalLength As Integer

    ''' <summary> Gets or sets the Nth complex roots of unity. </summary>
    ''' <value> The unity roots. </value>
    Protected ReadOnly Property UnityRoots As Complex()

    ''' <summary> Gets or sets the radix. </summary>
    ''' <value> The radix. </value>
    Public ReadOnly Property Radix() As Integer

    ''' <summary> The multiplicity. </summary>
    Private _Multiplicity As Integer

    ''' <summary> Gets or sets the multiplicity. </summary>
    ''' <remarks>
    ''' we don't use the Multiplicity in any transformlet methods, but it is used to execute the
    ''' whole plan, and since there is one per transformlet we save ourselves from creating an
    ''' additional container class by storing it in the Transformlet.
    ''' </remarks>
    ''' <value> The multiplicity. </value>
    Public Property Multiplicity() As Integer
        Get
            Return Me._Multiplicity
        End Get
        Friend Set(ByVal value As Integer)
            Me._Multiplicity = value
        End Set
    End Property

    ''' <summary> The FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The x. </param>
    ''' <param name="y">    The y. </param>
    ''' <param name="ns">   The ns. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overridable Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim v(Me.Radix - 1) As Complex
        Dim dx As Integer = Me.TotalLength \ Me.Radix
        For j As Integer = 0 To dx - 1
            ' note: the j-loop could be parallelized, if the v-buffer is not shared between threads
            Dim xi As Integer = j
            Dim ui As Integer = 0
            If sign < 0 Then
                ui = Me.TotalLength
            End If
            Dim du As Integer = (dx \ ns) * (j Mod ns)
            If sign < 0 Then
                du = -du
            End If
            ' basically, we need to copy x[j + r * dx] * u[r * d.u] into v[r]
            ' we do this in a complicated-looking way in order to avoid unnecessary multiplication when u = 1
            ' such a complex multiply requires 6 flops which results in a no-op; we have shown this to be a measurable time-saver
            If False Then
                ' all u-factors are 1, so we can just reference x directly without copying into v
                ' to do this, we need to change FftKernel to accept an offset and stride for x
            Else
                v(0) = x(xi) ' the first u is guaranteed to be 1
                For r As Integer = 1 To Me.Radix - 1
                    xi += dx
                    ui += du
                    v(r) = x(xi) * Me.UnityRoots(ui)
                Next r
            End If
            Dim y0 As Integer = Expand(j, ns, Me.Radix)
            Me.FftKernel(v, y, y0, ns, sign)
        Next j
    End Sub

    ''' <summary> The FFT kernel. </summary>
    ''' <remarks>
    ''' This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1
    ''' case to avoid unnecessary complex multiplications. For good performance, override this with a
    ''' custom kernel for each radix. I am a little worried that this call is virtual. It's not in
    ''' the innermost loop (which is inside it), but it is in the next loop out. But the whole
    ''' transformlet architecture gives a significant performance boost over our last architecture,
    ''' so it's a price I'm willing to pay for now.
    ''' </remarks>
    ''' <param name="x">    The source vector. </param>
    ''' <param name="y">    The target vector. </param>
    ''' <param name="y0">   The initial y index. </param>
    ''' <param name="dy">   The stride. </param>
    ''' <param name="sign"> The gives the sign of the Fourier transform in the exponent. </param>
    Public Overridable Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        Dim dx As Integer = Me.TotalLength \ Me.Radix

        ' y sub I is the y index we are currently computing; initialize it to y0
        Dim yi As Integer = y0

        ' the first index is the zero frequency component that just adds all the x's
        ' encode this specially so we are not unnecessarily multiplying by complex 1 R times
        y(yi) = 0.0
        For j As Integer = 0 To Me.Radix - 1
            y(yi) += x(j)
        Next j

        ' now do the higher index entries
        For i As Integer = 1 To Me.Radix - 1
            yi += dy
            y(yi) = x(0)
            Dim ui As Integer = 0
            If sign < 0 Then
                ui = Me.TotalLength
            End If
            Dim du As Integer = dx * i
            If sign < 0 Then
                du = -du
            End If
            For j As Integer = 1 To Me.Radix - 1
                ui += du
                If ui >= Me.TotalLength Then
                    ui -= Me.TotalLength
                End If
                If ui < 0 Then
                    ui += Me.TotalLength
                End If
                y(yi) += x(j) * Me.UnityRoots(ui)
            Next j
        Next i

    End Sub

    ''' <summary> Expands the specified index L. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="index"> The index L. </param>
    ''' <param name="n1">    The n1. </param>
    ''' <param name="n2">    The n2. </param>
    ''' <returns> The expanded index L. </returns>
    Protected Shared Function Expand(ByVal index As Integer, ByVal n1 As Integer, ByVal n2 As Integer) As Integer
        Return (index \ n1) * n1 * n2 + (index Mod n1)
    End Function

End Class

''' <summary> Class RadixTwoTransformlet. </summary>
''' <remarks>
''' (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class RadixTwoTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixTwoTransformlet" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(2, totalLength, unitRoots)
    End Sub

    ''' <summary> The pass FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The x. </param>
    ''' <param name="y">    The y. </param>
    ''' <param name="ns">   The ns. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim dx As Integer = Me.TotalLength \ 2
        For j As Integer = 0 To dx - 1
            Dim du As Integer = (dx \ ns) * (j Mod ns)
            Dim y0 As Integer = Expand(j, ns, 2)
            If sign < 0 Then
                FftKernel(x(j), x(j + dx) * Me.UnityRoots(Me.TotalLength - du), y(y0), y(y0 + ns))
            Else
                FftKernel(x(j), x(j + dx) * Me.UnityRoots(du), y(y0), y(y0 + ns))
            End If
        Next j
    End Sub

    ''' <summary> Performs a length-2 FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x0"> The x0. </param>
    ''' <param name="x1"> The x1. </param>
    ''' <param name="y0"> [in,out] The y0. </param>
    ''' <param name="y1"> [in,out] The y1. </param>
    Private Overloads Shared Sub FftKernel(ByVal x0 As Complex, ByVal x1 As Complex, ByRef y0 As Complex, ByRef y1 As Complex)

        Dim a0 As Double = x0.Real
        Dim b0 As Double = x0.Imaginary
        Dim a1 As Double = x1.Real
        Dim b1 As Double = x1.Imaginary
        y0 = New Complex(a0 + a1, b0 + b1)
        y1 = New Complex(a0 - a1, b0 - b1)
        ' for some reason, this looks to be faster than using the complex add and subtract; i don't see why

        ' this kernel has 4 flops, all adds/subs

        ' the naive R=2 kernel has 1 complex multiply and 2 complex adds
        ' a complex multiply requires 6 flops and complex add 2 ops
        ' so the naive kernel has 6 * 1 + 2 * 2 = 6 + 4 = 10 flops
        ' we have saved a factor 2.5

    End Sub

End Class

''' <summary> Class RadixThreeTransformlet. </summary>
''' <remarks>
''' (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class RadixThreeTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixThreeTransformlet" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(3, totalLength, unitRoots)
    End Sub

    ''' <summary> The FFT pass. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The x. </param>
    ''' <param name="y">    The y. </param>
    ''' <param name="ns">   The ns. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim dx As Integer = Me.TotalLength \ 3
        For j As Integer = 0 To dx - 1
            Dim du As Integer = (dx \ ns) * (j Mod ns)
            Dim y0 As Integer = Expand(j, ns, 3)
            If sign < 0 Then
                FftKernel(x(j), x(j + dx) * Me.UnityRoots(Me.TotalLength - du),
                          x(j + 2 * dx) * Me.UnityRoots(Me.TotalLength - 2 * du), y(y0), y(y0 + ns), y(y0 + 2 * ns), -1)
            Else
                FftKernel(x(j), x(j + dx) * Me.UnityRoots(du), x(j + 2 * dx) *
                          Me.UnityRoots(2 * du), y(y0), y(y0 + ns), y(y0 + 2 * ns), 1)
            End If
        Next j
    End Sub

    ''' <summary> Performs a length-3 FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x0">   The x0. </param>
    ''' <param name="x1">   The x1. </param>
    ''' <param name="x2">   The x2. </param>
    ''' <param name="y0">   [in,out] The y0. </param>
    ''' <param name="y1">   [in,out] The y1. </param>
    ''' <param name="y2">   [in,out] The y2. </param>
    ''' <param name="sign"> The sign. </param>
    Private Overloads Shared Sub FftKernel(ByVal x0 As Complex, ByVal x1 As Complex, ByVal x2 As Complex, ByRef y0 As Complex, ByRef y1 As Complex, ByRef y2 As Complex, ByVal sign As Integer)
        Dim a12p As Double = x1.Real + x2.Real
        Dim b12p As Double = x1.Imaginary + x2.Imaginary
        Dim sa As Double = x0.Real + R31.Real * a12p
        Dim sb As Double = x0.Imaginary + R31.Real * b12p
        Dim ta As Double = R31.Imaginary * (x1.Real - x2.Real)
        Dim tb As Double = R31.Imaginary * (x1.Imaginary - x2.Imaginary)
        If sign < 0 Then
            ta = -ta
            tb = -tb
        End If
        y0 = New Complex(x0.Real + a12p, x0.Imaginary + b12p)
        y1 = New Complex(sa - tb, sb + ta)
        y2 = New Complex(sa + tb, sb - ta)

        ' this kernel has 16 flops

        ' the naive kernel for R=3 has 4 complex multiplies and 6 complex adds
        ' a complex multiply requires 6 flops and a complex add 2 flops
        ' so the naive kernel has 4 * 6 + 6 * 2 = 24 + 12 = 36 flops
        ' we have saved a factor 2.25, actually a bit more since we have also removed index loop accounting
    End Sub

    ''' <summary> The first r 3. </summary>
    Private Shared ReadOnly R31 As New Complex(-1.0 / 2.0, Math.Sqrt(3.0) / 2.0)

End Class

''' <summary> Class RadixFourTransformlet. </summary>
''' <remarks>
''' (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class RadixFourTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixFourTransformlet" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(4, totalLength, unitRoots)
    End Sub

    ''' <summary> Performs a length-4 FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The source vector. </param>
    ''' <param name="y">    The target vector. </param>
    ''' <param name="y0">   The initial y index. </param>
    ''' <param name="dy">   The stride. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        Dim a02p As Double = x(0).Real + x(2).Real
        Dim b02p As Double = x(0).Imaginary + x(2).Imaginary
        Dim a02m As Double = x(0).Real - x(2).Real
        Dim b02m As Double = x(0).Imaginary - x(2).Imaginary
        Dim a13p As Double = x(1).Real + x(3).Real
        Dim b13p As Double = x(1).Imaginary + x(3).Imaginary
        Dim a13m As Double = x(1).Real - x(1).Real
        Dim b13m As Double = x(1).Imaginary - x(3).Imaginary

        y(y0) = New Complex(a02p + a13p, b02p + b13p)
        y(y0 + dy) = New Complex(a02m - b13m, b02m + a13m)
        y(y0 + 2 * dy) = New Complex(a02p - a13p, b02p - b13p)
        y(y0 + 3 * dy) = New Complex(a02m + b13m, b02m - a13m)

    End Sub

End Class

''' <summary> Class RadixFiveTransformlet. </summary>
''' <remarks>
''' (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class RadixFiveTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixFiveTransformlet" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(5, totalLength, unitRoots)
    End Sub

    ''' <summary> Performs a length-5 FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The source vector. </param>
    ''' <param name="y">    The target vector. </param>
    ''' <param name="y0">   The initial y index. </param>
    ''' <param name="dy">   The stride. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)
        ' first set of combinations
        Dim a14p As Double = x(1).Real + x(4).Real
        Dim a14m As Double = x(1).Real - x(4).Real
        Dim a23p As Double = x(2).Real + x(3).Real
        Dim a23m As Double = x(2).Real - x(3).Real
        Dim b14p As Double = x(1).Imaginary + x(4).Imaginary
        Dim b14m As Double = x(1).Imaginary - x(4).Imaginary
        Dim b23p As Double = x(2).Imaginary + x(3).Imaginary
        Dim b23m As Double = x(2).Imaginary - x(3).Imaginary
        ' second set of combinations, for v[1] and v[4]
        Dim s14a As Double = x(0).Real + R51.Real * a14p + R52.Real * a23p
        Dim s14b As Double = x(0).Imaginary + R51.Real * b14p + R52.Real * b23p
        Dim t14a As Double = R51.Imaginary * a14m + R52.Imaginary * a23m
        Dim t14b As Double = R51.Imaginary * b14m + R52.Imaginary * b23m
        ' second set of combinations, for v[2] and v[3]
        Dim s23a As Double = x(0).Real + R52.Real * a14p + R51.Real * a23p
        Dim s23b As Double = x(0).Imaginary + R52.Real * b14p + R51.Real * b23p
        Dim t23a As Double = R52.Imaginary * a14m - R51.Imaginary * a23m
        Dim t23b As Double = R52.Imaginary * b14m - R51.Imaginary * b23m
        ' take care of sign
        If sign < 0 Then
            t14a = -t14a
            t14b = -t14b
            t23a = -t23a
            t23b = -t23b
        End If
        ' bring together results
        y(y0) = New Complex(x(0).Real + a14p + a23p, x(0).Imaginary + b14p + b23p)
        y(y0 + dy) = New Complex(s14a - t14b, s14b + t14a)
        y(y0 + 2 * dy) = New Complex(s23a - t23b, s23b + t23a)
        y(y0 + 3 * dy) = New Complex(s23a + t23b, s23b - t23a)
        y(y0 + 4 * dy) = New Complex(s14a + t14b, s14b - t14a)
    End Sub

    ''' <summary> The fifth s. </summary>
    Private Shared ReadOnly S5 As Double = Math.Sqrt(5.0)

    ''' <summary> The first r 5. </summary>
    Private Shared ReadOnly R51 As New Complex((S5 - 1.0) / 4.0, Math.Sqrt((5.0 + S5) / 8.0))

    ''' <summary> The second r 5. </summary>
    Private Shared ReadOnly R52 As New Complex(-(S5 + 1.0) / 4.0, Math.Sqrt((5.0 - S5) / 8.0))

End Class

''' <summary> Class RadixSevenTransformlet. </summary>
''' <remarks>
''' (c) 2012 David Wright (http://www.meta-numerics.net). Licensed under the Microsoft Public
''' License (Ms-PL). Unless required by applicable law or agreed to in writing, this software is
''' provided "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </remarks>
Friend Class RadixSevenTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixSevenTransformlet" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(7, totalLength, unitRoots)
    End Sub

    ''' <summary> Performs the length 7 FFT. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">    The source vector. </param>
    ''' <param name="y">    The target vector. </param>
    ''' <param name="y0">   The initial y index. </param>
    ''' <param name="dy">   The stride. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)
        ' relevant sums and differences
        Dim a16p As Double = x(1).Real + x(6).Real
        Dim a16m As Double = x(1).Real - x(6).Real
        Dim a25p As Double = x(2).Real + x(5).Real
        Dim a25m As Double = x(2).Real - x(5).Real
        Dim a34p As Double = x(3).Real + x(4).Real
        Dim a34m As Double = x(3).Real - x(4).Real
        Dim b16p As Double = x(1).Imaginary + x(6).Imaginary
        Dim b16m As Double = x(1).Imaginary - x(6).Imaginary
        Dim b25p As Double = x(2).Imaginary + x(5).Imaginary
        Dim b25m As Double = x(2).Imaginary - x(5).Imaginary
        Dim b34p As Double = x(3).Imaginary + x(4).Imaginary
        Dim b34m As Double = x(3).Imaginary - x(4).Imaginary
        ' combinations used in y[1] and y[6]
        Dim s16a As Double = x(0).Real + R71.Real * a16p + R72.Real * a25p + R73.Real * a34p
        Dim s16b As Double = x(0).Imaginary + R71.Real * b16p + R72.Real * b25p + R73.Real * b34p
        Dim t16a As Double = R71.Imaginary * a16m + R72.Imaginary * a25m + R73.Imaginary * a34m
        Dim t16b As Double = R71.Imaginary * b16m + R72.Imaginary * b25m + R73.Imaginary * b34m
        ' combinations used in y[2] and y[5]
        Dim s25a As Double = x(0).Real + R71.Real * a34p + R72.Real * a16p + R73.Real * a25p
        Dim s25b As Double = x(0).Imaginary + R71.Real * b34p + R72.Real * b16p + R73.Real * b25p
        Dim t25a As Double = R71.Imaginary * a34m - R72.Imaginary * a16m + R73.Imaginary * a25m
        Dim t25b As Double = R71.Imaginary * b34m - R72.Imaginary * b16m + R73.Imaginary * b25m
        ' combinations used in y[3] and y[4]
        Dim s34a As Double = x(0).Real + R71.Real * a25p + R72.Real * a34p + R73.Real * a16p
        Dim s34b As Double = x(0).Imaginary + R71.Real * b25p + R72.Real * b34p + R73.Real * b16p
        Dim t34a As Double = R71.Imaginary * a25m - R72.Imaginary * a34m - R73.Imaginary * a16m
        Dim t34b As Double = R71.Imaginary * b25m - R72.Imaginary * b34m - R73.Imaginary * b16m
        ' if sign is negative, invert t's
        If sign < 0 Then
            t16a = -t16a
            t16b = -t16b
            t25a = -t25a
            t25b = -t25b
            t34a = -t34a
            t34b = -t34b
        End If
        ' combine to get results
        y(y0) = New Complex(x(0).Real + a16p + a25p + a34p, x(0).Imaginary + b16p + b25p + b34p)
        y(y0 + dy) = New Complex(s16a - t16b, s16b + t16a)
        y(y0 + 2 * dy) = New Complex(s25a + t25b, s25b - t25a)
        y(y0 + 3 * dy) = New Complex(s34a + t34b, s34b - t34a)
        y(y0 + 4 * dy) = New Complex(s34a - t34b, s34b + t34a)
        y(y0 + 5 * dy) = New Complex(s25a - t25b, s25b + t25a)
        y(y0 + 6 * dy) = New Complex(s16a + t16b, s16b - t16a)
    End Sub

    ' seventh roots of unity
    ' a la Gauss, these are not expressible in closed form using rational values and rational roots

    ''' <summary> The first r 7. </summary>
    Private Shared ReadOnly R71 As New Complex(0.62348980185873348, 0.7818314824680298)

    ''' <summary> The second r 7. </summary>
    Private Shared ReadOnly R72 As New Complex(-0.22252093395631439, 0.97492791218182362)

    ''' <summary> The third r 7. </summary>
    Private Shared ReadOnly R73 As New Complex(-0.90096886790241915, 0.43388373911755812)

End Class

''' <summary> Class BluesteinTransformlet. </summary>
''' <remarks>
''' The Bluestein technique works as follows: <para>
''' Given the length-N FT</para><para>
''' \tilde{x}_m = \sum_{n=0}^{N-1} x_n \exp{i \pm 2 \pi m n / N}</para><para>
''' use m n = \FRAC{m^2 + n^2 - (m - n)^2}{2} to turn this into</para><para>
''' \tilde{x}_m = \exp{i \pm \pi m^2 / N} \sum_{n=0}^{N-1} x_n \exp{i \pm \pi n^2 / N} \exp{i \mp
''' \pi (m - n)^2 / N}</para><para>
''' The summed expression is a convolution of</para><para>
'''  a_n = x_n \exp{i \pm \pi n^2 / N}</para><para>
'''  b_n = \exp{i \mp \pi n^2 / N}</para><para>
''' A convolution can be done via an FT of any length larger than 2N-1. The 2N is necessary so
''' that a_0 can be multiplied by b_{-N} and a_N can be multiplied by b_0. This thus the
''' sequences to be convolved are</para><para>
'''  0 0  0       0   0  a_0 a_1 a_2 ... a_n 0 0 0 0 0 b_n ... b_2 b_1 b_0 b_1 b_2 ... b_n 0 0 0
''' Since this is a convolution, it doesn't matter how far out we zero-pad. We pick an M &gt;= 2N-
''' 1 that is composed of small prime factors, so we won't need the Bluestein technique to do the
''' convolution itself.</para> (c) 2012 David Wright (http://www.meta-numerics.net).<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Friend Class BluesteinTransformlet
    Inherits Transformlet

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="radix">       The radix. </param>
    ''' <param name="totalLength"> The total length. </param>
    ''' <param name="unitRoots">   The unit roots. </param>
    Public Sub New(ByVal radix As Integer, ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(radix, totalLength, unitRoots)

        ' figure out the right Bluestein length and create a transformer for it
        Me.Nb = SetBluesteinLength(2 * radix - 1)
        Me.Ft = New FourierTransformer(Me.Nb)

        ' compute the Bluestein coefficients and compute the FT of the filter based on them
        Me.B = ComputeBluesteinCoefficients(radix)
        Dim c(Me.Nb - 1) As Complex
        c(0) = 1.0
        For i As Integer = 1 To radix - 1
            c(i) = Me.B(i).Conjugate
            c(Me.Nb - i) = c(i)
        Next i
        Me.Bt = Me.Ft.Transform(c)

    End Sub

    ''' <summary> The Length of convolution transform. </summary>
    ''' <value> The nb. </value>
    Private Property Nb As Integer

    ''' <summary> The Fourier transform for convolution transform. </summary>
    ''' <value> The ft. </value>
    Private Property Ft As FourierTransformer

    ''' <summary> The R Bluestein coefficients. </summary>
    ''' <value> The b. </value>
    Private Property B As Complex()

    ''' <summary>
    ''' The Nb-length Fourier transform of the symmetric Bluestein coefficient filter.
    ''' </summary>
    ''' <value> The bt. </value>
    Private Property Bt As Complex()

    ''' <summary> Computes the Complex Bluestein coefficients. </summary>
    ''' <remarks>
    ''' This method computes b_n = \exp{i \pi n^2 / N}. If we do this naively, by computing sin and
    ''' Cos of \pi n^2 / N, then the argument can get large, up to N \pi, and the inaccuracy of trig
    ''' methods for large arguments will hit us To avoid this, note that the difference n^2 - (n-1)^2
    ''' = 2n-1. So we can add 2n-1 each time and take the result mod 2N to keep the argument less
    ''' than 2 \pi.
    ''' </remarks>
    ''' <param name="radix"> The radix. </param>
    ''' <returns> The Complex he Bluestein coefficients. </returns>
    Private Shared Function ComputeBluesteinCoefficients(ByVal radix As Integer) As Complex()

        Dim b(radix - 1) As Complex
        Dim t As Double = Math.PI / radix
        b(0) = 1.0
        Dim s As Integer = 0
        Dim twoR As Integer = 2 * radix
        For i As Integer = 1 To radix - 1
            s += (2 * i - 1)
            If s >= twoR Then
                s -= twoR
            End If
            Dim ts As Double = t * s
            b(i) = New Complex(Math.Cos(ts), Math.Sin(ts))
        Next i
        Return b
    End Function

    ''' <summary> The FFT kernel. </summary>
    ''' <remarks>
    ''' This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1
    ''' case to avoid unnecessary complex multiplications. For good performance, override this with a
    ''' custom kernel for each radix. I am a little worried that this call is virtual. It's not in
    ''' the innermost loop (which is inside it), but it is in the next loop out. But the whole
    ''' transformlet architecture gives a significant performance boost over our last architecture,
    ''' so it's a price I'm willing to pay for now.
    ''' </remarks>
    ''' <param name="x">    The source vector. </param>
    ''' <param name="y">    The target vector. </param>
    ''' <param name="y0">   The initial y index. </param>
    ''' <param name="dy">   The stride. </param>
    ''' <param name="sign"> The sign. </param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        ' all we have to do here is convolve (b x) with b-star
        ' to do this convolution, we need to multiply the DFT of (b x) with the DFT of b-star, the Inverse DFT the result back
        ' we have already stored the DFT of b-star

        ' create c = b x and transform it into Fourier space
        Dim c(Me.Nb - 1) As Complex
        If sign > 0 Then
            For i As Integer = 0 To Me.Radix - 1
                c(i) = Me.B(i) * x(i)
            Next i
        Else
            For i As Integer = 0 To Me.Radix - 1
                c(i) = Me.B(i) * x(i).Conjugate
            Next i
        End If
        Dim ct() As Complex = Me.Ft.Transform(c)

        ' multiply b-star and c = b x in Fourier space, and inverse transform the product back into configuration space
        For i As Integer = 0 To Me.Nb - 1
            ct(i) = ct(i) * Me.Bt(i)
        Next i
        c = Me._Ft.InverseTransform(ct)

        ' read off the result
        If sign > 0 Then
            For i As Integer = 0 To Me.Radix - 1
                y(y0 + i * dy) = Me.B(i) * c(i)
            Next i
        Else
            For i As Integer = 0 To Me.Radix - 1
                y(y0 + i * dy) = Me.B(i).Conjugate * c(i).Conjugate
            Next i
        End If

        ' for the sign < 0 case, we have used the fact that the convolution of (b-star x) with b is
        ' just the convolution of (b x-star) with b-star, starred

    End Sub

    ''' <summary> Sets the Bluestein length. </summary>
    ''' <remarks>
    ''' This is all about determining a good value to use for the Bluestein length. We choose a
    ''' length based on powers of two and three, since those give very fast Fourier transforms. Our
    ''' method is heuristic and not optimized.
    ''' </remarks>
    ''' <param name="totalLength"> The total length. </param>
    ''' <returns> The Bluestein length. </returns>
    Private Shared Function SetBluesteinLength(ByVal totalLength As Integer) As Integer

        ' try the next power of two
        Dim t As Integer = BluesteinTransformlet.NextPowerOfBase(totalLength, 2)
        Dim m As Integer = t

        ' see if replacing factors of 4 by 3, which shortens the length, will still be long enough
        Do While m Mod 4 = 0
            t = (m \ 4) * 3
            If t < totalLength Then
                Exit Do
            End If
            If t < m Then
                m = t
            End If
        Loop

        ' try the next power of three
        t = BluesteinTransformlet.NextPowerOfBase(totalLength, 3)
        If (t > 0) AndAlso (t < m) Then
            m = t
        End If

        Return m

    End Function

    ''' <summary> Returns the next the power of base. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="n"> The n. </param>
    ''' <param name="b"> The b. </param>
    ''' <returns> The next the power of base. </returns>
    Private Shared Function NextPowerOfBase(ByVal n As Integer, ByVal b As Integer) As Integer
        Dim m As Integer = b
        Do While m <= Int32.MaxValue
            If m >= n Then
                Return m
            End If
            m *= b
        Loop
        Return -1
    End Function

End Class

''' <summary> Class FourierAlgorithms. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Friend NotInheritable Class FourierAlgorithms

    ''' <summary>
    ''' Prevents a default instance of the <see cref="FourierAlgorithms" /> class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The 2*PI
    ''' </summary>
    Public Const TwoPI As Double = 2.0 * Math.PI

    ''' <summary>
    ''' Computes the Complex Nth roots of unity, which are the factors in a length-N Fourier
    ''' transform.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="totalLength"> The total length (N). </param>
    ''' <param name="sign">        The sign. </param>
    ''' <returns>
    ''' The Complex Nth roots of unity, which are the factors in a length-N Fourier transform.
    ''' </returns>
    Public Shared Function ComputeRoots(ByVal totalLength As Integer, ByVal sign As Integer) As Complex()
        Dim u(totalLength) As Complex
        Dim t As Double = sign * TwoPI / totalLength
        u(0) = 1.0
        For r As Integer = 1 To totalLength - 1
            Dim rt As Double = r * t
            u(r) = New Complex(Math.Cos(rt), Math.Sin(rt))
        Next r
        u(totalLength) = 1.0
        Return u
    End Function

End Class
