Imports isr.Algorithms.Signals.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Builds the default caption. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <returns> The caption. </returns>
        Friend Function BuildDefaultCaption() As String
            Dim suffix As New System.Text.StringBuilder
            suffix.Append(" ")
            Return isr.Core.ApplicationInfo.BuildApplicationTitleCaption(suffix.ToString)
        End Function

        ''' <summary> Applies the default trace level. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Friend Sub ApplyTraceLevel()
            Me.Logger.ApplyTraceLevel(My.MySettings.Default.TraceLevel)
        End Sub

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Function TryinitializeKnownState() As Boolean

            Dim activity As String = String.Empty
            Dim affirmative As Boolean = True
            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                activity = $"Application is initializing {IIf(My.MyApplication.InDesignMode, "design", "runtime")} mode."
                Me.SplashTraceEvent(TraceEventType.Information, activity)

                ' Apply command line results.
                If CommandLineInfo.DevicesEnabled.HasValue Then
                    activity = $"{IIf(CommandLineInfo.DevicesEnabled.Value, "Enabled", "Disabling")} use of devices from command line"
                    Me.SplashTraceEvent(TraceEventType.Information, activity)
                End If
                If CommandLineInfo.UserInterface.HasValue Then
                    activity = $"Selected {CommandLineInfo.UserInterface} from the command line"
                    Me.SplashTraceEvent(TraceEventType.Information, activity)
                End If

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.SplashTraceEvent(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
                affirmative = False

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try
            Return affirmative

        End Function

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ProcessShutDown()
            Try
                If My.Application.SaveMySettingsOnExit Then
                    Me.Logger.TraceEventOverride(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Saving assembly settings")
                    ' Save library settings here
                End If
            Catch
            Finally
            End Try
        End Sub

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)
            If e IsNot Nothing Then
                Dim activity As String = "parsing command line"
                Dim args As New isr.Core.ActionEventArgs
                Me.SplashTraceEvent(TraceEventType.Information, My.MyApplication.TraceEventId, activity)
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine, args)
                If e.Cancel Then
                    Me.SplashTraceEvent(TraceEventType.Warning, My.MyApplication.TraceEventId, $"Failed {activity};. {args.Details}")
                End If
            End If
        End Sub

    End Class


End Namespace

