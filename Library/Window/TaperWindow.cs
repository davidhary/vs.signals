﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary>
    /// The base class for the applying taper data windows to signals before calculating the Fourier
    /// transform.
    /// </summary>
    /// <remarks>
    /// David, 2005-11-17. See http://en.wikipedia.org/wiki/Window_function <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class TaperWindow : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TaperWindow" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="windowType"> Type of the window. </param>
        protected TaperWindow( TaperWindowType windowType ) : base()
        {
            this.WindowType = windowType;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make This method Overridable (virtual) because a derived class should not be able to
        /// override This method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this._Amplitudes = null;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The amplitudes. </summary>
        private double[] _Amplitudes;

        /// <summary> Returns the taper window amplitudes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The amplitudes of the taper window. </returns>
        public double[] Amplitudes()
        {
            return this._Amplitudes;
        }

        /// <summary>
        /// Calculates the taper window amplitude for the provided element. The element number is between
        /// 0 to N-1.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="element"> The element. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public abstract double Amplitude( int element );

        /// <summary> Calculates the taper window amplitudes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Holds the element count of the time series. </param>
        /// <returns> The taper window power. </returns>
        public double Create( int elementCount )
        {
            if ( elementCount < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Must be greater than 1" );
            }

            // check if window already created.
            if ( this.ElementCount == elementCount )
            {
                return this.Power;
            }

            // set the size.
            this.ElementCount = elementCount;

            // allocate the amplitude time series
            this._Amplitudes = new double[elementCount];

            // holds the amplitude of the window
            double taperFactor;   // The window amplitude
            int first = this.Amplitudes().GetLowerBound( 0 );
            int last = this.Amplitudes().GetUpperBound( 0 );

            // initialize the sum of squares
            double ssq = 0.0d;

            // apply the same taper factor amplitude to the first and last indexes.
            while ( first < last )
            {

                // calculate the window factor
                taperFactor = this.Amplitude( first );

                // Get the sum of powers
                ssq += taperFactor * taperFactor;

                // Save the amplitude
                this._Amplitudes[first] = taperFactor;
                this._Amplitudes[last] = taperFactor;

                // Shift indexes
                first += 1;
                last -= 1;
            }

            // add the two sides
            ssq += ssq;

            // if odd value we need one more calculation
            if ( elementCount % 2 == 1 )
            {
                // calculate the window factor
                taperFactor = this.Amplitude( first );
                ssq += taperFactor * taperFactor;
                this._Amplitudes[first] = taperFactor;
            }

            // Get the average power
            this.Power = ssq / Convert.ToDouble( elementCount );
            return this.Power;
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> Holds the time series amplitude where the modified signal is
        /// returned. </param>
        /// <returns> The taper window power. </returns>
        public double Apply( Complex[] timeSeries )
        {
            if ( timeSeries is object && timeSeries.Length > 0 )
            {
                if ( this.ElementCount != timeSeries.Length )
                {
                    // if new length, recalculate the window.
                    _ = this.Create( timeSeries.Length );
                }
                // apply the window
                timeSeries.ScaleReals( this.Amplitudes() );
                // return the power 
                return this.Power;
            }
            else
            {
                return 0d;
            }
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> Holds the time series amplitude where the modified signal is
        /// returned. </param>
        /// <returns> The taper window power. </returns>
        public double Apply( double[] timeSeries )
        {
            if ( timeSeries is null || timeSeries.Length == 0 )
            {
                return 0d;
            }

            if ( this._ElementCount != timeSeries.Length )
            {
                // if new length, recalculate the window.
                _ = this.Create( timeSeries.Length );
            }

            // apply the window
            timeSeries.Scale( this.Amplitudes() );

            // return the power 
            return this.Power;
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> Holds the time series amplitude where the modified signal is
        /// returned. </param>
        /// <returns> The taper window power. </returns>
        public double Apply( float[] timeSeries )
        {
            if ( timeSeries is null || timeSeries.Length == 0 )
            {
                return 0d;
            }

            if ( this._ElementCount != timeSeries.Length )
            {
                // if new length, recalculate the window.
                _ = this.Create( timeSeries.Length );
            }
            // apply the window
            timeSeries.Scale( this.Amplitudes() );

            // return the power 
            return this.Power;
        }

        /// <summary>
        /// Gets or sets the base phase.  This value equals 2*Pi/N for cosine-based Windows and 2/N for
        /// linear (e.g., Parzen or Bartlett) Windows.
        /// </summary>
        /// <value> The base phase. </value>
        protected double BasePhase { get; set; }

        /// <summary>
        /// Returns the number of elements in the time series sample that was used to compute the taper
        /// window.
        /// </summary>
        /// <value> The length of the time series. </value>
        public int TimeSeriesLength { get; private set; }

        /// <summary> Number of elements. </summary>
        private int _ElementCount;

        /// <summary> Gets or sets the local size of the taper window. </summary>
        /// <value> The number of elements. </value>
        protected virtual int ElementCount
        {
            get => this._ElementCount;

            set {
                this._ElementCount = value;
                this.TimeSeriesLength = value;
            }
        }

        /// <summary>
        /// Gets or sets the power (average sum of squares of amplitudes) of the taper window.
        /// </summary>
        /// <remarks>
        /// Represents the average amount by which the power of the signal was attenuated due to the
        /// tapering effect of the taper data window.
        /// </remarks>
        /// <value> The power. </value>
        public double Power { get; private set; }

        /// <summary> Gets or sets the window type. </summary>
        /// <value> The type of the window. </value>
        public TaperWindowType WindowType { get; private set; }

        #endregion

    }

    /// <summary> Applies a Bartlett taper window. </summary>
    /// <remarks>
    /// The Bartlett window implements a absolute linear window:
    /// A =  1 - | 2 * k / (N-1) |, k=0,1,...N-1. <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class BartlettTaperWindow : TaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public BartlettTaperWindow() : base( TaperWindowType.Bartlett )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>
        /// Gets or sets the length of the window last created. Used by inherited classes to set the base
        /// phase.
        /// </summary>
        /// <value> The number of elements. </value>
        protected override int ElementCount
        {
            get => base.ElementCount;

            set {
                base.ElementCount = value;
                this.BasePhase = 2d / Convert.ToDouble( value - 1 );
            }
        }

        /// <summary> Calculates the Bartlett taper window amplitude for the provided element. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="element"> The element number (i) for calculating the phase. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public override double Amplitude( int element )
        {
            return 1d - Math.Abs( this.BasePhase * element );
        }

        #endregion

    }

    /// <summary> Applies a Blackman-Harris taper window. </summary>
    /// <remarks>
    /// The Blackman window implements a third order cosine window aimed at reducing side lobes.
    /// <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class BlackmanHarrisTaperWindow : CosineTaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public BlackmanHarrisTaperWindow() : base( TaperWindowType.BlackmanHarris )
        {
            this.A0 = 0.355768d;
            this.A1 = 0.487396d;
            this.A2 = 0.144232d;
            this.A3 = 0.012604d;
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        // onDisposeManagedResources

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

    }

    /// <summary> Applies a Blackman taper window. </summary>
    /// <remarks>
    /// The Blackman window implements a second order cosine window:
    /// A =  0.42 - 0.5 * cos2a + 0.08 * cos4a where a is the base angle Pi*k/N, k=0,1,...N-1.
    /// <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class BlackmanTaperWindow : TaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public BlackmanTaperWindow() : base( TaperWindowType.Blackman )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        // onDisposeManagedResources

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> The Exact Blackman Window coefficients: A0 </summary>
        private const double _A0 = 7938d / 18608d;

        /// <summary> The Exact Blackman Window coefficients: A1 </summary>
        private const double _A1 = 9240d / 18608d;

        /// <summary> The Exact Blackman Window coefficients: A2 </summary>
        private const double _A2 = 1430d / 18608d;

        /// <summary>
        /// Gets or sets the length of the window last created. Used by inherited classes to set the base
        /// phase.
        /// </summary>
        /// <value> The number of elements. </value>
        protected override int ElementCount
        {
            get => base.ElementCount;

            set {
                base.ElementCount = value;
                this.BasePhase = 2d * Math.PI / Convert.ToDouble( value - 1 );
            }
        }

        /// <summary>
        /// Calculates the Blackman taper window amplitude for the provided element. The phase is
        /// calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="element"> The element number (i) for calculating the phase. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public override double Amplitude( int element )
        {
            double cos2a = Math.Cos( this.BasePhase * element );
            double cos4a = 2.0d * cos2a * cos2a - 1.0d;
            return _A0 - _A1 * cos2a + _A2 * cos4a;
        }

        #endregion

    }

    /// <summary> Applies a Cosine taper window. </summary>
    /// <remarks>
    /// The Cosine window implements a generalized Window for the following format:
    /// A =  A0 - A1 * cos2a + A2 * cos4a - A3 * cos6a where a is the base angle Pi*k/N, k=0,1,...N-
    /// 1. The default values of the cosine window are set for the Blackman window, i.e., A0 = 0.42,
    /// A1 = 0.5, A2 = 0.08, and A3 = 0. <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class CosineTaperWindow : TaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TaperWindow" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="windowType"> Type of the window. </param>
        protected CosineTaperWindow( TaperWindowType windowType ) : base( windowType )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="CosineTaperWindow" /> class. </summary>
        /// <remarks> Initializes the coefficients to using the Exact Blackman Window. </remarks>

        // instantiate the base class
        public CosineTaperWindow() : base( TaperWindowType.Cosine )
        {
            this.A0 = 7938d / 18608d;
            this.A1 = 9240d / 18608d;
            this.A2 = 1430d / 18608d;
            this.A3 = 0.0d;
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        // onDisposeManagedResources

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Gets or sets the zero-order (offset) coefficient of the cosine window. </summary>
        /// <value> a 0. </value>
        public double A0 { get; set; }

        /// <summary>
        /// Gets or sets the first-order (cosine(2a)) coefficient of the cosine window.
        /// </summary>
        /// <value> a 1. </value>
        public double A1 { get; set; }

        /// <summary>
        /// Gets or sets the second-order (cosine(4a)) coefficient of the cosine window.
        /// </summary>
        /// <value> a 2. </value>
        public double A2 { get; set; }

        /// <summary>
        /// Gets or sets the third-order (cosine(6a)) coefficient of the cosine window.
        /// </summary>
        /// <value> a 3. </value>
        public double A3 { get; set; }

        /// <summary>
        /// Gets or sets the length of the window last created. Used by inherited classes to set the base
        /// phase.
        /// </summary>
        /// <value> The number of elements. </value>
        protected override int ElementCount
        {
            get => base.ElementCount;

            set {
                base.ElementCount = value;
                this.BasePhase = 2d * Math.PI / Convert.ToDouble( value - 1 );
            }
        }

        /// <summary>
        /// Calculates the Blackman taper window amplitude for the provided element. The phase is
        /// calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="element"> The element number (i) for calculating the phase. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public override double Amplitude( int element )
        {
            double cos2a = Math.Cos( this.BasePhase * element );
            double cos4a = 2.0d * cos2a * cos2a - 1.0d;
            double cos6a = cos2a * (2.0d * cos4a - 1.0d);
            return this.A0 - this.A1 * cos2a + this.A2 * cos4a - this.A3 * cos6a;
        }

        #endregion

    }

    /// <summary> Applies a Hamming taper window. </summary>
    /// <remarks>
    /// The Hamming window implements a first order cosine window:
    /// A =  0.54 - 0.46 * cos2a where a is the base angle Pi*k/N, k=0,1,...N-1. <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class HammingTaperWindow : TaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public HammingTaperWindow() : base( TaperWindowType.Hamming )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> The Hamming Window coefficients: A0. </summary>
        private const double _A0 = 0.54d;

        /// <summary> The Hamming Window coefficients: A1. </summary>
        private const double _A1 = 0.46d;

        /// <summary>
        /// Gets or sets the length of the window last created. Used by inherited classes to set the base
        /// phase.
        /// </summary>
        /// <value> The number of elements. </value>
        protected override int ElementCount
        {
            get => base.ElementCount;

            set {
                base.ElementCount = value;
                this.BasePhase = 2d * Math.PI / Convert.ToDouble( value - 1 );
            }
        }

        /// <summary>
        /// Calculates the Blackman taper window amplitude for the provided element. The phase is
        /// calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="element"> The element number (i) for calculating the phase. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public override double Amplitude( int element )
        {
            return _A0 - _A1 * Math.Cos( this.BasePhase * element );
        }

        #endregion

    }

    /// <summary> Applies a Hanning taper window. </summary>
    /// <remarks>
    /// The Hanning window implements a first order cosine window:
    /// A =  0.5 - 0.5 * cos2a where a is the base angle Pi*k/N, k=0,1,...N-1. <para>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class HanningTaperWindow : TaperWindow
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public HanningTaperWindow() : base( TaperWindowType.Hanning )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> The Hanning Window coefficients: A0. </summary>
        private const double _A0 = 0.5d;

        /// <summary> The Hanning Window coefficients: A1. </summary>
        private const double _A1 = 0.5d;

        /// <summary>
        /// Gets or sets the length of the window last created. Used by inherited classes to set the base
        /// phase.
        /// </summary>
        /// <value> The number of elements. </value>
        protected override int ElementCount
        {
            get => base.ElementCount;

            set {
                base.ElementCount = value;
                this.BasePhase = 2d * Math.PI / Convert.ToDouble( value - 1 );
            }
        }

        /// <summary>
        /// Calculates the Blackman taper window amplitude for the provided element. The phase is
        /// calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="element"> The element number (i) for calculating the phase. </param>
        /// <returns> The amplitude for the provided element. </returns>
        public override double Amplitude( int element )
        {
            return _A0 - _A1 * Math.Cos( this.BasePhase * element );
        }

        #endregion

    }

    /// <summary> Enumerates the taper windows types. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public enum TaperWindowType
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary> An enum constant representing the bartlett option. </summary>
        [System.ComponentModel.Description( "Bartlett" )]
        Bartlett,

        /// <summary> An enum constant representing the blackman option. </summary>
        [System.ComponentModel.Description( "Blackman" )]
        Blackman,

        /// <summary> An enum constant representing the cosine option. </summary>
        [System.ComponentModel.Description( "Cosine" )]
        Cosine,

        /// <summary> An enum constant representing the hamming option. </summary>
        [System.ComponentModel.Description( "Hamming" )]
        Hamming,

        /// <summary> An enum constant representing the hanning option. </summary>
        [System.ComponentModel.Description( "Hanning" )]
        Hanning,

        /// <summary> An enum constant representing the blackman harris option. </summary>
        [System.ComponentModel.Description( "Blackman-Harris" )]
        BlackmanHarris
    }
}