﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Signals

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Digital Signal Processing Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Digital Signal Processing Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Signals.Library"

    End Class

End Namespace


