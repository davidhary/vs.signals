using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isr.Algorithms.Signals.Wisdom
{
    /// <summary>   An array safe handle. </summary>
    /// <remarks>   David, 2020-12-05. </remarks>
    internal class ArraySafeHandle : isr.Core.Win32.SafeHandle
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="dataTypeSize"> Size of the data type. </param>
        /// <param name="handle">       The handle. </param>
        public ArraySafeHandle( int dataTypeSize, IntPtr handle ) : base( handle )
        {
            this._DataTypeSize = dataTypeSize;
        }

        private readonly int _DataTypeSize;

        protected override bool ReleaseHandle()
        {
            if ( this._DataTypeSize == 4 )
            {
                FftwF.SafeNativeMethods.Free( this.handle );
            }
            else if ( this._DataTypeSize == 8 )
            {
                FftwR.SafeNativeMethods.Free( this.handle );
            }
            else
            {
                // if byte size not specified we assume nothing was allocated.
            }
            return true;
        }
    }

}
