Imports System.Runtime.CompilerServices

''' <summary> Integer value extensions.</summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para></remarks>
Public Module IntegerExtensions

#Region " INTEGER ARRAYS "

    ''' <summary> Returns the index of the first maximum of given array. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The index of the first maximum of given array. </returns>
    <Extension()>
    Public Function IndexFirstMaximum(ByVal values() As Integer) As Integer

        If values Is Nothing OrElse values.Length = 0 Then
            Return -1
        End If

        Dim maxIndex As Integer = values.GetLowerBound(0)
        Dim max As Integer = values(maxIndex)
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            Dim candidate As Integer = values(i)
            If max < candidate Then
                max = candidate
                maxIndex = i
            End If
        Next
        Return maxIndex

    End Function

#End Region

End Module
