## ISR Algorithms Signals Forms<sub>&trade;</sub>: DSP Library User Windows Forms
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.1.6969 2019-01-30*  
Uses new core libraries.

*4.0.6166 2016-11-18*  
Uses VS 2015.

*3.0.4805 2013-02-26*  
Changes status bar to status strip.

*3.0.4715 2012-11-28*  
Adds application log extensions. Uses SQL Server
exception message box.

*3.0.4711 2012-11-24*  
Uses complex only functions.

*2.2.4707 2012-11-20*  
Converted to VS10. Uses Mixed Radix FFT from the Meta
Numerics library.

*2.1.4706 2012-11-19*  
Prepared for VS10.

*2.1.4232 2011-08-03*  
Standardizes code elements and documentation.

*2.1.4213 2011-07-15*  
Simplifies the assembly information.

*2.1.2961 2008-02-09*  
Updates to .NET 3.5.

*2.0.2818 2007-09-19*  
Removes Mixed-Radix FFT.

*2.0.2817 2007-09-19*  
Updates to Visual Studio 8. Uses Wisdom FFT.

*1.0.2225 2006-02-03*  
Modifies Taper Filter to specify type and filter
frequencies and transition band. Adds validate Outcome structure..

*1.0.2219 2006-01-28*  
Removes Visual Basic import.

*1.0.2205 2006-01-14*  
Adds new support and exceptions libraries. Uses INt32,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.2147 2005-11-17*  
Converts FFT pro to .NET

\(C\) 1998 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries]{.underline}\
[Core Controls Library](https://bitbucket.org/davidhary/vs.core)  
[Signals Library](https://bitbucket.org/davidhary/vs.signals)
