''' <summary> Class to create signals as real values or complex arrays. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class Signal

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a default constructor.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
    End Sub

#End Region

#Region " FREQUENCIES "

    ''' <summary> Returns a Frequency series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
    '''                               sample used to get the Fourier transform.  This is the
    '''                               inverse of  the frequency between adjacent data points. </param>
    ''' <param name="elements">       The number of elements. </param>
    ''' <returns> A Frequency series. </returns>
    Public Shared Function Frequencies(ByVal sampleInterval As Single, ByVal elements As Integer) As Single()

        Return Signal.Ramp(1.0F / sampleInterval, elements)

    End Function

    ''' <summary> Return frequency values starting at zero. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
    '''                               sample used to get the Fourier transform.  This is the
    '''                               inverse of  the frequency between adjacent data points. </param>
    ''' <param name="elements">       The number of elements. </param>
    ''' <returns> A Frequency series. </returns>
    Public Shared Function Frequencies(ByVal sampleInterval As Double, ByVal elements As Integer) As Double()
        Return Signal.Ramp(1.0R / sampleInterval, elements)
    End Function

    ''' <summary> Return time values starting at zero. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
    '''                               sample used to get the Fourier transform.  This is the
    '''                               inverse of  the frequency between adjacent data points. </param>
    ''' <param name="elements">       The number of elements. </param>
    ''' <returns> A Double() </returns>
    Public Shared Function Times(ByVal sampleInterval As Double, ByVal elements As Integer) As Double()
        Return Signal.Ramp(sampleInterval, elements)
    End Function

#End Region

#Region " MATH "

    ''' <summary>Gets or sets the number of degrees per radian</summary>
    Public Const DegreesPerRadian As Double = 180.0R / Math.PI

    ''' <summary>Gets or sets the number of degrees per radian</summary>
    Public Const RadiansPerDegree As Double = Math.PI / 180.0R

    ''' <summary> Converts radians to degrees. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="radians"> The radians. </param>
    ''' <returns> The value in degrees. </returns>
    Public Shared Function ToDegrees(ByVal radians As Double) As Double

        Return radians * Signal.DegreesPerRadian

    End Function

    ''' <summary> Converts radians to degrees. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="degrees"> The degrees. </param>
    ''' <returns> The value in radians. </returns>
    Public Shared Function ToRadians(ByVal degrees As Double) As Double

        Return degrees * Signal.RadiansPerDegree

    End Function

#End Region

#Region " DC "

    ''' <summary> Returns an array with a DC Level. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="level">    The DC level for the array. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> An array with a DC Level. </returns>
    Public Shared Function DC(ByVal level As Integer, ByVal elements As Integer) As Integer()
        Dim output(elements - 1) As Integer
        For i As Integer = 0 To elements - 1
            output(i) = level
        Next
        Return output
    End Function

    ''' <summary> Returns an array with a DC Level. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="level">    The DC level for the array. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> An array with a DC Level. </returns>
    Public Shared Function DC(ByVal level As Int64, ByVal elements As Integer) As Int64()
        Dim output(elements - 1) As Int64
        For i As Integer = 0 To elements - 1
            output(i) = level
        Next
        Return output
    End Function

    ''' <summary> Returns an array with a DC Level. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="level">    The DC level for the array. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> An array with a DC Level. </returns>
    Public Shared Function DC(ByVal level As Single, ByVal elements As Integer) As Single()
        Dim output(elements - 1) As Single
        For i As Integer = 0 To elements - 1
            output(i) = level
        Next
        Return output
    End Function

    ''' <summary> Returns an array with a DC Level. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="level">    The DC level for the array. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> An array with a DC Level. </returns>
    Public Shared Function DC(ByVal level As Double, ByVal elements As Integer) As Double()
        Dim output(elements - 1) As Double
        For i As Integer = 0 To elements - 1
            output(i) = level
        Next
        Return output
    End Function

#End Region

#Region " RAMP "

    ''' <summary> Returns an array with increasing values starting at zero. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="increment"> Amount to increment by. </param>
    ''' <param name="elements">  The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal increment As Double, ByVal elements As Integer) As Double()
        Dim output(elements - 1) As Double
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = i * increment
        Next
        Return output
    End Function

    ''' <summary> Returns an array with increasing values starting at zero. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="increment"> Amount to increment by. </param>
    ''' <param name="elements">  The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal increment As Integer, ByVal elements As Integer) As Integer()
        Dim output(elements - 1) As Integer
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = i * increment
        Next
        Return output
    End Function

    ''' <summary> Returns an array with increasing values starting at zero. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="increment"> Amount to increment by. </param>
    ''' <param name="elements">  The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal increment As Single, ByVal elements As Integer) As Single()
        Dim output(elements - 1) As Single
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = i * increment
        Next
        Return output
    End Function

    ''' <summary> Returns an array with increasing values starting at the initial value. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="initialValue"> The initial value. </param>
    ''' <param name="increment">    Amount to increment by. </param>
    ''' <param name="elements">     The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal initialValue As Double, ByVal increment As Double, ByVal elements As Integer) As Double()
        Dim output(elements - 1) As Double
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = initialValue + i * increment
        Next
        Return output
    End Function

    ''' <summary> Returns an array with increasing values starting at the initial value. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="initialValue"> The initial value. </param>
    ''' <param name="increment">    Amount to increment by. </param>
    ''' <param name="elements">     The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal initialValue As Integer, ByVal increment As Integer, ByVal elements As Integer) As Integer()
        Dim output(elements - 1) As Integer
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = initialValue + i * increment
        Next
        Return output
    End Function

    ''' <summary> Returns an array with increasing values starting at the initial value. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="initialValue"> The initial value. </param>
    ''' <param name="increment">    Amount to increment by. </param>
    ''' <param name="elements">     The number of elements. </param>
    ''' <returns> An array with ramp values. </returns>
    Public Shared Function Ramp(ByVal initialValue As Single, ByVal increment As Single, ByVal elements As Integer) As Single()
        Dim output(elements - 1) As Single
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = initialValue + i * increment
        Next
        Return output
    End Function

#End Region

#Region " RANDOM "

    ''' <summary> Creates a random uniform [0,1) signal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="seed">     The seed. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> a random uniform [0,1) signal. </returns>
    ''' <example>
    ''' <code lang="VB.NET">
    ''' Sub Form_Click
    ''' Dim elements = 100
    ''' Dim r() As Single = isr.Algorithms.Signals.Signal.Random(elements)
    ''' End Sub
    ''' </code>
    ''' </example>
    Public Shared Function Random(ByVal seed As Double, ByVal elements As Integer) As Double()
        Dim rng As New System.Random(CInt(seed))
        Dim output(elements - 1) As Double
        For i As Integer = 0 To elements - 1
            output(i) = rng.NextDouble
        Next
        Return output
    End Function

    ''' <summary> Creates a random uniform [0,1) signal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="seed">     The seed. </param>
    ''' <param name="elements"> The number of elements. </param>
    ''' <returns> a random uniform [0,1) signal. </returns>
    ''' <example>
    ''' <code lang="VB.NET">
    ''' Sub Form_Click
    ''' Dim elements = 100
    ''' Dim r() As Single = isr.Algorithms.Signals.Signal.Random(elements)
    ''' End Sub
    ''' </code>
    ''' </example>
    Public Shared Function Random(ByVal seed As Single, ByVal elements As Integer) As Single()
        Dim rng As New System.Random(CInt(Math.Max(Math.Min(seed, Integer.MaxValue), Integer.MinValue)))
        Dim output(elements - 1) As Single
        For i As Integer = 0 To elements - 1
            output(i) = CSng(rng.NextDouble)
        Next
        Return output
    End Function

#End Region

#Region " SINE "

    ''' <summary> Calculates a sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequency"> The frequency of the sine wave in cycles per total duration of the
    '''                          signal. </param>
    ''' <param name="phase">     The phase of the sine wave in radians. </param>
    ''' <param name="elements">  The number of elements. </param>
    ''' <returns> a sine wave array. </returns>
    ''' <example>
    ''' <code lang="VB.NET">
    ''' Sub Form_Click
    ''' Dim elements As Integer= 100
    ''' Dim phase as Single = -0.1 * System.Math.PI
    ''' Dim frequency as Single = 1.1
    ''' Dim sine() As Single
    ''' sine = isr.Algorithms.Signals.Signal.Sine(frequency , phase , elements)
    ''' End Sub
    ''' </code>
    ''' </example>
    Public Shared Function Sine(ByVal frequency As Single, ByVal phase As Single, ByVal elements As Integer) As Single()
        Dim output(elements - 1) As Single
        Dim deltaPhase As Double = 2 * System.Math.PI * frequency / elements
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = CSng(System.Math.Sin(phase))
            phase = CSng(phase + deltaPhase)
        Next
        Return output
    End Function

    ''' <summary> Calculates a sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequency"> The frequency of the sine wave in cycles per total duration of the
    '''                          signal. </param>
    ''' <param name="phase">     The phase of the sine wave in radians. </param>
    ''' <param name="elements">  The number of elements. </param>
    ''' <returns> a sine wave array. </returns>
    ''' <example>
    ''' <code lang="VB.NET">
    ''' Sub Form_Click
    ''' Dim elements As Integer = 100
    ''' Dim phase as Double = -0.1 * System.Math.PI
    ''' Dim frequency as Double = 1.1
    ''' Dim sine() As Double
    ''' sine = isr.Algorithms.Signals.Signal.Sine(frequency , phase , elements)
    ''' End Sub
    ''' </code>
    ''' </example>
    Public Shared Function Sine(ByVal frequency As Double, ByVal phase As Double, ByVal elements As Integer) As Double()
        Dim output(elements - 1) As Double
        Dim deltaPhase As Double = 2 * System.Math.PI * frequency / elements
        For i As Integer = 0 To elements - 1
            ' multiplication is used to prevent round off error in Int64 arrays,
            output(i) = System.Math.Sin(phase)
            phase += deltaPhase
        Next
        Return output
    End Function

#End Region

End Class
