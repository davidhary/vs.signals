﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals.Wisdom
{

    /// <summary> Calculates the Fast Fourier Transform using the FFTW algorithm. </summary>
    /// <remarks>
    /// Includes procedures for calculating the forward and inverse Fourier transform. FFTW is best
    /// at handling sizes of the form 2^a x 3^b x 5^c 7^d x 11^e x 13^f, where e + f is either 0 or 1,
    /// and the other exponents are arbitrary. Other sizes are computed by means of a slow, general-
    /// purpose algorithm (which nevertheless retains O(n log n)
    /// performance even for prime sizes). It is possible to customize FFTW for different array sizes;
    /// see Chapter 8 [Installation and Customization], page 57. Transforms whose sizes are powers of
    /// 2 are especially fast.
    /// <para>
    /// FFTW computes an unnormalized transform: computing a forward followed by a backward transform
    /// (or vice versa)
    /// will result in the original data multiplied by the size of the transform (the product of the
    /// dimensions).
    /// </para> <para>
    /// David, 2007-09-01, 2.0.2800 </para><para>
    /// (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Dft : FourierTransformBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public Dft() : base( FourierTransformType.Wisdom )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // Free managed resources when explicitly called
                    }
                    // Free shared unmanaged resources
                    if ( this._ComplexDataF is object )
                    {
                        this._ComplexDataF.Dispose();
                        this._ComplexDataF = null;
                    }

                    if ( this._ComplexDataR is object )
                    {
                        this._ComplexDataR.Dispose();
                        this._ComplexDataR = null;
                    }

                    if ( this._DftPlanF is object )
                    {
                        this._DftPlanF.Dispose();
                        this._DftPlanF = null;
                    }

                    if ( this._DftPlanR is object )
                    {
                        this._DftPlanR.Dispose();
                        this._DftPlanR = null;
                    }
                }
            }
            finally
            {
                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PROPERTIES "

        #endregion

        #region " COMPLEX "

        /// <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The values. </param>
        public override void Forward( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( values ), "Array must be longer than 1" );
            var reals = new double[values.Length];
            var imaginaries = new double[values.Length];
            values.CopyTo( reals, imaginaries );
            this.Forward( reals, imaginaries );
            ComplexArrays.Copy( reals, imaginaries, values );
        }

        #endregion

        #region " DOUBLE "

        /// <summary>
        /// Gets or sets reference to the DFT plan for Double DFT.
        /// </summary>
        private PlanR _DftPlanR;

        /// <summary>
        /// Holds the input and output data.
        /// </summary>
        private ComplexArrayR _ComplexDataR;

        /// <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( double[] reals, double[] imaginaries )
        {

            // Initializes
            base.Forward( reals, imaginaries );

            // execute the DFT
            this._DftPlanR.Execute();

            // get the data
            _ = this._ComplexDataR.CopyTo( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );

            // set the element count
            this.TimeSeriesLength = reals.Length;

            // allocate the data and copy it to the complex array.
            this._ComplexDataR = new ComplexArrayR( reals, imaginaries, false );

            // get a new plan.
            this._DftPlanR = Planner.Create( reals.Length, this._ComplexDataR, this._ComplexDataR, TransformDirection.Forward, PlannerOptions.Estimate );
        }

        #endregion

        #region " SINGLE "

        /// <summary>
        /// Gets or sets reference to the DFT plan for Double DFT.
        /// </summary>
        private PlanF _DftPlanF;

        /// <summary>
        /// Holds the input and output data.
        /// </summary>
        private ComplexArrayF _ComplexDataF;

        /// <summary> Calculates the Mixed-Radix Fourier transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( float[] reals, float[] imaginaries )
        {

            // copy data to the Complex Data Array
            base.Forward( reals, imaginaries );

            // execute the DFT
            this._DftPlanF.Execute();

            // get the data
            _ = this._ComplexDataF.CopyTo( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );

            // set the element count
            this.TimeSeriesLength = reals.Length;

            // allocate the data and copy it to the complex array.
            this._ComplexDataF = new ComplexArrayF( reals, imaginaries, false );

            // get a new plan.
            this._DftPlanF = Planner.Create( this.TimeSeriesLength, this._ComplexDataF, this._ComplexDataF, TransformDirection.Forward, PlannerOptions.Estimate );
        }

        #endregion

    }
}