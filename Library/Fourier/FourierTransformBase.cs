using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary>
    /// Provides a base class for fast and discrete Fourier transform calculations.
    /// </summary>
    /// <remarks>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2098-06-03, 1.0.00. David, 2005-09-27, 1.0.2096, Create based on FFT pro </para>
    /// </remarks>
    public abstract class FourierTransformBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="transformType"> Type of the transform. </param>
        protected FourierTransformBase( FourierTransformType transformType ) : base()
        {
            this.TransformType = transformType;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // Free managed resources when explicitly called
                    }
                    // Free shared unmanaged resources
                    this.DftCache = null;
                    this.DftTable = null;
                    this.SineTable = null;
                    this.CosineTable = null;
                    this.RealCache = null;
                    this.ImaginaryCache = null;
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Length of the time series. </summary>
        private int _TimeSeriesLength;

        /// <summary>
        /// Gets or sets the local size of the time series arrays used for calculating the Fourier
        /// transform.  This value is held separately allowing the first call to determine if any cache
        /// needs to be recalculated in case of the time series length is different from the previous
        /// calculation. Setting the time series length also sets the
        /// <see cref="HalfSpectrumLength">length of the half spectrum</see>.
        /// </summary>
        /// <value> The number of elements. </value>
        public virtual int TimeSeriesLength
        {
            get => this._TimeSeriesLength;

            set {
                this._TimeSeriesLength = value;
                this.HalfSpectrumLength = ( int ) (Math.Floor( this.TimeSeriesLength / 2d ) + 1d);
            }
        }

        /// <summary> The number of elements in half the spectrum. </summary>
        /// <value> The length of the half spectrum. </value>
        public int HalfSpectrumLength { get; private set; }

        /// <summary> The number of elements in the spectrum. </summary>
        /// <value> The length of the spectrum. </value>
        public int FullSpectrumLength => this.TimeSeriesLength;

        /// <summary> Gets or sets the Fourier Transform type. </summary>
        /// <value> The type of the transform. </value>
        public FourierTransformType TransformType { get; private set; }

        #endregion

        #region " COMPLEX "

        /// <summary> Calculates the Forward Fourier Transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> Takes the time series and returns the spectrum values. </param>
        public virtual void Forward( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( values ), "Array must be longer than 1" );
            // Initialize if new transform.  Move out to the calling methods
            this.Initialize( values );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> Holds the inputs and returns the outputs. </param>
        protected virtual void Initialize( Complex[] values )
        {
            this.TimeSeriesLength = values.Length;
        }

        /// <summary> Calculates the Inverse Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> Takes the spectrum values and returns the time series. </param>
        public void Inverse( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( values ), "Array must be longer than 1" );
            values.Swap();
            this.Forward( values );
            values.Swap();
        }

        #endregion

        #region " DOUBLE "

        /// <summary> Calculates the Forward Fourier Transform. </summary>
        /// <remarks>
        /// Swap reals and imaginaries to computer the inverse
        /// Fourier transform.
        /// TODO: Remove initialize and place in the spectrum calculations.
        /// </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public virtual void Forward( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            // Initialize if new transform.  Move out to the calling methods
            this.Initialize( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected virtual void Initialize( double[] reals, double[] imaginaries )
        {
            this.TimeSeriesLength = reals.Length;
        }

        /// <summary> Calculates the Inverse Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void Inverse( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            this.Forward( imaginaries, reals );
        }

        #endregion

        #region " SINGLE "

        /// <summary> Calculates the Forward Fourier Transform. </summary>
        /// <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public virtual void Forward( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            // Initialize if new transform. Move out to the calling methods
            this.Initialize( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected virtual void Initialize( float[] reals, float[] imaginaries )
        {
            this.TimeSeriesLength = reals.Length;
        }

        /// <summary> Calculates the Inverse Fourier Transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void Inverse( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            this.Forward( imaginaries, reals );
        }

        #endregion

        #region " DFT (Sine and Cosine) Tables "

        /// <summary> Returns the cosine table. </summary>
        /// <value> The cosine table. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected double[] CosineTable { get; private set; }

        /// <summary> Returns the sine table. </summary>
        /// <value> The sine table. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected double[] SineTable { get; private set; }

        /// <summary> Returns the cache of real values. </summary>
        /// <value> The real cache. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected double[] RealCache { get; private set; }

        /// <summary> Returns the cache of imaginary values. </summary>
        /// <value> The imaginary cache. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected double[] ImaginaryCache { get; private set; }

        /// <summary> Gets or sets the size of the dft tables. </summary>
        /// <value> The size of the dft tables. </value>
        private int DftTablesSize { get; set; }

        /// <summary>
        /// Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
        /// start at Delta and end at 360 degrees and are suitable for calculating the DFT for the Mixed
        /// Radix Algorithm.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the size of the sine tables. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected bool BuildShiftedDftTables( int elementCount )
        {

            // throw an exception if size not right
            if ( elementCount <= 1 )
            {
                throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Must be greater than 1" );
            }

            // exit if sine tables already constructed
            if ( this.DftTablesSize == elementCount )
            {
                return true;
            }

            // store the new size
            this.DftTablesSize = elementCount;

            // allocate the sine and cosine tables 
            this.CosineTable = new double[elementCount];
            this.SineTable = new double[elementCount];
            this.RealCache = new double[elementCount];
            this.ImaginaryCache = new double[elementCount];

            // cosine and sine factors
            double delta = 2d * Math.PI / Convert.ToDouble( elementCount );
            double cosineDelta = Math.Cos( delta );
            double sineDelta = Math.Sin( delta );

            // Sine and cosine temporary values
            double sineAlpha;
            double cosineAlpha;

            // start at last element 
            int beta = elementCount - 1;

            // Set last element of Cosine table to 1.0
            double cosineBeta = 1.0d;
            this.CosineTable[beta] = cosineBeta;

            // set last element of Sine table to 0.0
            double sineBeta = 0.0d;
            this.SineTable[beta] = sineBeta;

            // Use trigonometric relationships to build the tables
            int alpha = 0;
            do
            {
                cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta;
                sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta;
                cosineBeta = cosineAlpha;
                sineBeta = -sineAlpha;
                this.CosineTable[alpha] = cosineAlpha;
                this.SineTable[alpha] = sineAlpha;
                beta -= 1;
                this.CosineTable[beta] = cosineBeta;
                this.SineTable[beta] = sineBeta;
                alpha += 1;
            }
            while ( alpha < beta );
            return true;
        }

        /// <summary>
        /// Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
        /// start at zero and end at 360 - delta degrees and are suitable for calculating the Discrete
        /// Fourier transform and the sliding FFT.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the size of the sine tables. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected bool BuildDftTables( int elementCount )
        {

            // throw an exception if size not right
            if ( elementCount <= 1 )
            {
                throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Must be greater than 1" );
            }

            // exit if sine tables already constructed
            if ( this.DftTablesSize == elementCount )
            {
                return true;
            }

            // store the new size
            this.DftTablesSize = elementCount;

            // allocate the sine and cosine tables 
            this.CosineTable = new double[elementCount];
            this.SineTable = new double[elementCount];

            // cosine and sine factors
            double delta = 2d * Math.PI / Convert.ToDouble( elementCount );
            double cosineDelta = Math.Cos( delta );
            double sineDelta = Math.Sin( delta );

            // start at the first element
            int alpha = 0;
            int beta = elementCount;

            // Sine and cosine temporary values
            double sineAlpha = 0.0d;
            double cosineAlpha = 1.0d;
            double cosineBeta;
            double sineBeta;

            // Set first element of Cosine table to 1.0
            this.CosineTable[alpha] = cosineAlpha;

            // set first element of Sine table to 0.0
            this.SineTable[alpha] = sineAlpha;

            // Use trigonometric and inverse relationships to
            // build the tables
            do
            {
                // Get the next angle for the left end size, which
                // equals the current angle plus the base angle
                cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta;
                sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta;

                // Store the new values
                cosineAlpha = cosineBeta;
                sineAlpha = sineBeta;

                // Save the next angle on the left-end
                alpha += 1;
                this.CosineTable[alpha] = cosineAlpha;
                this.SineTable[alpha] = sineAlpha;

                // Save the right-end elements as the mirror image
                // of the left end elements
                beta -= 1;
                this.CosineTable[beta] = cosineBeta;
                this.SineTable[beta] = -sineBeta;
            }
            while ( alpha < beta );
            return true;
        }

        /// <summary> Copies the real- and imaginary-parts to the cache. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       . </param>
        /// <param name="imaginaries"> . </param>
        protected void CopyToCache( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );

            // copies arrays to the cache
            this.RealCache = new double[reals.Length + 1];
            reals.CopyTo( this.RealCache, 0 );
            this.ImaginaryCache = new double[imaginaries.Length + 1];
            imaginaries.CopyTo( this.ImaginaryCache, 0 );
        }

        /// <summary> Copies the real- and imaginary-parts to the cache. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       . </param>
        /// <param name="imaginaries"> . </param>
        protected void CopyToCache( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );

            // copies arrays to the cache
            this.RealCache = new double[reals.Length + 1];
            reals.Copy( this.RealCache );
            this.ImaginaryCache = new double[imaginaries.Length + 1];
            imaginaries.Copy( this.ImaginaryCache );
        }

        #endregion

        #region " Complex DFT (Sine and Cosine) Table "

        /// <summary> Returns the complex DFT table. </summary>
        /// <value> The complex DFT table. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected Complex[] DftTable { get; private set; }

        /// <summary> Returns the cache of DFT values. </summary>
        /// <value> The complex cache of the DFT table. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        protected Complex[] DftCache { get; private set; }

        /// <summary> Size of the DFT table. </summary>
        /// <value> The size of the dft table. </value>
        private int DftTableSize { get; set; }

        /// <summary>
        /// Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
        /// start at Delta and end at 360 degrees and are suitable for calculating the DFT for the Mixed
        /// Radix Algorithm.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the size of the sine tables. </param>
        protected void BuildShiftedDftTable( int elementCount )
        {

            // throw an exception if size not right
            if ( elementCount <= 1 )
                throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Must be greater than 1" );

            // exit if sine tables already constructed
            if ( this.DftTableSize == elementCount )
                return;

            // store the new size
            this.DftTableSize = elementCount;

            // allocate DFT cache and tables 
            this.DftTable = new Complex[elementCount];
            this.DftCache = new Complex[elementCount];

            // cosine and sine factors
            double delta = 2d * Math.PI / Convert.ToDouble( elementCount );
            double cosineDelta = Math.Cos( delta );
            double sineDelta = Math.Sin( delta );

            // Sine and cosine temporary values
            double sineAlpha;
            double cosineAlpha;

            // start at last element 
            int beta = elementCount - 1;

            // Set last element of Cosine table to 1.0
            double cosineBeta = 1.0d;

            // set last element of Sine table to 0.0
            double sineBeta = 0.0d;
            this.DftTable[beta] = new Complex( cosineBeta, sineBeta );

            // Use trigonometric relationships to build the tables
            int alpha = 0;
            do
            {
                cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta;
                sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta;
                cosineBeta = cosineAlpha;
                sineBeta = -sineAlpha;
                this.DftTable[alpha] = new Complex( cosineAlpha, sineAlpha );
                beta -= 1;
                this.DftTable[beta] = new Complex( cosineBeta, sineBeta );
                alpha += 1;
            }
            while ( alpha < beta );
        }

        /// <summary>
        /// Builds sine and cosine tables for calculating the DFT. These DFT tables start at zero and end
        /// at 360 - delta degrees and are suitable for calculating the Discrete Fourier transform and
        /// the sliding FFT.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the size of the sine tables. </param>
        protected void BuildDftTable( int elementCount )
        {

            // throw an exception if size not right
            if ( elementCount <= 1 )
                throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Must be greater than 1" );

            // exit if sine tables already constructed
            if ( this.DftTableSize == elementCount )
                return;

            // store the new size
            this.DftTableSize = elementCount;

            // allocate the sine and cosine tables 
            this.DftTable = new Complex[elementCount];

            // cosine and sine factors
            double delta = 2d * Math.PI / elementCount;
            double cosineDelta = Math.Cos( delta );
            double sineDelta = Math.Sin( delta );

            // start at the first element
            int alpha = 0;
            int beta = elementCount;

            // Sine and cosine temporary values
            double sineAlpha = 0.0d;
            double cosineAlpha = 1.0d;
            double cosineBeta;
            double sineBeta;

            // Set first element of Cosine table to 1.0
            // set first element of Sine table to 0.0
            this.DftTable[alpha] = new Complex( cosineAlpha, sineAlpha );

            // Use trigonometric and inverse relationships to
            // build the tables
            do
            {
                // Get the next angle for the left end size, which
                // equals the current angle plus the base angle
                cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta;
                sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta;

                // Store the new values
                cosineAlpha = cosineBeta;
                sineAlpha = sineBeta;

                // Save the next angle on the left-end
                alpha += 1;
                this.DftTable[alpha] = new Complex( cosineAlpha, sineAlpha );

                // Save the right-end elements as the mirror image
                // of the left end elements
                beta -= 1;
                this.DftTable[beta] = new Complex( cosineBeta, -sineBeta );
            }
            while ( alpha < beta );
        }

        /// <summary> Copies the real- and imaginary-parts to the cache. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        protected void CopyToCache( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );

            // copies arrays to the cache
            this.DftCache = new Complex[values.Length];
            values.CopyTo( this.DftCache );
        }

        #endregion

    }
}
