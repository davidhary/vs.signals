using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Algorithms.SignalsTests
{

    /// <summary> Wisdom unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class WisdomTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener);
                _TestSite.AddTraceMessagesQueue(Signals.My.MyLibrary.UnpublishedTraceMessages);
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (_TestSite is object)
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{nameof(TestInfo)} settings should exist");
            double expectedUpperLimit = 12d;
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{nameof(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}");
            TestInfo.ClearMessageQueue();
            Assert.IsTrue(WisdomSettings.Get().Exists, $"{typeof(WisdomSettings)} not found");
            TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get
            {
                return _TestSite;
            }
        }

        #endregion

        #region " SPECTRUM TESTS "

        /// <summary> Tests double precision spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fft">        The FFT. </param>
        /// <param name="scaleFft">   True to scale FFT. </param>
        /// <param name="removeMean"> True to remove mean. </param>
        private static void DoublePrecisionSpectrumTest(Signals.FourierTransformBase fft, bool scaleFft, bool removeMean)
        {
            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            using (var spectrum = new Signals.Spectrum(fft) { IsScaleFft = scaleFft, IsRemoveMean = removeMean })
            {
                spectrum.IsScaleFft = scaleFft;
                spectrum.IsRemoveMean = removeMean;
                Assert.AreEqual(scaleFft, spectrum.IsScaleFft, $"{nameof(Signals.Spectrum.IsScaleFft)} should be {scaleFft}");
            }
        }

        /// <summary> (Unit Test Method) tests double precision discrete Fourier transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestMethod()]
        public void DoublePrecisionDiscreteFourierTransformTest()
        {
            bool scaleFft = true;
            bool removemean = true;
            using (Signals.FourierTransformBase fft = new Signals.Wisdom.Dft())
            {
                DoublePrecisionSpectrumTest(fft, scaleFft, removemean);
            }
        }

        #endregion

    }
}
