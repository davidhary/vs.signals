﻿/// <summary> Class to create signals as real values or complex arrays. </summary>
/// <remarks>
/// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para>
/// </remarks>
using System;

namespace isr.Algorithms.Signals
{
    public sealed class Signal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor to prevent the compiler from generating a default constructor.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private Signal()
        {
        }

        #endregion

        #region " FREQUENCIES "

        /// <summary> Returns a Frequency series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
        /// sample used to get the Fourier transform.  This is the
        /// inverse of  the frequency between adjacent data points. </param>
        /// <param name="elements">       The number of elements. </param>
        /// <returns> A Frequency series. </returns>
        public static float[] Frequencies( float sampleInterval, int elements )
        {
            return Ramp( 1.0f / sampleInterval, elements );
        }

        /// <summary> Return frequency values starting at zero. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
        /// sample used to get the Fourier transform.  This is the
        /// inverse of  the frequency between adjacent data points. </param>
        /// <param name="elements">       The number of elements. </param>
        /// <returns> A Frequency series. </returns>
        public static double[] Frequencies( double sampleInterval, int elements )
        {
            return Ramp( 1.0d / sampleInterval, elements );
        }

        /// <summary> Return time values starting at zero. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sampleInterval"> Specifies the sample interval, i.e., the duration of the time
        /// sample used to get the Fourier transform.  This is the
        /// inverse of  the frequency between adjacent data points. </param>
        /// <param name="elements">       The number of elements. </param>
        /// <returns> A Double() </returns>
        public static double[] Times( double sampleInterval, int elements )
        {
            return Ramp( sampleInterval, elements );
        }

        #endregion

        #region " MATH "

        /// <summary>Gets or sets the number of degrees per radian</summary>
        public const double DegreesPerRadian = 180.0d / Math.PI;

        /// <summary>Gets or sets the number of degrees per radian</summary>
        public const double RadiansPerDegree = Math.PI / 180.0d;

        /// <summary> Converts radians to degrees. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="radians"> The radians. </param>
        /// <returns> The value in degrees. </returns>
        public static double ToDegrees( double radians )
        {
            return radians * DegreesPerRadian;
        }

        /// <summary> Converts radians to degrees. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="degrees"> The degrees. </param>
        /// <returns> The value in radians. </returns>
        public static double ToRadians( double degrees )
        {
            return degrees * RadiansPerDegree;
        }

        #endregion

        #region " DC "

        /// <summary> Returns an array with a DC Level. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="level">    The DC level for the array. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> An array with a DC Level. </returns>
        public static int[] DC( int level, int elements )
        {
            var output = new int[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = level;
            return output;
        }

        /// <summary> Returns an array with a DC Level. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="level">    The DC level for the array. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> An array with a DC Level. </returns>
        public static long[] DC( long level, int elements )
        {
            var output = new long[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = level;
            return output;
        }

        /// <summary> Returns an array with a DC Level. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="level">    The DC level for the array. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> An array with a DC Level. </returns>
        public static float[] DC( float level, int elements )
        {
            var output = new float[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = level;
            return output;
        }

        /// <summary> Returns an array with a DC Level. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="level">    The DC level for the array. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> An array with a DC Level. </returns>
        public static double[] DC( double level, int elements )
        {
            var output = new double[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = level;
            return output;
        }

        #endregion

        #region " RAMP "

        /// <summary> Returns an array with increasing values starting at zero. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="increment"> Amount to increment by. </param>
        /// <param name="elements">  The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static double[] Ramp( double increment, int elements )
        {
            var output = new double[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = i * increment;
            return output;
        }

        /// <summary> Returns an array with increasing values starting at zero. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="increment"> Amount to increment by. </param>
        /// <param name="elements">  The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static int[] Ramp( int increment, int elements )
        {
            var output = new int[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = i * increment;
            return output;
        }

        /// <summary> Returns an array with increasing values starting at zero. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="increment"> Amount to increment by. </param>
        /// <param name="elements">  The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static float[] Ramp( float increment, int elements )
        {
            var output = new float[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = i * increment;
            return output;
        }

        /// <summary> Returns an array with increasing values starting at the initial value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="initialValue"> The initial value. </param>
        /// <param name="increment">    Amount to increment by. </param>
        /// <param name="elements">     The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static double[] Ramp( double initialValue, double increment, int elements )
        {
            var output = new double[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = initialValue + i * increment;
            return output;
        }

        /// <summary> Returns an array with increasing values starting at the initial value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="initialValue"> The initial value. </param>
        /// <param name="increment">    Amount to increment by. </param>
        /// <param name="elements">     The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static int[] Ramp( int initialValue, int increment, int elements )
        {
            var output = new int[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = initialValue + i * increment;
            return output;
        }

        /// <summary> Returns an array with increasing values starting at the initial value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="initialValue"> The initial value. </param>
        /// <param name="increment">    Amount to increment by. </param>
        /// <param name="elements">     The number of elements. </param>
        /// <returns> An array with ramp values. </returns>
        public static float[] Ramp( float initialValue, float increment, int elements )
        {
            var output = new float[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = initialValue + i * increment;
            return output;
        }

        #endregion

        #region " RANDOM "

        /// <summary> Creates a random uniform [0,1) signal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="seed">     The seed. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> a random uniform [0,1) signal. </returns>
        /// <example>
        /// <code lang="VB.NET">
        /// Sub Form_Click
        /// Dim elements = 100
        /// Dim r() As Single = isr.Algorithms.Signals.Signal.Random(elements)
        /// End Sub
        /// </code>
        /// </example>
        public static double[] Random( double seed, int elements )
        {
            var rng = new Random( ( int ) seed );
            var output = new double[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = rng.NextDouble();
            return output;
        }

        /// <summary> Creates a random uniform [0,1) signal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="seed">     The seed. </param>
        /// <param name="elements"> The number of elements. </param>
        /// <returns> a random uniform [0,1) signal. </returns>
        /// <example>
        /// <code lang="VB.NET">
        /// Sub Form_Click
        /// Dim elements = 100
        /// Dim r() As Single = isr.Algorithms.Signals.Signal.Random(elements)
        /// End Sub
        /// </code>
        /// </example>
        public static float[] Random( float seed, int elements )
        {
            var rng = new Random( ( int ) Math.Max( Math.Min( seed, int.MaxValue ), int.MinValue ) );
            var output = new float[elements];
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
                output[i] = ( float ) rng.NextDouble();
            return output;
        }

        #endregion

        #region " SINE "

        /// <summary> Calculates a sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> The frequency of the sine wave in cycles per total duration of the
        /// signal. </param>
        /// <param name="phase">     The phase of the sine wave in radians. </param>
        /// <param name="elements">  The number of elements. </param>
        /// <returns> a sine wave array. </returns>
        /// <example>
        /// <code lang="VB.NET">
        /// Sub Form_Click
        /// Dim elements As Integer= 100
        /// Dim phase as Single = -0.1 * System.Math.PI
        /// Dim frequency as Single = 1.1
        /// Dim sine() As Single
        /// sine = isr.Algorithms.Signals.Signal.Sine(frequency , phase , elements)
        /// End Sub
        /// </code>
        /// </example>
        public static float[] Sine( float frequency, float phase, int elements )
        {
            var output = new float[elements];
            double deltaPhase = 2d * Math.PI * frequency / elements;
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
            {
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = ( float ) Math.Sin( phase );
                phase = ( float ) (phase + deltaPhase);
            }

            return output;
        }

        /// <summary> Calculates a sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> The frequency of the sine wave in cycles per total duration of the
        /// signal. </param>
        /// <param name="phase">     The phase of the sine wave in radians. </param>
        /// <param name="elements">  The number of elements. </param>
        /// <returns> a sine wave array. </returns>
        /// <example>
        /// <code lang="VB.NET">
        /// Sub Form_Click
        /// Dim elements As Integer = 100
        /// Dim phase as Double = -0.1 * System.Math.PI
        /// Dim frequency as Double = 1.1
        /// Dim sine() As Double
        /// sine = isr.Algorithms.Signals.Signal.Sine(frequency , phase , elements)
        /// End Sub
        /// </code>
        /// </example>
        public static double[] Sine( double frequency, double phase, int elements )
        {
            var output = new double[elements];
            double deltaPhase = 2d * Math.PI * frequency / elements;
            for ( int i = 0, loopTo = elements - 1; i <= loopTo; i++ )
            {
                // multiplication is used to prevent round off error in Int64 arrays,
                output[i] = Math.Sin( phase );
                phase += deltaPhase;
            }

            return output;
        }

        #endregion

    }
}