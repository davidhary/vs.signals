﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Calculates the Discrete Fourier Transform. </summary>
    /// <remarks>
    /// Includes procedures for calculating the forward and inverse DFT.<p>
    /// Benchmarks:</p><p>
    /// CPU     Data    Speed</p><p>
    /// Score   Points  (ms)</p><p>
    /// 7.1     1000    1</p><p>
    /// 7.1     1024    1</p><p>
    /// 7.1     8192    10</p> <para>
    /// David, 2098-06-03. 1.0.00  </para><para>
    /// David, 2005-09-27. 1.0.2096. Create based on FFT pro. (c) 1998 Integrated Scientific Resources,
    /// Inc. All rights reserved.  </para><para>
    /// Licensed under The MIT License.</para><para></para>
    /// </remarks>
    public class DiscreteFourierTransform : FourierTransformBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public DiscreteFourierTransform() : base( FourierTransformType.Dft )
        {
        }

        #endregion

        #region " COMPLEX "

        /// <summary> Calculates the Forward Fourier Transform. </summary>
        /// <remarks> Swap real and imaginary values to compute the inverse Fourier transform. </remarks>
        /// <param name="values"> Takes the time series and returns the spectrum values. </param>
        public override void Forward( Complex[] values )
        {
            base.Forward( values );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the inputs and returns the outputs. </param>
        protected override void Initialize( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            // build the DFT table
            this.BuildDftTable( values.Length );

            // Set element count
            this.TimeSeriesLength = values.Length;

            // save initial values to the cache
            this.CopyToCache( values );
            Complex value;
            int k;
            for ( int j = 0, loopTo = base.TimeSeriesLength - 1; j <= loopTo; j++ )
            {
                value = this.DftCache[0];
                k = 0;
                for ( int i = 1, loopTo1 = base.TimeSeriesLength - 1; i <= loopTo1; i++ )
                {
                    k += j;
                    if ( k >= base.TimeSeriesLength )
                    {
                        k -= base.TimeSeriesLength;
                    }

                    value += this.DftCache[i] * this.DftTable[k].Conjugate();
                }

                values[j] = value;
            }
        }

        #endregion

        #region " DOUBLE "

        /// <summary> Calculates the Discrete Fourier Transform. </summary>
        /// <remarks>
        /// Computes the DFT directly without using any of the special rotation and permutation
        /// algorithms that are part of the Fast Fourier Transform (FFT)
        /// literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        /// </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( double[] reals, double[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // build the DFT tables
            _ = this.BuildDftTables( reals.Length );

            // Set element count
            base.TimeSeriesLength = reals.Length;

            // save initial values to the cache
            this.CopyToCache( reals, imaginaries );
            double realValue;
            double imaginaryValue;
            int k;
            for ( int j = 0, loopTo = base.TimeSeriesLength - 1; j <= loopTo; j++ )
            {
                realValue = this.RealCache[0];
                imaginaryValue = this.ImaginaryCache[0];
                k = 0;
                for ( int i = 1, loopTo1 = base.TimeSeriesLength - 1; i <= loopTo1; i++ )
                {
                    k += j;
                    if ( k >= base.TimeSeriesLength )
                    {
                        k -= base.TimeSeriesLength;
                    }

                    realValue += this.CosineTable[k] * this.RealCache[i] + this.SineTable[k] * this.ImaginaryCache[i];
                    imaginaryValue += this.CosineTable[k] * this.ImaginaryCache[i] - this.SineTable[k] * this.RealCache[i];
                }

                reals[j] = realValue;
                imaginaries[j] = imaginaryValue;
            }
        }

        #endregion

        #region " SINGLE "

        /// <summary> Calculates the Discrete Fourier Transform. </summary>
        /// <remarks>
        /// Computes the DFT directly without using any of the special rotation and permutation
        /// algorithms that are part of the Fast Fourier Transform (FFT)
        /// literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
        /// </remarks>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public override void Forward( float[] reals, float[] imaginaries )
        {
            base.Forward( reals, imaginaries );
        }

        /// <summary> Initializes the transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        protected override void Initialize( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );

            // build the DFT tables
            _ = this.BuildDftTables( reals.Length );

            // Set element count
            base.TimeSeriesLength = reals.Length;

            // save initial values to the cache
            this.CopyToCache( reals, imaginaries );
            double realValue;
            double imaginaryValue;
            int k;
            for ( int j = 0, loopTo = base.TimeSeriesLength - 1; j <= loopTo; j++ )
            {
                realValue = this.RealCache[0];
                imaginaryValue = this.ImaginaryCache[0];
                k = 0;
                for ( int i = 1, loopTo1 = base.TimeSeriesLength - 1; i <= loopTo1; i++ )
                {
                    k += j;
                    if ( k >= base.TimeSeriesLength )
                    {
                        k -= base.TimeSeriesLength;
                    }

                    realValue += this.CosineTable[k] * this.RealCache[i] + this.SineTable[k] * this.ImaginaryCache[i];
                    imaginaryValue += this.CosineTable[k] * this.ImaginaryCache[i] - this.SineTable[k] * this.RealCache[i];
                }

                reals[j] = ( float ) realValue;
                imaginaries[j] = ( float ) imaginaryValue;
            }
        }

        #endregion

    }
}