## ISR Algorithms Signals Library<sub>&trade;</sub>: Digital Signal Processing Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.1.7645 2020-12-05*  
Uses safe handle.

*4.1.7607 2020-10-29*  
Converted to C#.

*4.1.6969 2019-01-30*  
Uses new core libraries.

*4.1.6667 2018-04-03*  
2018 release.

*4.0.6166 2016-11-18*  
Uses VS 2015.

*3.0.5133 2014-01-20*  
Replaces invalid scale factor exception and null
spectrum densities exception and spectrum. Scales exception with Invalid
operation exception. Documents return values for functions. Removes
interfaces. Removes the validation outcome construct. Removes the
convergence exception. Replaces dimension mismatch with argument out of
range exception. Sets assembly product to 2014.

*3.0.4763 2013-01-15*  
Adds array copy from double to single types.

*3.0.4711 2012-11-24*  
Uses complex only functions.

*2.2.4707 2012-11-20*  
Converted to VS10. Uses Mixed Radix FFT from the Meta
Numerics library.

*2.1.4706 2012-11-19*  
Prepared for VS10.

*2.1.4232 2011-08-03*  
Standardizes code elements and documentation.

*2.1.4213 2011-07-15*  
Simplifies the assembly information.

*2.1.2961 2008-02-09*  
Updates to .NET 3.5.

*2.0.2818 2007-09-19*  
Removes Mixed-Radix FFT.

*2.0.2817 2007-09-19*  
Updates to Visual Studio 8. Uses Wisdom FFT.

*1.0.2225 2006-02-03*  
Modifies Taper Filter to specify type and filter
frequencies and transition band. Adds validate Outcome structure..

*1.0.2219 2006-01-28*  
Removes Visual Basic import.

*1.0.2205 2006-01-14*  
Adds new support and exceptions libraries. Uses INt32,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.2147 2005-11-17 Converts FFT pro to .NET

*1.0 1998 FFT pro.

\(C\) 1998 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudio.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Signals Library](https://bitbucket.org/davidhary/vs.signals)  
[Meta Numerics](http://www.meta-numerics.net)  
[Wisdom FFT](http://www.fftw.org/)  
[Wisdom FFT C\#](http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html)
