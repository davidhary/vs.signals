﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Algorithms.Signals.Forms
{
    [DesignerGenerated()]
    public partial class SpectrumView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __PointsToDisplayTextBox = new TextBox();
            __PointsToDisplayTextBox.Validating += new System.ComponentModel.CancelEventHandler(PointsToDisplayTextBox_Validating);
            _SignalListBox = new ListBox();
            _DataTabPage = new TabPage();
            _TimingTextBox = new TextBox();
            _TimingListBoxLabel = new Label();
            _PointsToDisplayTextBoxLabel = new Label();
            _SignalListBoxLabel = new Label();
            _DoubleRadioButton = new RadioButton();
            _RemoveMeanCheckBox = new CheckBox();
            _ExampleComboBox = new ComboBox();
            _TaperWindowCheckBox = new CheckBox();
            _Tabs = new TabControl();
            _SignalTabPage = new TabPage();
            __SignalChartPanel = new Panel();
            __SignalChartPanel.Paint += new PaintEventHandler(SignalChartPanel_Paint);
            __SignalChartPanel.Resize += new EventHandler(SignalChartPanel_Resize);
            _SignalOptionsPanel = new Panel();
            _SignalComboBoxLabel = new Label();
            __SignalComboBox = new ComboBox();
            __SignalComboBox.Validated += new EventHandler(SignalComboBox_Validated);
            _SignalDurationTextBox = new TextBox();
            _SignalDurationTextBoxLabel = new Label();
            __PointsTextBox = new TextBox();
            __PointsTextBox.Validating += new System.ComponentModel.CancelEventHandler(PointsTextBox_Validating);
            __PhaseTextBox = new TextBox();
            __PhaseTextBox.Validating += new System.ComponentModel.CancelEventHandler(PhaseTextBox_Validating);
            __CyclesTextBox = new TextBox();
            __CyclesTextBox.Validating += new System.ComponentModel.CancelEventHandler(CyclesTextBox_Validating);
            _PointsTextBoxLabel = new Label();
            _PhaseTextBoxLabel = new Label();
            _CyclesTextBoxLabel = new Label();
            _SpectrumTabPage = new TabPage();
            __SpectrumChartPanel = new Panel();
            __SpectrumChartPanel.Paint += new PaintEventHandler(SpectrumChartPanel_Paint);
            __SpectrumChartPanel.Resize += new EventHandler(SpectrumChartPanel_Resize);
            _SpectrumOptionsPanel = new Panel();
            _FilterComboBoxLabel = new Label();
            _FilterComboBox = new ComboBox();
            _SingleRadioButton = new RadioButton();
            _ExampleComboBoxLabel = new Label();
            __StartStopCheckBox = new CheckBox();
            __StartStopCheckBox.CheckedChanged += new EventHandler(StartStopCheckBox_CheckedChanged);
            _MessagesTabPage = new TabPage();
            _MessagesList = new Core.Forma.MessagesBox();
            _ErrorProvider = new ErrorProvider(components);
            _StatusStrip = new StatusStrip();
            _StatusToolStripStatusLabel = new ToolStripStatusLabel();
            _CountToolStripStatusLabel = new ToolStripStatusLabel();
            _ErrorToolStripStatusLabel = new ToolStripStatusLabel();
            _TimeToolStripStatusLabel = new ToolStripStatusLabel();
            _DataTabPage.SuspendLayout();
            _Tabs.SuspendLayout();
            _SignalTabPage.SuspendLayout();
            _SignalOptionsPanel.SuspendLayout();
            _SpectrumTabPage.SuspendLayout();
            _SpectrumOptionsPanel.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _PointsToDisplayTextBox
            // 
            __PointsToDisplayTextBox.AcceptsReturn = true;
            __PointsToDisplayTextBox.BackColor = SystemColors.Window;
            __PointsToDisplayTextBox.Cursor = Cursors.IBeam;
            __PointsToDisplayTextBox.ForeColor = SystemColors.WindowText;
            __PointsToDisplayTextBox.Location = new Point(129, 15);
            __PointsToDisplayTextBox.MaxLength = 0;
            __PointsToDisplayTextBox.Name = "__PointsToDisplayTextBox";
            __PointsToDisplayTextBox.RightToLeft = RightToLeft.No;
            __PointsToDisplayTextBox.Size = new Size(65, 25);
            __PointsToDisplayTextBox.TabIndex = 1;
            // 
            // _SignalListBox
            // 
            _SignalListBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _SignalListBox.BackColor = SystemColors.Window;
            _SignalListBox.Cursor = Cursors.Default;
            _SignalListBox.ForeColor = SystemColors.WindowText;
            _SignalListBox.ItemHeight = 17;
            _SignalListBox.Location = new Point(272, 24);
            _SignalListBox.Name = "_SignalListBox";
            _SignalListBox.RightToLeft = RightToLeft.No;
            _SignalListBox.Size = new Size(295, 293);
            _SignalListBox.TabIndex = 5;
            // 
            // _DataTabPage
            // 
            _DataTabPage.Controls.Add(_TimingTextBox);
            _DataTabPage.Controls.Add(_TimingListBoxLabel);
            _DataTabPage.Controls.Add(__PointsToDisplayTextBox);
            _DataTabPage.Controls.Add(_SignalListBox);
            _DataTabPage.Controls.Add(_PointsToDisplayTextBoxLabel);
            _DataTabPage.Controls.Add(_SignalListBoxLabel);
            _DataTabPage.Location = new Point(4, 26);
            _DataTabPage.Name = "_DataTabPage";
            _DataTabPage.Size = new Size(578, 338);
            _DataTabPage.TabIndex = 2;
            _DataTabPage.Text = "Data";
            // 
            // _TimingTextBox
            // 
            _TimingTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _TimingTextBox.Location = new Point(16, 64);
            _TimingTextBox.Multiline = true;
            _TimingTextBox.Name = "_TimingTextBox";
            _TimingTextBox.Size = new Size(246, 253);
            _TimingTextBox.TabIndex = 3;
            // 
            // _TimingListBoxLabel
            // 
            _TimingListBoxLabel.AutoSize = true;
            _TimingListBoxLabel.Location = new Point(15, 45);
            _TimingListBoxLabel.Name = "_TimingListBoxLabel";
            _TimingListBoxLabel.Size = new Size(85, 17);
            _TimingListBoxLabel.TabIndex = 2;
            _TimingListBoxLabel.Text = "Timing Data: ";
            // 
            // _PointsToDisplayTextBoxLabel
            // 
            _PointsToDisplayTextBoxLabel.AutoSize = true;
            _PointsToDisplayTextBoxLabel.BackColor = SystemColors.Control;
            _PointsToDisplayTextBoxLabel.Cursor = Cursors.Default;
            _PointsToDisplayTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PointsToDisplayTextBoxLabel.Location = new Point(16, 19);
            _PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel";
            _PointsToDisplayTextBoxLabel.RightToLeft = RightToLeft.No;
            _PointsToDisplayTextBoxLabel.Size = new Size(112, 17);
            _PointsToDisplayTextBoxLabel.TabIndex = 0;
            _PointsToDisplayTextBoxLabel.Text = "Points to Display: ";
            _PointsToDisplayTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _SignalListBoxLabel
            // 
            _SignalListBoxLabel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _SignalListBoxLabel.AutoSize = true;
            _SignalListBoxLabel.BackColor = SystemColors.Control;
            _SignalListBoxLabel.Cursor = Cursors.Default;
            _SignalListBoxLabel.ForeColor = SystemColors.ControlText;
            _SignalListBoxLabel.Location = new Point(298, 5);
            _SignalListBoxLabel.Name = "_SignalListBoxLabel";
            _SignalListBoxLabel.RightToLeft = RightToLeft.No;
            _SignalListBoxLabel.Size = new Size(235, 17);
            _SignalListBoxLabel.TabIndex = 4;
            _SignalListBoxLabel.Text = "Signal                       IFFT{ FFT{ Signal}}";
            // 
            // _DoubleRadioButton
            // 
            _DoubleRadioButton.AutoSize = true;
            _DoubleRadioButton.Checked = true;
            _DoubleRadioButton.Location = new Point(124, 9);
            _DoubleRadioButton.Name = "_DoubleRadioButton";
            _DoubleRadioButton.Size = new Size(124, 21);
            _DoubleRadioButton.TabIndex = 0;
            _DoubleRadioButton.TabStop = true;
            _DoubleRadioButton.Text = "Double Precision";
            // 
            // _RemoveMeanCheckBox
            // 
            _RemoveMeanCheckBox.AutoSize = true;
            _RemoveMeanCheckBox.BackColor = SystemColors.Control;
            _RemoveMeanCheckBox.Cursor = Cursors.Default;
            _RemoveMeanCheckBox.ForeColor = SystemColors.ControlText;
            _RemoveMeanCheckBox.Location = new Point(8, 9);
            _RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox";
            _RemoveMeanCheckBox.RightToLeft = RightToLeft.No;
            _RemoveMeanCheckBox.Size = new Size(111, 21);
            _RemoveMeanCheckBox.TabIndex = 0;
            _RemoveMeanCheckBox.Text = "Remove Mean";
            _RemoveMeanCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ExampleComboBox
            // 
            _ExampleComboBox.BackColor = SystemColors.Window;
            _ExampleComboBox.Cursor = Cursors.Default;
            _ExampleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _ExampleComboBox.ForeColor = SystemColors.WindowText;
            _ExampleComboBox.Items.AddRange(new object[] { "Mixed Radix FFT", "Sliding FFT" });
            _ExampleComboBox.Location = new Point(395, 34);
            _ExampleComboBox.Name = "_ExampleComboBox";
            _ExampleComboBox.RightToLeft = RightToLeft.No;
            _ExampleComboBox.Size = new Size(178, 25);
            _ExampleComboBox.TabIndex = 5;
            // 
            // _TaperWindowCheckBox
            // 
            _TaperWindowCheckBox.AutoSize = true;
            _TaperWindowCheckBox.BackColor = SystemColors.Control;
            _TaperWindowCheckBox.Cursor = Cursors.Default;
            _TaperWindowCheckBox.ForeColor = SystemColors.ControlText;
            _TaperWindowCheckBox.Location = new Point(8, 34);
            _TaperWindowCheckBox.Name = "_TaperWindowCheckBox";
            _TaperWindowCheckBox.RightToLeft = RightToLeft.No;
            _TaperWindowCheckBox.Size = new Size(112, 21);
            _TaperWindowCheckBox.TabIndex = 1;
            _TaperWindowCheckBox.Text = "Taper Window";
            _TaperWindowCheckBox.UseVisualStyleBackColor = false;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_SignalTabPage);
            _Tabs.Controls.Add(_SpectrumTabPage);
            _Tabs.Controls.Add(_DataTabPage);
            _Tabs.Controls.Add(_MessagesTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.Location = new Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new Size(586, 368);
            _Tabs.TabIndex = 2;
            // 
            // _SignalTabPage
            // 
            _SignalTabPage.Controls.Add(__SignalChartPanel);
            _SignalTabPage.Controls.Add(_SignalOptionsPanel);
            _SignalTabPage.Location = new Point(4, 26);
            _SignalTabPage.Name = "_SignalTabPage";
            _SignalTabPage.Size = new Size(578, 338);
            _SignalTabPage.TabIndex = 0;
            _SignalTabPage.Text = "Signal";
            // 
            // _SignalChartPanel
            // 
            __SignalChartPanel.Dock = DockStyle.Fill;
            __SignalChartPanel.Location = new Point(0, 60);
            __SignalChartPanel.Name = "__SignalChartPanel";
            __SignalChartPanel.Size = new Size(578, 278);
            __SignalChartPanel.TabIndex = 30;
            // 
            // _SignalOptionsPanel
            // 
            _SignalOptionsPanel.Controls.Add(_SignalComboBoxLabel);
            _SignalOptionsPanel.Controls.Add(__SignalComboBox);
            _SignalOptionsPanel.Controls.Add(_SignalDurationTextBox);
            _SignalOptionsPanel.Controls.Add(_SignalDurationTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(__PointsTextBox);
            _SignalOptionsPanel.Controls.Add(__PhaseTextBox);
            _SignalOptionsPanel.Controls.Add(__CyclesTextBox);
            _SignalOptionsPanel.Controls.Add(_PointsTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(_PhaseTextBoxLabel);
            _SignalOptionsPanel.Controls.Add(_CyclesTextBoxLabel);
            _SignalOptionsPanel.Dock = DockStyle.Top;
            _SignalOptionsPanel.Location = new Point(0, 0);
            _SignalOptionsPanel.Name = "_SignalOptionsPanel";
            _SignalOptionsPanel.Size = new Size(578, 60);
            _SignalOptionsPanel.TabIndex = 0;
            // 
            // _SignalComboBoxLabel
            // 
            _SignalComboBoxLabel.AutoSize = true;
            _SignalComboBoxLabel.Location = new Point(370, 8);
            _SignalComboBoxLabel.Name = "_SignalComboBoxLabel";
            _SignalComboBoxLabel.Size = new Size(46, 17);
            _SignalComboBoxLabel.TabIndex = 8;
            _SignalComboBoxLabel.Text = "Signal:";
            // 
            // _SignalComboBox
            // 
            __SignalComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            __SignalComboBox.FormattingEnabled = true;
            __SignalComboBox.Location = new Point(418, 4);
            __SignalComboBox.Name = "__SignalComboBox";
            __SignalComboBox.Size = new Size(121, 25);
            __SignalComboBox.TabIndex = 9;
            // 
            // _SignalDurationTextBox
            // 
            _SignalDurationTextBox.AcceptsReturn = true;
            _SignalDurationTextBox.BackColor = SystemColors.Window;
            _SignalDurationTextBox.Cursor = Cursors.IBeam;
            _SignalDurationTextBox.ForeColor = SystemColors.WindowText;
            _SignalDurationTextBox.Location = new Point(107, 30);
            _SignalDurationTextBox.MaxLength = 0;
            _SignalDurationTextBox.Name = "_SignalDurationTextBox";
            _SignalDurationTextBox.RightToLeft = RightToLeft.No;
            _SignalDurationTextBox.Size = new Size(46, 25);
            _SignalDurationTextBox.TabIndex = 3;
            // 
            // _SignalDurationTextBoxLabel
            // 
            _SignalDurationTextBoxLabel.AutoSize = true;
            _SignalDurationTextBoxLabel.BackColor = SystemColors.Control;
            _SignalDurationTextBoxLabel.Cursor = Cursors.Default;
            _SignalDurationTextBoxLabel.ForeColor = SystemColors.ControlText;
            _SignalDurationTextBoxLabel.Location = new Point(8, 34);
            _SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel";
            _SignalDurationTextBoxLabel.RightToLeft = RightToLeft.No;
            _SignalDurationTextBoxLabel.Size = new Size(97, 17);
            _SignalDurationTextBoxLabel.TabIndex = 2;
            _SignalDurationTextBoxLabel.Text = "Duration [Sec]: ";
            _SignalDurationTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _PointsTextBox
            // 
            __PointsTextBox.AcceptsReturn = true;
            __PointsTextBox.BackColor = SystemColors.Window;
            __PointsTextBox.Cursor = Cursors.IBeam;
            __PointsTextBox.ForeColor = SystemColors.WindowText;
            __PointsTextBox.Location = new Point(107, 4);
            __PointsTextBox.MaxLength = 0;
            __PointsTextBox.Name = "__PointsTextBox";
            __PointsTextBox.RightToLeft = RightToLeft.No;
            __PointsTextBox.Size = new Size(46, 25);
            __PointsTextBox.TabIndex = 1;
            __PointsTextBox.Text = "1000";
            // 
            // _PhaseTextBox
            // 
            __PhaseTextBox.AcceptsReturn = true;
            __PhaseTextBox.BackColor = SystemColors.Window;
            __PhaseTextBox.Cursor = Cursors.IBeam;
            __PhaseTextBox.ForeColor = SystemColors.WindowText;
            __PhaseTextBox.Location = new Point(278, 30);
            __PhaseTextBox.MaxLength = 0;
            __PhaseTextBox.Name = "__PhaseTextBox";
            __PhaseTextBox.RightToLeft = RightToLeft.No;
            __PhaseTextBox.Size = new Size(48, 25);
            __PhaseTextBox.TabIndex = 7;
            // 
            // _CyclesTextBox
            // 
            __CyclesTextBox.AcceptsReturn = true;
            __CyclesTextBox.BackColor = SystemColors.Window;
            __CyclesTextBox.Cursor = Cursors.IBeam;
            __CyclesTextBox.ForeColor = SystemColors.WindowText;
            __CyclesTextBox.Location = new Point(278, 4);
            __CyclesTextBox.MaxLength = 0;
            __CyclesTextBox.Name = "__CyclesTextBox";
            __CyclesTextBox.RightToLeft = RightToLeft.No;
            __CyclesTextBox.Size = new Size(48, 25);
            __CyclesTextBox.TabIndex = 5;
            // 
            // _PointsTextBoxLabel
            // 
            _PointsTextBoxLabel.AutoSize = true;
            _PointsTextBoxLabel.BackColor = SystemColors.Control;
            _PointsTextBoxLabel.Cursor = Cursors.Default;
            _PointsTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PointsTextBoxLabel.Location = new Point(55, 8);
            _PointsTextBoxLabel.Name = "_PointsTextBoxLabel";
            _PointsTextBoxLabel.RightToLeft = RightToLeft.No;
            _PointsTextBoxLabel.Size = new Size(50, 17);
            _PointsTextBoxLabel.TabIndex = 0;
            _PointsTextBoxLabel.Text = "Points: ";
            _PointsTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _PhaseTextBoxLabel
            // 
            _PhaseTextBoxLabel.AutoSize = true;
            _PhaseTextBoxLabel.BackColor = SystemColors.Control;
            _PhaseTextBoxLabel.Cursor = Cursors.Default;
            _PhaseTextBoxLabel.ForeColor = SystemColors.ControlText;
            _PhaseTextBoxLabel.Location = new Point(191, 34);
            _PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel";
            _PhaseTextBoxLabel.RightToLeft = RightToLeft.No;
            _PhaseTextBoxLabel.Size = new Size(85, 17);
            _PhaseTextBoxLabel.TabIndex = 6;
            _PhaseTextBoxLabel.Text = "Phase [Deg]: ";
            _PhaseTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _CyclesTextBoxLabel
            // 
            _CyclesTextBoxLabel.AutoSize = true;
            _CyclesTextBoxLabel.BackColor = SystemColors.Control;
            _CyclesTextBoxLabel.Cursor = Cursors.Default;
            _CyclesTextBoxLabel.ForeColor = SystemColors.ControlText;
            _CyclesTextBoxLabel.Location = new Point(175, 8);
            _CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel";
            _CyclesTextBoxLabel.RightToLeft = RightToLeft.No;
            _CyclesTextBoxLabel.Size = new Size(101, 17);
            _CyclesTextBoxLabel.TabIndex = 4;
            _CyclesTextBoxLabel.Text = "Frequency [Hz]: ";
            _CyclesTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _SpectrumTabPage
            // 
            _SpectrumTabPage.Controls.Add(__SpectrumChartPanel);
            _SpectrumTabPage.Controls.Add(_SpectrumOptionsPanel);
            _SpectrumTabPage.Location = new Point(4, 26);
            _SpectrumTabPage.Name = "_SpectrumTabPage";
            _SpectrumTabPage.Size = new Size(578, 338);
            _SpectrumTabPage.TabIndex = 1;
            _SpectrumTabPage.Text = "Spectrum";
            // 
            // _SpectrumChartPanel
            // 
            __SpectrumChartPanel.Dock = DockStyle.Fill;
            __SpectrumChartPanel.Location = new Point(0, 64);
            __SpectrumChartPanel.Name = "__SpectrumChartPanel";
            __SpectrumChartPanel.Size = new Size(578, 274);
            __SpectrumChartPanel.TabIndex = 1;
            // 
            // _SpectrumOptionsPanel
            // 
            _SpectrumOptionsPanel.Controls.Add(_FilterComboBoxLabel);
            _SpectrumOptionsPanel.Controls.Add(_FilterComboBox);
            _SpectrumOptionsPanel.Controls.Add(_DoubleRadioButton);
            _SpectrumOptionsPanel.Controls.Add(_SingleRadioButton);
            _SpectrumOptionsPanel.Controls.Add(_ExampleComboBoxLabel);
            _SpectrumOptionsPanel.Controls.Add(__StartStopCheckBox);
            _SpectrumOptionsPanel.Controls.Add(_RemoveMeanCheckBox);
            _SpectrumOptionsPanel.Controls.Add(_ExampleComboBox);
            _SpectrumOptionsPanel.Controls.Add(_TaperWindowCheckBox);
            _SpectrumOptionsPanel.Dock = DockStyle.Top;
            _SpectrumOptionsPanel.Location = new Point(0, 0);
            _SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel";
            _SpectrumOptionsPanel.Size = new Size(578, 64);
            _SpectrumOptionsPanel.TabIndex = 0;
            // 
            // _FilterComboBoxLabel
            // 
            _FilterComboBoxLabel.AutoSize = true;
            _FilterComboBoxLabel.Location = new Point(247, 15);
            _FilterComboBoxLabel.Name = "_FilterComboBoxLabel";
            _FilterComboBoxLabel.Size = new Size(77, 17);
            _FilterComboBoxLabel.TabIndex = 2;
            _FilterComboBoxLabel.Text = "Taper Filter:";
            // 
            // _FilterComboBox
            // 
            _FilterComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _FilterComboBox.FormattingEnabled = true;
            _FilterComboBox.Location = new Point(247, 34);
            _FilterComboBox.Name = "_FilterComboBox";
            _FilterComboBox.Size = new Size(141, 25);
            _FilterComboBox.TabIndex = 3;
            // 
            // _SingleRadioButton
            // 
            _SingleRadioButton.AutoSize = true;
            _SingleRadioButton.Location = new Point(125, 34);
            _SingleRadioButton.Name = "_SingleRadioButton";
            _SingleRadioButton.Size = new Size(117, 21);
            _SingleRadioButton.TabIndex = 1;
            _SingleRadioButton.Text = "Single Precision";
            // 
            // _ExampleComboBoxLabel
            // 
            _ExampleComboBoxLabel.AutoSize = true;
            _ExampleComboBoxLabel.Location = new Point(395, 14);
            _ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel";
            _ExampleComboBoxLabel.Size = new Size(67, 17);
            _ExampleComboBoxLabel.TabIndex = 4;
            _ExampleComboBoxLabel.Text = "Calculate: ";
            _ExampleComboBoxLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _StartStopCheckBox
            // 
            __StartStopCheckBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __StartStopCheckBox.Appearance = Appearance.Button;
            __StartStopCheckBox.Location = new Point(511, 9);
            __StartStopCheckBox.Name = "__StartStopCheckBox";
            __StartStopCheckBox.Size = new Size(64, 24);
            __StartStopCheckBox.TabIndex = 6;
            __StartStopCheckBox.Text = "&Start";
            __StartStopCheckBox.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(_MessagesList);
            _MessagesTabPage.Location = new Point(4, 26);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new Size(552, 338);
            _MessagesTabPage.TabIndex = 3;
            _MessagesTabPage.Text = "Log";
            // 
            // _MessagesList
            // 
            _MessagesList.BackColor = SystemColors.Info;
            _MessagesList.CausesValidation = false;
            _MessagesList.Dock = DockStyle.Fill;
            _MessagesList.Location = new Point(0, 0);
            _MessagesList.Multiline = true;
            _MessagesList.Name = "_MessagesList";
            _MessagesList.PresetCount = 50;
            _MessagesList.ReadOnly = true;
            _MessagesList.ResetCount = 100;
            _MessagesList.ScrollBars = ScrollBars.Both;
            _MessagesList.Size = new Size(552, 338);
            _MessagesList.TabIndex = 4;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _StatusToolStripStatusLabel, _CountToolStripStatusLabel, _ErrorToolStripStatusLabel, _TimeToolStripStatusLabel });
            _StatusStrip.Location = new Point(0, 368);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Size = new Size(586, 22);
            _StatusStrip.TabIndex = 4;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusToolStripStatusLabel
            // 
            _StatusToolStripStatusLabel.Name = "_StatusToolStripStatusLabel";
            _StatusToolStripStatusLabel.Size = new Size(471, 17);
            _StatusToolStripStatusLabel.Spring = true;
            _StatusToolStripStatusLabel.Text = "Ready";
            _StatusToolStripStatusLabel.TextAlign = ContentAlignment.MiddleLeft;
            _StatusToolStripStatusLabel.ToolTipText = "Status";
            // 
            // _CountToolStripStatusLabel
            // 
            _CountToolStripStatusLabel.Name = "_CountToolStripStatusLabel";
            _CountToolStripStatusLabel.Size = new Size(13, 17);
            _CountToolStripStatusLabel.Text = "0";
            _CountToolStripStatusLabel.ToolTipText = "FFT counter";
            // 
            // _ErrorToolStripStatusLabel
            // 
            _ErrorToolStripStatusLabel.Name = "_ErrorToolStripStatusLabel";
            _ErrorToolStripStatusLabel.Size = new Size(34, 17);
            _ErrorToolStripStatusLabel.Text = "0.000";
            _ErrorToolStripStatusLabel.ToolTipText = "RMS difference between signal and inverse FFT";
            // 
            // _TimeToolStripStatusLabel
            // 
            _TimeToolStripStatusLabel.Name = "_TimeToolStripStatusLabel";
            _TimeToolStripStatusLabel.Size = new Size(53, 17);
            _TimeToolStripStatusLabel.Text = "0.000 ms";
            _TimeToolStripStatusLabel.ToolTipText = "Time to calculate an FFT";
            // 
            // SpectrumPanel
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            ClientSize = new Size(586, 390);
            Controls.Add(_Tabs);
            Controls.Add(_StatusStrip);
            Name = "SpectrumPanel";
            Text = "Spectrum Panel";
            _DataTabPage.ResumeLayout(false);
            _DataTabPage.PerformLayout();
            _Tabs.ResumeLayout(false);
            _SignalTabPage.ResumeLayout(false);
            _SignalOptionsPanel.ResumeLayout(false);
            _SignalOptionsPanel.PerformLayout();
            _SpectrumTabPage.ResumeLayout(false);
            _SpectrumOptionsPanel.ResumeLayout(false);
            _SpectrumOptionsPanel.PerformLayout();
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        private TextBox __PointsToDisplayTextBox;

        private TextBox _PointsToDisplayTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PointsToDisplayTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PointsToDisplayTextBox != null)
                {
                    __PointsToDisplayTextBox.Validating -= PointsToDisplayTextBox_Validating;
                }

                __PointsToDisplayTextBox = value;
                if (__PointsToDisplayTextBox != null)
                {
                    __PointsToDisplayTextBox.Validating += PointsToDisplayTextBox_Validating;
                }
            }
        }

        private ListBox _SignalListBox;
        private TabPage _DataTabPage;
        private Label _PointsToDisplayTextBoxLabel;
        private Label _SignalListBoxLabel;
        private RadioButton _DoubleRadioButton;
        private CheckBox _RemoveMeanCheckBox;
        private ComboBox _ExampleComboBox;
        private CheckBox _TaperWindowCheckBox;
        private TabControl _Tabs;
        private TabPage _SignalTabPage;
        private Panel __SignalChartPanel;

        private Panel _SignalChartPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SignalChartPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SignalChartPanel != null)
                {
                    __SignalChartPanel.Paint -= SignalChartPanel_Paint;
                    __SignalChartPanel.Resize -= SignalChartPanel_Resize;
                }

                __SignalChartPanel = value;
                if (__SignalChartPanel != null)
                {
                    __SignalChartPanel.Paint += SignalChartPanel_Paint;
                    __SignalChartPanel.Resize += SignalChartPanel_Resize;
                }
            }
        }

        private Panel _SignalOptionsPanel;
        private TextBox _SignalDurationTextBox;
        private Label _SignalDurationTextBoxLabel;
        private TextBox __PointsTextBox;

        private TextBox _PointsTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PointsTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PointsTextBox != null)
                {
                    __PointsTextBox.Validating -= PointsTextBox_Validating;
                }

                __PointsTextBox = value;
                if (__PointsTextBox != null)
                {
                    __PointsTextBox.Validating += PointsTextBox_Validating;
                }
            }
        }

        private TextBox __PhaseTextBox;

        private TextBox _PhaseTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PhaseTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PhaseTextBox != null)
                {
                    __PhaseTextBox.Validating -= PhaseTextBox_Validating;
                }

                __PhaseTextBox = value;
                if (__PhaseTextBox != null)
                {
                    __PhaseTextBox.Validating += PhaseTextBox_Validating;
                }
            }
        }

        private TextBox __CyclesTextBox;

        private TextBox _CyclesTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CyclesTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CyclesTextBox != null)
                {
                    __CyclesTextBox.Validating -= CyclesTextBox_Validating;
                }

                __CyclesTextBox = value;
                if (__CyclesTextBox != null)
                {
                    __CyclesTextBox.Validating += CyclesTextBox_Validating;
                }
            }
        }

        private Label _PointsTextBoxLabel;
        private Label _PhaseTextBoxLabel;
        private Label _CyclesTextBoxLabel;
        private TabPage _SpectrumTabPage;
        private Panel __SpectrumChartPanel;

        private Panel _SpectrumChartPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SpectrumChartPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SpectrumChartPanel != null)
                {
                    __SpectrumChartPanel.Paint -= SpectrumChartPanel_Paint;
                    __SpectrumChartPanel.Resize -= SpectrumChartPanel_Resize;
                }

                __SpectrumChartPanel = value;
                if (__SpectrumChartPanel != null)
                {
                    __SpectrumChartPanel.Paint += SpectrumChartPanel_Paint;
                    __SpectrumChartPanel.Resize += SpectrumChartPanel_Resize;
                }
            }
        }

        private Panel _SpectrumOptionsPanel;
        private Label _ExampleComboBoxLabel;
        private CheckBox __StartStopCheckBox;

        private CheckBox _StartStopCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartStopCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartStopCheckBox != null)
                {
                    __StartStopCheckBox.CheckedChanged -= StartStopCheckBox_CheckedChanged;
                }

                __StartStopCheckBox = value;
                if (__StartStopCheckBox != null)
                {
                    __StartStopCheckBox.CheckedChanged += StartStopCheckBox_CheckedChanged;
                }
            }
        }

        private RadioButton _SingleRadioButton;
        private TabPage _MessagesTabPage;
        private Core.Forma.MessagesBox _MessagesList;
        private ErrorProvider _ErrorProvider;
        private Label _TimingListBoxLabel;
        private TextBox _TimingTextBox;
        private Label _SignalComboBoxLabel;
        private ComboBox __SignalComboBox;

        private ComboBox _SignalComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SignalComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SignalComboBox != null)
                {
                    __SignalComboBox.Validated -= SignalComboBox_Validated;
                }

                __SignalComboBox = value;
                if (__SignalComboBox != null)
                {
                    __SignalComboBox.Validated += SignalComboBox_Validated;
                }
            }
        }

        private Label _FilterComboBoxLabel;
        private ComboBox _FilterComboBox;
        private StatusStrip _StatusStrip;
        private ToolStripStatusLabel _StatusToolStripStatusLabel;
        private ToolStripStatusLabel _CountToolStripStatusLabel;
        private ToolStripStatusLabel _ErrorToolStripStatusLabel;
        private ToolStripStatusLabel _TimeToolStripStatusLabel;
    }
}