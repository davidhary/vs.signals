using System;
using System.Diagnostics;
using System.Linq;

using isr.Algorithms.Signals.Forms.ExceptionExtensions;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Algorithms.Signals.Forms
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary>
    /// A sealed class the parses the command line and provides the command line values.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-02-02, x.x.4050.x. </para>
    /// </remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " PARSER "

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Gets the view option. </summary>
        /// <value> The view option. </value>
        public static string ViewOption => "/vu:";

        /// <summary> Validate the command line. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        /// not handled. </exception>
        /// <param name="commandLineArguments"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArguments )
        {
            if ( commandLineArguments is object )
            {
                foreach ( string argument in commandLineArguments )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( ViewOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( $"Unknown command line argument '{argument}' was detected. Should be Ignored.", nameof( commandLineArguments ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( string commandLineArgs )
        {
            if ( !string.IsNullOrWhiteSpace( commandLineArgs ) )
                ParseCommandLine( new System.Collections.ObjectModel.ReadOnlyCollection<string>( commandLineArgs.Split( ' ' ) ) );
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            var commandLineBuilder = new System.Text.StringBuilder();
            if ( commandLineArgs is object )
            {
                foreach ( string argument in commandLineArgs )
                {
                    _ = commandLineBuilder.AppendFormat( "{0} ", argument );
                    if ( argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Nop = true;
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        DevicesEnabled = argument.Substring( ViewOption.Length ).StartsWith( "n", StringComparison.OrdinalIgnoreCase );
                    }
                    else if ( argument.StartsWith( ViewOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        UserInterfaceName = argument.Substring( ViewOption.Length );
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }

            CommandLine = commandLineBuilder.ToString();
        }

        /// <summary>   Parses the command line. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        /// <param name="commandLineArgs">  The command line arguments. </param>
        /// <returns>   A Tuple. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                try
                {
                    activity = $"joining the command Line arguments";
                    CommandLine = string.Join( ",", commandLineArgs );

                    activity = $"parsing command Line: {CommandLine}";
                    CommandLineInfo.ParseCommandLine( commandLineArgs );
                }
                catch ( ArgumentException ex )
                {
                    result = (false, $"Failed {activity};  Unknown argument ignored");
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, result.Details );
                }
                catch ( Exception ex )
                {
                    result = (false, $"Failed {activity};, {ex.ToFullBlownString()} ");
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity}" );
                }
            }

            return result;
        }


        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets or sets the command line. </summary>
        /// <value> The command line. </value>
        public static string CommandLine { get; private set; }

        /// <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
        /// <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
        /// <value> The no-op. </value>
        public static bool Nop { get; set; }

        /// <summary> Name of the user interface. </summary>
        private static string _UserInterfaceName = string.Empty;

        /// <summary> Gets or sets the name of the user interface. </summary>
        /// <value> The name of the user interface. </value>
        public static string UserInterfaceName
        {
            get => _UserInterfaceName;

            set {
                _UserInterfaceName = value;
                if ( !Enum.TryParse( value, true, out UserInterface ui ) )
                {
                    ui = Signals.Forms.UserInterface.Unknown;
                }

                CommandLineInfo.UserInterface = ui;
            }
        }

        /// <summary> Gets or sets the virtual instrument. </summary>
        /// <value> The virtual instrument. </value>
        public static UserInterface? UserInterface { get; set; }

        #endregion

    }

    /// <summary> Values that represent virtual instruments. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public enum UserInterface
    {

        /// <summary> An enum constant representing the unknown option. </summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown,

        /// <summary> An enum constant representing the spectrum option. </summary>
        [System.ComponentModel.Description( "Spectrum" )]
        Spectrum,

        /// <summary> An enum constant representing the spectrum Milliseconds option. </summary>
        [System.ComponentModel.Description( "Spectrum Chart" )]
        SpectrumMs,

        /// <summary> An enum constant representing the wisdom option. </summary>
        [System.ComponentModel.Description( "Wisdom" )]
        Wisdom,

        /// <summary> An enum constant representing the wisdom Milliseconds option. </summary>
        [System.ComponentModel.Description( "Wisdom Chart" )]
        WisdomMs
    }
}
