Imports System.Numerics

''' <summary> Calculates the Fast Fourier Transform using the Meta Numerics Library. </summary>
''' <remarks>
''' Includes procedures for calculating the forward and inverse Fourier transform.  <para>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 1/20/2014 </para>
''' </remarks>
Public Class MixedRadixFourierTransform
    Inherits FourierTransformBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New(FourierTransformType.MixedRadix)
    End Sub

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates a Mixed-Radix Fourier transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overrides Sub Forward(ByVal values() As Complex)
        MyBase.Forward(values)
    End Sub

    ''' <summary> Initializes and performs the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    Protected Overrides Sub Initialize(ByVal values() As Complex)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim ft As FourierTransformer = New FourierTransformer(values.Length)
        ft.Transform(values).CopyTo(values)
    End Sub

#End Region

#Region " DOUBLE "

    ''' <summary> Calculates a Mixed-Radix Fourier transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals As Double(), imaginaries As Double())
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary> Initializes and performs the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals As Double(), imaginaries As Double())
        If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
        Dim ft As FourierTransformer = New FourierTransformer(reals.Length)
        ft.Transform(reals, imaginaries).CopyTo(reals, imaginaries)
    End Sub

#End Region

#Region " SINGLE "

    ''' <summary> Calculates a Mixed-Radix Fourier transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals As Single(), imaginaries As Single())
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary> Initializes and performs the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals As Single(), imaginaries As Single())
        If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
        Dim ft As FourierTransformer = New FourierTransformer(reals.Length)
        ft.Transform(reals, imaginaries).CopyTo(reals, imaginaries)
    End Sub

#End Region


End Class


