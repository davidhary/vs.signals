# Signals Libraries

libraries for digital signal processing.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Chart](https://www.bitbucket.org/davidhary/vs.chart) - Chart Libraries
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [Signals](https://www.bitbucket.org/davidhary/vs.signals) - Signals Libraries

```
git clone git@bitbucket.org:davidhary/vs.chart.git
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.signals.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Core\Core
.\Libraries\VS\Algorithms\Signals
.\Libraries\VS\Visuals\Chart
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Authors"></a>
## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Signals Library](https://bitbucket.org/davidhary/vs.signals)  
[Meta Numerics](http://www.meta-numerics.net)  
[Wisdom FFT](http://www.fftw.org/)  
[Wisdom FFT C\#](http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html)
