using System;
using System.Runtime.InteropServices;

namespace isr.Algorithms.Signals.Wisdom
{

    /// <summary> Base class for FFTW memory management. </summary>
    /// <remarks>
    /// Arrays allocated using the <see cref="MemoryArray">memory array</see> should be deallocated
    /// by the same construct rather than the ordinary free (or, heaven forbid, delete).
    /// <para>
    /// A program linking to an FFTW library compiled with SIMD support can obtain a nonnegligible
    /// speedup for most complex and real to complex and complex to real transforms. In order to
    /// obtain this speedup, however, the arrays passed to FFTW must be specially aligned in memory
    /// (typically 16-byte aligned). Often this alignment is more stringent than that provided by the
    /// usual memory allocation routines.
    /// </para>
    /// <para>
    /// In order to guarantee proper alignment for SIMD, therefore, in case your program is ever
    /// linked against a SIMD-using FFTW, we recommend allocating your transform data with
    /// fftw_malloc and de-allocating it with fftw_free. These have exactly the same interface and
    /// behavior as malloc/free, except that for a SIMD FFTW they ensure that the returned pointer
    /// has the necessary alignment (by calling memalign or its equivalent on your OS).
    /// </para>
    /// <para>
    /// You are not required to use fftw_malloc. You can allocate your data in any way that you like,
    /// from malloc to new (in C++) to a fixed-size array declaration. If the array happens not to be
    /// properly aligned, FFTW will not use the SIMD extensions.
    /// </para> <para>
    /// David, 2007-09-01, 2.0.2800 </para><para>
    /// (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class MemoryArray : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new memory array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length">          Logical length of the array (number of elements) </param>
        /// <param name="dataTypeSize">    Number of bytes in the data type. </param>
        /// <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        /// for each logical element. </param>
        protected MemoryArray( int length, int dataTypeSize, int subElementCount ) : base()
        {
            this.DataTypeSize = dataTypeSize;
            this.SubElementCount = subElementCount;
            this.Handle = IntPtr.Zero;
            this.Length = length;
            this.Handle = Allocate( length, dataTypeSize, subElementCount );
            this.SafeHandle = new ArraySafeHandle( dataTypeSize, this.Handle );
        }

        /// <summary>
        /// Creates a new <see cref="System.Single">single precision</see> memory using the given
        /// <paramref name="data"/>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="data">            An array holding the data. </param>
        /// <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        /// for each logical element. </param>
        /// <param name="isPinned">        Specifies if the array is to be pinned in memory. </param>
        protected MemoryArray( float[] data, int subElementCount, bool isPinned ) : base()
        {
            this.DataTypeSize = 4;
            this.SubElementCount = subElementCount;
            this.Handle = IntPtr.Zero;
            this.Handle = Allocate( data, isPinned );
            this.SafeHandle = new ArraySafeHandle( this.DataTypeSize, this.Handle );
        }

        /// <summary>
        /// Creates a new <see cref="System.Double">double precision</see> memory using the given
        /// <paramref name="data"/>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="data">            An array holding the data. </param>
        /// <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        /// for each logical element. </param>
        /// <param name="isPinned">        Specifies if the array is to be pinned in memory. </param>
        protected MemoryArray( double[] data, int subElementCount, bool isPinned ) : base()
        {
            this.DataTypeSize = 8;
            this.SubElementCount = subElementCount;
            this.Handle = IntPtr.Zero;
            this.Handle = Allocate( data, isPinned );
            this.SafeHandle = new ArraySafeHandle( this.DataTypeSize, this.Handle );
        }

        /// <summary>
        /// Creates a new <see cref="System.Single">single precision</see> memory using the
        /// <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        /// part</paramref> values.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        protected MemoryArray( float[] reals, float[] imaginaries, bool isPinned ) : base()
        {
            this.DataTypeSize = 4;
            this.SubElementCount = 2;
            this.Handle = IntPtr.Zero;
            this.Handle = Allocate( reals, imaginaries, isPinned );
            this.SafeHandle = new ArraySafeHandle( this.DataTypeSize, this.Handle );
        }

        /// <summary>
        /// Creates a new <see cref="System.Double">double precision</see> memory using the
        /// <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        /// part</paramref> values.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        protected MemoryArray( double[] reals, double[] imaginaries, bool isPinned ) : base()
        {
            this.DataTypeSize = 8;
            this.SubElementCount = 2;
            this.Handle = IntPtr.Zero;
            this.Handle = Allocate( reals, imaginaries, isPinned );
            this.SafeHandle = new ArraySafeHandle( this.DataTypeSize, this.Handle );
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The disposed. </value>
        protected bool Disposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources
                    this.Free();
                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.Disposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        ~MemoryArray()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " STATIC METHODS AND PROPERTIES "

        /// <summary> Allocates an FFTW-compatible array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="totalElementCount"> Product of number of <see cref="Length">elements</see> and
        /// <see cref="SubElementCount">number of sub elements</see>.
        /// </param>
        /// <param name="dataTypeSize">      Number of bytes in the data type. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( int totalElementCount, int dataTypeSize )
        {
            return totalElementCount < 2
                ? throw new ArgumentOutOfRangeException( nameof( totalElementCount ), "Element count must be longer than 1" )
                : dataTypeSize == 4
                ? FftwF.SafeNativeMethods.Malloc( dataTypeSize * totalElementCount )
                : dataTypeSize == 8
                    ? FftwR.SafeNativeMethods.Malloc( dataTypeSize * totalElementCount )
                    : throw new ArgumentOutOfRangeException( nameof( dataTypeSize ), "Must be 4 or 8" );
        }

        /// <summary> Allocates an FFTW-compatible array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount">    Logical length of the array (number of elements). </param>
        /// <param name="dataTypeSize">    Number of bytes in the data type. </param>
        /// <param name="subElementCount"> Number of sub-elements. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( int elementCount, int dataTypeSize, int subElementCount )
        {
            return Allocate( elementCount * subElementCount, dataTypeSize );
        }

        /// <summary>
        /// Allocates an FFTW-compatible array.
        /// Pins and copies the <paramref name="data">data</paramref> to memory space pointed to by the
        /// returned handle.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="data">     . </param>
        /// <param name="isPinned"> Specifies the condition for pinning the array in memory. This handle
        /// type allows the address of the pinned object to be taken. This
        /// prevents the garbage collector from moving the object and hence
        /// undermines the efficiency of the garbage collector. Use the Free
        /// method to free the allocated handle as soon as possible. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( float[] data, bool isPinned )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }

            IntPtr handle;
            if ( isPinned )
            {
                handle = GCHandle.ToIntPtr( GCHandle.Alloc( data, GCHandleType.Pinned ) );
            }
            else
            {
                handle = Allocate( data.Length, 4 );
                Marshal.Copy( data, 0, handle, data.Length );
            }

            return handle;
        }

        /// <summary>
        /// Allocates an FFTW-compatible array.
        /// Pins and copies the <paramref name="data">data</paramref> to memory space pointed to by the
        /// returned handle.
        /// </summary>
        /// <remarks>
        /// From Tamas Szalay: "With the <paramref name="isPinned">true</paramref> the provided array is
        /// <see cref="GCHandleType.Pinned"/>
        /// to ensure that the array does not move. Using malloc(), the actual transform is faster by up
        /// to 3x but not always, and you know you're using an FFTW-friendly method. However, you need to
        /// store two copies of the data that you copy back and forth using Marshal.Copy, which takes up
        /// space and time. Using GCHandle.Pinned, however, the behavior is less predictable. I was doing
        /// speed tests, and some arrays seemed to be up to 2-3x slower than their GCHandle.Pinned
        /// counterparts while others were indistinguishable. I assume this was due to whatever alignment,
        /// but I could find no good way of fixing it. If anybody has a solution short of manually
        /// aligning via unsafe code, do let me know.".
        /// </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="data">     . </param>
        /// <param name="isPinned"> Specifies the condition for pinning the array in memory. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( double[] data, bool isPinned )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }

            IntPtr handle;
            if ( isPinned )
            {
                handle = GCHandle.ToIntPtr( GCHandle.Alloc( data, GCHandleType.Pinned ) );
            }
            else
            {
                handle = Allocate( data.Length, 8 );
                Marshal.Copy( data, 0, handle, data.Length );
            }

            return handle;
        }

        /// <summary>
        /// Allocates a <see cref="System.Single">single precision</see> FFTW-compatible array. Creates a
        /// complex array using the <paramref name="reals">real-</paramref> and
        /// <paramref name="imaginaries">imaginary-part</paramref> values.
        /// Then pins and copies the complex array to memory space pointed to by the returned handle.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies the condition for pinning the array in memory. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( float[] reals, float[] imaginaries, bool isPinned )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new float[(reals.Length + reals.Length)];

            // set the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                data[j] = reals[i];
                j += 1;
                data[j] = imaginaries[i];
                j += 1;
            }

            // get a handle to the array.
            return Allocate( data, isPinned );
        }

        /// <summary>
        /// Allocates a <see cref="System.Double">double precision</see> FFTW-compatible array. Creates a
        /// complex array using the <paramref name="reals">real-</paramref> and
        /// <paramref name="imaginaries">imaginary-part</paramref> values.
        /// Then pins and copies the complex array to memory space pointed to by the returned handle.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies the condition for pinning the array in memory. </param>
        /// <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        public static IntPtr Allocate( double[] reals, double[] imaginaries, bool isPinned )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new double[(reals.Length + reals.Length)];

            // set the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                data[j] = reals[i];
                j += 1;
                data[j] = imaginaries[i];
                j += 1;
            }

            // get a handle to the array.
            return Allocate( data, isPinned );
        }

        /// <summary> Copies the data to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="data">   Array of data values.  This could be an array of complex or real-part
        /// values. </param>
        /// <param name="target"> <see cref="IntPtr">Points</see> to the FFTW-Compatible array. </param>
        /// <returns> A True if ok. </returns>
        public static bool CopyFrom( float[] data, IntPtr target )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }

            Marshal.Copy( data, 0, target, data.Length );
            return true;
        }

        /// <summary> Copies the data to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="data">   Array of data values.  This could be an array of complex or real-part
        /// values. </param>
        /// <param name="target"> <see cref="IntPtr">Points</see> to the FFTW-Compatible array. </param>
        /// <returns> A True if ok. </returns>
        public static bool CopyFrom( double[] data, IntPtr target )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }

            Marshal.Copy( data, 0, target, data.Length );
            return true;
        }

        /// <summary>
        /// Copies the real- and imaginary-part values to an existing FFTW-compatible array.
        /// Creates a complex array using the <paramref name="reals">real-</paramref> and
        /// <paramref name="imaginaries">imaginary-part</paramref> values.
        /// Then copies the complex array to memory space pointed to by the
        /// <paramref name="target">pointer</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="target">      <see cref="IntPtr">Points</see> to the FFTW-Comaprible array. </param>
        /// <returns> A True if ok. </returns>
        public static bool CopyFrom( float[] reals, float[] imaginaries, IntPtr target )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new float[(reals.Length + reals.Length)];

            // set the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                data[j] = reals[i];
                j += 1;
                data[j] = imaginaries[i];
                j += 1;
            }

            Marshal.Copy( data, 0, target, data.Length );
            return true;
        }

        /// <summary>
        /// Copies the real- and imaginary-part values to an existing FFTW-compatible array.
        /// Creates a complex array using the <paramref name="reals">real-</paramref> and
        /// <paramref name="imaginaries">imaginary-part</paramref> values.
        /// Then copies the complex array to memory space pointed to by the
        /// <paramref name="target">pointer</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="target">      <see cref="IntPtr">Points</see> to the FFTW-Comaprible array. </param>
        /// <returns> A True if ok. </returns>
        public static bool CopyFrom( double[] reals, double[] imaginaries, IntPtr target )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new double[(reals.Length + reals.Length)];

            // set the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                data[j] = reals[i];
                j += 1;
                data[j] = imaginaries[i];
                j += 1;
            }

            Marshal.Copy( data, 0, target, data.Length );
            return true;
        }

        /// <summary>
        /// Retrieves complex data from the specified <paramref name="source">pointer</paramref> to the
        /// <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        /// part</paramref> arrays.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="source">      A <see cref="IntPtr">handle</see> to the array. </param>
        /// <param name="reals">       An array receiving the real-part values. </param>
        /// <param name="imaginaries"> An array receiving imaginary-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool CopyTo( IntPtr source, double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new double[(reals.Length + reals.Length)];
            Marshal.Copy( source, data, 0, data.Length );

            // get the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                reals[i] = data[j];
                j += 1;
                imaginaries[i] = data[j];
                j += 1;
            }

            return true;
        }

        /// <summary>
        /// Retrieves complex data from the specified <paramref name="source">source</paramref> to the
        /// <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        /// part</paramref> arrays.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="source">      A <see cref="IntPtr">handle</see> to the array. </param>
        /// <param name="reals">       An array receiving the real-part values. </param>
        /// <param name="imaginaries"> An array receiving imaginary-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool CopyTo( IntPtr source, float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" );
            }

            if ( reals.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            }

            // allocate complex data array.
            var data = new float[(reals.Length + reals.Length)];
            Marshal.Copy( source, data, 0, data.Length );

            // get the array data
            int j = 0;
            for ( int i = 0, loopTo = reals.Length - 1; i <= loopTo; i++ )
            {
                reals[i] = data[j];
                j += 1;
                imaginaries[i] = data[j];
                j += 1;
            }

            return true;
        }

        /// <summary>
        /// Retrieves data from the specified <paramref name="source">source</paramref> to the
        /// <paramref name="data">data-</paramref> array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="source"> A <see cref="IntPtr">handle</see> to the array. </param>
        /// <param name="data">   An array receiving the source values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool CopyTo( IntPtr source, float[] data )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }
            // fetch the data.
            Marshal.Copy( source, data, 0, data.Length );
            return true;
        }

        /// <summary>
        /// Retrieves data from the specified <paramref name="source">source</paramref> to the
        /// <paramref name="data">data-</paramref> array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="source"> A <see cref="IntPtr">handle</see> to the array. </param>
        /// <param name="data">   An array receiving the source data. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool CopyTo( IntPtr source, double[] data )
        {
            if ( data is null )
                throw new ArgumentNullException( nameof( data ) );
            if ( data.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( data ), "Array must be longer than 1" );
            }

            // fetch the data.
            Marshal.Copy( source, data, 0, data.Length );
            return true;
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Gets or sets the size of the data type in bytes. </summary>
        /// <value> The size of the data type. </value>
        public int DataTypeSize { get; private set; }

        /// <summary> Frees allocated memory. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Free()
        {
#if true
            // use the safe handle
            // Note there are three interesting states here:
            // 1) The Safe Handle contains an invalid handle (Allocate array failed);
            // 2) Dispose was already called: Save Handle is closed;
            // 3) Save Handle is null, due to an async exception before allocating the memory.
            //    Note that the finalizer runs if the constructor fails.
            if ( this.HasHandle() )
            {
                this.SafeHandle.Dispose();
            }
#else
            // free resources directly.
            if ( this.Handle != IntPtr.Zero)
            {
                if ( this.DataTypeSize == 4)
                {
                    FftwF.SafeNativeMethods.Free( this.Handle );
                }
                else if ( this.DataTypeSize == 8)
                {
                    FftwR.SafeNativeMethods.Free( this.Handle );
                }
                else
                {
                    // if byte size not specified we assume nothing was allocated.
                }
            }
#endif
            this.Handle = IntPtr.Zero;
        }

        /// <summary>   Gets or sets the safe handle of the array. </summary>
        /// <value> The safe handle. </value>
        internal ArraySafeHandle SafeHandle { get; private set; }

        /// <summary>   Query if this object has handle. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <returns>   True if a valid open handle exists; otherw3ise, false. </returns>
        internal bool HasHandle() => this.SafeHandle is object && !this.SafeHandle.IsInvalid && !this.SafeHandle.IsClosed;

        /// <summary>
        /// Gets or sets reference to the <see cref="IntPtr">source</see> to the
        /// <see cref="MemoryArray">array</see>.
        /// </summary>
        /// <value> The handle. </value>
        public IntPtr Handle { get; private set; }

        /// <summary>
        /// Gets or sets the number of logical elements in the <see cref="MemoryArray">array</see>. Each
        /// element of a complex array consists of 2 sub-elements.  The element count of the array mapped
        /// by the memory array includes <see cref="Length">element count</see> times
        /// <see cref="SubElementCount">sub-element count</see>
        /// elements.  This for example, an <see cref="Array">array</see> of 1024 elements maps into a
        /// <see cref="RealArrayF">real-parts array</see> of 1024 or to a
        /// <see cref="ComplexArrayF">complex array</see>
        /// of only 512 elements.
        /// </summary>
        /// <value> The length. </value>
        public int Length { get; private set; }

        /// <summary> Gets or sets the number of sub elements for each array element. </summary>
        /// <value> The number of sub elements. </value>
        public int SubElementCount { get; private set; }

        #endregion

    }

    /// <summary>
    /// Complex array suitable for FFTW memory management.  The array consists of alternating Real-
    /// and Imaginary-parts. Thus, each logical element is composed of a real and imaginary parts of
    /// a complex number.
    /// </summary>
    /// <remarks> David, 2007-09-01, 2.0.2800. </remarks>
    public class ComplexArrayF : MemoryArray
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates an FFTW-compatible array of complex numbers. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> Logical length of the array (number of elements) </param>
        public ComplexArrayF( int length ) : base( length, 4, 2 )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from a complex array by either pinning the array or copying
        /// the array values to a source.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="complex">  Array of alternating real- and imaginary-parts. </param>
        /// <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        public ComplexArrayF( float[] complex, bool isPinned ) : base( complex, 2, isPinned )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from real- and imaginary-part values,
        /// Converts the real- and imaginary-parts to a complex array and then either pins the array or
        /// copies its values to a pointer.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        public ComplexArrayF( float[] reals, float[] imaginaries, bool isPinned ) : base( reals, imaginaries, isPinned )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Copies complex values to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="complex"> Array of alternating real- and imaginary-parts. </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( float[] complex )
        {
            return complex is null
                ? throw new ArgumentNullException( nameof( complex ) )
                : complex.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( complex ), "Array must be longer than 1" )
                : CopyFrom( complex, this.Handle );
        }

        /// <summary>
        /// Copies real- and imaginary-part values to an existing FFTW-compatible array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( float[] reals, float[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : reals.Length != imaginaries.Length
                ? throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyFrom( reals, imaginaries, this.Handle );
        }

        /// <summary>
        /// Retrieves real- and imaginary-part values from <see cref="ComplexArrayF">complex</see> array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array receiving the real-part values. </param>
        /// <param name="imaginaries"> An array receiving the imaginary-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool CopyTo( float[] reals, float[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : reals.Length != imaginaries.Length
                ? throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyTo( this.Handle, reals, imaginaries );
        }

        #endregion

    }

    /// <summary> real-parts array suitable for FFTW memory management. </summary>
    /// <remarks> David, 2007-09-01, 2.0.2800. </remarks>
    public class RealArrayF : MemoryArray
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates an FFTW-compatible array of real-part values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> Logical length of the array (number of elements) </param>
        public RealArrayF( int length ) : base( length, 4, 1 )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from an array of real-part values by either pinning the
        /// array or copying the array values to a pointer.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">    Array of real-parts. </param>
        /// <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        public RealArrayF( float[] reals, bool isPinned ) : base( reals, 1, isPinned )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Copies real-part values to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> Array of real-parts. </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( float[] reals )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyFrom( reals, this.Handle );
        }

        /// <summary> Retrieves real-part values from an existing FFTW-compatible array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> An array receiving the real-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool CopyTo( float[] reals )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyTo( this.Handle, reals );
        }

        #endregion

    }

    /// <summary>
    /// Complex array suitable for FFTW memory management.  The array consists of alternating real-
    /// and imaginary-parts. Thus, each logical element is composed of a real and imaginary parts of
    /// a complex number.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public class ComplexArrayR : MemoryArray
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates an FFTW-compatible array of complex numbers. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> Logical length of the array (number of elements) </param>
        public ComplexArrayR( int length ) : base( length, 8, 2 )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from a complex array by either pinning the array or copying
        /// the array values to a pointer.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="complex">  Array of alternating real- and imaginary-parts. </param>
        /// <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        public ComplexArrayR( double[] complex, bool isPinned ) : base( complex, 2, isPinned )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from real- and imaginary-part values,
        /// Converts the real- and imaginary-parts to a complex array and then either pins the array or
        /// copies its values to a pointer.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        public ComplexArrayR( double[] reals, double[] imaginaries, bool isPinned ) : base( reals, imaginaries, isPinned )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Copies complex values to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="complex"> Array of alternating real- and imaginary-parts. </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( double[] complex )
        {
            return complex is null
                ? throw new ArgumentNullException( nameof( complex ) )
                : complex.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( complex ), "Array must be longer than 1" )
                : CopyFrom( complex, this.Handle );
        }

        /// <summary>
        /// Copies real- and imaginary-part values to an existing FFTW-compatible array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array with real-part values. </param>
        /// <param name="imaginaries"> An array with imaginary-part values. </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( double[] reals, double[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : reals.Length != imaginaries.Length
                ? throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyFrom( reals, imaginaries, this.Handle );
        }

        /// <summary>
        /// Retrieves real- and imaginary-part values from <see cref="ComplexArrayF">complex</see> array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       An array receiving the real-part values. </param>
        /// <param name="imaginaries"> An array receiving the imaginary-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool CopyTo( double[] reals, double[] imaginaries )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : imaginaries is null
                ? throw new ArgumentNullException( nameof( imaginaries ) )
                : reals.Length != imaginaries.Length
                ? throw new ArgumentOutOfRangeException( nameof( reals ), reals, "The arrays of real- and imaginary-parts must have the same size" )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyTo( this.Handle, reals, imaginaries );
        }

        #endregion

    }

    /// <summary> real-parts array suitable for FFTW memory management. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public class RealArrayR : MemoryArray
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates an FFTW-compatible array of real-part values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> Logical length of the array (number of elements) </param>
        public RealArrayR( int length ) : base( length, 8, 1 )
        {
        }

        /// <summary>
        /// Creates an FFTW-compatible array from an array of real-part values by either pinning the
        /// array or copying the array values to a pointer.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="reals">    Array of real-parts. </param>
        /// <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        public RealArrayR( double[] reals, bool isPinned ) : base( reals, 1, isPinned )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Copies the real-part values to an existing FFTW-compatible array.   </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> . </param>
        /// <returns> A True if ok. </returns>
        public bool CopyFrom( double[] reals )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyFrom( reals, this.Handle );
        }

        /// <summary> Retrieves real-part values from an existing FFTW-compatible array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> An array receiving the real-part values. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool CopyTo( double[] reals )
        {
            return reals is null
                ? throw new ArgumentNullException( nameof( reals ) )
                : reals.Length < 2
                ? throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" )
                : CopyTo( this.Handle, reals );
        }

        #endregion

    }
}
