using System;

namespace isr.Algorithms.Signals.Wisdom
{

    /// <summary> Base class for storing and destroying FFTW plans. </summary>
    /// <remarks>
    /// David, 2007-09-01, 2.0.2800. Created <para>
    /// The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    /// fastest way to compute the transform on your machine. The planner produces a data structure
    /// called a plan that contains this information. Subsequently, the plan is executed to transform
    /// the array of input data as dictated by the plan. </para><para>
    /// (c) 2007 Integrated Scientific Resources, Inc. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// </para>
    /// </remarks>
    public abstract class Plan : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Constructs a plan. </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="planHandle">   A <see cref="IntPtr">handle</see> to the plan. </param>
        /// <param name="dataTypeSize"> Size of the data type. </param>
        protected Plan( IntPtr planHandle, int dataTypeSize ) : base()
        {
            if ( planHandle == IntPtr.Zero )
                throw new ArgumentNullException( nameof( planHandle ), "Invalid memory reference" );
            this.DataTypeSize = dataTypeSize;
            this.Handle = planHandle;
            this.SafeHandle = new PlanSafeHandle( dataTypeSize, this.Handle );
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> True if disposed. </summary>
        private bool _Disposed;

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The disposed. </value>
        protected bool Disposed
        {
            get => this._Disposed;

            private set => this._Disposed = value;
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        // Free managed resources when explicitly called
                    }

                    // Free shared unmanaged resources
                    this.Free();
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.Disposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        ~Plan()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " HANDLE "

        /// <summary> Gets or sets the size of the data type in bytes. </summary>
        /// <value> The size of the data type. </value>
        public int DataTypeSize { get; private set; }

        /// <summary> Gets or sets the handle to the plan. </summary>
        /// <value> The handle. </value>
        public IntPtr Handle { get; private set; }

        /// <summary>   Gets or sets the safe handle of the array. </summary>
        /// <value> The safe handle. </value>
        internal PlanSafeHandle SafeHandle { get; private set; }

        /// <summary>   Query if this object has handle. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <returns>   True if a valid open handle exists; otherw3ise, false. </returns>
        internal bool HasHandle() => this.SafeHandle is object && !this.SafeHandle.IsInvalid && !this.SafeHandle.IsClosed;

        /// <summary> Deallocates an FFTW plan and all associated resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Free( )
        {
#if true
            // use the safe handle
            // Note there are three interesting states here:
            // 1) The Safe Handle contains an invalid handle (Allocate array failed);
            // 2) Dispose was already called: Save Handle is closed;
            // 3) Save Handle is null, due to an async exception before allocating the plan.
            //    Note that the finalizer runs if the constructor fails.
            if ( this.HasHandle() )
            {
                this.SafeHandle.Dispose();
            }
#else
            if ( this.Handle != IntPtr.Zero )
            {
                if ( this.DataTypeSize == 4 )
                {
                    FftwF.SafeNativeMethods.Destroy_plan( this.Handle );
                }
                else if ( this.DataTypeSize == 8 )
                {
                    FftwR.SafeNativeMethods.Destroy_plan( this.Handle );
                }
                else
                {
                    // if byte size not specified we assume nothing was allocated.
                }
            }
#endif
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>
        /// Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
        /// free.
        /// </summary>
        /// <remarks>
        /// After calling Cleanup, all existing plans become undefined, and you should not attempt to
        /// execute them nor to destroy them. You can however create and execute/destroy new plans, in
        /// which case FFTW starts accumulating wisdom information again. Cleanup does not deallocate
        /// your plans; you should still call fftw_destroy_plan for this purpose.
        /// </remarks>
        public void Cleanup()
        {
            if ( this.DataTypeSize == 4 )
            {
                FftwF.SafeNativeMethods.Cleanup();
            }
            else if ( this.DataTypeSize == 8 )
            {
                FftwR.SafeNativeMethods.Cleanup();
            }
            else
            {
                // if byte size not specified we assume nothing was allocated.
            }
        }

        /// <summary>
        /// Executes the FFTW plan, provided that the input and output arrays still exist.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Execute()
        {
            if ( this.DataTypeSize == 4 )
            {
                FftwF.SafeNativeMethods.Execute( this.Handle );
            }
            else if ( this.DataTypeSize == 8 )
            {
                FftwR.SafeNativeMethods.Execute( this.Handle );
            }
            else
            {
                // if byte size not specified we assume nothing was allocated.
            }
        }

        /// <summary> The time limit. </summary>
        private double _TimeLimit;

        /// <summary> Gets or sets the maximum time that can be used by the planner. </summary>
        /// <remarks>
        /// Instructs FFTW to spend at most seconds (approximately) in the planner. If seconds ==
        /// -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW plans with a
        /// progressively wider range of algorithms until the given time limit is reached or the
        /// given range of algorithms is explored, returning the best available plan. For example,
        /// specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode, then
        /// finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead, the
        /// planner will further progress to Exhaustive mode.
        /// </remarks>
        /// <value> The time limit. </value>
        public double TimeLimit
        {
            get => this._TimeLimit;

            set {
                this._TimeLimit = value;
                if ( this.DataTypeSize == 4 )
                {
                    FftwF.SafeNativeMethods.Set_timelimit( value );
                }
                else if ( this.DataTypeSize == 8 )
                {
                    FftwR.SafeNativeMethods.Set_timelimit( value );
                }
                else
                {
                    // if byte size not specified we assume nothing was allocated.
                }
            }
        }

        #endregion

        #region " STATIC METHODS AND PROPERTIES "

        /// <summary>
        /// Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
        /// free.
        /// </summary>
        /// <remarks>
        /// After calling Cleanup, all existing plans become undefined, and you should not attempt to
        /// execute them nor to destroy them. You can however create and execute/destroy new plans, in
        /// which case FFTW starts accumulating wisdom information again. Cleanup does not deallocate
        /// your plans; you should still call fftw_destroy_plan for this purpose.
        /// </remarks>
        public static void Cleanup( int dataTypeSize )
        {
            if ( dataTypeSize == 4 )
            {
                FftwF.SafeNativeMethods.Cleanup();
            }
            else if ( dataTypeSize == 8 )
            {
                FftwR.SafeNativeMethods.Cleanup();
            }
            else
            {
            }
        }

        /// <summary> Gets or sets the maximum time that can be used by the planner. </summary>
        /// <remarks>
        /// Instructs FFTW to spend at most seconds (approximately) in the planner. If seconds ==
        /// -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW plans with a
        /// progressively wider range of algorithms until the given time limit is reached or the
        /// given range of algorithms is explored, returning the best available plan. For example,
        /// specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode, then
        /// finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead, the
        /// planner will further progress to Exhaustive mode.
        /// </remarks>
        /// <value> The time limit in seconds. </value>
        public static void SetTimeLimit( int dataTypeSize, double value)
        {
            if ( dataTypeSize == 4 )
            {
                FftwF.SafeNativeMethods.Set_timelimit( value );
            }
            else if ( dataTypeSize == 8 )
            {
                FftwR.SafeNativeMethods.Set_timelimit( value );
            }
            else
            {
            }
        }

        #endregion

    }

    #region " SINGLE PRECISION "

    /// <summary> Stores and destroys FFTW plans. </summary>
    /// <remarks>
    /// The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    /// fastest way to compute the transform on your machine. The planner produces a data structure
    /// called a plan that contains this information. Subsequently, the plan is executed to transform
    /// the array of input data as dictated by the plan. <para>
    /// David, 2007-09-01, 2.0.2800 </para><para>
    /// (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class PlanF : Plan
    {

#region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a plan. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="plan"> A <see cref="IntPtr">handle</see> to the plan. </param>
        public PlanF( IntPtr plan ) : base( plan, 4 )
        {
        }

#endregion

    }

#endregion

#region " DOUBLE PRECISION "

    /// <summary> Stores and destroys FFTW plans. </summary>
    /// <remarks>
    /// David, 2007-09-01, 2.0.2800. Create.  <para>
    /// The computation of the transform is split into two phases. First, FFTW’s planner “learns” the
    /// fastest way to compute the transform on your machine. The planner produces a data structure
    /// called a plan that contains this information. Subsequently, the plan is executed to transform
    /// the array of input data as dictated by the plan. (c) 2007 Integrated Scientific Resources,
    /// Inc. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class PlanR : Plan
    {

#region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a plan. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="plan"> A <see cref="IntPtr">handle</see> to the plan. </param>
        public PlanR( IntPtr plan ) : base( plan, 8 )
        {
        }

#endregion

    }

#endregion

}
