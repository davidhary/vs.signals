# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [4.1.7914] - 2022-02-18
* Add runtimes. Fix project files.

## [4.1.7645] - 2020-12-05
### Wisdom Changed 
* Uses safe handle.

## [4.1.7607] - 2020-10-29
* Converted to C#

## [4.1.6969] - 2019-01-30
* Uses new core libraries.

## [4.1.6667] - 2018-04-03
* 2018 release.

## [4.0.6166] - 2016-11-18
* Uses VS 2015.

## [3.0.5133] - 2014-01-20
* Replaces invalid scale factor exception and null spectrum densities exception and spectrum. 
* Scales exception with Invalid operation exception. 
* Documents return values for functions. 
* Removes interfaces. 
* Removes the validation outcome construct. 
* Removes the convergence exception. 
* Replaces dimension mismatch with argument out of range exception.
*  Sets assembly product to 2014.

## [3.0.4763] - 2013-01-13
* Adds array copy from double to single types.

## [3.0.4711] - 2012-11-24
* Uses complex only functions.

## [2.1.4707] - 2012-11-20
* Converted to VS10. Uses Mixed Radix FFT from the Meta Numerics library.

## [2.1.4706] - 2012-11-19
* Prepared for VS10.

## [2.1.4232] - 2011-08-11
* Standardizes code elements and documentation.

## [2.1.4213] - 2011-07-15
* Simplifies the assembly information.

## [2.1.2961] - 2008-02-09
* Updates to .NET 3.5.

## [2.0.2817] - 2007-09-19
* Updates to Visual Studio 8. Uses Wisdom FFT.

## [1.0.2225] - 2006-02-03
* Modifies Taper Filter to specify type and filter frequencies and transition band. 
* Adds validate Outcome structure..

## [1.0.2219] - 2006-01-28
* Removes Visual Basic import.

## [1.0.2205] - 2006-01-14
* Adds new support and exceptions libraries. 
* Uses Int32, Int64, and Int16 instead of Integer, Long, and Short.

## [1.0.2147] - 2005-11-17
* Converts FFT pro to .NET

## [1.0.1998] - 1998-01-01
* ISR FFT pro.

\(C\) 1998 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
