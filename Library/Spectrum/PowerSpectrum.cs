using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> The base class for the power spectrum. </summary>
    /// <remarks>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class PowerSpectrum : Spectrum
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
        /// calculating the spectrum. </param>
        // instantiate the base class
        public PowerSpectrum( FourierTransformBase fourierTransform ) : base( fourierTransform )
        {
        }

        #endregion

        #region " WELCH METHOD COMMON "

        /// <summary> Gets the average count. </summary>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The average count. </value>
        public int AverageCount { get; private set; }

        #endregion

        #region " DENSITIES "

        /// <summary> Clears and allocates the power spectrum densities. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> Specifies the length of the spectrum thus allowing for a Int16er spectrum
        /// than is calculated. Specify 0 to let the program set the maximum
        /// length. </param>
        public void Clear( int length )
        {
            this.Scaled = false;
            this.AverageCount = 0;
            this._Densities = length > 0 ? (new double[length]) : null;
        }

        /// <summary> Copies the density and the number of averages. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values">        The values. </param>
        /// <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
        /// <param name="spectrumCount"> Specifies the number of points to copy. </param>
        /// <param name="scaled">        The scaled sentinel. </param>
        public void CopyToDensities( double[] values, int averageCount, int spectrumCount, bool scaled )
        {
            if ( values is null )
            {
                this.Scaled = false;
                this._Densities = null;
            }
            else
            {
                this.Scaled = scaled;
                this.AverageCount = averageCount;
                this._Densities = new double[spectrumCount];
                Array.Copy( values, this._Densities, spectrumCount );
            }
        }

        /// <summary> Copies the density and the number of averages. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values">        The values. </param>
        /// <param name="averageCount">  Specifies the number of averages used to calculate the densities. </param>
        /// <param name="spectrumCount"> Specifies the number of points to copy. </param>
        /// <param name="scaled">        The scaled sentinel. </param>
        public void CopyToDensities( float[] values, int averageCount, int spectrumCount, bool scaled )
        {
            if ( values is null )
            {
                this.Scaled = false;
                this._Densities = null;
            }
            else
            {
                this.Scaled = scaled;
                this.AverageCount = averageCount;
                this._Densities = new double[spectrumCount];
                Array.Copy( values, this._Densities, spectrumCount );
            }
        }

        /// <summary> Returns the indexed density. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
        /// <param name="index">        The index. </param>
        /// <returns> The indexed density. </returns>
        public double Density( bool halfSpectrum, int index )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            if ( index < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( index ), index, $"{nameof( index )} {index} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( index > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( index ), index, $"{nameof( index )} {index} Index must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );
            if ( this.Scaled )
            {
                if ( halfSpectrum )
                {
                    if ( index == 0 )
                    {
                        return this.Densities()[index];
                    }
                    else
                    {
                        double tempValue = this.Densities()[index];
                        return tempValue + tempValue;
                    }
                }
                else
                {
                    return this.Densities()[index];
                }
            }
            else
            {
                double factor = this.SpectrumScaleFactor( halfSpectrum );
                return index == 0 & halfSpectrum ? 0.5d * factor * this.Densities()[index] : factor * this.Densities()[index];
            }
        }

        /// <summary> The power spectrum densities. </summary>
        private double[] _Densities;

        /// <summary> Returns the spectrum densities. </summary>
        /// <remarks>
        /// Unless<see cref="Scale()"/> is applied and not <see cref="Restore()"/>, the spectrum
        /// densities are not scaled, which allows adding spectrum segments for Welch method power
        /// spectrum calculation. Use <see cref="Restore()"/> in case the densities were scaled and new
        /// spectra need to be appended.
        /// </remarks>
        /// <returns> The power spectrum densities. </returns>
        public double[] Densities()
        {
            return this._Densities;
        }

        /// <summary> Returns the scaled spectrum densities. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <returns> The scaled densities. </returns>
        public double[] ScaledDensities()
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );

            // allocate the outcome array
            var output = new double[this.Densities().Length];
            if ( this.Scaled )
            {
                output = new double[this.Densities().Length];
                for ( int i = 0, loopTo = this.Densities().Length - 1; i <= loopTo; i++ )
                    output[i] = this.Densities()[i];
            }
            else
            {
                double factor = this.SpectrumScaleFactor( false );
                for ( int i = 0, loopTo1 = this.Densities().Length - 1; i <= loopTo1; i++ )
                    output[i] = factor * this.Densities()[i];
            }

            return output;
        }

        /// <summary> Returns the scaled spectrum densities. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
        /// <returns> The scaled spectrum densities. </returns>
        public double[] ScaledDensities( bool halfSpectrum )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            double[] output;
            if ( this.Scaled )
            {
                if ( halfSpectrum )
                {
                    output = new double[this.HalfSpectrumLength];
                    output[0] = this.Densities()[0];
                    for ( int i = 1, loopTo = this.HalfSpectrumLength - 1; i <= loopTo; i++ )
                    {
                        double tempValue = this.Densities()[i];
                        output[i] = tempValue + tempValue;
                    }
                }
                else
                {
                    output = new double[this.Densities().Length];
                    for ( int i = 0, loopTo1 = this.Densities().Length - 1; i <= loopTo1; i++ )
                        output[i] = this.Densities()[i];
                }
            }
            else
            {
                double factor = this.SpectrumScaleFactor( false );
                if ( halfSpectrum )
                {
                    output = new double[this.HalfSpectrumLength];
                    output[0] = 0.5d * factor * this.Densities()[0];
                    for ( int i = 1, loopTo2 = this.HalfSpectrumLength - 1; i <= loopTo2; i++ )
                    {
                        double tempValue = factor * this.Densities()[i];
                        output[i] = tempValue + tempValue;
                    }
                }
                else
                {
                    output = new double[this.Densities().Length];
                    for ( int i = 0, loopTo3 = this.Densities().Length - 1; i <= loopTo3; i++ )
                        output[i] = factor * this.Densities()[i];
                }
            }

            return output;
        }

        /// <summary> Returns the scaled spectrum densities. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="halfSpectrum">  Specifies is the scaling is doubled for displaying the half
        /// spectrum. </param>
        /// <param name="startingIndex"> From index. </param>
        /// <param name="endingIndex">   To index. </param>
        /// <returns> The scaled spectrum densities. </returns>
        public double[] ScaledDensities( bool halfSpectrum, int startingIndex, int endingIndex )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            if ( startingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( startingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} fromIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} endingIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );


            // allocate outcome space
            var output = new double[endingIndex - startingIndex + 1];
            if ( this.Scaled )
            {
                if ( halfSpectrum )
                {
                    int j = 0;
                    if ( startingIndex == 0 )
                    {
                        output[j] = this.Densities()[startingIndex];
                        startingIndex += 1;
                        j += 1;
                    }

                    for ( int i = startingIndex, loopTo = endingIndex; i <= loopTo; i++ )
                    {
                        double tempValue = this.Densities()[i];
                        output[j] = tempValue + tempValue;
                        j += 1;
                    }
                }
                else
                {
                    int j = 0;
                    output = new double[endingIndex - startingIndex + 1];
                    for ( int i = startingIndex, loopTo1 = endingIndex; i <= loopTo1; i++ )
                    {
                        output[j] = this.Densities()[i];
                        j += 1;
                    }
                }
            }
            else
            {
                double factor = this.SpectrumScaleFactor( halfSpectrum );
                int j = 0;
                if ( startingIndex == 0 & halfSpectrum )
                {
                    output[j] = 0.5d * factor * this.Densities()[startingIndex];
                    startingIndex += 1;
                    j += 1;
                }

                // Scale the rest of the spectrum
                for ( int i = startingIndex, loopTo2 = endingIndex; i <= loopTo2; i++ )
                {
                    output[j] = factor * this.Densities()[i];
                    j += 1;
                }
            }

            return output;
        }

        /// <summary> Returns the length of the densities array. </summary>
        /// <value> The length of the densities. </value>
        public int DensitiesLength => this.Densities() is null ? 0 : this.Densities().Length;

        #endregion

        #region " MAGNITUDES "

        /// <summary> Returns the indexed magnitude. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
        /// <param name="index">        The index. </param>
        /// <returns> The indexed magnitude. </returns>
        public double Magnitude( bool halfSpectrum, int index )
        {
            return this.Densities() is null
                ? throw new InvalidOperationException( "Spectrum densities where not created." )
                : index < this.Densities().GetLowerBound( 0 )
                ? throw new ArgumentOutOfRangeException( nameof( index ), index, $"{nameof( index )} {index} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" )
                : index > this.Densities().GetUpperBound( 0 )
                ? throw new ArgumentOutOfRangeException( nameof( index ), index, $"{nameof( index )} {index} Index must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" )
                : Math.Sqrt( this.Density( halfSpectrum, index ) );
        }

        /// <summary> Returns the scaled magnitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <returns> The scaled magnitude spectrum. </returns>
        public double[] ScaledMagnitudes()
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );

            // allocate the outcome array
            var output = new double[this.Densities().Length];
            if ( this.Scaled )
            {
                output = new double[this.Densities().Length];
                for ( int i = 0, loopTo = this.Densities().Length - 1; i <= loopTo; i++ )
                    output[i] = Math.Sqrt( this.Densities()[i] );
            }
            else
            {
                double factor = this.SpectrumScaleFactor( false );
                for ( int i = 0, loopTo1 = this.Densities().Length - 1; i <= loopTo1; i++ )
                    output[i] = Math.Sqrt( factor * this.Densities()[i] );
            }

            return output;
        }

        /// <summary> Returns the scaled magnitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <param name="halfSpectrum"> Specifies is the scaling is doubled. </param>
        /// <returns> The scaled magnitude spectrum. </returns>
        public double[] ScaledMagnitudes( bool halfSpectrum )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            double[] output;
            if ( this.Scaled )
            {
                if ( halfSpectrum )
                {
                    output = new double[this.HalfSpectrumLength];
                    output[0] = Math.Sqrt( this.Densities()[0] );
                    for ( int i = 1, loopTo = this.HalfSpectrumLength - 1; i <= loopTo; i++ )
                    {
                        double tempValue = this.Densities()[i];
                        output[i] = Math.Sqrt( tempValue + tempValue );
                    }
                }
                else
                {
                    output = new double[this.Densities().Length];
                    for ( int i = 0, loopTo1 = this.Densities().Length - 1; i <= loopTo1; i++ )
                        output[i] = Math.Sqrt( this.Densities()[i] );
                }
            }
            else
            {
                double factor = this.SpectrumScaleFactor( false );
                if ( halfSpectrum )
                {
                    output = new double[this.HalfSpectrumLength];
                    output[0] = Math.Sqrt( 0.5d * factor * this.Densities()[0] );
                    for ( int i = 1, loopTo2 = this.HalfSpectrumLength - 1; i <= loopTo2; i++ )
                    {
                        double tempValue = factor * this.Densities()[i];
                        output[i] = Math.Sqrt( tempValue + tempValue );
                    }
                }
                else
                {
                    output = new double[this.Densities().Length];
                    for ( int i = 0, loopTo3 = this.Densities().Length - 1; i <= loopTo3; i++ )
                        output[i] = Math.Sqrt( factor * this.Densities()[i] );
                }
            }

            return output;
        }

        /// <summary> Returns the scaled magnitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="halfSpectrum">  Specifies is the scaling is doubled for displaying the half
        /// spectrum. </param>
        /// <param name="startingIndex"> From index. </param>
        /// <param name="endingIndex">   To index. </param>
        /// <returns> The scaled magnitude spectrum. </returns>
        public double[] ScaledMagnitudes( bool halfSpectrum, int startingIndex, int endingIndex )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            if ( startingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( startingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} fromIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} endingIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );

            // allocate outcome space
            var output = new double[endingIndex - startingIndex + 1];
            if ( this.Scaled )
            {
                if ( halfSpectrum )
                {
                    int j = 0;
                    if ( startingIndex == 0 )
                    {
                        output[j] = Math.Sqrt( this.Densities()[startingIndex] );
                        startingIndex += 1;
                        j += 1;
                    }

                    for ( int i = startingIndex, loopTo = endingIndex; i <= loopTo; i++ )
                    {
                        double tempValue = this.Densities()[i];
                        output[j] = Math.Sqrt( tempValue + tempValue );
                        j += 1;
                    }
                }
                else
                {
                    int j = 0;
                    output = new double[endingIndex - startingIndex + 1];
                    for ( int i = startingIndex, loopTo1 = endingIndex; i <= loopTo1; i++ )
                    {
                        output[j] = Math.Sqrt( this.Densities()[i] );
                        j += 1;
                    }
                }
            }
            else
            {
                double factor = this.SpectrumScaleFactor( halfSpectrum );
                int j = 0;
                if ( startingIndex == 0 & halfSpectrum )
                {
                    output[j] = Math.Sqrt( 0.5d * factor * this.Densities()[startingIndex] );
                    startingIndex += 1;
                    j += 1;
                }

                // Scale the rest of the spectrum
                for ( int i = startingIndex, loopTo2 = endingIndex; i <= loopTo2; i++ )
                {
                    output[j] = Math.Sqrt( factor * this.Densities()[i] );
                    j += 1;
                }
            }

            return output;
        }

        #endregion

        #region " POWER "

        /// <summary> Returns the index of the spectrum peak. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="fromIndex"> From index. </param>
        /// <param name="toIndex">   To index. </param>
        /// <returns> The index of the spectrum peak. </returns>
        public int PeakIndex( int fromIndex, int toIndex )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            if ( fromIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( fromIndex ), fromIndex, "Starting index must exceed lower bound" );
            if ( fromIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( fromIndex ), fromIndex, "Starting index must not exceed spectrum length" );
            if ( toIndex < fromIndex )
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Ending index must exceed starting index" );
            if ( toIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( toIndex ), toIndex, "Ending index must not exceed spectrum length" );

            // Set Max to first value
            int maxDensityIndex = fromIndex;
            float maxDensity = ( float ) this.Densities()[maxDensityIndex];
            float maxCandidate;
            for ( int i = fromIndex, loopTo = Math.Min( toIndex, this.HalfSpectrumLength - 1 ); i <= loopTo; i++ )
            {

                // Get candidate
                maxCandidate = ( float ) this.Densities()[i];

                // Select a new maximum if lower than element.
                if ( maxCandidate > maxDensity )
                {
                    maxDensity = maxCandidate;
                    maxDensityIndex = i;
                }
            }

            // return the index
            return maxDensityIndex;
        }

        /// <summary> Searches for the half power indexes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="spectrumPeakIndex"> Zero-based index of the spectrum peak. </param>
        /// <returns> The found half power indexes. </returns>
        public (double Lower, double Upper) FindHalfPowerPoints( int spectrumPeakIndex )
        {

            // Step down to get low frequency.
            int searchIndex = spectrumPeakIndex;
            int lowerBound = 0;
            int upperBound = this.Densities().GetUpperBound( 0 );
            double targetDensity = this.Densities()[spectrumPeakIndex];
            // find the first power lower than half power on the low frequency end.
            while ( this.Densities()[searchIndex] > targetDensity & searchIndex > lowerBound )
                searchIndex -= 1;

            // look for the peak lower frequency
            double lowerIndex = lowerBound;
            if ( searchIndex > lowerBound )
            {
                lowerIndex = searchIndex;
                // check if we need to interpolate
                double psd2 = this.Densities()[searchIndex + 1];
                double psd1 = this.Densities()[searchIndex];
                if ( psd2 != psd1 )
                {
                    lowerIndex += (targetDensity - psd1) / (psd2 - psd1);
                }
            }

            // Step up to get high frequency.
            searchIndex = spectrumPeakIndex;
            // find the first power lower than half power on the high frequency end.
            while ( this.Densities()[searchIndex] > targetDensity & searchIndex < upperBound )
                searchIndex += 1;

            // Find the high frequency value.
            double upperIndex = upperBound;
            if ( searchIndex < upperBound )
            {
                upperIndex = searchIndex;
                // check if we need to interpolate
                double psd2 = this.Densities()[searchIndex - 1];
                double psd1 = this.Densities()[searchIndex];
                if ( psd2 != psd1 )
                {
                    upperIndex += (psd1 - targetDensity) / (psd2 - psd1);
                }
            }

            return (lowerIndex, upperIndex);
        }

        /// <summary>
        /// Returns the total power for the specified index range but using both side of the spectrum and
        /// assuming the spectrum is for real valued time series, that is, that the spectrum is symmetric.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid because spectrum densities were not
        /// created. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="startingIndex"> From index. </param>
        /// <param name="endingIndex">   To index. </param>
        /// <returns>
        /// The total power for the specified index range but using both side of the spectrum and
        /// assuming the spectrum is for real valued time series, that is, that the spectrum is symmetric.
        /// </returns>
        public double TotalPower( int startingIndex, int endingIndex )
        {
            if ( this.Densities() is null )
                throw new InvalidOperationException( "Spectrum densities where not created." );
            if ( startingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( startingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( startingIndex ), startingIndex, $"{nameof( startingIndex )} {startingIndex} fromIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex < this.Densities().GetLowerBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} must exceed lower bound {this.Densities().GetLowerBound( 0 )}" );
            if ( endingIndex > this.Densities().GetUpperBound( 0 ) )
                throw new ArgumentOutOfRangeException( nameof( endingIndex ), endingIndex, $"{nameof( endingIndex )} {endingIndex} endingIndex must not exceed upper bound {this.Densities().GetLowerBound( 0 )}" );

            // the DC spectrum is scaled by half because the Spectrum Scale Factor
            // is derived for the half spectrum (twice as large).
            double total = 0d;
            if ( startingIndex == 0 )
            {
                total += 0.5d * this.Densities()[0];
                startingIndex += 1;
            }
            // go up to half spectrum
            for ( int i = startingIndex, loopTo = Math.Min( endingIndex, this.HalfSpectrumLength - 1 ); i <= loopTo; i++ )
                // add the power
                total += this.Densities()[i];
            if ( !this.Scaled )
            {
                // apply double the spectrum power factor to correct for the two-sided spectrum.
                total *= this.SpectrumScaleFactor( true );
            }
            else
            {
                // apply double the spectrum power factor to correct for the two-sided spectrum.
                total *= 2d;
            }

            return total;
        }

        /// <summary> Returns the total power for the specified index range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <param name="fromIndex"> From index. </param>
        /// <returns> The total power for the specified index range. </returns>
        public double TotalPower( int fromIndex )
        {
            return this.Densities() is null
                ? throw new InvalidOperationException( "Spectrum densities where not created." )
                : this.TotalPower( fromIndex, this.Densities().GetUpperBound( 0 ) );
        }

        /// <summary> Returns the total power. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <returns> The total power. </returns>
        public double TotalPower()
        {
            return this.Densities() is null
                ? throw new InvalidOperationException( "Spectrum densities where not created." )
                : this.TotalPower( this.Densities().GetLowerBound( 0 ), this.Densities().GetUpperBound( 0 ) );
        }

        #endregion

        #region " SCALE "

        /// <summary>
        /// Restore the densities to their pre-scaled values so that additional averages can be added.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void Restore()
        {
            double factor = this.SpectrumScaleFactor( false );
            factor = factor <= float.Epsilon
                ? throw new InvalidOperationException( "Invalid scale factor not caught by the Spectrum Scale Factor function--Contact Developer." )
                : 1.0d / factor;

            // un-scale the rest of the spectrum
            for ( int i = 0, loopTo = this.Densities().GetUpperBound( 0 ); i <= loopTo; i++ )
                this.Densities()[i] *= factor;

            // un-tag power spectrum as not scaled allowing adding spectrum averages.
            this.Scaled = false;
        }

        /// <summary> Scales the spectrum densities based on the number of averages. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Scale()
        {
            double factor = this.SpectrumScaleFactor( false );

            // Scale the rest of the spectrum
            for ( int i = 0, loopTo = this.Densities().GetUpperBound( 0 ); i <= loopTo; i++ )
                this.Densities()[i] *= factor;

            // tag power spectrum as scaled.  This prevents adding new averages until
            // the spectrum is cleared or restored.
            this.Scaled = true;
        }

        /// <summary>
        /// Returns the spectrum scale factor including the effect of the taper window power.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="halfSpectrum"> Specifies is the scaling is doubled for displaying the half
        /// spectrum. </param>
        /// <returns> The spectrum scale factor including the effect of the taper window power. </returns>
        public double SpectrumScaleFactor( bool halfSpectrum )
        {
            if ( this.TimeSeriesLength == 0 )
            {
                throw new InvalidOperationException( "Invalid scale factor--The spectrum time series is set to zero." );
            }
            else if ( this.AverageCount == 0 )
            {
                throw new InvalidOperationException( "Invalid scale factor--Average count is zero." );
            }

            double factor = 1d / (Convert.ToDouble( this.AverageCount ) * this.TimeSeriesLength * this.TimeSeriesLength);
            if ( halfSpectrum )
            {
                // The factor of two ensure double the power for half spectrum.
                factor *= 2d;
            }
            // add taper window power.
            if ( this.TaperWindow is object )
            {
                if ( this.TaperWindow.Power <= float.Epsilon )
                {
                    throw new InvalidOperationException( "Invalid scale factor--Taper window has zero power." );
                }

                factor /= this.TaperWindow.Power;
            }

            return factor;
        }

        /// <summary>
        /// Gets or sets the condition for the densities were scaled.  Once the densities are scaled no
        /// more averages can be appended until the spectrum is
        /// <see cref="Restore">restored</see>
        /// </summary>
        /// <value> The scaled sentinel. </value>
        private bool Scaled { get; set; }

        #endregion

        #region " WELCH METHOD: COMPLEX "

        /// <summary> Appends the spectrum to the existing spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="spectrum"> The spectrum. </param>
        public void Append( Complex[] spectrum )
        {
            if ( this.Scaled )
                throw new InvalidOperationException( "Because densities were already scaled they must be restored before adding move averages to the power spectrum." );
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            if ( spectrum.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( spectrum ), "Spectrum must be longer than 1" );
            if ( this.Densities() is null || this.Densities().Length != spectrum.Length )
            {
                this._Densities = new double[spectrum.Length];
            }

            for ( int i = 0, loopTo = this.Densities().Length - 1; i <= loopTo; i++ )
            {
                double abs = spectrum[i].Abs();
                this.Densities()[i] += abs * abs;
            }

            // increment the average count
            this.AverageCount += 1;
        }

        /// <summary>
        /// Calculates the spectrum for complex data and appends to the existing spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="timeSeries"> The time series. </param>
        public void TransformAppend( Complex[] timeSeries )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            if ( timeSeries.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( timeSeries ), "Time series must be longer than 1" );
            // calculate the spectrum
            base.Calculate( timeSeries );
            // add the spectrum
            this.Append( timeSeries );
        }

        /// <summary>
        /// Calculates the spectrum for real data and appends to the existing spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeSeries"> The time series. </param>
        /// <param name="dataPoints"> Specifies the number of data points to use. </param>
        public void TransformAppend( Complex[] timeSeries, int dataPoints )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            if ( dataPoints == timeSeries.Length )
            {
                this.TransformAppend( timeSeries );
            }
            else
            {
                // Get the time series data
                var ts = new Complex[dataPoints];
                Array.Copy( timeSeries, ts, Math.Min( ts.Length, timeSeries.Length ) );
                this.TransformAppend( ts );
            }
        }

        #endregion

        #region " WELCH METHOD: DOUBLE "

        /// <summary>
        /// Calculates the spectrum for real data and appends to the existing spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeSeries"> The time series. </param>
        /// <param name="dataPoints"> Specifies the number of data points to use. </param>
        public void TransformAppend( double[] timeSeries, int dataPoints )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            var ts = new Complex[dataPoints];
            timeSeries.CopyTo( ts );
            this.TransformAppend( ts );
        }

        /// <summary> Calculates the spectrum for real data segment by segment. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeSeries">            The time series. </param>
        /// <param name="fourierTransformSize">  Size of the Fourier transform. </param>
        /// <param name="timeSeriesSegmentStep"> The time series segment step. </param>
        /// <returns> The number of segments. </returns>
        public int Transform( double[] timeSeries, int fourierTransformSize, int timeSeriesSegmentStep )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            var spectrumValues = new Complex[fourierTransformSize];
            int spectrumSegmentsCount = 0;
            int iSegmentStart = 0;
            while ( iSegmentStart + fourierTransformSize <= timeSeries.Length )
            {

                // increment the number of segments
                spectrumSegmentsCount += 1;

                // populate the complex array.
                timeSeries.CopyTo( iSegmentStart, spectrumValues );
                this.TransformAppend( spectrumValues );

                // increment the start of segment sample
                iSegmentStart += timeSeriesSegmentStep;
                isr.Core.ApplianceBase.DoEvents();
            }

            return spectrumSegmentsCount;
        }

        #endregion

        #region " WELCH METHOD: SINGLE "

        /// <summary>
        /// Calculates the spectrum for real data and appends to the existing spectrum.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeSeries"> The time series. </param>
        /// <param name="dataPoints"> Specifies the number of data points to use. </param>
        public void TransformAppend( float[] timeSeries, int dataPoints )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            var ts = new Complex[dataPoints];
            timeSeries.CopyTo( ts );
            this.TransformAppend( ts );
        }

        /// <summary> Calculates the spectrum for real data segment by segment. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeSeries">            The time series. </param>
        /// <param name="fourierTransformSize">  Size of the Fourier transform. </param>
        /// <param name="timeSeriesSegmentStep"> The time series segment step. </param>
        /// <returns> The number of segments. </returns>
        public int Transform( float[] timeSeries, int fourierTransformSize, int timeSeriesSegmentStep )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            var spectrumValues = new Complex[fourierTransformSize];
            int spectrumSegmentsCount = 0;
            int iSegmentStart = 0;
            while ( iSegmentStart + fourierTransformSize <= timeSeries.Length )
            {

                // increment the number of segments
                spectrumSegmentsCount += 1;

                // populate the complex array.
                timeSeries.CopyTo( iSegmentStart, spectrumValues );
                this.TransformAppend( spectrumValues );

                // increment the start of segment sample
                iSegmentStart += timeSeriesSegmentStep;
                isr.Core.ApplianceBase.DoEvents();
            }

            return spectrumSegmentsCount;
        }

        #endregion

        #region " FILE I/O "

        /// <summary> Reads spectrum densities from the . </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reader"> Specifies reference to an open binary reader. </param>
        public void ReadDensities( System.IO.BinaryReader reader )
        {
            if ( reader is null )
                throw new ArgumentNullException( nameof( reader ) );

            // assume the densities were scaled.
            this.Scaled = true;

            // read the densities parameters
            this.AverageCount = reader.ReadInt32();
            int elementCount = reader.ReadInt32();

            // allocate data array
            this._Densities = new double[elementCount];

            // Read the densities from the file
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
                this.Densities()[i] = reader.ReadDouble();
        }

        /// <summary> Writes the densities to a data file.  The densities are written as scaled. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid
        /// because spectrum densities were not created.
        /// </exception>
        /// <param name="writer"> Specifies reference to an open binary writer. </param>
        public void WriteDensities( System.IO.BinaryWriter writer )
        {
            if ( writer is null )
                throw new ArgumentNullException( nameof( writer ) );
            if ( this.Densities() is null )
            {
                throw new InvalidOperationException( "Spectrum densities where not created." );
            }

            // write the spectrum parameters
            writer.Write( this.AverageCount );
            int elementCount = this.Densities().Length;
            writer.Write( elementCount );
            if ( this.Scaled )
            {
                // write values to the file
                for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
                    writer.Write( this.Densities()[i] );
            }
            else
            {
                double factor = this.SpectrumScaleFactor( false );
                for ( int i = 0, loopTo1 = elementCount - 1; i <= loopTo1; i++ )
                    writer.Write( factor * this.Densities()[i] );
            }
        }

        #endregion

    }
}
