Namespace Wisdom

    ''' <summary> Calculate the Sliding Fourier Transform. </summary>
    ''' <remarks> (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Class SlidingFourierTransform
        Inherits FourierTransform

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs This class.</summary>
        Public Sub New()
            ' instantiate the base class
            MyBase.New(FourierTransformType.Sliding)
        End Sub

#End Region

#Region " DOUBLE "

        ''' <summary>Initializes the sine and cosine tables for calculating the sliding 
        '''   Fourier transform.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <remarks>Allocates and calculates sine and cosine tables for the sliding 
        '''   Fourier transform and maps the data arrays to the internal memory space 
        '''   of the sliding Fourier transform.
        ''' </remarks>
        ''' <example>
        '''   Sub Form_Click
        '''     Dim fft As New isr.Dsp.MixedRadixFft
        '''     Dim sliding As New isr.Dsp.SlidingFft
        '''     Dim signalLength As integer     ' Signal points
        '''     Dim reals() As Double           ' real-part of the Fourier Transform
        '''     Dim imaginaries() As Double     ' imaginary-part of the Fourier Transform
        '''     Dim magnitude() As Double       ' Fourier Transform magnitude
        '''     Dim samplingRate As Double
        '''     Dim frequencyAxis () As Double  ' Frequency
        '''     Dim newReal As Double
        '''     Dim newImaginary As Double
        '''     Dim oldReal As Double
        '''     Dim oldImaginary As Double
        '''     ' Set signal points
        '''     signalLength = 250
        '''     ' Create 10 cycles of the sine wave with 0 phase
        '''     Dim signal() as Double = isr.Dsp.Helper.Sine(10 / signalLength, 0)
        '''     ' Allocate arrays for real and imaginary-parts
        '''     ReDim reals(signalLength - 1)
        '''     ReDim imaginaries(signalLength)
        '''     ' Place signal in the real-part for DFT.
        '''     signal.CopyTo(reals(),0)
        '''     ' Set options
        '''     fft.IsRemoveMean False
        '''     fft.TaperWindow = 0
        '''     ' Compute the Fourier transform to initialize the sliding Fourier transform
        '''     fft.Forward ( reals(), imaginaries())
        '''     ' set the most recent values of the signal
        '''     dim firstPoint as integer = 0
        '''     newReal = signal(signalLength-1)
        '''     newImaginary = 0
        '''     oldReal = signal(firstPoint)
        '''     oldImaginary = 0
        '''     sliding.Initialize(reals(), imaginaries())
        '''     ' Get the Previous values of the signal
        '''     ' Get new values of the signal
        '''     newReal = 0.99
        '''     newImaginary = 0
        '''     signal(firstPoint) = newReal
        '''     ....
        '''     ' Compute the sliding Fourier transform.
        '''     fft.Update (newReal, newImaginary, oldReal, oldImaginary)
        '''     ....
        '''     ' increment
        '''     firstPoint += 1
        '''     oldReal = signal(firstPoint)
        '''     oldImaginary = 0
        '''     newReal = 1.01
        '''     newImaginary = 0
        '''     signal(firstPoint) = newReal
        '''     ' Compute the sliding Fourier transform.
        '''     fft.Update (newReal, newImaginary, oldReal, oldImaginary)
        '''     ....
        '''     ' Compute the magnitude
        '''     Dim magnitudes() = isr.Dsp.Helper.Magnitude(reals(), Imaginaries())
        '''   End Sub
        ''' </example>
        Protected Overrides Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If

            ' save the real- and imaginary-parts into the cache
            MyBase.CopyToCache(reals, imaginaries)

            ' Allocate the sine tables.
            MyBase.BuildDftTables(reals.Length)

        End Sub

        ''' <summary>Calculates the sliding Fast Fourier Transform.
        ''' </summary>
        ''' <param name="newReal">The new real value to use in calculating the 
        '''   sliding Fourier transform</param>
        ''' <param name="newImaginary">The new imaginary value to use in calculating 
        '''   the sliding Fourier transform.</param>
        ''' <param name="oldReal">The first real value in the time series 
        '''   previously used to calculate the Fourier transform.</param>
        ''' <param name="oldImaginary">The first imaginary value in the time series 
        '''   previously used to calculate the Fourier transform.</param>
        Public Function Update(ByVal newReal As Double, ByVal newImaginary As Double,
                               ByVal oldReal As Double, ByVal oldImaginary As Double) As Boolean
            ' get the difference term
            Dim realDelta As Double = newReal - oldReal
            Dim imagDelta As Double = newImaginary - oldImaginary

            ' Update the sliding Fourier transform
            For i As Integer = 0 To MyBase.RealCache.Length - 1
                newReal = MyBase.RealCache(i) + realDelta
                newImaginary = MyBase.ImaginaryCache(i) + imagDelta
                MyBase.RealCache(i) = MyBase.CosineTable(i) * newReal - MyBase.SineTable(i) * newImaginary
                MyBase.ImaginaryCache(i) = MyBase.CosineTable(i) * newImaginary + MyBase.SineTable(i) * newReal
            Next i
        End Function

#End Region

#Region " SINGLE "

        ''' <summary>Initializes the sine and cosine tables for calculating the 
        '''   sliding Fourier transform.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <remarks>Allocates and calculates sine and cosine tables for the sliding 
        '''   Fourier transform and  maps the data arrays to the internal memory space of 
        '''   the sliding Fourier transform.
        ''' </remarks>
        ''' <example>
        '''   Sub Form_Click
        '''     Dim fft As New isr.Dsp.MixedRadixFft
        '''     Dim sliding As New isr.Dsp.SlidingFft
        '''     Dim signalLength As integer   ' Signal points
        '''     Dim reals() As Single         ' real-part of Fourier transform
        '''     Dim imaginaries() As Single   ' imaginary-part of Fourier transform
        '''     Dim magnitude() As Single     ' Fourier transform magnitude
        '''     Dim samplingRate As Single
        '''     Dim frequencyAxis () As Single  ' Frequency
        '''     Dim newReal As Single
        '''     Dim newImaginary As Single
        '''     Dim oldReal As Single
        '''     Dim oldImaginary As Single
        '''     ' Set signal points
        '''     signalLength = 250
        '''     ' Create 10 cycles of the sine wave with 0 phase
        '''     Dim signal() as Single = isr.Dsp.Helper.Sine(10 / signalLength, 0)
        '''     ' Allocate arrays for real and imaginary-parts
        '''     ReDim reals(signalLength - 1)
        '''     ReDim imaginaries(signalLength - 1)
        '''     ' Place signal in the real-part for DFT.
        '''     signal.CopyTo(reals(),0)
        '''     ' Set options
        '''     fft.IsRemoveMean False
        '''     fft.TaperWindow = 0
        '''     ' Compute the Mixed Radix Fourier transform
        '''     ' to initialize the sliding Fourier transform
        '''     fft.Forward ( reals(), imaginaries())
        '''     ' set the most recent values of the signal
        '''     dim firstPoint as integer = 0
        '''     newReal = signal(signalLength-1)
        '''     newImaginary = 0
        '''     oldReal = signal(firstPoint)
        '''     oldImaginary = 0
        '''     sliding.Initialize(reals(), imaginaries())
        '''     ' Get the Previous values of the signal
        '''     ' Get new values of the signal
        '''     newReal = 0.99
        '''     newImaginary = 0
        '''     signal(firstPoint) = newReal
        '''     ....
        '''     ' Compute the sliding Fourier transform.
        '''     fft.Update (newReal, newImaginary, oldReal, oldImaginary)
        '''     ....
        '''     ' increment
        '''     firstPoint += 1
        '''     oldReal = signal(firstPoint)
        '''     oldImaginary = 0
        '''     newReal = 1.01
        '''     newImaginary = 0
        '''     signal(firstPoint) = newReal
        '''     ' Compute the sliding Fourier transform.
        '''     fft.Update (newReal, newImaginary, oldReal, oldImaginary)
        '''     ....
        '''     ' Compute the magnitude
        '''     Dim magnitudes() = isr.Dsp.Helper.Magnitude(reals(), Imaginaries())
        '''   End Sub
        ''' </example>
        Protected Overrides Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If

            ' save the real- and imaginary-parts into the cache
            MyBase.CopyToCache(reals, imaginaries)

            ' Allocate the sine tables.
            MyBase.BuildDftTables(reals.Length)

        End Sub

        ''' <summary>Calculates the sliding Fast Fourier Transform.
        ''' </summary>
        ''' <param name="newReal">The new real value to use in calculating the 
        '''   sliding Fourier transform</param>
        ''' <param name="newImaginary">The new imaginary value to use in calculating 
        '''   the sliding Fourier transform</param>
        ''' <param name="oldReal">The first real value in the time series 
        '''   previously used to calculate the Fourier transform.</param>
        ''' <param name="oldImaginary">The first imaginary value in the time series 
        '''   previously used to calculate the Fourier transform.</param>
        Public Function Update(ByVal newReal As Single, ByVal newImaginary As Single,
                               ByVal oldReal As Single, ByVal oldImaginary As Single) As Boolean

            ' get the difference term
            Dim realDelta As Single = newReal - oldReal
            Dim imagDelta As Single = newImaginary - oldImaginary

            ' Update the sliding Fourier transform
            For i As Integer = 0 To MyBase.RealCache.Length - 1
                newReal = CSng(MyBase.RealCache(i) + realDelta)
                newImaginary = CSng(MyBase.ImaginaryCache(i) + imagDelta)
                MyBase.RealCache(i) = MyBase.CosineTable(i) * newReal - MyBase.SineTable(i) * newImaginary
                MyBase.ImaginaryCache(i) = MyBase.CosineTable(i) * newImaginary + MyBase.SineTable(i) * newReal
            Next i

        End Function

#End Region

    End Class
End Namespace
