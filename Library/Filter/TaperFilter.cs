﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary>
    /// Provides shared services for applying taper filter windows in the frequency domain.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed partial class TaperFilter
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TaperFilter" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="filterType"> Type of the filter. </param>
        public TaperFilter( TaperFilterType filterType ) : base()
        {
            this.FilterType = filterType;
        }

        #endregion

        #region " SHARED "

        /// <summary> Validates the filter frequencies. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="filterType">     Type of the filter. </param>
        /// <param name="samplingRate">   The sampling rate. </param>
        /// <param name="lowFrequency">   The low frequency. </param>
        /// <param name="highFrequency">  The high frequency. </param>
        /// <param name="transitionBand"> The transition band. </param>
        /// <param name="details">        [in,out] The outcome of the validation. </param>
        /// <returns> <c>True</c> if passed; otherwise, <c>False</c>. </returns>
        public static bool ValidateFilterFrequencies( TaperFilterType filterType, double samplingRate, double lowFrequency, double highFrequency, double transitionBand, ref string details )
        {
            bool validated = true;
            if ( samplingRate <= 0d )
            {
                details = $"Sampling rate ({samplingRate}) must be positive.";
                validated = false;
            }
            else if ( transitionBand <= 0d )
            {
                details = $"Transition band ({transitionBand}) must be positive.";
                validated = false;
            }
            else if ( filterType == TaperFilterType.HighPass )
            {
                if ( highFrequency - transitionBand / 2d <= 0d )
                {
                    validated = false;
                    details = $"High Pass Filter frequency ({highFrequency}) must exceed half the transition band ({transitionBand / 2d}).";
                }
                else if ( highFrequency + transitionBand / 2d > samplingRate / 2d )
                {
                    validated = false;
                    details = $"High Pass Filter frequency ({highFrequency}) plus half transition band ({transitionBand / 2d}) must be lower than half the sampling rate ({samplingRate / 2d}).";
                }
            }
            else if ( filterType == TaperFilterType.LowPass )
            {
                if ( lowFrequency - transitionBand / 2d <= 0d )
                {
                    validated = false;
                    details = $"Low Pass Filter frequency ({lowFrequency}) must exceed half the transition band ({transitionBand / 2d}).";
                }
                else if ( lowFrequency + transitionBand / 2d > samplingRate / 2d )
                {
                    validated = false;
                    details = $"Low Pass Filter frequency ({highFrequency}) plus half transition band ({transitionBand / 2d}) must be lower than half the sampling rate ({samplingRate / 2d}).";
                }
            }
            else if ( filterType == TaperFilterType.BandPass )
            {
                if ( lowFrequency - transitionBand / 2d <= 0d )
                {
                    validated = false;
                    details = $"Low pass-band filter frequency ({lowFrequency}) must exceed half the transition band ({transitionBand / 2d}).";
                }
                else if ( highFrequency - transitionBand < lowFrequency )
                {
                    validated = false;
                    details = $"High pass-band filter frequency ({highFrequency}) must exceed the low frequency ({lowFrequency}) plus the transition band ({transitionBand}).";
                }
                else if ( highFrequency + transitionBand / 2d > samplingRate / 2d )
                {
                    validated = false;
                    details = $"High pass-band frequency ({highFrequency}) plus half the transition band ({transitionBand / 2d}) must be lower than half the sampling rate ({samplingRate / 2d}).";
                }
            }
            else if ( filterType == TaperFilterType.BandReject )
            {
                if ( lowFrequency - transitionBand / 2d <= 0d )
                {
                    validated = false;
                    details = $"Low stop-band filter frequency ({lowFrequency}) must exceed half the transition band ({transitionBand / 2d}).";
                }
                else if ( highFrequency - transitionBand < lowFrequency )
                {
                    validated = false;
                    details = $"High stop-band filter frequency ({highFrequency}) must exceed the low frequency ({lowFrequency}) plus the transition band ({transitionBand}).";
                }
                else if ( highFrequency + transitionBand / 2d > samplingRate / 2d )
                {
                    validated = false;
                    details = $"High stop-band frequency ({highFrequency}) plus half transition band ({transitionBand / 2d}) must be lower than half the sampling rate ({samplingRate / 2d}).";
                }
            }

            return validated;
        }

        /// <summary> Returns the index in the spectrum matching the given frequency. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="frequency">        The frequency. </param>
        /// <param name="samplingRate">     The sampling rate. </param>
        /// <param name="timeSeriesLength"> Length of the time series. </param>
        /// <returns> The index in the spectrum matching the given frequency. </returns>
        private static int FrequencyIndex( double frequency, double samplingRate, int timeSeriesLength )
        {
            return samplingRate <= float.Epsilon
                ? throw new ArgumentOutOfRangeException( nameof( samplingRate ), "Sampling rate must be positive" )
                : timeSeriesLength <= 2
                ? throw new ArgumentOutOfRangeException( nameof( timeSeriesLength ), "Time series length must exceed 2" )
                : frequency <= float.Epsilon
                ? throw new ArgumentOutOfRangeException( nameof( frequency ), "Frequency must be positive" )
                : ( int ) (frequency * timeSeriesLength / samplingRate);
        }

        #endregion

        #region " METHODS "

        /// <summary> Validates filter settings. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="details"> [in,out] The validation failure details. </param>
        /// <returns> <c>True</c> if validated; otherwise, <c>False</c>. </returns>
        public bool ValidateFilterFrequencies( ref string details )
        {
            return ValidateFilterFrequencies( this.FilterType, this.SamplingRate, this.LowFrequency, this.HighFrequency, this.TransitionBand, ref details );
        }

        /// <summary>
        /// Tapers the frequency band for filtering the signal in the frequency domain.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="spectrum">     The spectrum. </param>
        /// <param name="samplingRate"> The sampling rate. </param>
        public void Taper( Complex[] spectrum, double samplingRate )
        {
            if ( spectrum is null )
                throw new ArgumentNullException( nameof( spectrum ) );
            if ( spectrum.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( spectrum ), "The spectrum must have more than a one element." );
            }

            this.SamplingRate = samplingRate;
            this.TimeSeriesLength = spectrum.Length;
            string details = string.Empty;
            if ( !this.ValidateFilterFrequencies( ref details ) )
            {
                throw new InvalidOperationException( details );
            }

            switch ( this.FilterType )
            {
                case TaperFilterType.BandPass:
                    {

                        // high pass at the low frequency
                        spectrum.HighPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ) );

                        // low pass at the high frequency.
                        spectrum.LowPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ) );
                        break;
                    }

                case TaperFilterType.BandReject:
                    {
                        double midFrequency = (this.LowFrequency + this.HighFrequency) / 2d;

                        // low pass at the low frequency but up to the mid band
                        spectrum.LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ) );

                        // high pass at the high frequency but only from the mid band
                        spectrum.HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ), this.FrequencyIndex( midFrequency ) );
                        break;
                    }

                case TaperFilterType.HighPass:
                    {
                        spectrum.HighPassTaper( this.FrequencyIndex( this.HighFrequency ), this.FrequencyIndex( this.TransitionBand ) );
                        break;
                    }

                case TaperFilterType.LowPass:
                    {
                        spectrum.LowPassTaper( this.FrequencyIndex( this.LowFrequency ), this.FrequencyIndex( this.TransitionBand ) );
                        break;
                    }

                case TaperFilterType.None:
                    {
                        break;
                    }
            }
        }

        /// <summary> Returns the frequency index for the specified frequency. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> The frequency. </param>
        /// <returns> The frequency index for the specified frequency. </returns>
        private int FrequencyIndex( double frequency )
        {
            return FrequencyIndex( frequency, this.SamplingRate, this.TimeSeriesLength );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the <see cref="isr.Algorithms.Signals.TaperFilterType">type</see>
        /// of filter.
        /// </summary>
        /// <value> The type of the filter. </value>
        public TaperFilterType FilterType { get; set; }

        /// <summary> Gets or sets the low frequency of the filter. </summary>
        /// <value> The low frequency. </value>
        public double LowFrequency { get; set; }

        /// <summary> Gets or sets the high frequency of the filter. </summary>
        /// <value> The high frequency. </value>
        public double HighFrequency { get; set; }

        /// <summary> Gets or sets the filter transition band. </summary>
        /// <value> The transition band. </value>
        public double TransitionBand { get; set; }

        /// <summary> Gets or sets the sampling rate for calculating frequency indexes. </summary>
        /// <value> The sampling rate. </value>
        private double SamplingRate { get; set; }

        /// <summary>
        /// Gets or sets the number of elements in the time series sample that is processed to compute
        /// the Spectrum.
        /// </summary>
        /// <value> The length of the time series. </value>
        public int TimeSeriesLength { get; set; }

        #endregion

    }
}