<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SpectrumView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._PointsToDisplayTextBox = New System.Windows.Forms.TextBox()
        Me._SignalListBox = New System.Windows.Forms.ListBox()
        Me._DataTabPage = New System.Windows.Forms.TabPage()
        Me._TimingTextBox = New System.Windows.Forms.TextBox()
        Me._TimingListBoxLabel = New System.Windows.Forms.Label()
        Me._PointsToDisplayTextBoxLabel = New System.Windows.Forms.Label()
        Me._SignalListBoxLabel = New System.Windows.Forms.Label()
        Me._DoubleRadioButton = New System.Windows.Forms.RadioButton()
        Me._RemoveMeanCheckBox = New System.Windows.Forms.CheckBox()
        Me._ExampleComboBox = New System.Windows.Forms.ComboBox()
        Me._TaperWindowCheckBox = New System.Windows.Forms.CheckBox()
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._SignalTabPage = New System.Windows.Forms.TabPage()
        Me._SignalChartPanel = New System.Windows.Forms.Panel()
        Me._SignalOptionsPanel = New System.Windows.Forms.Panel()
        Me._SignalComboBoxLabel = New System.Windows.Forms.Label()
        Me._SignalComboBox = New System.Windows.Forms.ComboBox()
        Me._SignalDurationTextBox = New System.Windows.Forms.TextBox()
        Me._SignalDurationTextBoxLabel = New System.Windows.Forms.Label()
        Me._PointsTextBox = New System.Windows.Forms.TextBox()
        Me._PhaseTextBox = New System.Windows.Forms.TextBox()
        Me._CyclesTextBox = New System.Windows.Forms.TextBox()
        Me._PointsTextBoxLabel = New System.Windows.Forms.Label()
        Me._PhaseTextBoxLabel = New System.Windows.Forms.Label()
        Me._CyclesTextBoxLabel = New System.Windows.Forms.Label()
        Me._SpectrumTabPage = New System.Windows.Forms.TabPage()
        Me._SpectrumChartPanel = New System.Windows.Forms.Panel()
        Me._SpectrumOptionsPanel = New System.Windows.Forms.Panel()
        Me._FilterComboBoxLabel = New System.Windows.Forms.Label()
        Me._FilterComboBox = New System.Windows.Forms.ComboBox()
        Me._SingleRadioButton = New System.Windows.Forms.RadioButton()
        Me._ExampleComboBoxLabel = New System.Windows.Forms.Label()
        Me._StartStopCheckBox = New System.Windows.Forms.CheckBox()
        Me._MessagesTabPage = New System.Windows.Forms.TabPage()
        Me._MessagesList = New isr.Core.Forma.MessagesBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._CountToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ErrorToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._TimeToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._DataTabPage.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._SignalTabPage.SuspendLayout()
        Me._SignalOptionsPanel.SuspendLayout()
        Me._SpectrumTabPage.SuspendLayout()
        Me._SpectrumOptionsPanel.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_PointsToDisplayTextBox
        '
        Me._PointsToDisplayTextBox.AcceptsReturn = True
        Me._PointsToDisplayTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PointsToDisplayTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PointsToDisplayTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PointsToDisplayTextBox.Location = New System.Drawing.Point(129, 15)
        Me._PointsToDisplayTextBox.MaxLength = 0
        Me._PointsToDisplayTextBox.Name = "_PointsToDisplayTextBox"
        Me._PointsToDisplayTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsToDisplayTextBox.Size = New System.Drawing.Size(65, 25)
        Me._PointsToDisplayTextBox.TabIndex = 1
        '
        '_SignalListBox
        '
        Me._SignalListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SignalListBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalListBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalListBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SignalListBox.ItemHeight = 17
        Me._SignalListBox.Location = New System.Drawing.Point(272, 24)
        Me._SignalListBox.Name = "_SignalListBox"
        Me._SignalListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalListBox.Size = New System.Drawing.Size(295, 293)
        Me._SignalListBox.TabIndex = 5
        '
        '_DataTabPage
        '
        Me._DataTabPage.Controls.Add(Me._TimingTextBox)
        Me._DataTabPage.Controls.Add(Me._TimingListBoxLabel)
        Me._DataTabPage.Controls.Add(Me._PointsToDisplayTextBox)
        Me._DataTabPage.Controls.Add(Me._SignalListBox)
        Me._DataTabPage.Controls.Add(Me._PointsToDisplayTextBoxLabel)
        Me._DataTabPage.Controls.Add(Me._SignalListBoxLabel)
        Me._DataTabPage.Location = New System.Drawing.Point(4, 26)
        Me._DataTabPage.Name = "_DataTabPage"
        Me._DataTabPage.Size = New System.Drawing.Size(578, 338)
        Me._DataTabPage.TabIndex = 2
        Me._DataTabPage.Text = "Data"
        '
        '_TimingTextBox
        '
        Me._TimingTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._TimingTextBox.Location = New System.Drawing.Point(16, 64)
        Me._TimingTextBox.Multiline = True
        Me._TimingTextBox.Name = "_TimingTextBox"
        Me._TimingTextBox.Size = New System.Drawing.Size(246, 253)
        Me._TimingTextBox.TabIndex = 3
        '
        '_TimingListBoxLabel
        '
        Me._TimingListBoxLabel.AutoSize = True
        Me._TimingListBoxLabel.Location = New System.Drawing.Point(15, 45)
        Me._TimingListBoxLabel.Name = "_TimingListBoxLabel"
        Me._TimingListBoxLabel.Size = New System.Drawing.Size(85, 17)
        Me._TimingListBoxLabel.TabIndex = 2
        Me._TimingListBoxLabel.Text = "Timing Data: "
        '
        '_PointsToDisplayTextBoxLabel
        '
        Me._PointsToDisplayTextBoxLabel.AutoSize = True
        Me._PointsToDisplayTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PointsToDisplayTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PointsToDisplayTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsToDisplayTextBoxLabel.Location = New System.Drawing.Point(16, 19)
        Me._PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel"
        Me._PointsToDisplayTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsToDisplayTextBoxLabel.Size = New System.Drawing.Size(112, 17)
        Me._PointsToDisplayTextBoxLabel.TabIndex = 0
        Me._PointsToDisplayTextBoxLabel.Text = "Points to Display: "
        Me._PointsToDisplayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SignalListBoxLabel
        '
        Me._SignalListBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SignalListBoxLabel.AutoSize = True
        Me._SignalListBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SignalListBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalListBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalListBoxLabel.Location = New System.Drawing.Point(298, 5)
        Me._SignalListBoxLabel.Name = "_SignalListBoxLabel"
        Me._SignalListBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalListBoxLabel.Size = New System.Drawing.Size(235, 17)
        Me._SignalListBoxLabel.TabIndex = 4
        Me._SignalListBoxLabel.Text = "Signal                       IFFT{ FFT{ Signal}}"
        '
        '_DoubleRadioButton
        '
        Me._DoubleRadioButton.AutoSize = True
        Me._DoubleRadioButton.Checked = True
        Me._DoubleRadioButton.Location = New System.Drawing.Point(124, 9)
        Me._DoubleRadioButton.Name = "_DoubleRadioButton"
        Me._DoubleRadioButton.Size = New System.Drawing.Size(124, 21)
        Me._DoubleRadioButton.TabIndex = 0
        Me._DoubleRadioButton.TabStop = True
        Me._DoubleRadioButton.Text = "Double Precision"
        '
        '_RemoveMeanCheckBox
        '
        Me._RemoveMeanCheckBox.AutoSize = True
        Me._RemoveMeanCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._RemoveMeanCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._RemoveMeanCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RemoveMeanCheckBox.Location = New System.Drawing.Point(8, 9)
        Me._RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox"
        Me._RemoveMeanCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RemoveMeanCheckBox.Size = New System.Drawing.Size(111, 21)
        Me._RemoveMeanCheckBox.TabIndex = 0
        Me._RemoveMeanCheckBox.Text = "Remove Mean"
        Me._RemoveMeanCheckBox.UseVisualStyleBackColor = False
        '
        '_ExampleComboBox
        '
        Me._ExampleComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._ExampleComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ExampleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ExampleComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ExampleComboBox.Items.AddRange(New Object() {"Mixed Radix FFT", "Sliding FFT"})
        Me._ExampleComboBox.Location = New System.Drawing.Point(395, 34)
        Me._ExampleComboBox.Name = "_ExampleComboBox"
        Me._ExampleComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExampleComboBox.Size = New System.Drawing.Size(178, 25)
        Me._ExampleComboBox.TabIndex = 5
        '
        '_TaperWindowCheckBox
        '
        Me._TaperWindowCheckBox.AutoSize = True
        Me._TaperWindowCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._TaperWindowCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._TaperWindowCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._TaperWindowCheckBox.Location = New System.Drawing.Point(8, 34)
        Me._TaperWindowCheckBox.Name = "_TaperWindowCheckBox"
        Me._TaperWindowCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TaperWindowCheckBox.Size = New System.Drawing.Size(112, 21)
        Me._TaperWindowCheckBox.TabIndex = 1
        Me._TaperWindowCheckBox.Text = "Taper Window"
        Me._TaperWindowCheckBox.UseVisualStyleBackColor = False
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._SignalTabPage)
        Me._Tabs.Controls.Add(Me._SpectrumTabPage)
        Me._Tabs.Controls.Add(Me._DataTabPage)
        Me._Tabs.Controls.Add(Me._MessagesTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(586, 368)
        Me._Tabs.TabIndex = 2
        '
        '_SignalTabPage
        '
        Me._SignalTabPage.Controls.Add(Me._SignalChartPanel)
        Me._SignalTabPage.Controls.Add(Me._SignalOptionsPanel)
        Me._SignalTabPage.Location = New System.Drawing.Point(4, 26)
        Me._SignalTabPage.Name = "_SignalTabPage"
        Me._SignalTabPage.Size = New System.Drawing.Size(578, 338)
        Me._SignalTabPage.TabIndex = 0
        Me._SignalTabPage.Text = "Signal"
        '
        '_SignalChartPanel
        '
        Me._SignalChartPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SignalChartPanel.Location = New System.Drawing.Point(0, 60)
        Me._SignalChartPanel.Name = "_SignalChartPanel"
        Me._SignalChartPanel.Size = New System.Drawing.Size(578, 278)
        Me._SignalChartPanel.TabIndex = 30
        '
        '_SignalOptionsPanel
        '
        Me._SignalOptionsPanel.Controls.Add(Me._SignalComboBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._SignalComboBox)
        Me._SignalOptionsPanel.Controls.Add(Me._SignalDurationTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._SignalDurationTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._PointsTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._PhaseTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._CyclesTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._PointsTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._PhaseTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._CyclesTextBoxLabel)
        Me._SignalOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._SignalOptionsPanel.Location = New System.Drawing.Point(0, 0)
        Me._SignalOptionsPanel.Name = "_SignalOptionsPanel"
        Me._SignalOptionsPanel.Size = New System.Drawing.Size(578, 60)
        Me._SignalOptionsPanel.TabIndex = 0
        '
        '_SignalComboBoxLabel
        '
        Me._SignalComboBoxLabel.AutoSize = True
        Me._SignalComboBoxLabel.Location = New System.Drawing.Point(370, 8)
        Me._SignalComboBoxLabel.Name = "_SignalComboBoxLabel"
        Me._SignalComboBoxLabel.Size = New System.Drawing.Size(46, 17)
        Me._SignalComboBoxLabel.TabIndex = 8
        Me._SignalComboBoxLabel.Text = "Signal:"
        '
        '_SignalComboBox
        '
        Me._SignalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SignalComboBox.FormattingEnabled = True
        Me._SignalComboBox.Location = New System.Drawing.Point(418, 4)
        Me._SignalComboBox.Name = "_SignalComboBox"
        Me._SignalComboBox.Size = New System.Drawing.Size(121, 25)
        Me._SignalComboBox.TabIndex = 9
        '
        '_SignalDurationTextBox
        '
        Me._SignalDurationTextBox.AcceptsReturn = True
        Me._SignalDurationTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalDurationTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._SignalDurationTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SignalDurationTextBox.Location = New System.Drawing.Point(107, 30)
        Me._SignalDurationTextBox.MaxLength = 0
        Me._SignalDurationTextBox.Name = "_SignalDurationTextBox"
        Me._SignalDurationTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalDurationTextBox.Size = New System.Drawing.Size(46, 25)
        Me._SignalDurationTextBox.TabIndex = 3
        '
        '_SignalDurationTextBoxLabel
        '
        Me._SignalDurationTextBoxLabel.AutoSize = True
        Me._SignalDurationTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SignalDurationTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalDurationTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalDurationTextBoxLabel.Location = New System.Drawing.Point(8, 34)
        Me._SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel"
        Me._SignalDurationTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalDurationTextBoxLabel.Size = New System.Drawing.Size(97, 17)
        Me._SignalDurationTextBoxLabel.TabIndex = 2
        Me._SignalDurationTextBoxLabel.Text = "Duration [Sec]: "
        Me._SignalDurationTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PointsTextBox
        '
        Me._PointsTextBox.AcceptsReturn = True
        Me._PointsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PointsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PointsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PointsTextBox.Location = New System.Drawing.Point(107, 4)
        Me._PointsTextBox.MaxLength = 0
        Me._PointsTextBox.Name = "_PointsTextBox"
        Me._PointsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsTextBox.Size = New System.Drawing.Size(46, 25)
        Me._PointsTextBox.TabIndex = 1
        Me._PointsTextBox.Text = "1000"
        '
        '_PhaseTextBox
        '
        Me._PhaseTextBox.AcceptsReturn = True
        Me._PhaseTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PhaseTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PhaseTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PhaseTextBox.Location = New System.Drawing.Point(278, 30)
        Me._PhaseTextBox.MaxLength = 0
        Me._PhaseTextBox.Name = "_PhaseTextBox"
        Me._PhaseTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PhaseTextBox.Size = New System.Drawing.Size(48, 25)
        Me._PhaseTextBox.TabIndex = 7
        '
        '_CyclesTextBox
        '
        Me._CyclesTextBox.AcceptsReturn = True
        Me._CyclesTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._CyclesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._CyclesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._CyclesTextBox.Location = New System.Drawing.Point(278, 4)
        Me._CyclesTextBox.MaxLength = 0
        Me._CyclesTextBox.Name = "_CyclesTextBox"
        Me._CyclesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesTextBox.Size = New System.Drawing.Size(48, 25)
        Me._CyclesTextBox.TabIndex = 5
        '
        '_PointsTextBoxLabel
        '
        Me._PointsTextBoxLabel.AutoSize = True
        Me._PointsTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PointsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PointsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsTextBoxLabel.Location = New System.Drawing.Point(55, 8)
        Me._PointsTextBoxLabel.Name = "_PointsTextBoxLabel"
        Me._PointsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsTextBoxLabel.Size = New System.Drawing.Size(50, 17)
        Me._PointsTextBoxLabel.TabIndex = 0
        Me._PointsTextBoxLabel.Text = "Points: "
        Me._PointsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PhaseTextBoxLabel
        '
        Me._PhaseTextBoxLabel.AutoSize = True
        Me._PhaseTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PhaseTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PhaseTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PhaseTextBoxLabel.Location = New System.Drawing.Point(191, 34)
        Me._PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel"
        Me._PhaseTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PhaseTextBoxLabel.Size = New System.Drawing.Size(85, 17)
        Me._PhaseTextBoxLabel.TabIndex = 6
        Me._PhaseTextBoxLabel.Text = "Phase [Deg]: "
        Me._PhaseTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_CyclesTextBoxLabel
        '
        Me._CyclesTextBoxLabel.AutoSize = True
        Me._CyclesTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._CyclesTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CyclesTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CyclesTextBoxLabel.Location = New System.Drawing.Point(175, 8)
        Me._CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel"
        Me._CyclesTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesTextBoxLabel.Size = New System.Drawing.Size(101, 17)
        Me._CyclesTextBoxLabel.TabIndex = 4
        Me._CyclesTextBoxLabel.Text = "Frequency [Hz]: "
        Me._CyclesTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SpectrumTabPage
        '
        Me._SpectrumTabPage.Controls.Add(Me._SpectrumChartPanel)
        Me._SpectrumTabPage.Controls.Add(Me._SpectrumOptionsPanel)
        Me._SpectrumTabPage.Location = New System.Drawing.Point(4, 26)
        Me._SpectrumTabPage.Name = "_SpectrumTabPage"
        Me._SpectrumTabPage.Size = New System.Drawing.Size(578, 338)
        Me._SpectrumTabPage.TabIndex = 1
        Me._SpectrumTabPage.Text = "Spectrum"
        '
        '_SpectrumChartPanel
        '
        Me._SpectrumChartPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SpectrumChartPanel.Location = New System.Drawing.Point(0, 64)
        Me._SpectrumChartPanel.Name = "_SpectrumChartPanel"
        Me._SpectrumChartPanel.Size = New System.Drawing.Size(578, 274)
        Me._SpectrumChartPanel.TabIndex = 1
        '
        '_SpectrumOptionsPanel
        '
        Me._SpectrumOptionsPanel.Controls.Add(Me._FilterComboBoxLabel)
        Me._SpectrumOptionsPanel.Controls.Add(Me._FilterComboBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._DoubleRadioButton)
        Me._SpectrumOptionsPanel.Controls.Add(Me._SingleRadioButton)
        Me._SpectrumOptionsPanel.Controls.Add(Me._ExampleComboBoxLabel)
        Me._SpectrumOptionsPanel.Controls.Add(Me._StartStopCheckBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._RemoveMeanCheckBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._ExampleComboBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._TaperWindowCheckBox)
        Me._SpectrumOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._SpectrumOptionsPanel.Location = New System.Drawing.Point(0, 0)
        Me._SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel"
        Me._SpectrumOptionsPanel.Size = New System.Drawing.Size(578, 64)
        Me._SpectrumOptionsPanel.TabIndex = 0
        '
        '_FilterComboBoxLabel
        '
        Me._FilterComboBoxLabel.AutoSize = True
        Me._FilterComboBoxLabel.Location = New System.Drawing.Point(247, 15)
        Me._FilterComboBoxLabel.Name = "_FilterComboBoxLabel"
        Me._FilterComboBoxLabel.Size = New System.Drawing.Size(77, 17)
        Me._FilterComboBoxLabel.TabIndex = 2
        Me._FilterComboBoxLabel.Text = "Taper Filter:"
        '
        '_FilterComboBox
        '
        Me._FilterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._FilterComboBox.FormattingEnabled = True
        Me._FilterComboBox.Location = New System.Drawing.Point(247, 34)
        Me._FilterComboBox.Name = "_FilterComboBox"
        Me._FilterComboBox.Size = New System.Drawing.Size(141, 25)
        Me._FilterComboBox.TabIndex = 3
        '
        '_SingleRadioButton
        '
        Me._SingleRadioButton.AutoSize = True
        Me._SingleRadioButton.Location = New System.Drawing.Point(125, 34)
        Me._SingleRadioButton.Name = "_SingleRadioButton"
        Me._SingleRadioButton.Size = New System.Drawing.Size(117, 21)
        Me._SingleRadioButton.TabIndex = 1
        Me._SingleRadioButton.Text = "Single Precision"
        '
        '_ExampleComboBoxLabel
        '
        Me._ExampleComboBoxLabel.AutoSize = True
        Me._ExampleComboBoxLabel.Location = New System.Drawing.Point(395, 14)
        Me._ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel"
        Me._ExampleComboBoxLabel.Size = New System.Drawing.Size(67, 17)
        Me._ExampleComboBoxLabel.TabIndex = 4
        Me._ExampleComboBoxLabel.Text = "Calculate: "
        Me._ExampleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_StartStopCheckBox
        '
        Me._StartStopCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StartStopCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me._StartStopCheckBox.Location = New System.Drawing.Point(511, 9)
        Me._StartStopCheckBox.Name = "_StartStopCheckBox"
        Me._StartStopCheckBox.Size = New System.Drawing.Size(64, 24)
        Me._StartStopCheckBox.TabIndex = 6
        Me._StartStopCheckBox.Text = "&Start"
        Me._StartStopCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._MessagesList)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 26)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Size = New System.Drawing.Size(552, 338)
        Me._MessagesTabPage.TabIndex = 3
        Me._MessagesTabPage.Text = "Log"
        '
        '_MessagesList
        '
        Me._MessagesList.BackColor = System.Drawing.SystemColors.Info
        Me._MessagesList.CausesValidation = False
        Me._MessagesList.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesList.Location = New System.Drawing.Point(0, 0)
        Me._MessagesList.Multiline = True
        Me._MessagesList.Name = "_MessagesList"
        Me._MessagesList.PresetCount = 50
        Me._MessagesList.ReadOnly = True
        Me._MessagesList.ResetCount = 100
        Me._MessagesList.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesList.Size = New System.Drawing.Size(552, 338)
        Me._MessagesList.TabIndex = 4
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusToolStripStatusLabel, Me._CountToolStripStatusLabel, Me._ErrorToolStripStatusLabel, Me._TimeToolStripStatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 368)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(586, 22)
        Me._StatusStrip.TabIndex = 4
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusToolStripStatusLabel
        '
        Me._StatusToolStripStatusLabel.Name = "_StatusToolStripStatusLabel"
        Me._StatusToolStripStatusLabel.Size = New System.Drawing.Size(471, 17)
        Me._StatusToolStripStatusLabel.Spring = True
        Me._StatusToolStripStatusLabel.Text = "Ready"
        Me._StatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._StatusToolStripStatusLabel.ToolTipText = "Status"
        '
        '_CountToolStripStatusLabel
        '
        Me._CountToolStripStatusLabel.Name = "_CountToolStripStatusLabel"
        Me._CountToolStripStatusLabel.Size = New System.Drawing.Size(13, 17)
        Me._CountToolStripStatusLabel.Text = "0"
        Me._CountToolStripStatusLabel.ToolTipText = "FFT counter"
        '
        '_ErrorToolStripStatusLabel
        '
        Me._ErrorToolStripStatusLabel.Name = "_ErrorToolStripStatusLabel"
        Me._ErrorToolStripStatusLabel.Size = New System.Drawing.Size(34, 17)
        Me._ErrorToolStripStatusLabel.Text = "0.000"
        Me._ErrorToolStripStatusLabel.ToolTipText = "RMS difference between signal and inverse FFT"
        '
        '_TimeToolStripStatusLabel
        '
        Me._TimeToolStripStatusLabel.Name = "_TimeToolStripStatusLabel"
        Me._TimeToolStripStatusLabel.Size = New System.Drawing.Size(53, 17)
        Me._TimeToolStripStatusLabel.Text = "0.000 ms"
        Me._TimeToolStripStatusLabel.ToolTipText = "Time to calculate an FFT"
        '
        'SpectrumPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(586, 390)
        Me.Controls.Add(Me._Tabs)
        Me.Controls.Add(Me._StatusStrip)
        Me.Name = "SpectrumPanel"
        Me.Text = "Spectrum Panel"
        Me._DataTabPage.ResumeLayout(False)
        Me._DataTabPage.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._SignalTabPage.ResumeLayout(False)
        Me._SignalOptionsPanel.ResumeLayout(False)
        Me._SignalOptionsPanel.PerformLayout()
        Me._SpectrumTabPage.ResumeLayout(False)
        Me._SpectrumOptionsPanel.ResumeLayout(False)
        Me._SpectrumOptionsPanel.PerformLayout()
        Me._MessagesTabPage.ResumeLayout(False)
        Me._MessagesTabPage.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _PointsToDisplayTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalListBox As System.Windows.Forms.ListBox
    Private WithEvents _DataTabPage As System.Windows.Forms.TabPage
    Private WithEvents _PointsToDisplayTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SignalListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DoubleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _RemoveMeanCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ExampleComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _TaperWindowCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    Private WithEvents _SignalTabPage As System.Windows.Forms.TabPage
    Private WithEvents _SignalChartPanel As System.Windows.Forms.Panel
    Private WithEvents _SignalOptionsPanel As System.Windows.Forms.Panel
    Private WithEvents _SignalDurationTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalDurationTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PointsTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PhaseTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CyclesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PointsTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PhaseTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _CyclesTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SpectrumTabPage As System.Windows.Forms.TabPage
    Private WithEvents _SpectrumChartPanel As System.Windows.Forms.Panel
    Private WithEvents _SpectrumOptionsPanel As System.Windows.Forms.Panel
    Private WithEvents _ExampleComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _StartStopCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SingleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _MessagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _MessagesList As isr.Core.Forma.MessagesBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _TimingListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _TimingTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SignalComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _FilterComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FilterComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _StatusToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _CountToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ErrorToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _TimeToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
End Class
