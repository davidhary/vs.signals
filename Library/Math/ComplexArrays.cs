﻿using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary> Complex arrays. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public static class ComplexArrays
    {

        #region " MAGNITUDE "

        /// <summary> Calculates magnitudes of the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The magnitudes of the complex values. </returns>
        public static double[] Magnitudes( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                var result = new double[values.Length];
                for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                    result[i] = Complex.Abs( values[i] );
                return result;
            }
            else
            {
                return Array.Empty<double>();
            }
        }

        #endregion

        #region " ARRAYS "

        /// <summary> Adds a value to the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        public static void Add( this Complex[] values, Complex value )
        {
            if ( values is object && values.Length > 0 )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                    values[i] += value;
            }
        }

        /// <summary> Returns the index of the first maximum of the complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> Index of the first maximum of the complex array. </returns>
        public static int IndexFirstMaximum( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                int maxIndex = values.GetLowerBound( 0 );
                double max = values[maxIndex].Abs();
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    double abs = values[i].Abs();
                    if ( max < abs )
                    {
                        max = abs;
                        maxIndex = i;
                    }
                }

                return maxIndex;
            }
            else
            {
                return -1;
            }
        }

        /// <summary> Calculates the complex mean of the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The complex mean of the complex values. </returns>
        public static Complex Mean( this Complex[] values )
        {
            return values is object && values.Length > 0 ? values.Sum() / values.Length : 0;
        }

        /// <summary> Calculates the mean of the real values of the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The mean of the real values of the complex values. </returns>
        public static double RealMean( this Complex[] values )
        {
            return values is object && values.Length > 0 ? values.SumReals() / values.Length : 0d;
        }

        /// <summary> Removes the mean from the values array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        public static void RemoveMean( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                values.Add( -values.Mean() );
            }
        }

        /// <summary> Removes the mean from the values array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        public static void RemoveRealMean( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                values.Add( new Complex( -values.RealMean(), 0d ) );
            }
        }

        /// <summary> Multiplies the complex values by a scalar. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        public static void Scale( this Complex[] values, Complex value )
        {
            if ( values is object && values.Length > 0 )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                    values[i] *= value;
            }
        }

        /// <summary> Multiplies the complex values by a scalar array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this Complex[] values, Complex[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= scalars[i];
                }
            }
        }

        /// <summary> Multiplies the complex values by a scalar array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this Complex[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= scalars[i];
                }
            }
        }

        /// <summary>
        /// Multiplies the complex array real values by a scalar array and zeros the imaginary parts.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void ScaleReals( this Complex[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] = new Complex( values[i].Real * scalars[i], 0d );
                }
            }
        }

        /// <summary> Swaps values between two array locations. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="data">       The array containing the data. </param>
        /// <param name="leftIndex">  An array index. </param>
        /// <param name="rightIndex"> An array index. </param>
        public static void Swap( this Complex[] data, int leftIndex, int rightIndex )
        {
            if ( data is object )
            {
                var cache = data[leftIndex];
                data[leftIndex] = data[rightIndex];
                data[rightIndex] = cache;
            }
        }

        /// <summary> Calculates the sum of the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The sum of the complex values. </returns>
        public static Complex Sum( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                Complex cache = 0;
                foreach ( Complex c in values )
                    cache += c;
                return cache;
            }
            else
            {
                return 0;
            }
        }

        /// <summary> Calculates the sum of the real value elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The sum of the real value elements. </returns>
        public static double SumReals( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                double cache = 0d;
                foreach ( Complex c in values )
                    cache += c.Real;
                return cache;
            }
            else
            {
                return 0d;
            }
        }

        /// <summary> Calculates the sum of the complex values squared. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The sum of the complex values squared. </returns>
        public static double SumSquares( this Complex[] values )
        {
            if ( values is object && values.Length > 0 )
            {
                double cache = 0d;
                foreach ( Complex c in values )
                {
                    double abs = c.Abs();
                    cache += abs + abs;
                }
            }
            else
            {
                return 0d;
            }

            return default;
        }

        /// <summary> Calculates the root mean square (RMS) value of the complex values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The root mean square (RMS) value of the complex values. </returns>
        public static double RootMeanSquare( this Complex[] values )
        {
            return values is object && values.Length > 0 ? Math.Sqrt( values.SumSquares() / values.Length ) : 0d;
        }

        /// <summary> Swaps the imaginary and real values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        public static void Swap( this Complex[] values )
        {
            if ( values is object )
            {
                for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                    values[i] = values[i].Swap();
            }
        }

        #endregion

        #region " COPY "

        /// <summary> Copy values between complex arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray">      The source array. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void CopyTo( this Complex[] sourceArray, Complex[] destinationArray )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( sourceArray.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = Math.Max( sourceArray.GetLowerBound( 0 ), destinationArray.GetLowerBound( 0 ) ), loopTo = Math.Min( sourceArray.GetUpperBound( 0 ), destinationArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    destinationArray[i] = sourceArray[i];
            }
        }

        #endregion

        #region " COPY DOUBLE "

        /// <summary> Copies the real values to the complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray">      The source array. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void CopyTo( this double[] sourceArray, Complex[] destinationArray )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( sourceArray.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = Math.Max( sourceArray.GetLowerBound( 0 ), destinationArray.GetLowerBound( 0 ) ), loopTo = Math.Min( sourceArray.GetUpperBound( 0 ), destinationArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    destinationArray[i] = new Complex( sourceArray[i], 0d );
            }
        }

        /// <summary> Copies the complex values to a real and imaginary arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray">      The source array. </param>
        /// <param name="startingIndex">    Zero-based index of the starting. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void CopyTo( this double[] sourceArray, int startingIndex, Complex[] destinationArray )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( sourceArray.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = destinationArray.GetLowerBound( 0 ), loopTo = destinationArray.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    destinationArray[i] = new Complex( sourceArray[startingIndex], 0d );
                    startingIndex += 1;
                }
            }
        }

        /// <summary> Copies the real and imaginary values to the complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">            The real values. </param>
        /// <param name="imaginaries">      The imaginaries. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void Copy( double[] reals, double[] imaginaries, Complex[] destinationArray )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            if ( reals.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), destinationArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), destinationArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    destinationArray[i] = new Complex( reals[i], imaginaries[i] );
            }
        }

        /// <summary> Copies the complex values to a real and imaginary arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="sourceArray"> The source array. </param>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        public static void CopyTo( this Complex[] sourceArray, double[] reals, double[] imaginaries )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            if ( reals.Length > 0 && sourceArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), sourceArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), sourceArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                {
                    reals[i] = sourceArray[i].Real;
                    imaginaries[i] = sourceArray[i].Imaginary;
                }
            }
        }

        /// <summary> Copies the complex real values to a real array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray"> The source array. </param>
        /// <param name="reals">       The reals. </param>
        public static void CopyTo( this Complex[] sourceArray, double[] reals )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( reals.Length > 0 && sourceArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), sourceArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), sourceArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    reals[i] = sourceArray[i].Real;
            }
        }

        #endregion

        #region " COPY SINGLE "

        /// <summary> Copies the real values to the complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray">      The source array. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void CopyTo( this float[] sourceArray, Complex[] destinationArray )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( sourceArray.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = Math.Max( sourceArray.GetLowerBound( 0 ), destinationArray.GetLowerBound( 0 ) ), loopTo = Math.Min( sourceArray.GetUpperBound( 0 ), destinationArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    destinationArray[i] = new Complex( sourceArray[i], 0d );
            }
        }

        /// <summary> Copies the complex values to a real and imaginary arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray">      The source array. </param>
        /// <param name="startingIndex">    Zero-based index of the starting. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void CopyTo( this float[] sourceArray, int startingIndex, Complex[] destinationArray )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( sourceArray.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = destinationArray.GetLowerBound( 0 ), loopTo = destinationArray.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    destinationArray[i] = new Complex( sourceArray[startingIndex], 0d );
                    startingIndex += 1;
                }
            }
        }

        /// <summary> Copies the real and imaginary values to the complex array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">            The reals. </param>
        /// <param name="imaginaries">      The imaginaries. </param>
        /// <param name="destinationArray"> The destination array. </param>
        public static void Copy( float[] reals, float[] imaginaries, Complex[] destinationArray )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( destinationArray is null )
                throw new ArgumentNullException( nameof( destinationArray ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            if ( reals.Length > 0 && destinationArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), destinationArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), destinationArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    destinationArray[i] = new Complex( reals[i], imaginaries[i] );
            }
        }

        /// <summary> Copies the complex values to a real and imaginary arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="sourceArray"> The source array. </param>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        public static void CopyTo( this Complex[] sourceArray, float[] reals, float[] imaginaries )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            if ( reals.Length > 0 && sourceArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), sourceArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), sourceArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                {
                    reals[i] = ( float ) sourceArray[i].Real;
                    imaginaries[i] = ( float ) sourceArray[i].Imaginary;
                }
            }
        }

        /// <summary> Copies the complex real values to a real array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceArray"> The source array. </param>
        /// <param name="reals">       The reals. </param>
        public static void CopyTo( this Complex[] sourceArray, float[] reals )
        {
            if ( sourceArray is null )
                throw new ArgumentNullException( nameof( sourceArray ) );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( reals.Length > 0 && sourceArray.Length > 0 )
            {
                for ( int i = Math.Max( reals.GetLowerBound( 0 ), sourceArray.GetLowerBound( 0 ) ), loopTo = Math.Min( reals.GetUpperBound( 0 ), sourceArray.GetUpperBound( 0 ) ); i <= loopTo; i++ )
                    reals[i] = ( float ) sourceArray[i].Real;
            }
        }

        #endregion

        #region " CREATE DOUBLE "

        /// <summary> Creates a complex array from the array of real values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals"> The reals. </param>
        /// <returns> A complex array from the array of real values. </returns>
        public static Complex[] ToComplex( this double[] reals )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            var x = new Complex[reals.Length];
            reals.CopyTo( x );
            return x;
        }

        /// <summary>
        /// Converts the real values element to a complex array of the specified length.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">        The reals. </param>
        /// <param name="elementCount"> The element count. </param>
        /// <returns> A complex array of the specified length. </returns>
        public static Complex[] ToComplex( this double[] reals, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( elementCount == 0 )
            {
                return Array.Empty<Complex>();
            }
            else
            {
                var x = new Complex[elementCount];
                reals.CopyTo( x );
                return x;
            }
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">         The reals. </param>
        /// <param name="startingIndex"> Zero-based index of the starting. </param>
        /// <param name="elementCount">  The element count. </param>
        /// <returns> The complex array of the real and imaginary values. </returns>
        public static Complex[] ToComplex( this double[] reals, int startingIndex, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( elementCount == 0 )
            {
                return Array.Empty<Complex>();
            }
            else
            {
                var x = new Complex[elementCount];
                reals.CopyTo( startingIndex, x );
                return x;
            }
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        /// <returns> The complex array of the real and imaginary values. </returns>
        public static Complex[] ToComplex( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            var x = new Complex[reals.Length];
            Copy( reals, imaginaries, x );
            return x;
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. Real and imaginary array
        /// size mismatch. </exception>
        /// <param name="reals">        The reals. </param>
        /// <param name="imaginaries">  The imaginaries. </param>
        /// <param name="elementCount"> The element count. </param>
        /// <returns> The complex array of the real and imaginary values. </returns>
        public static Complex[] ToComplex( double[] reals, double[] imaginaries, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            var x = new Complex[elementCount];
            Copy( reals, imaginaries, x );
            return x;
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">      A Complex() to process. </param>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        public static void ToComplex( this Complex[] values, double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            Copy( reals, imaginaries, values );
        }


        #endregion

        #region " CREATE SINGLE "

        /// <summary> Creates a complex array from the array of real values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals"> The reals. </param>
        /// <returns> A complex array from the array of real values. </returns>
        public static Complex[] ToComplex( this float[] reals )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            var x = new Complex[reals.Length];
            reals.CopyTo( x );
            return x;
        }

        /// <summary>
        /// Converts the real values element to a complex array of the specified length.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">        The reals. </param>
        /// <param name="elementCount"> The element count. </param>
        /// <returns>
        /// The real values element converted to a complex array of the specified length.
        /// </returns>
        public static Complex[] ToComplex( this float[] reals, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( elementCount == 0 )
            {
                return Array.Empty<Complex>();
            }
            else
            {
                var x = new Complex[elementCount];
                reals.CopyTo( x );
                return x;
            }
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reals">         The reals. </param>
        /// <param name="startingIndex"> Zero-based index of the starting. </param>
        /// <param name="elementCount">  The element count. </param>
        /// <returns> The complex array of the real and imaginary values. </returns>
        public static Complex[] ToComplex( this float[] reals, int startingIndex, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( elementCount == 0 )
            {
                return Array.Empty<Complex>();
            }
            else
            {
                var x = new Complex[elementCount];
                reals.CopyTo( startingIndex, x );
                return x;
            }
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        /// <returns> The real and imaginary values to complex. </returns>
        public static Complex[] ToComplex( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            var x = new Complex[reals.Length];
            Copy( reals, imaginaries, x );
            return x;
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">        The reals. </param>
        /// <param name="imaginaries">  The imaginaries. </param>
        /// <param name="elementCount"> The element count. </param>
        /// <returns> The complex array of the real and imaginary values. </returns>
        public static Complex[] ToComplex( float[] reals, float[] imaginaries, int elementCount )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            var x = new Complex[elementCount];
            Copy( reals, imaginaries, x );
            return x;
        }

        /// <summary> Converts the real and imaginary values to complex. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">      A Complex() to process. </param>
        /// <param name="reals">       The reals. </param>
        /// <param name="imaginaries"> The imaginaries. </param>
        public static void ToComplex( this Complex[] values, float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            Copy( reals, imaginaries, values );
        }

        #endregion

    }
}