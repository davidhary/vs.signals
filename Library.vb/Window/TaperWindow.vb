Imports System.Numerics

''' <summary>
''' The base class for the applying taper data windows to signals before calculating the Fourier
''' transform.
''' </summary>
''' <remarks>
''' David, 11/17/2005. See http://en.wikipedia.org/wiki/Window_function <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public MustInherit Class TaperWindow
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TaperWindow" /> class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="windowType"> Type of the window. </param>
    Protected Sub New(ByVal windowType As TaperWindowType)
        MyBase.New()
        Me._WindowType = windowType
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make This method Overridable (virtual) because a derived class should not be able to
    ''' override This method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._Amplitudes = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The amplitudes. </summary>
    Private _Amplitudes() As Double

    ''' <summary> Returns the taper window amplitudes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The amplitudes of the taper window. </returns>
    Public Function Amplitudes() As Double()
        Return Me._Amplitudes
    End Function

    ''' <summary>
    ''' Calculates the taper window amplitude for the provided element. The element number is between
    ''' 0 to N-1.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="element"> The element. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public MustOverride Function Amplitude(ByVal element As Integer) As Double

    ''' <summary> Calculates the taper window amplitudes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Holds the element count of the time series. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Create(ByVal elementCount As Integer) As Double

        If elementCount < 2 Then
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, "Must be greater than 1")
        End If

        ' check if window already created.
        If Me.ElementCount = elementCount Then
            Return Me.Power
        End If

        ' set the size.
        Me.ElementCount = elementCount

        ' allocate the amplitude time series
        ReDim Me._Amplitudes(elementCount - 1)

        ' holds the amplitude of the window
        Dim taperFactor As Double   ' The window amplitude

        Dim first As Integer = Me.Amplitudes.GetLowerBound(0)
        Dim last As Integer = Me.Amplitudes.GetUpperBound(0)

        ' initialize the sum of squares
        Dim ssq As Double = 0.0R

        ' apply the same taper factor amplitude to the first and last indexes.
        Do While first < last

            ' calculate the window factor
            taperFactor = Me.Amplitude(first)

            ' Get the sum of powers
            ssq += taperFactor * taperFactor

            ' Save the amplitude
            Me._Amplitudes(first) = taperFactor
            Me._Amplitudes(last) = taperFactor

            ' Shift indexes
            first += 1
            last -= 1

        Loop

        ' add the two sides
        ssq += ssq

        ' if odd value we need one more calculation
        If (elementCount Mod 2) = 1 Then
            ' calculate the window factor
            taperFactor = Me.Amplitude(first)
            ssq += taperFactor * taperFactor
            Me._Amplitudes(first) = taperFactor
        End If

        ' Get the average power
        Me._Power = ssq / Convert.ToDouble(elementCount)

        Return Me.Power

    End Function

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> Holds the time series amplitude where the modified signal is
    '''                           returned. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Apply(ByVal timeSeries() As Complex) As Double
        If timeSeries IsNot Nothing AndAlso timeSeries.Length > 0 Then
            If Me.ElementCount <> timeSeries.Length Then
                ' if new length, recalculate the window.
                Me.Create(timeSeries.Length)
            End If
            ' apply the window
            timeSeries.ScaleReals(Me.Amplitudes)
            ' return the power 
            Return Me.Power
        Else
            Return 0
        End If
    End Function

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> Holds the time series amplitude where the modified signal is
    '''                           returned. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Apply(ByVal timeSeries() As Double) As Double
        If timeSeries Is Nothing OrElse timeSeries.Length = 0 Then
            Return 0
        End If
        If Me._ElementCount <> timeSeries.Length Then
            ' if new length, recalculate the window.
            Me.Create(timeSeries.Length)
        End If

        ' apply the window
        timeSeries.Scale(Me.Amplitudes)

        ' return the power 
        Return Me.Power

    End Function

    ''' <summary> Applies the taper window. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> Holds the time series amplitude where the modified signal is
    '''                           returned. </param>
    ''' <returns> The taper window power. </returns>
    Public Function Apply(ByVal timeSeries() As Single) As Double
        If timeSeries Is Nothing OrElse timeSeries.Length = 0 Then
            Return 0
        End If
        If Me._ElementCount <> timeSeries.Length Then
            ' if new length, recalculate the window.
            Me.Create(timeSeries.Length)
        End If
        ' apply the window
        timeSeries.Scale(Me.Amplitudes)

        ' return the power 
        Return Me.Power
    End Function

    ''' <summary>
    ''' Gets or sets the base phase.  This value equals 2*Pi/N for cosine-based Windows and 2/N for
    ''' linear (e.g., Parzen or Bartlett) Windows.
    ''' </summary>
    ''' <value> The base phase. </value>
    Protected Property BasePhase() As Double

    ''' <summary>
    ''' Returns the number of elements in the time series sample that was used to compute the taper
    ''' window.
    ''' </summary>
    ''' <value> The length of the time series. </value>
    Public ReadOnly Property TimeSeriesLength() As Integer

    ''' <summary> Number of elements. </summary>
    Private _ElementCount As Integer

    ''' <summary> Gets or sets the local size of the taper window. </summary>
    ''' <value> The number of elements. </value>
    Protected Overridable Property ElementCount() As Integer
        Get
            Return Me._ElementCount
        End Get
        Set(ByVal value As Integer)
            Me._ElementCount = value
            Me._TimeSeriesLength = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the power (average sum of squares of amplitudes) of the taper window.
    ''' </summary>
    ''' <remarks>
    ''' Represents the average amount by which the power of the signal was attenuated due to the
    ''' tapering effect of the taper data window.
    ''' </remarks>
    ''' <value> The power. </value>
    Public ReadOnly Property Power() As Double

    ''' <summary> Gets or sets the window type. </summary>
    ''' <value> The type of the window. </value>
    Public ReadOnly Property WindowType() As TaperWindowType

#End Region

End Class

''' <summary> Applies a Bartlett taper window. </summary>
''' <remarks>
''' The Bartlett window implements a absolute linear window:
'''   A =  1 - | 2 * k / (N-1) |, k=0,1,...N-1. <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class BartlettTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Bartlett)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Gets or sets the length of the window last created. Used by inherited classes to set the base
    ''' phase.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal value As Integer)
            MyBase.ElementCount = value
            MyBase.BasePhase = 2 / Convert.ToDouble(value - 1)
        End Set
    End Property

    ''' <summary> Calculates the Bartlett taper window amplitude for the provided element. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return 1 - Math.Abs(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary> Applies a Blackman-Harris taper window. </summary>
''' <remarks>
''' The Blackman window implements a third order cosine window aimed at reducing side lobes.
''' <para>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class BlackmanHarrisTaperWindow
    Inherits CosineTaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.BlackmanHarris)
        Me.A0 = 0.355768
        Me.A1 = 0.487396
        Me.A2 = 0.144232
        Me.A3 = 0.012604
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

End Class

''' <summary> Applies a Blackman taper window. </summary>
''' <remarks>
''' The Blackman window implements a second order cosine window:
'''   A =  0.42 - 0.5 * cos2a + 0.08 * cos4a where a is the base angle Pi*k/N, k=0,1,...N-1.
'''   <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class BlackmanTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Blackman)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> The Exact Blackman Window coefficients: A0 </summary>
    Private Const _A0 As Double = 7938 / 18608

    ''' <summary> The Exact Blackman Window coefficients: A1 </summary>
    Private Const _A1 As Double = 9240 / 18608

    ''' <summary> The Exact Blackman Window coefficients: A2 </summary>
    Private Const _A2 As Double = 1430 / 18608

    ''' <summary>
    ''' Gets or sets the length of the window last created. Used by inherited classes to set the base
    ''' phase.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal value As Integer)
            MyBase.ElementCount = value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(value - 1)
        End Set
    End Property

    ''' <summary>
    ''' Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Dim cos2a As Double = Math.Cos(MyBase.BasePhase * element)
        Dim cos4a As Double = 2.0R * cos2a * cos2a - 1.0R
        Return _A0 - _A1 * cos2a + _A2 * cos4a

    End Function

#End Region

End Class

''' <summary> Applies a Cosine taper window. </summary>
''' <remarks>
''' The Cosine window implements a generalized Window for the following format:
'''   A =  A0 - A1 * cos2a + A2 * cos4a - A3 * cos6a where a is the base angle Pi*k/N, k=0,1,...N-
'''   1. The default values of the cosine window are set for the Blackman window, i.e., A0 = 0.42,
'''   A1 = 0.5, A2 = 0.08, and A3 = 0. <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class CosineTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TaperWindow" /> class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="windowType"> Type of the window. </param>
    Protected Sub New(ByVal windowType As TaperWindowType)
        MyBase.New(windowType)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="CosineTaperWindow" /> class. </summary>
    ''' <remarks> Initializes the coefficients to using the Exact Blackman Window. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Cosine)
        Me._A0 = 7938 / 18608
        Me._A1 = 9240 / 18608
        Me._A2 = 1430 / 18608
        Me._A3 = 0.0R
    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    ' onDisposeManagedResources

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Gets or sets the zero-order (offset) coefficient of the cosine window. </summary>
    ''' <value> a 0. </value>
    Public Property A0() As Double

    ''' <summary>
    ''' Gets or sets the first-order (cosine(2a)) coefficient of the cosine window.
    ''' </summary>
    ''' <value> a 1. </value>
    Public Property A1() As Double

    ''' <summary>
    ''' Gets or sets the second-order (cosine(4a)) coefficient of the cosine window.
    ''' </summary>
    ''' <value> a 2. </value>
    Public Property A2() As Double

    ''' <summary>
    ''' Gets or sets the third-order (cosine(6a)) coefficient of the cosine window.
    ''' </summary>
    ''' <value> a 3. </value>
    Public Property A3() As Double

    ''' <summary>
    ''' Gets or sets the length of the window last created. Used by inherited classes to set the base
    ''' phase.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal value As Integer)
            MyBase.ElementCount = value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(value - 1)
        End Set
    End Property

    ''' <summary>
    ''' Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Dim cos2a As Double = Math.Cos(MyBase.BasePhase * element)
        Dim cos4a As Double = 2.0R * cos2a * cos2a - 1.0R
        Dim cos6a As Double = cos2a * (2.0R * cos4a - 1.0R)
        Return Me.A0 - Me.A1 * cos2a + Me.A2 * cos4a - Me.A3 * cos6a

    End Function

#End Region

End Class

''' <summary> Applies a Hamming taper window. </summary>
''' <remarks>
''' The Hamming window implements a first order cosine window:
'''   A =  0.54 - 0.46 * cos2a where a is the base angle Pi*k/N, k=0,1,...N-1. <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class HammingTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Hamming)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> The Hamming Window coefficients: A0. </summary>
    Private Const _A0 As Double = 0.54R

    ''' <summary> The Hamming Window coefficients: A1. </summary>
    Private Const _A1 As Double = 0.46R

    ''' <summary>
    ''' Gets or sets the length of the window last created. Used by inherited classes to set the base
    ''' phase.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal value As Integer)
            MyBase.ElementCount = value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(value - 1)
        End Set
    End Property

    ''' <summary>
    ''' Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return _A0 - _A1 * Math.Cos(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary> Applies a Hanning taper window. </summary>
''' <remarks>
''' The Hanning window implements a first order cosine window:
'''   A =  0.5 - 0.5 * cos2a where a is the base angle Pi*k/N, k=0,1,...N-1. <para>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class HanningTaperWindow
    Inherits TaperWindow

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs This class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(TaperWindowType.Hanning)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if This method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> The Hanning Window coefficients: A0. </summary>
    Private Const _A0 As Double = 0.5R

    ''' <summary> The Hanning Window coefficients: A1. </summary>
    Private Const _A1 As Double = 0.5R

    ''' <summary>
    ''' Gets or sets the length of the window last created. Used by inherited classes to set the base
    ''' phase.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Protected Overrides Property ElementCount() As Integer
        Get
            Return MyBase.ElementCount
        End Get
        Set(ByVal value As Integer)
            MyBase.ElementCount = value
            MyBase.BasePhase = 2 * Math.PI / Convert.ToDouble(value - 1)
        End Set
    End Property

    ''' <summary>
    ''' Calculates the Blackman taper window amplitude for the provided element. The phase is
    ''' calculated as 2 * PI * i /(N - 1), i = 0 to N-1.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="element"> The element number (i) for calculating the phase. </param>
    ''' <returns> The amplitude for the provided element. </returns>
    Public Overloads Overrides Function Amplitude(ByVal element As Integer) As Double

        Return _A0 - _A1 * Math.Cos(MyBase.BasePhase * element)

    End Function

#End Region

End Class

''' <summary> Enumerates the taper windows types. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public Enum TaperWindowType

    ''' <summary> An enum constant representing the none option. </summary>
    <System.ComponentModel.Description("None")> None

    ''' <summary> An enum constant representing the bartlett option. </summary>
    <System.ComponentModel.Description("Bartlett")> Bartlett

    ''' <summary> An enum constant representing the blackman option. </summary>
    <System.ComponentModel.Description("Blackman")> Blackman

    ''' <summary> An enum constant representing the cosine option. </summary>
    <System.ComponentModel.Description("Cosine")> Cosine

    ''' <summary> An enum constant representing the hamming option. </summary>
    <System.ComponentModel.Description("Hamming")> Hamming

    ''' <summary> An enum constant representing the hanning option. </summary>
    <System.ComponentModel.Description("Hanning")> Hanning

    ''' <summary> An enum constant representing the blackman harris option. </summary>
    <System.ComponentModel.Description("Blackman-Harris")> BlackmanHarris
End Enum

