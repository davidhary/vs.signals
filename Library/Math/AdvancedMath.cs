﻿


/// <summary> Contains methods that compute advanced functions with real arguments. </summary>
/// <remarks>
/// Trig: internal utility trig functions that are accurate for large arguments Factor: Prime
/// factorization. (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para>
/// </remarks>
using System;

namespace isr.Algorithms.Signals
{
    public sealed partial class AdvancedMath
    {

        /// <summary> Computes the Sine for large arguments. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        /// <returns> The Sine for large arguments. </returns>
        internal static double Sin( double x, double y )
        {
            return Math.Sin( Reduce( x, y ) );
        }

        /// <summary> Computes the Cosine for large arguments. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        /// <returns> The Cosine for large arguments. </returns>
        internal static double Cos( double x, double y )
        {
            return Math.Cos( Reduce( x, y ) );
        }

        /// <summary> The maximum value. </summary>
        private static readonly double MaxValue = Convert.ToDouble( decimal.MaxValue );

        /// <summary> The two pi. </summary>
        public const decimal TwoPI = 2m * 3.1415926535897932384626433833m;

        /// <summary> Reduces an argument to its corresponding argument between -2Pi and 2Pi. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        /// <returns> The reduced argument to its corresponding argument between -2Pi and 2Pi. </returns>
        internal static double Reduce( double x, double y )
        {
            double t = x + ( double ) TwoPI * y;
            if ( Math.Abs( t ) < 64.0d || Math.Abs( t ) > MaxValue )
            {
                // if the argument is small we don't need the high accuracy reduction
                // if the argument is too big, we can't do the high accuracy reduction because it would overflow a decimal variable
                return t;
            }
            else
            {
                // otherwise, convert to decimal, subtract a multiple of 2 Pi, and return

                // reduce x by factors of 2 Pi
                decimal dx = Convert.ToDecimal( x );
                decimal dn = decimal.Truncate( dx / TwoPI );
                dx -= dn * TwoPI;

                // reduce y by factors of 1
                decimal dy = Convert.ToDecimal( y );
                decimal dm = decimal.Truncate( dy / 1m );
                dy -= dm * 1m;

                // form the argument
                decimal dt = dx + dy * TwoPI;
                return Convert.ToDouble( dt );
            }
        }
    }
}