﻿
using System;

namespace isr.Algorithms.Signals
{
    public partial class WisdomNativeTest
    {

        /// <summary>
        /// pointers to unmanaged arrays
        /// </summary>
        private IntPtr _Pin, _Pout;

        /// <summary>
        /// pointers to the FFTW plan objects
        /// </summary>
        private IntPtr _Fplan1, _Fplan2, _Fplan3;
    }
}