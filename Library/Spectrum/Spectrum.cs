using System;
using System.Numerics;

namespace isr.Algorithms.Signals
{

    /// <summary>
    /// Spectrum base class for calculating the spectrum using the Fourier Transform.  Includes pre-
    /// processing fro removing mean and applying a taper data window. Includes post-processing
    /// methods for high- pass and low-pass filtering in the frequency domain.
    /// </summary>
    /// <remarks>
    /// (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Spectrum : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs This class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fourierTransform"> Holds reference to the Fourier Transform class to use for
        /// calculating the spectrum. </param>
        // instantiate the base class
        public Spectrum( FourierTransformBase fourierTransform ) : base()
        {
            // set reference to the Fourier Transform class
            this.FourierTransform = fourierTransform;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make This method Overridable (virtual) because a derived class should not be able to
        /// override This method.
        /// </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.
            // this disposes all child classes.
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if This method releases both managed and unmanaged resources;
        /// False if This method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // Free managed resources when explicitly called
                        // remove the reference to the external objects.
                    }
                    // Free shared unmanaged resources
                    this.FourierTransform = null;
                    this.TaperWindow = null;
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " COMMON MEMBERS "

        /// <summary> Gets reference to the Fourier Transform used for calculating the spectrum. </summary>
        /// <value> The Fourier transform. </value>
        protected FourierTransformBase FourierTransform { get; private set; }

        /// <summary> The number of elements in half the spectrum including the DC point. </summary>
        /// <value> The length of the half spectrum. </value>
        public int HalfSpectrumLength => this.FourierTransform.HalfSpectrumLength;

        /// <summary> Gets the length of the full spectrum. </summary>
        /// <value> The length of the full spectrum. </value>
        public int FullSpectrumLength => this.FourierTransform.FullSpectrumLength;

        /// <summary>
        /// Gets or Sets the condition as True scale the Spectrum.  It is useful to set This to false
        /// whenever averaging transforms so that the scaling is done at the end.
        /// </summary>
        /// <value> The is scale FFT. </value>
        public bool IsScaleFft { get; set; }

        /// <summary>
        /// Gets or Sets the condition as True remove the mean before computing the Spectrum.
        /// </summary>
        /// <value> The is remove mean. </value>
        public bool IsRemoveMean { get; set; }

        /// <summary>
        /// Returns the power (average sum of squares of amplitudes) of the taper window.
        /// </summary>
        /// <remarks>
        /// Represents the average amount by which the power of the signal was attenuated due to the
        /// tapering effect of the taper data window.
        /// </remarks>
        /// <value> The taper window power. </value>
        public double TaperWindowPower => this.TaperWindow is null ? 1.0d : this.TaperWindow.Power;

        /// <summary>
        /// Gets or sets the number of elements in the time series sample that is processed to compute
        /// the Spectrum.
        /// </summary>
        /// <remarks>
        /// This must be set before setting the filter frequencies. The size of the Fourier transform is
        /// set when calling the Fourier Transform. That call is used to update the time series length.
        /// This value is allowed to be set independently in case the spectrum is read from a file.
        /// </remarks>
        /// <value> The length of the time series. </value>
        public int TimeSeriesLength
        {
            get => this.FourierTransform.TimeSeriesLength;

            set => this.FourierTransform.TimeSeriesLength = value;
        }

        /// <summary> Populates the spectrum frequency array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="halfSpectrum"> True to half spectrum. </param>
        /// <returns> A Double() </returns>
        public double[] PopulateFrequencies( bool halfSpectrum )
        {
            double frequencyResolution = this.SamplingRate / this.FullSpectrumLength;
            return halfSpectrum ? Signal.Frequencies( frequencyResolution, this.HalfSpectrumLength ) : Signal.Frequencies( frequencyResolution, this.FullSpectrumLength );
        }

        /// <summary>
        /// Gets the sampling rate for use when setting the <see cref="TaperFilter"/> properties. Must be
        /// set before setting the filter frequencies.
        /// </summary>
        /// <value> The sampling rate. </value>
        public double SamplingRate { get; set; }

        /// <summary> Gets the frequency resolution. </summary>
        /// <value> The frequency resolution. </value>
        public double FrequencyResolution => this.SamplingRate / this.FullSpectrumLength;

        /// <summary>
        /// Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperFilter">taper
        /// Filter</see>.
        /// </summary>
        /// <value> The taper filter. </value>
        public TaperFilter TaperFilter { get; set; }

        /// <summary>
        /// Gets or sets reference to the <see cref="isr.Algorithms.Signals.TaperWindow">taper
        /// window</see>
        /// used for reducing side lobes.
        /// </summary>
        /// <value> The taper window. </value>
        public TaperWindow TaperWindow { get; set; }

        /// <summary>
        /// Returns the FFT scale factor taking into account the effect of the taper window.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The FFT scale factor taking into account the effect of the taper window. </returns>
        public double FftScaleFactor()
        {
            return 1.0d / (Math.Sqrt( this.TaperWindowPower ) * this.TimeSeriesLength);
        }

        #endregion

        #region " COMPLEX "

        /// <summary> Initializes the spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="timeSeries"> The time series. </param>
        public virtual void Initialize( Complex[] timeSeries )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            if ( timeSeries.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( timeSeries ), "Time series must be longer than 1" );
            // initializes the time series length
            this.TimeSeriesLength = timeSeries.Length;
            // Create the taper window
            if ( this.TaperWindow is object )
                _ = this.TaperWindow.Create( timeSeries.Length );
        }

        /// <summary> Applies the filter described by values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The input time series where the spectrum is returned. </param>
        public void ApplyFilter( Complex[] values )
        {
            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {
                this.TaperFilter.Taper( values, this.SamplingRate );
            }
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void ApplyTaperWindow( Complex[] timeSeries )
        {
            if ( this.TaperWindow is object )
            {
                _ = this.TaperWindow.Apply( timeSeries );
            }
        }

        /// <summary> Removes the mean. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void RemoveMean( Complex[] timeSeries )
        {
            if ( this.IsRemoveMean )
            {
                timeSeries.RemoveRealMean();
            }
        }

        /// <summary>
        /// Scales the specified spectrum if <see cref="IsScaleFft">option</see> is set.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="spectrum"> The spectrum. </param>
        public void ScaleFft( Complex[] spectrum )
        {
            // Scale the Spectrum if the scale option is set
            if ( spectrum is object && this.IsScaleFft )
                spectrum.Scale( this.FftScaleFactor() );
        }

        /// <summary>
        /// Calculates the spectrum using the Fourier Transform with pre- and post-processing.
        /// </summary>
        /// <remarks>
        /// Applies filter, removes mean, calculates the spectrum and appends to the previous spectrum.
        /// </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The input time series where the spectrum is returned. </param>
        public virtual void Calculate( Complex[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( values ), "Time series must have more than 1 value" );
            }

            // Remove the mean if the mean option is set
            this.RemoveMean( values );

            // Apply the taper window
            this.ApplyTaperWindow( values );

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( values );

            // scale if required
            this.ScaleFft( values );

            // apply the filter as necessary
            this.ApplyFilter( values );
        }

        /// <summary>
        /// Filters the real valued signal using low pass and high pass taper frequency windows.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="timeSeries"> The time series. </param>
        public void Filter( Complex[] timeSeries )
        {
            if ( timeSeries is null )
                throw new ArgumentNullException( nameof( timeSeries ) );
            if ( timeSeries.Length < 2 )
            {
                throw new ArgumentOutOfRangeException( nameof( timeSeries ), "Time series must be longer than 1" );
            }

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( timeSeries );

            // Scale the Spectrum.
            timeSeries.Scale( 1.0d / timeSeries.Length );

            // apply the filter as necessary
            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {
                this.ApplyFilter( timeSeries );
            }

            // inverse transform
            this.FourierTransform.Inverse( timeSeries );
        }

        #endregion

        #region " DOUBLE "

        /// <summary> Initializes the spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void Initialize( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );

            // Create the taper window
            if ( this.TaperWindow is object )
            {
                _ = this.TaperWindow.Create( reals.Length );
            }

            // apply the transform
            this.FourierTransform.Forward( reals, imaginaries );
        }

        /// <summary> Applies the filter to the spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void ApplyFilter( double[] reals, double[] imaginaries )
        {
            if ( this.TaperFilter is null )
                throw new InvalidOperationException( "Taper filter not defined" );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            _ = this.TaperFilter.Taper( this.SamplingRate, reals, imaginaries );
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void ApplyTaperWindow( double[] timeSeries )
        {
            if ( this.TaperWindow is object )
            {
                _ = this.TaperWindow.Apply( timeSeries );
            }
        }

        /// <summary> Removes the mean. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void RemoveMean( double[] timeSeries )
        {
            if ( this.IsRemoveMean )
            {
                timeSeries.RemoveMean();
            }
        }

        /// <summary>
        /// Calculates the spectrum using the Fourier Transform with pre and post processing.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        /// <example> Sub Form_Click End Sub </example>
        public void Calculate( double[] reals, double[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            this.RemoveMean( reals );
            this.ApplyTaperWindow( reals );

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( reals, imaginaries );

            // Check if the scale option is set
            if ( this.IsScaleFft )
            {

                // Set the scale factor
                double scaleFactor = 1.0d / (Math.Sqrt( this.TaperWindowPower ) * reals.Length);

                // Scale the spectrum.
                reals.Scale( scaleFactor );
                imaginaries.Scale( scaleFactor );
            }

            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {

                // apply the filter as necessary
                this.ApplyFilter( reals, imaginaries );
            }
        }

        /// <summary>
        /// Filters the real valued signal using low pass and high pass taper frequency windows.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> Holds the real values. </param>
        /// <example> Sub Form_Click End Sub </example>
        public void Filter( double[] reals )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );

            // Initialize imaginary array.
            var imaginaries = new double[reals.Length];

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( reals, imaginaries );

            // Set the scale factor
            double scaleFactor = 1.0d / reals.Length;

            // Scale the Spectrum.
            reals.Scale( scaleFactor );
            imaginaries.Scale( scaleFactor );
            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {

                // apply the filter as necessary
                this.ApplyFilter( reals, imaginaries );
            }

            // inverse transform
            this.FourierTransform.Inverse( reals, imaginaries );
        }


        #endregion

        #region " SINGLE "

        /// <summary> Initializes the spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void Initialize( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            // Create the taper window
            if ( this.TaperWindow is object )
                _ = this.TaperWindow.Create( reals.Length );
            // apply the transform
            this.FourierTransform.Forward( reals, imaginaries );
        }

        /// <summary> Applies the filter to the spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void ApplyFilter( float[] reals, float[] imaginaries )
        {
            if ( this.TaperFilter is null )
                throw new InvalidOperationException( "Taper filter not defined" );
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            _ = this.TaperFilter.Taper( this.SamplingRate, reals, imaginaries );
        }

        /// <summary> Applies the taper window. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void ApplyTaperWindow( float[] timeSeries )
        {
            if ( this.TaperWindow is object )
            {
                _ = this.TaperWindow.Apply( timeSeries );
            }
        }

        /// <summary> Removes the mean. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        public void RemoveMean( float[] timeSeries )
        {
            if ( this.IsRemoveMean )
            {
                timeSeries.RemoveMean();
            }
        }

        /// <summary>
        /// Calculates the spectrum using the Fourier Transform with pre and post processing.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals">       Holds the real values. </param>
        /// <param name="imaginaries"> Holds the imaginary values. </param>
        public void Calculate( float[] reals, float[] imaginaries )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( imaginaries is null )
                throw new ArgumentNullException( nameof( imaginaries ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );
            if ( reals.Length != imaginaries.Length )
                throw new ArgumentOutOfRangeException( nameof( reals ), reals, $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal" );
            this.RemoveMean( reals );
            this.ApplyTaperWindow( reals );

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( reals, imaginaries );

            // Check if the scale option is set
            if ( this.IsScaleFft )
            {

                // Set the scale factor
                float scaleFactor = ( float ) (1.0d / (Math.Sqrt( this.TaperWindowPower ) * reals.Length));

                // Scale the Spectrum.
                reals.Scale( scaleFactor );
                imaginaries.Scale( scaleFactor );
            }

            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {

                // apply the filter as necessary
                this.ApplyFilter( reals, imaginaries );
            }
        }

        /// <summary>
        /// Filters the real valued signal using low pass and high pass taper frequency windows.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="reals"> Holds the real values. </param>
        /// <example> Sub Form_Click End Sub </example>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        public void Filter( float[] reals )
        {
            if ( reals is null )
                throw new ArgumentNullException( nameof( reals ) );
            if ( reals.Length < 2 )
                throw new ArgumentOutOfRangeException( nameof( reals ), "Array must be longer than 1" );

            // Initialize imaginary array.
            var imaginaries = Array.Empty<float>();
            imaginaries = new float[reals.Length];

            // Calculate the forward Fourier transform  
            this.FourierTransform.Forward( reals, imaginaries );

            // Set the scale factor
            float scaleFactor = ( float ) (1.0d / reals.Length);

            // Scale the Spectrum.
            reals.Scale( scaleFactor );
            imaginaries.Scale( scaleFactor );
            if ( this.TaperFilter is object && this.TaperFilter.FilterType != TaperFilterType.None )
            {

                // apply the filter as necessary
                this.ApplyFilter( reals, imaginaries );
            }

            // inverse transform
            this.FourierTransform.Inverse( reals, imaginaries );
        }

        #endregion

    }
}
