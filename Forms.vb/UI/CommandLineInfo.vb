﻿Imports isr.Algorithms.Signals.ExceptionExtensions

''' <summary>
''' A sealed class the parses the command line and provides the command line values.
''' </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 02/02/2011, x.x.4050.x. </para>
''' </remarks>
Public NotInheritable Class CommandLineInfo

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

    #End Region

    #Region " PARSER "

    ''' <summary> Gets the no-operation command line option. </summary>
    ''' <value> The no operation option. </value>
    Public Shared ReadOnly Property NoOperationOption As String
        Get
            Return "/nop"
        End Get
    End Property

    ''' <summary> Gets the Device-Enabled option. </summary>
    ''' <value> The Device enabled option. </value>
    Public Shared ReadOnly Property DevicesEnabledOption As String
        Get
            Return "/d:"
        End Get
    End Property

    ''' <summary> Gets the view option. </summary>
    ''' <value> The view option. </value>
    Public Shared ReadOnly Property ViewOption As String
        Get
            Return "/vu:"
        End Get
    End Property

    ''' <summary> Validate the command line. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentException"> This exception is raised if a command line argument is
    '''                                      not handled. </exception>
    ''' <param name="commandLineArguments"> The command line arguments. </param>
    Public Shared Sub ValidateCommandLine(ByVal commandLineArguments As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArguments IsNot Nothing Then
            For Each argument As String In commandLineArguments
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.ViewOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    Throw New ArgumentException($"Unknown command line argument '{argument}' was detected. Should be Ignored.", NameOf(commandLineArguments))
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As String)
        If Not String.IsNullOrWhiteSpace(commandLineArgs) Then ParseCommandLine(New ObjectModel.ReadOnlyCollection(Of String)(commandLineArgs.Split(" "c)))
    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        Dim commandLineBuilder As New System.Text.StringBuilder
        If commandLineArgs IsNot Nothing Then
            For Each argument As String In commandLineArgs
                commandLineBuilder.AppendFormat("{0} ", argument)
                If argument.StartsWith(NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    Nop = True
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.DevicesEnabled = argument.Substring(CommandLineInfo.ViewOption.Length).StartsWith("n", StringComparison.OrdinalIgnoreCase)
                ElseIf argument.StartsWith(CommandLineInfo.ViewOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.UserInterfaceName = argument.Substring(CommandLineInfo.ViewOption.Length)
                Else
                    ' do nothing
                End If
            Next
        End If
        _CommandLine = commandLineBuilder.ToString

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    ''' <param name="e">               Action event information. </param>
    ''' <returns> <c>True</c> if success or false if Exception occurred. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String), ByVal e As isr.Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        If commandLineArgs Is Nothing OrElse Not commandLineArgs.Any Then
            CommandLineInfo._CommandLine = String.Empty
        Else
            Try
                CommandLineInfo._CommandLine = String.Join(",", commandLineArgs)
                activity = $"Parsing the commandLine {CommandLineInfo.CommandLine}"
                ' Data.ParseCommandLine(commandLineArgs)
                CommandLineInfo.ParseCommandLine(commandLineArgs)
            Catch ex As ArgumentException
                e.RegisterOutcomeEvent(TraceEventType.Information, $"Unknown argument value for '{ex.ParamName}' ignored")
            Catch ex As System.Exception
                e.RegisterError($"Failed {activity};. {ex.ToFullBlownString}")
            End Try
        End If
        Return Not e.Failed
    End Function

    #End Region

    #Region " COMMAND LINE ELEMENTS "

    ''' <summary> Gets or sets the command line. </summary>
    ''' <value> The command line. </value>
    Public Shared ReadOnly Property CommandLine As String

    ''' <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
    ''' <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
    Public Shared Property DevicesEnabled() As Boolean?

    ''' <summary> Gets or sets the no operation option. </summary>
    ''' <value> The no-op. </value>
    Public Shared Property Nop As Boolean

    ''' <summary> Name of the user interface. </summary>
    Private Shared _UserInterfaceName As String = String.Empty

    ''' <summary> Gets or sets the name of the user interface. </summary>
    ''' <value> The name of the user interface. </value>
    Public Shared Property UserInterfaceName As String
        Get
            Return CommandLineInfo._UserInterfaceName
        End Get
        Set(value As String)
            CommandLineInfo._UserInterfaceName = value
            Dim ui As UserInterface
            If Not [Enum].TryParse(Of UserInterface)(value, True, ui) Then
                ui = isr.Algorithms.Signals.UserInterface.Unknown
            End If
            CommandLineInfo.UserInterface = ui
        End Set
    End Property

    ''' <summary> Gets or sets the virtual instrument. </summary>
    ''' <value> The virtual instrument. </value>
    Public Shared Property UserInterface As UserInterface?

    #End Region

End Class

''' <summary> Values that represent virtual instruments. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public Enum UserInterface

    ''' <summary> An enum constant representing the unknown option. </summary>
    <System.ComponentModel.Description("Unknown")> Unknown

    ''' <summary> An enum constant representing the spectrum option. </summary>
    <System.ComponentModel.Description("Spectrum")> Spectrum

    ''' <summary> An enum constant representing the spectrum Milliseconds option. </summary>
    <System.ComponentModel.Description("Spectrum Chart")> SpectrumMs

    ''' <summary> An enum constant representing the wisdom option. </summary>
    <System.ComponentModel.Description("Wisdom")> Wisdom

    ''' <summary> An enum constant representing the wisdom Milliseconds option. </summary>
    <System.ComponentModel.Description("Wisdom Chart")> WisdomMs
End Enum
