Imports System.Numerics

''' <summary> An engine for performing Fourier transforms on complex series. </summary>
''' <remarks>
''' <para>A Fourier transform decomposes a function into a sum of different frequency components.
''' This is useful for a wide array of applications.</para>
''' <para>Mathematically, the DFT is an N-dimensional linear transformation
''' with coefficients that are the Nth complex roots of unity.</para>
''' <img src="../images/Fourier.png" />
''' <para>An instance of the FourierTransformer class performs DFTs on series of a particular
''' length, given by its <see cref="FourierTransformer.Length" /> property. This specialization
''' allows certain parts of the DFT calculation, which are independent of the transformed series
''' but dependent on the length of the series, to be performed only once and then re-used for all
''' transforms of that length. This saves time and improves performance. If you need to perform
''' DFTs on series with different lengths, simply create a separate instance of the
''' FourierTransform class for each required length.</para>
''' <para>Many simple DFT implementations require that the series length be a power of two (2, 4,
''' 8, 16, etc.). Meta.Numerics supports DFTs of any length. Our DFT implementation is fast --
''' order O(N log N) -- for all lengths, including lengths that have large prime factors.</para>
''' &lt;para&gt;
''' </remarks>
''' <seealso href="http://en.wikipedia.org/wiki/Discrete-time_Fourier_transform"/>
Public NotInheritable Class FourierTransformer

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the Fourier transformer. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="size"> The series length of the transformer, which must be positive. </param>
    Public Sub New(ByVal size As Integer)
        Me.New(size, FourierSign.Negative, FourierNormalization.None)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the Fourier transformer with the given sign and normalization
    ''' conventions.
    ''' </summary>
    ''' <remarks>
    ''' <para>There are multiple conventions for both the sign of the exponent and the overall
    ''' normalization of Fourier transforms. The default conventions for some widely used software
    ''' packages are summarized in the following table.</para>
    ''' <table>
    ''' <tr><th>Software</th><th>Sign</th><th>Normalization</th></tr>
    ''' <tr><td>Meta.Numerics</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    ''' <tr><td>Matlab</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    ''' <tr><td>Mathematica</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.Unitary"/></td></tr>
    ''' <tr><td>Numerical
    ''' Recipes</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    ''' </table>
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="size">                    The series length of the transformer, which must be
    '''                                        positive. </param>
    ''' <param name="signConvention">          The sign convention of the transformer. </param>
    ''' <param name="normalizationConvention"> The normalization convention of the transformer. </param>
    Public Sub New(ByVal size As Integer, ByVal signConvention As FourierSign, ByVal normalizationConvention As FourierNormalization)

        If size < 1 Then Throw New ArgumentOutOfRangeException(NameOf(size))

        Me._Length = size
        Me._SignConvention = signConvention
        Me._NormalizationConvention = normalizationConvention

        ' pre-compute the Nth complex roots of unity
        Me._Roots = FourierAlgorithms.ComputeRoots(size, +1)

        ' decompose the size into prime factors
        Me._Factors = AdvancedMath.Factor(size)

        ' store a plan for the transform based on the prime factorization
        Me._Plan = New Generic.List(Of Transformlet)()
        For Each factor As Factor In Me.Factors

            Dim t As Transformlet
            Select Case factor.Value
                ' use a radix-specialized Transformlet when available
                Case 2
                    t = New RadixTwoTransformlet(size, Me.Roots)
                Case 3
                    t = New RadixThreeTransformlet(size, Me.Roots)
                    ' eventually, we should make an optimized radix-4 transform
                Case 5
                    t = New RadixFiveTransformlet(size, Me.Roots)
                Case 7
                    t = New RadixSevenTransformlet(size, Me.Roots)
                Case 11, 13
                    ' the base Transformlet is R^2, but when R is small, this can still be faster than the Bluestein algorithm
                    ' timing measurements appear to indicate that this is the case for radix 11 and 13
                    ' eventually, we should make optimized Winograd transformlets for these factors
                    t = New Transformlet(factor.Value, size, Me.Roots)
                Case Else
                    ' for large factors with no available specialized Transformlet, use the Bluestein algorithm
                    t = New BluesteinTransformlet(factor.Value, size, Me.Roots)
            End Select

            t.Multiplicity = factor.Multiplicity

            ' if ((factor.Value == 2) && (factor.Multiplicity % 2 == 0)) {
            '     t = new RadixFourTransformlet(size, roots);
            '     t.Multiplicity = factor.Multiplicity / 2;}

            Me.Plan.Add(t)

        Next factor

    End Sub

#End Region

#Region "MEMBRS "

    ''' <summary> The factors. </summary>
    ''' <value> The factors. </value>
    Private ReadOnly Property Factors As Generic.List(Of Factor)

    ''' <summary> The plan. </summary>
    ''' <value> The plan. </value>
    Private ReadOnly Property Plan As Generic.List(Of Transformlet)

    ''' <summary> The roots. </summary>
    ''' <value> The roots. </value>
    Private ReadOnly Property Roots As Complex()

    ''' <summary> The series length for which the transformer is specialized. </summary>
    ''' <value> The length. </value>
    Public ReadOnly Property Length As Integer

    ''' <summary> Gets or sets the normalization convention used by the transformer. </summary>
    ''' <value> The normalization convention. </value>
    Public ReadOnly Property NormalizationConvention() As FourierNormalization

    ''' <summary> Gets or sets the normalization convention used by the transformer. </summary>
    ''' <value> The sign convention. </value>
    Public ReadOnly Property SignConvention() As FourierSign

    ''' <summary> Gets the sign. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The sign. </returns>
    Private Function GetSign() As Integer
        Return If(Me.SignConvention = FourierSign.Positive, +1, -1)
    End Function

#End Region

#Region "COMPLEX "

    ''' <summary> Normalizes the specified x. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The x. </param>
    ''' <param name="f"> The f. </param>
    Private Shared Sub Normalize(ByVal x() As Complex, ByVal f As Double)
        For i As Integer = 0 To x.Length - 1
            x(i) = New Complex(f * x(i).Real, f * x(i).Imaginary)
        Next i
    End Sub

    ''' <summary> Transforms the specified x. </summary>
    ''' <remarks>
    ''' This is an internal transform method that does not do checking, modifies the input array, and
    ''' requires you to give it a scratch array. x is the input array, which is overwritten by the
    ''' output array, and y is a scratch array of the same length. The transform works by carrying
    ''' out each Transformlet in the plan, with input from x and output to y, then switching y and x
    ''' so that the input is in x for the next Transformlet.
    ''' </remarks>
    ''' <param name="x">    [in,out] The x. </param>
    ''' <param name="y">    [in,out] The y. </param>
    ''' <param name="sign"> The sign. </param>
    Friend Sub Transform(ByRef x() As Complex, ByRef y() As Complex, ByVal sign As Integer)
        Dim ns As Integer = 1
        For Each t As Transformlet In Me._Plan
            For k As Integer = 0 To t.Multiplicity - 1
                t.FftPass(x, y, ns, sign)
                ' we avoid element-by-element copying by just switching the arrays referenced by x and y
                ' this is why x and y must be passed in with the ref keyword
                Dim temp() As Complex = x
                x = y
                y = temp
                ns *= t.Radix
            Next k
        Next t
    End Sub

    ''' <summary> Computes the Fourier transform of the given series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The series to transform. </param>
    ''' <returns> The discrete Fourier transform of the series. </returns>
    Public Function Transform(ByVal values As Generic.IList(Of Complex)) As Complex()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count <> Me._Length Then Throw New ArgumentOutOfRangeException(NameOf(values), $"Data Size {values.Count} does not match expected size of {Me.Length}")

        ' copy the original values into a new array
        Dim x(Me.Length - 1) As Complex
        values.CopyTo(x, 0)

        ' normalize the copy appropriately
        If Me.NormalizationConvention = FourierNormalization.Unitary Then
            FourierTransformer.Normalize(x, 1.0 / Math.Sqrt(Me._Length))
        ElseIf Me.NormalizationConvention = FourierNormalization.Inverse Then
            FourierTransformer.Normalize(x, 1.0 / Me._Length)
        End If

        ' create a scratch array
        Dim y(Me.Length - 1) As Complex

        ' do the FFT
        Me.Transform(x, y, Me.GetSign())

        Return x

    End Function

    ''' <summary> Computes the Fourier transform of the given series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The series to transform. </param>
    ''' <returns> The discrete Fourier transform of the series. </returns>
    Public Function Transform(ByVal values As Complex()) As Complex()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Length <> Me._Length Then Throw New ArgumentOutOfRangeException(NameOf(values), $"Data Size {values.Length} does not match expected size of {Me.Length}")

        ' normalize the copy appropriately
        If Me._NormalizationConvention = FourierNormalization.Unitary Then
            FourierTransformer.Normalize(values, 1.0 / Math.Sqrt(Me._Length))
        ElseIf Me._NormalizationConvention = FourierNormalization.Inverse Then
            FourierTransformer.Normalize(values, 1.0 / Me._Length)
        End If

        ' create a scratch array
        Dim y(Me.Length - 1) As Complex

        ' do the FFT
        Me.Transform(values, y, Me.GetSign())

        Return values

    End Function

    ''' <summary> Computes the inverse Fourier transform of the given series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The series to invert. </param>
    ''' <returns> The inverse discrete Fourier transform of the series. </returns>
    Public Function InverseTransform(ByVal values As Complex()) As Complex()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Length <> Me._Length Then Throw New ArgumentOutOfRangeException(NameOf(values), $"Data Size {values.Length} does not match expected size of {Me.Length}")

        ' normalize the copy appropriately
        If Me._NormalizationConvention = FourierNormalization.None Then
            Normalize(values, 1.0 / Me._Length)
        ElseIf Me._NormalizationConvention = FourierNormalization.Unitary Then
            Normalize(values, 1.0 / Math.Sqrt(Me._Length))
        End If

        ' create a scratch array
        Dim y(Me._Length - 1) As Complex

        ' do the FFT
        Me.Transform(values, y, -Me.GetSign())

        Return values

    End Function

#End Region

#Region " DOUBLE "

    ''' <summary> Computes the Fourier transform of the given series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    ''' <returns> The discrete Fourier transform of the series. </returns>
    Public Function Transform(ByVal reals As Double(), imaginaries As Double()) As Complex()
        Return Me.Transform(ComplexArrays.ToComplex(reals, imaginaries))
    End Function

#End Region

#Region " SINGLE "

    ''' <summary> Computes the Fourier transform of the given series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    ''' <returns> The discrete Fourier transform of the series. </returns>
    Public Function Transform(ByVal reals As Single(), imaginaries As Single()) As Complex()
        Return Me.Transform(ComplexArrays.ToComplex(reals, imaginaries))
    End Function

#End Region

End Class

''' <summary>
''' Specifies the normalization convention to be used in a forward Fourier transform.
''' </summary>
''' <remarks>
''' <para>The most common convention in signal processing applications is
''' <see cref="FourierNormalization.None"/>.</para>
''' </remarks>
Public Enum FourierNormalization

    ''' <summary> The series is not normalized. </summary>
    None

    ''' <summary> The series is multiplied by 1/N<sup>1/2</sup>. </summary>
    Unitary

    ''' <summary> The series is multiplied by 1/N. </summary>
    Inverse
End Enum

''' <summary>
''' Specifies the sign convention to be used in the exponent of a forward Fourier transform.
''' </summary>
''' <remarks>
''' <para>The most common convention in signal processing applications is
''' <see cref="FourierSign.Negative"/>.</para>
''' </remarks>
Public Enum FourierSign

    ''' <summary> The exponent has positive imaginary values. </summary>
    Positive

    ''' <summary> The exponent has negative imaginary values. </summary>
    Negative
End Enum

