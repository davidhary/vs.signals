Namespace Wisdom

    ''' <summary>Calculates the spectrum using the Fourier Transform.  Includes 
    '''   pre-processing fro removing mean and applying a taper data window. Includes
    '''   post-processing methods for high-pass and low-pass filtering in the frequency
    '''   domain.</summary>
    ''' <remarks>
    ''' (c) 1998 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
    ''' David, 06/03/98, 1.0.00. Created <para>
    ''' David, 09/27/05, 1.0.2096. Create based on FFT pro </para></remarks>
    Public Class Spectrum
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>Constructs This class.</summary>
        ''' <param name="FourierTransform">Holds reference to the Fourier Transform class to use for 
        '''   calculating the spectrum.</param>
        Public Sub New(ByVal fourierTransform As FourierTransformBase)

            ' instantiate the base class
            MyBase.New()

            ' set reference to the Fourier Transform class
            _FourierTransform = fourierTransform

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make This method overridable (virtual) because a derived 
        '''   class should not be able to override This method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _Disposed As Boolean

        ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
        Protected Property Disposed() As Boolean
            Get
                Return Me._Disposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._Disposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if This method releases both managed and unmanaged 
        '''   resources; False if This method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                        ' remove the reference to the external objects.
                        Me._FourierTransform = Nothing
                        Me._TaperWindow = Nothing

                    End If

                    ' Free shared unmanaged resources

                End If
            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.Disposed = True

            End Try

        End Sub

#End Region

#Region " COMMON "

        ''' <summary>Gets reference to the Fourier Transform used for calculating the spectrum.
        ''' </summary>
        Protected ReadOnly Property FourierTransform() As FourierTransformBase

        ''' <summary>Gets the size of the half spectrum including the DC point.</summary>
        Public ReadOnly Property HalfSpectrumLength() As Integer
            Get
                Return _fourierTransform.HalfSpectrumLength
            End Get
        End Property

        ''' <summary>Gets or Sets the condition as True scale the Spectrum.  It is useful to set
        '''   This to false whenever averaging transforms so that the scaling
        '''   is done at the end.</summary>
        Public Property IsScaleFft() As Boolean

        ''' <summary>Gets or Sets the condition as True remove the mean before computing the Spectrum.
        ''' </summary>
        Public Property IsRemoveMean() As Boolean

        ''' <summary>Returns the power (average sum of squares of amplitudes) of the 
        '''   taper window.</summary>
        ''' <remarks>Represents the average amount by which the power of the signal was 
        '''   attenuated due to the tapering effect of the taper data window.
        ''' </remarks>
        Public ReadOnly Property TaperWindowPower() As Double
            Get
                If Me.TaperWindow Is Nothing Then
                    Return 1.0R
                Else
                    Return Me.TaperWindow.Power()
                End If
            End Get
        End Property

        ''' <summary> Gets or sets the number of elements in the time series sample that is processed to
        ''' compute the Spectrum. </summary>
        ''' <remarks> This must be set before setting the filter frequencies. The size of the Fourier
        ''' transform is set when calling the Fourier Transform. That call is used to update the time
        ''' series length. This value is allowed to be set independently in case the spectrum is read
        ''' from a file. </remarks>
        ''' <value> The length of the time series. </value>
        Public Property TimeSeriesLength() As Integer
            Get
                Return Me.FourierTransform.TimeSeriesLength
            End Get
            Set(value As Integer)
                Me.FourierTransform.TimeSeriesLength = value
            End Set
        End Property

        ''' <summary>Gets or sets the low pass frequency.  Must be set before setting the 
        '''   filter frequencies.
        ''' </summary>
        Public Property SamplingRate() As Double

        ''' <summary>Gets or sets reference to the <see cref="TaperFilter">taper Filter</see>.</summary>
        Public Property TaperFilter() As TaperFilter

        ''' <summary>Gets or sets reference to the <see cref="TaperWindow">taper window</see>
        '''   used for reducing side lobes.</summary>
        Public Property TaperWindow() As TaperWindow

#End Region

#Region " DOUBLE "

        ''' <summary>Initializes the spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        ''' </example>
        Public Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")

            ' Create the taper window
            If Me.TaperWindow IsNot Nothing Then
                Me.TaperWindow.Create(reals.Length)
            End If

            ' apply the transform
            Me.FourierTransform.Forward(reals, imaginaries)

        End Sub

        ''' <summary>Applies the filter to the spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        Public Sub ApplyFilter(ByVal reals() As Double, ByVal imaginaries() As Double)
            If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            Me.TaperFilter.Taper(Me.SamplingRate, reals, imaginaries)
        End Sub

        ''' <summary> Applies the taper window. </summary>
        ''' <param name="timeSeries"> The time series. </param>
        Public Sub ApplyTaperWindow(ByVal timeSeries As Double())
            If Me.TaperWindow IsNot Nothing Then
                Me.TaperWindow.Apply(timeSeries)
            End If
        End Sub

        ''' <summary> Removes the mean. </summary>
        ''' <param name="timeSeries"> The time series. </param>
        Public Sub RemoveMean(ByVal timeSeries As Double())
            If Me.IsRemoveMean Then
                timeSeries.RemoveMean()
            End If
        End Sub

        ''' <summary>Calculates the spectrum using the Fourier Transform with pre
        '''   and post processing.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub Calculate(ByVal reals() As Double, ByVal imaginaries() As Double)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")
            Me.RemoveMean(reals)

            Me.ApplyTaperWindow(reals)

            ' Calculate the forward Fourier transform  
            Me.FourierTransform.Forward(reals, imaginaries)

            ' Check if the scale option is set
            If Me.IsScaleFft Then

                ' Set the scale factor
                Dim scaleFactor As Double = 1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length)

                ' Scale the spectrum.
                DoubleExtensions.Scale(reals, scaleFactor)
                DoubleExtensions.Scale(imaginaries, scaleFactor)

            End If

            If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

                ' apply the filter as necessary
                Me.ApplyFilter(reals, imaginaries)

            End If

        End Sub

        ''' <summary>Filters the real valued signal using low pass and high pass
        '''   taper frequency windows.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub Filter(ByVal reals() As Double)

            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            ' Initialize imaginary array.
            Dim imaginaries() As Double = New Double(reals.Length - 1) {}

            ' Calculate the forward Fourier transform  
            Me.FourierTransform.Forward(reals, imaginaries)

            ' Set the scale factor
            Dim scaleFactor As Double = 1.0R / reals.Length

            ' Scale the Spectrum.
            DoubleExtensions.Scale(reals, scaleFactor)
            DoubleExtensions.Scale(imaginaries, scaleFactor)

            If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

                ' apply the filter as necessary
                Me.ApplyFilter(reals, imaginaries)

            End If

            ' inverse transform
            Me.FourierTransform.Inverse(reals, imaginaries)

        End Sub


#End Region

#Region " SINGLE "

        ''' <summary>Initializes the spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        ''' </example>
        Public Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")
            ' Create the taper window
            If Not Me.TaperWindow Is Nothing Then Me.TaperWindow.Create(reals.Length)
            ' apply the transform
            Me.FourierTransform.Forward(reals, imaginaries)
        End Sub

        ''' <summary>Applies the filter to the spectrum.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        Public Sub ApplyFilter(ByVal reals() As Single, ByVal imaginaries() As Single)
            If Me.TaperFilter Is Nothing Then Throw New InvalidOperationException("Taper filter not defined")
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            Me.TaperFilter.Taper(Me.SamplingRate, reals, imaginaries)
        End Sub

        ''' <summary> Applies the taper window. </summary>
        ''' <param name="timeSeries"> The time series. </param>
        Public Sub ApplyTaperWindow(ByVal timeSeries As Single())
            If Me.TaperWindow IsNot Nothing Then
                Me.TaperWindow.Apply(timeSeries)
            End If
        End Sub

        ''' <summary> Removes the mean. </summary>
        ''' <param name="timeSeries"> The time series. </param>
        Public Sub RemoveMean(ByVal timeSeries As Single())
            If Me.IsRemoveMean Then
                timeSeries.RemoveMean()
            End If
        End Sub


        ''' <summary>Calculates the spectrum using the Fourier Transform with pre
        '''   and post processing.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <param name="imaginaries">Holds the imaginary values.</param>
        ''' <example>
        ''' </example>
        Public Sub Calculate(ByVal reals() As Single, ByVal imaginaries() As Single)
            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                            $"Real {reals.Length} and imaginary {imaginaries.Length} lengths must be equal")

            Me.RemoveMean(reals)

            Me.ApplyTaperWindow(reals)

            ' Calculate the forward Fourier transform  
            Me.FourierTransform.Forward(reals, imaginaries)

            ' Check if the scale option is set
            If Me.IsScaleFft Then

                ' Set the scale factor
                Dim scaleFactor As Single = CSng(1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length))

                ' Scale the Spectrum.
                SingleExtensions.Scale(reals, scaleFactor)
                SingleExtensions.Scale(imaginaries, scaleFactor)

            End If

            If Me._taperFilter IsNot Nothing AndAlso Me._taperFilter.FilterType <> TaperFilterType.None Then

                ' apply the filter as necessary
                Me.ApplyFilter(reals, imaginaries)

            End If

        End Sub

        ''' <summary>Filters the real valued signal using low pass and high pass
        '''   taper frequency windows.
        ''' </summary>
        ''' <param name="reals">Holds the real values.</param>
        ''' <example>
        '''   Sub Form_Click
        '''   End Sub
        ''' </example>
        Public Sub Filter(ByVal reals() As Single)

            If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")

            ' Initialize imaginary array.
            Dim imaginaries() As Single = {}
            ReDim imaginaries(reals.Length - 1)

            ' Calculate the forward Fourier transform  
            Me.FourierTransform.Forward(reals, imaginaries)

            ' Set the scale factor
            Dim scaleFactor As Single = CSng(1.0R / reals.Length)

            ' Scale the Spectrum.
            SingleExtensions.Scale(reals, scaleFactor)
            SingleExtensions.Scale(imaginaries, scaleFactor)

            If Me.TaperFilter IsNot Nothing AndAlso Me.TaperFilter.FilterType <> TaperFilterType.None Then

                ' apply the filter as necessary
                Me.ApplyFilter(reals, imaginaries)

            End If

            ' inverse transform
            Me.FourierTransform.Inverse(reals, imaginaries)

        End Sub

#End Region

    End Class

End Namespace
