Imports System.Numerics

''' <summary>
''' Provides a base class for fast and discrete Fourier transform calculations.
''' </summary>
''' <remarks>
''' (c) 1998 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 06/03/98, 1.0.00. David, 09/27/05, 1.0.2096, Create based on FFT pro </para>
''' </remarks>
Public MustInherit Class FourierTransformBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="transformType"> Type of the transform. </param>
    Protected Sub New(ByVal transformType As FourierTransformType)
        MyBase.New()
        Me._TransformType = transformType
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          False if this method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' Free managed resources when explicitly called
                End If
                ' Free shared unmanaged resources
                Me._DftCache = Nothing
                Me._DftTable = Nothing
                Me._SineTable = Nothing
                Me._CosineTable = Nothing
                Me._RealCache = Nothing
                Me._ImaginaryCache = Nothing
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Length of the time series. </summary>
    Private _TimeSeriesLength As Integer

    ''' <summary>
    ''' Gets or sets the local size of the time series arrays used for calculating the Fourier
    ''' transform.  This value is held separately allowing the first call to determine if any cache
    ''' needs to be recalculated in case of the time series length is different from the previous
    ''' calculation. Setting the time series length also sets the
    ''' <see cref="HalfSpectrumLength">length of the half spectrum</see>.
    ''' </summary>
    ''' <value> The number of elements. </value>
    Public Overridable Property TimeSeriesLength() As Integer
        Get
            Return Me._TimeSeriesLength
        End Get
        Set(ByVal value As Integer)
            Me._TimeSeriesLength = value
            Me._HalfSpectrumLength = CInt(Math.Floor(Me.TimeSeriesLength / 2) + 1)
        End Set
    End Property

    ''' <summary> The number of elements in half the spectrum. </summary>
    ''' <value> The length of the half spectrum. </value>
    Public ReadOnly Property HalfSpectrumLength() As Integer

    ''' <summary> The number of elements in the spectrum. </summary>
    ''' <value> The length of the spectrum. </value>
    Public ReadOnly Property FullSpectrumLength() As Integer
        Get
            Return Me.TimeSeriesLength
        End Get
    End Property

    ''' <summary> Gets or sets the Fourier Transform type. </summary>
    ''' <value> The type of the transform. </value>
    Public ReadOnly Property TransformType() As FourierTransformType

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overridable Sub Forward(ByVal values() As Complex)
        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))
        If values.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(values), "Array must be longer than 1")
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(values)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    Protected Overridable Sub Initialize(ByVal values() As Complex)
        Me.TimeSeriesLength = values.Length
    End Sub

    ''' <summary> Calculates the Inverse Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> Takes the spectrum values and returns the time series. </param>
    Public Sub Inverse(ByVal values() As Complex)
        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))
        If values.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(values), "Array must be longer than 1")
        values.Swap()
        Me.Forward(values)
        values.Swap()
    End Sub

#End Region

#Region " DOUBLE "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks>
    ''' Swap reals and imaginaries to computer the inverse
    '''   Fourier transform.
    ''' TODO: Remove initialize and place in the spectrum calculations.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overridable Sub Forward(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                         "The arrays of real- and imaginary-parts must have the same size")
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(reals, imaginaries)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overridable Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
        Me.TimeSeriesLength = reals.Length
    End Sub

    ''' <summary> Calculates the Inverse Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub Inverse(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                         "The arrays of real- and imaginary-parts must have the same size")
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        Me.Forward(imaginaries, reals)
    End Sub

#End Region

#Region " SINGLE "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap reals and imaginaries to computer the inverse Fourier transform. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overridable Sub Forward(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals,
                                                                                         "The arrays of real- and imaginary-parts must have the same size")
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        ' Initialize if new transform. Move out to the calling methods
        Me.Initialize(reals, imaginaries)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overridable Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
        Me.TimeSeriesLength = reals.Length
    End Sub

    ''' <summary> Calculates the Inverse Fourier Transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Sub Inverse(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
        If reals.Length < 2 Then Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        Me.Forward(imaginaries, reals)
    End Sub

#End Region

#Region " DFT (Sine and Cosine) Tables "

    ''' <summary> Returns the cosine table. </summary>
    ''' <value> The cosine table. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property CosineTable() As Double()

    ''' <summary> Returns the sine table. </summary>
    ''' <value> The sine table. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property SineTable() As Double()

    ''' <summary> Returns the cache of real values. </summary>
    ''' <value> The real cache. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property RealCache() As Double()

    ''' <summary> Returns the cache of imaginary values. </summary>
    ''' <value> The imaginary cache. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property ImaginaryCache() As Double()

    ''' <summary> Gets or sets the size of the dft tables. </summary>
    ''' <value> The size of the dft tables. </value>
    Private Property DftTablesSize As Integer

    ''' <summary>
    ''' Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
    ''' start at Delta and end at 360 degrees and are suitable for calculating the DFT for the Mixed
    ''' Radix Algorithm.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Function BuildShiftedDftTables(ByVal elementCount As Integer) As Boolean

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me.DftTablesSize = elementCount Then
            Return True
        End If

        ' store the new size
        Me.DftTablesSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim Me._CosineTable(elementCount - 1)
        ReDim Me._SineTable(elementCount - 1)
        ReDim Me._RealCache(elementCount - 1)
        ReDim Me._ImaginaryCache(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' Sine and cosine temporary values
        Dim sineAlpha As Double
        Dim cosineAlpha As Double

        ' start at last element 
        Dim beta As Integer = elementCount - 1

        ' Set last element of Cosine table to 1.0
        Dim cosineBeta As Double = 1.0#
        Me.CosineTable(beta) = cosineBeta

        ' set last element of Sine table to 0.0
        Dim sineBeta As Double = 0.0#
        Me.SineTable(beta) = sineBeta

        ' Use trigonometric relationships to build the tables
        Dim alpha As Integer = 0
        Do
            cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta
            sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta
            cosineBeta = cosineAlpha
            sineBeta = -sineAlpha

            Me.CosineTable(alpha) = cosineAlpha
            Me.SineTable(alpha) = sineAlpha

            beta -= 1
            Me.CosineTable(beta) = cosineBeta
            Me.SineTable(beta) = sineBeta

            alpha += 1

        Loop While alpha < beta

        Return True

    End Function

    ''' <summary>
    ''' Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
    ''' start at zero and end at 360 - delta degrees and are suitable for calculating the Discrete
    ''' Fourier transform and the sliding FFT.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Function BuildDftTables(ByVal elementCount As Integer) As Boolean

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me.DftTablesSize = elementCount Then
            Return True
        End If

        ' store the new size
        Me.DftTablesSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim Me._CosineTable(elementCount - 1)
        ReDim Me._SineTable(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' start at the first element
        Dim alpha As Integer = 0
        Dim beta As Integer = elementCount

        ' Sine and cosine temporary values
        Dim sineAlpha As Double = 0.0R
        Dim cosineAlpha As Double = 1.0R
        Dim cosineBeta As Double
        Dim sineBeta As Double

        ' Set first element of Cosine table to 1.0
        Me.CosineTable(alpha) = cosineAlpha

        ' set first element of Sine table to 0.0
        Me.SineTable(alpha) = sineAlpha

        ' Use trigonometric and inverse relationships to
        ' build the tables
        Do
            ' Get the next angle for the left end size, which
            ' equals the current angle plus the base angle
            cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta
            sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta

            ' Store the new values
            cosineAlpha = cosineBeta
            sineAlpha = sineBeta

            ' Save the next angle on the left-end
            alpha += 1
            Me.CosineTable(alpha) = cosineAlpha
            Me.SineTable(alpha) = sineAlpha

            ' Save the right-end elements as the mirror image
            ' of the left end elements
            beta -= 1
            Me.CosineTable(beta) = cosineBeta
            Me.SineTable(beta) = -sineBeta

        Loop While alpha < beta

        Return True

    End Function

    ''' <summary> Copies the real- and imaginary-parts to the cache. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">       . </param>
    ''' <param name="imaginaries"> . </param>
    Protected Sub CopyToCache(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))

        ' copies arrays to the cache
        ReDim Me._RealCache(reals.Length)
        reals.CopyTo(Me._RealCache, 0)
        ReDim Me._ImaginaryCache(imaginaries.Length)
        imaginaries.CopyTo(Me._ImaginaryCache, 0)

    End Sub

    ''' <summary> Copies the real- and imaginary-parts to the cache. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">       . </param>
    ''' <param name="imaginaries"> . </param>
    Protected Sub CopyToCache(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))

        ' copies arrays to the cache
        ReDim Me._RealCache(reals.Length)
        SingleExtensions.Copy(reals, Me._RealCache)
        ReDim Me._ImaginaryCache(imaginaries.Length)
        SingleExtensions.Copy(imaginaries, Me._ImaginaryCache)

    End Sub

#End Region

#Region " Complex DFT (Sine and Cosine) Table "

    ''' <summary> Returns the complex DFT table. </summary>
    ''' <value> The complex DFT table. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property DftTable() As Complex()

    ''' <summary> Returns the cache of DFT values. </summary>
    ''' <value> The complex cache of the DFT table. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Protected ReadOnly Property DftCache() As Complex()

    ''' <summary> Size of the DFT table. </summary>
    ''' <value> The size of the dft table. </value>
    Private Property DftTableSize As Integer

    ''' <summary>
    ''' Builds sine and cosine tables and allocate cache for calculating the DFT.  These DFT tables
    ''' start at Delta and end at 360 degrees and are suitable for calculating the DFT for the Mixed
    ''' Radix Algorithm.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    Protected Sub BuildShiftedDftTable(ByVal elementCount As Integer)

        ' throw an exception if size not right
        If elementCount <= 1 Then Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, "Must be greater than 1")

        ' exit if sine tables already constructed
        If Me.DftTableSize = elementCount Then Return

        ' store the new size
        Me.DftTableSize = elementCount

        ' allocate DFT cache and tables 
        ReDim Me._DftTable(elementCount - 1)
        ReDim Me._DftCache(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' Sine and cosine temporary values
        Dim sineAlpha As Double
        Dim cosineAlpha As Double

        ' start at last element 
        Dim beta As Integer = elementCount - 1

        ' Set last element of Cosine table to 1.0
        Dim cosineBeta As Double = 1.0#

        ' set last element of Sine table to 0.0
        Dim sineBeta As Double = 0.0#
        Me.DftTable(beta) = New Complex(cosineBeta, sineBeta)

        ' Use trigonometric relationships to build the tables
        Dim alpha As Integer = 0
        Do
            cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta
            sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta
            cosineBeta = cosineAlpha
            sineBeta = -sineAlpha

            Me.DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

            beta -= 1
            Me.DftTable(beta) = New Complex(cosineBeta, sineBeta)

            alpha += 1

        Loop While alpha < beta

    End Sub

    ''' <summary>
    ''' Builds sine and cosine tables for calculating the DFT. These DFT tables start at zero and end
    ''' at 360 - delta degrees and are suitable for calculating the Discrete Fourier transform and
    ''' the sliding FFT.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the size of the sine tables. </param>
    Protected Sub BuildDftTable(ByVal elementCount As Integer)

        ' throw an exception if size not right
        If elementCount <= 1 Then Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, "Must be greater than 1")

        ' exit if sine tables already constructed
        If Me.DftTableSize = elementCount Then Return

        ' store the new size
        Me.DftTableSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim Me._DftTable(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / elementCount
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' start at the first element
        Dim alpha As Integer = 0
        Dim beta As Integer = elementCount

        ' Sine and cosine temporary values
        Dim sineAlpha As Double = 0.0R
        Dim cosineAlpha As Double = 1.0R
        Dim cosineBeta As Double
        Dim sineBeta As Double

        ' Set first element of Cosine table to 1.0
        ' set first element of Sine table to 0.0
        Me.DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

        ' Use trigonometric and inverse relationships to
        ' build the tables
        Do
            ' Get the next angle for the left end size, which
            ' equals the current angle plus the base angle
            cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta
            sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta

            ' Store the new values
            cosineAlpha = cosineBeta
            sineAlpha = sineBeta

            ' Save the next angle on the left-end
            alpha += 1
            Me.DftTable(alpha) = New Complex(cosineAlpha, sineAlpha)

            ' Save the right-end elements as the mirror image
            ' of the left end elements
            beta -= 1
            Me.DftTable(beta) = New Complex(cosineBeta, -sineBeta)

        Loop While alpha < beta

    End Sub

    ''' <summary> Copies the real- and imaginary-parts to the cache. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    Protected Sub CopyToCache(ByVal values() As Complex)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        ' copies arrays to the cache
        ReDim Me._DftCache(values.Length - 1)
        values.CopyTo(Me._DftCache)

    End Sub

#End Region

End Class
