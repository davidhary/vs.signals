Imports System.Runtime.InteropServices

Namespace Wisdom

    ''' <summary> Base class for FFTW memory management. </summary>
    ''' <remarks>
    ''' Arrays allocated using the <see cref="MemoryArray">memory array</see> should be deallocated
    ''' by the same construct rather than the ordinary free (or, heaven forbid, delete).
    ''' <para>
    ''' A program linking to an FFTW library compiled with SIMD support can obtain a nonnegligible
    ''' speedup for most complex and real to complex and complex to real transforms. In order to
    ''' obtain this speedup, however, the arrays passed to FFTW must be specially aligned in memory
    ''' (typically 16-byte aligned). Often this alignment is more stringent than that provided by the
    ''' usual memory allocation routines.
    ''' </para>
    ''' <para>
    ''' In order to guarantee proper alignment for SIMD, therefore, in case your program is ever
    ''' linked against a SIMD-using FFTW, we recommend allocating your transform data with
    ''' fftw_malloc and de-allocating it with fftw_free. These have exactly the same interface and
    ''' behavior as malloc/free, except that for a SIMD FFTW they ensure that the returned pointer
    ''' has the necessary alignment (by calling memalign or its equivalent on your OS).
    ''' </para>
    ''' <para>
    ''' You are not required to use fftw_malloc. You can allocate your data in any way that you like,
    ''' from malloc to new (in C++) to a fixed-size array declaration. If the array happens not to be
    ''' properly aligned, FFTW will not use the SIMD extensions.
    ''' </para> <para>
    ''' David, 09/01/07, 2.0.2800 </para><para>
    ''' (c) 2007 Integrated Scientific Resources, Inc.</para><para>
    ''' Licensed under The MIT License. </para>
    ''' </remarks>
    Public MustInherit Class MemoryArray
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Creates a new memory array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="length">          Logical length of the array (number of elements) </param>
        ''' <param name="dataTypeSize">    Number of bytes in the data type. </param>
        ''' <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        '''                                for each logical element. </param>
        Protected Sub New(ByVal length As Integer, ByVal dataTypeSize As Integer, ByVal subElementCount As Integer)
            MyBase.New()
            Me._DataTypeSize = dataTypeSize
            Me._SubElementCount = subElementCount
            Me._Handle = IntPtr.Zero
            Me._Length = length
            Me._Handle = MemoryArray.Allocate(length, dataTypeSize, subElementCount)
        End Sub

        ''' <summary>
        ''' Creates a new <see cref="System.Single">single precision</see> memory using the given
        ''' <paramref name="data"/>
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="data">            An array holding the data. </param>
        ''' <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        '''                                for each logical element. </param>
        ''' <param name="isPinned">        Specifies if the array is to be pinned in memory. </param>
        Protected Sub New(ByVal data As Single(), ByVal subElementCount As Integer, ByVal isPinned As Boolean)
            MyBase.New()
            Me._DataTypeSize = 4
            Me._SubElementCount = subElementCount
            Me._Handle = IntPtr.Zero
            Me._Handle = MemoryArray.Allocate(data, isPinned)
        End Sub

        ''' <summary>
        ''' Creates a new <see cref="System.Double">double precision</see> memory using the given
        ''' <paramref name="data"/>
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="data">            An array holding the data. </param>
        ''' <param name="subElementCount"> The <see cref="SubElementCount">number of sub elements</see>
        '''                                for each logical element. </param>
        ''' <param name="isPinned">        Specifies if the array is to be pinned in memory. </param>
        Protected Sub New(ByVal data As Double(), ByVal subElementCount As Integer, ByVal isPinned As Boolean)
            MyBase.New()
            Me._DataTypeSize = 8
            Me._SubElementCount = subElementCount
            Me._Handle = IntPtr.Zero
            Me._Handle = MemoryArray.Allocate(data, isPinned)
        End Sub

        ''' <summary>
        ''' Creates a new <see cref="System.Single">single precision</see> memory using the
        ''' <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        ''' part</paramref> values.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        Protected Sub New(ByVal reals As Single(), ByVal imaginaries As Single(), ByVal isPinned As Boolean)
            MyBase.New()
            Me._DataTypeSize = 4
            Me._SubElementCount = 2
            Me._Handle = IntPtr.Zero
            Me._Handle = MemoryArray.Allocate(reals, imaginaries, isPinned)
        End Sub

        ''' <summary>
        ''' Creates a new <see cref="System.Double">double precision</see> memory using the
        ''' <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        ''' part</paramref> values.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        Protected Sub New(ByVal reals As Double(), ByVal imaginaries As Double(), ByVal isPinned As Boolean)
            MyBase.New()
            Me._DataTypeSize = 8
            Me._SubElementCount = 2
            Me._Handle = IntPtr.Zero
            Me._Handle = MemoryArray.Allocate(reals, imaginaries, isPinned)
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if disposed. </summary>
        Private _Disposed As Boolean

        ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
        ''' <value> The disposed. </value>
        Protected Property Disposed() As Boolean
            Get
                Return Me._Disposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._Disposed = value
            End Set
        End Property

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    Me.Free()

                End If

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.Disposed = True

            End Try

        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary> Allocates an FFTW-compatible array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="totalElementCount"> Product of number of <see cref="Length">elements</see> and
        '''                                  <see cref="SubElementCount">number of sub elements</see>. 
        ''' </param>
        ''' <param name="dataTypeSize">      Number of bytes in the data type. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal totalElementCount As Integer, ByVal dataTypeSize As Integer) As IntPtr
            If totalElementCount < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(totalElementCount), "Element count must be longer than 1")
            End If
            If dataTypeSize = 4 Then
                Return FftwF.SafeNativeMethods.Malloc(dataTypeSize * totalElementCount)
            ElseIf dataTypeSize = 8 Then
                Return FftwR.SafeNativeMethods.Malloc(dataTypeSize * totalElementCount)
            Else
                Throw New ArgumentOutOfRangeException(NameOf(dataTypeSize), "Must be 4 or 8")
            End If
        End Function

        ''' <summary> Allocates an FFTW-compatible array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="elementCount">    Logical length of the array (number of elements). </param>
        ''' <param name="dataTypeSize">    Number of bytes in the data type. </param>
        ''' <param name="subElementCount"> Number of sub-elements. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal elementCount As Integer, ByVal dataTypeSize As Integer, ByVal subElementCount As Integer) As IntPtr
            Return MemoryArray.Allocate(elementCount * subElementCount, dataTypeSize)
        End Function

        ''' <summary>
        ''' Allocates an FFTW-compatible array.  
        ''' Pins and copies the <paramref name="data">data</paramref> to memory space pointed to by the
        ''' returned handle.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="data">     . </param>
        ''' <param name="isPinned"> Specifies the condition for pinning the array in memory. This handle
        '''                         type allows the address of the pinned object to be taken. This
        '''                         prevents the garbage collector from moving the object and hence
        '''                         undermines the efficiency of the garbage collector. Use the Free
        '''                         method to free the allocated handle as soon as possible. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal data As Single(), ByVal isPinned As Boolean) As IntPtr
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If
            Dim handle As IntPtr
            If isPinned Then
                handle = GCHandle.ToIntPtr(GCHandle.Alloc(data, GCHandleType.Pinned))
            Else
                handle = MemoryArray.Allocate(data.Length, 4)
                System.Runtime.InteropServices.Marshal.Copy(data, 0, handle, data.Length)
            End If
            Return handle
        End Function

        ''' <summary>
        ''' Allocates an FFTW-compatible array.  
        ''' Pins and copies the <paramref name="data">data</paramref> to memory space pointed to by the
        ''' returned handle.
        ''' </summary>
        ''' <remarks>
        ''' From Tamas Szalay: "With the <paramref name="isPinned">true</paramref> the provided array is
        ''' <see cref="GCHandleType.Pinned"/>
        ''' to ensure that the array does not move. Using malloc(), the actual transform is faster by up
        ''' to 3x but not always, and you know you're using an FFTW-friendly method. However, you need to
        ''' store two copies of the data that you copy back and forth using Marshal.Copy, which takes up
        ''' space and time. Using GCHandle.Pinned, however, the behavior is less predictable. I was doing
        ''' speed tests, and some arrays seemed to be up to 2-3x slower than their GCHandle.Pinned
        ''' counterparts while others were indistinguishable. I assume this was due to whatever alignment,
        ''' but I could find no good way of fixing it. If anybody has a solution short of manually
        ''' aligning via unsafe code, do let me know.".
        ''' </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="data">     . </param>
        ''' <param name="isPinned"> Specifies the condition for pinning the array in memory. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal data As Double(), ByVal isPinned As Boolean) As IntPtr
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If
            Dim handle As IntPtr
            If isPinned Then
                handle = GCHandle.ToIntPtr(GCHandle.Alloc(data, GCHandleType.Pinned))
            Else
                handle = MemoryArray.Allocate(data.Length, 8)
                System.Runtime.InteropServices.Marshal.Copy(data, 0, handle, data.Length)
            End If
            Return handle
        End Function

        ''' <summary>
        ''' Allocates a <see cref="System.Single">single precision</see> FFTW-compatible array. Creates a
        ''' complex array using the <paramref name="reals">real-</paramref> and
        ''' <paramref name="imaginaries">imaginary-part</paramref> values.
        ''' Then pins and copies the complex array to memory space pointed to by the returned handle.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies the condition for pinning the array in memory. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal reals As Single(), ByVal imaginaries As Single(), ByVal isPinned As Boolean) As IntPtr
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Single

            ' set the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                data(j) = reals(i)
                j += 1
                data(j) = imaginaries(i)
                j += 1
            Next

            ' get a handle to the array.
            Return MemoryArray.Allocate(data, isPinned)

        End Function

        ''' <summary>
        ''' Allocates a <see cref="System.Double">double precision</see> FFTW-compatible array. Creates a
        ''' complex array using the <paramref name="reals">real-</paramref> and
        ''' <paramref name="imaginaries">imaginary-part</paramref> values.
        ''' Then pins and copies the complex array to memory space pointed to by the returned handle.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies the condition for pinning the array in memory. </param>
        ''' <returns> A <see cref="IntPtr">handle</see> to the array. </returns>
        Public Shared Function Allocate(ByVal reals As Double(), ByVal imaginaries As Double(), ByVal isPinned As Boolean) As IntPtr
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Double

            ' set the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                data(j) = reals(i)
                j += 1
                data(j) = imaginaries(i)
                j += 1
            Next

            ' get a handle to the array.
            Return MemoryArray.Allocate(data, isPinned)

        End Function

        ''' <summary> Copies the data to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="data">   Array of data values.  This could be an array of complex or real-part
        '''                       values. </param>
        ''' <param name="target"> <see cref="IntPtr">Points</see> to the FFTW-Compatible array. </param>
        ''' <returns> A True if ok. </returns>
        Public Shared Function CopyFrom(ByVal data As Single(), ByVal target As IntPtr) As Boolean
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If
            System.Runtime.InteropServices.Marshal.Copy(data, 0, target, data.Length)
            Return True
        End Function

        ''' <summary> Copies the data to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="data">   Array of data values.  This could be an array of complex or real-part
        '''                       values. </param>
        ''' <param name="target"> <see cref="IntPtr">Points</see> to the FFTW-Compatible array. </param>
        ''' <returns> A True if ok. </returns>
        Public Shared Function CopyFrom(ByVal data As Double(), ByVal target As IntPtr) As Boolean
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If
            System.Runtime.InteropServices.Marshal.Copy(data, 0, target, data.Length)
            Return True
        End Function

        ''' <summary>
        ''' Copies the real- and imaginary-part values to an existing FFTW-compatible array.  
        ''' Creates a complex array using the <paramref name="reals">real-</paramref> and
        ''' <paramref name="imaginaries">imaginary-part</paramref> values.
        ''' Then copies the complex array to memory space pointed to by the
        ''' <paramref name="target">pointer</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="target">      <see cref="IntPtr">Points</see> to the FFTW-Comaprible array. </param>
        ''' <returns> A True if ok. </returns>
        Public Shared Function CopyFrom(ByVal reals As Single(), ByVal imaginaries As Single(), ByVal target As IntPtr) As Boolean

            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Single

            ' set the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                data(j) = reals(i)
                j += 1
                data(j) = imaginaries(i)
                j += 1
            Next

            System.Runtime.InteropServices.Marshal.Copy(data, 0, target, data.Length)

            Return True

        End Function

        ''' <summary>
        ''' Copies the real- and imaginary-part values to an existing FFTW-compatible array.  
        ''' Creates a complex array using the <paramref name="reals">real-</paramref> and
        ''' <paramref name="imaginaries">imaginary-part</paramref> values.
        ''' Then copies the complex array to memory space pointed to by the
        ''' <paramref name="target">pointer</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="target">      <see cref="IntPtr">Points</see> to the FFTW-Comaprible array. </param>
        ''' <returns> A True if ok. </returns>
        Public Shared Function CopyFrom(ByVal reals As Double(), ByVal imaginaries As Double(), ByVal target As IntPtr) As Boolean

            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Double

            ' set the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                data(j) = reals(i)
                j += 1
                data(j) = imaginaries(i)
                j += 1
            Next

            System.Runtime.InteropServices.Marshal.Copy(data, 0, target, data.Length)

            Return True

        End Function

        ''' <summary>
        ''' Retrieves complex data from the specified <paramref name="source">pointer</paramref> to the
        ''' <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        ''' part</paramref> arrays.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="source">      A <see cref="IntPtr">handle</see> to the array. </param>
        ''' <param name="reals">       An array receiving the real-part values. </param>
        ''' <param name="imaginaries"> An array receiving imaginary-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function CopyTo(ByVal source As IntPtr, ByVal reals As Double(), ByVal imaginaries As Double()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Double
            System.Runtime.InteropServices.Marshal.Copy(source, data, 0, data.Length)

            ' get the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                reals(i) = data(j)
                j += 1
                imaginaries(i) = data(j)
                j += 1
            Next

            Return True

        End Function

        ''' <summary>
        ''' Retrieves complex data from the specified <paramref name="source">source</paramref> to the
        ''' <paramref name="reals">real-</paramref> and <paramref name="imaginaries">imaginary-
        ''' part</paramref> arrays.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="source">      A <see cref="IntPtr">handle</see> to the array. </param>
        ''' <param name="reals">       An array receiving the real-part values. </param>
        ''' <param name="imaginaries"> An array receiving imaginary-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function CopyTo(ByVal source As IntPtr, ByVal reals As Single(), ByVal imaginaries As Single()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            ' allocate complex data array.
            Dim data(reals.Length + reals.Length - 1) As Single
            System.Runtime.InteropServices.Marshal.Copy(source, data, 0, data.Length)

            ' get the array data
            Dim j As Integer = 0
            For i As Integer = 0 To reals.Length - 1
                reals(i) = data(j)
                j += 1
                imaginaries(i) = data(j)
                j += 1
            Next

            Return True

        End Function

        ''' <summary>
        ''' Retrieves data from the specified <paramref name="source">source</paramref> to the
        ''' <paramref name="data">data-</paramref> array.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="source"> A <see cref="IntPtr">handle</see> to the array. </param>
        ''' <param name="data">   An array receiving the source values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function CopyTo(ByVal source As IntPtr, ByVal data As Single()) As Boolean
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If
            ' fetch the data.
            System.Runtime.InteropServices.Marshal.Copy(source, data, 0, data.Length)
            Return True
        End Function

        ''' <summary>
        ''' Retrieves data from the specified <paramref name="source">source</paramref> to the
        ''' <paramref name="data">data-</paramref> array.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="source"> A <see cref="IntPtr">handle</see> to the array. </param>
        ''' <param name="data">   An array receiving the source data. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Shared Function CopyTo(ByVal source As IntPtr, ByVal data As Double()) As Boolean
            If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
            If data.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(data), "Array must be longer than 1")
            End If

            ' fetch the data.
            System.Runtime.InteropServices.Marshal.Copy(source, data, 0, data.Length)

            Return True

        End Function

        ''' <summary> Gets or sets the size of the data type in bytes. </summary>
        ''' <value> The size of the data type. </value>
        Public ReadOnly Property DataTypeSize() As Integer

        ''' <summary> Frees allocated memory. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        Public Sub Free()
            If Me._Handle <> IntPtr.Zero Then
                If Me._DataTypeSize = 4 Then
                    FftwF.SafeNativeMethods.Free(Me._Handle)
                ElseIf Me._DataTypeSize = 8 Then
                    FftwR.SafeNativeMethods.Free(Me._Handle)
                Else
                    ' if byte size not specified we assume nothing was allocated.
                End If
            End If
            Me._Handle = IntPtr.Zero
        End Sub

        ''' <summary>
        ''' Gets or sets reference to the <see cref="IntPtr">source</see> to the
        ''' <see cref="MemoryArray">array</see>.
        ''' </summary>
        ''' <value> The handle. </value>
        Public ReadOnly Property Handle() As IntPtr

        ''' <summary>
        ''' Gets or sets the number of logical elements in the <see cref="MemoryArray">array</see>. Each
        ''' element of a complex array consists of 2 sub-elements.  The element count of the array mapped
        ''' by the memory array includes <see cref="Length">element count</see> times
        ''' <see cref="SubElementCount">sub-element count</see>
        ''' elements.  This for example, an <see cref="Array">array</see> of 1024 elements maps into a
        ''' <see cref="RealArrayF">real-parts array</see> of 1024 or to a
        ''' <see cref="ComplexArrayF">complex array</see>
        ''' of only 512 elements.
        ''' </summary>
        ''' <value> The length. </value>
        Public ReadOnly Property Length() As Integer

        ''' <summary> Gets or sets the number of sub elements for each array element. </summary>
        ''' <value> The number of sub elements. </value>
        Public ReadOnly Property SubElementCount() As Integer

#End Region

    End Class

    ''' <summary>
    ''' Complex array suitable for FFTW memory management.  The array consists of alternating Real-
    ''' and Imaginary-parts. Thus, each logical element is composed of a real and imaginary parts of
    ''' a complex number.
    ''' </summary>
    ''' <remarks> David, 09/01/07, 2.0.2800. </remarks>
    Public Class ComplexArrayF
        Inherits MemoryArray

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Creates an FFTW-compatible array of complex numbers. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="length"> Logical length of the array (number of elements) </param>
        Public Sub New(ByVal length As Integer)
            MyBase.New(length, 4, 2)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from a complex array by either pinning the array or copying
        ''' the array values to a source.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="complex">  Array of alternating real- and imaginary-parts. </param>
        ''' <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        Public Sub New(ByVal complex As Single(), ByVal isPinned As Boolean)
            MyBase.New(complex, 2, isPinned)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from real- and imaginary-part values,  
        ''' Converts the real- and imaginary-parts to a complex array and then either pins the array or
        ''' copies its values to a pointer.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        Public Sub New(ByVal reals As Single(), ByVal imaginaries As Single(), ByVal isPinned As Boolean)
            MyBase.New(reals, imaginaries, isPinned)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary> Copies complex values to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="complex"> Array of alternating real- and imaginary-parts. </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal complex As Single()) As Boolean
            If complex Is Nothing Then Throw New ArgumentNullException(NameOf(complex))
            If complex.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(complex), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(complex, MyBase.Handle)
        End Function

        ''' <summary>
        ''' Copies real- and imaginary-part values to an existing FFTW-compatible array.  
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal reals As Single(), ByVal imaginaries As Single()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(reals, imaginaries, MyBase.Handle)
        End Function

        ''' <summary>
        ''' Retrieves real- and imaginary-part values from <see cref="ComplexArrayF">complex</see> array.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array receiving the real-part values. </param>
        ''' <param name="imaginaries"> An array receiving the imaginary-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overloads Function CopyTo(ByVal reals As Single(), ByVal imaginaries As Single()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            Return MemoryArray.CopyTo(MyBase.Handle, reals, imaginaries)

        End Function

#End Region

    End Class

    ''' <summary> real-parts array suitable for FFTW memory management. </summary>
    ''' <remarks> David, 09/01/07, 2.0.2800. </remarks>
    Public Class RealArrayF
        Inherits MemoryArray

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Creates an FFTW-compatible array of real-part values. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="length"> Logical length of the array (number of elements) </param>
        Public Sub New(ByVal length As Integer)
            MyBase.New(length, 4, 1)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from an array of real-part values by either pinning the
        ''' array or copying the array values to a pointer.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">    Array of real-parts. </param>
        ''' <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        Public Sub New(ByVal reals As Single(), ByVal isPinned As Boolean)
            MyBase.New(reals, 1, isPinned)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary> Copies real-part values to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals"> Array of real-parts. </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal reals As Single()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(reals, MyBase.Handle)
        End Function

        ''' <summary> Retrieves real-part values from an existing FFTW-compatible array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals"> An array receiving the real-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overloads Function CopyTo(ByVal reals As Single()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            Return MemoryArray.CopyTo(MyBase.Handle, reals)

        End Function

#End Region

    End Class

    ''' <summary>
    ''' Complex array suitable for FFTW memory management.  The array consists of alternating real-
    ''' and imaginary-parts. Thus, each logical element is composed of a real and imaginary parts of
    ''' a complex number.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Class ComplexArrayR

        Inherits MemoryArray

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Creates an FFTW-compatible array of complex numbers. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="length"> Logical length of the array (number of elements) </param>
        Public Sub New(ByVal length As Integer)
            MyBase.New(length, 8, 2)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from a complex array by either pinning the array or copying
        ''' the array values to a pointer.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="complex">  Array of alternating real- and imaginary-parts. </param>
        ''' <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        Public Sub New(ByVal complex As Double(), ByVal isPinned As Boolean)
            MyBase.New(complex, 2, isPinned)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from real- and imaginary-part values,  
        ''' Converts the real- and imaginary-parts to a complex array and then either pins the array or
        ''' copies its values to a pointer.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <param name="isPinned">    Specifies if the array is to be pinned in memory. </param>
        Public Sub New(ByVal reals As Double(), ByVal imaginaries As Double(), ByVal isPinned As Boolean)
            MyBase.New(reals, imaginaries, isPinned)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary> Copies complex values to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="complex"> Array of alternating real- and imaginary-parts. </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal complex As Double()) As Boolean
            If complex Is Nothing Then Throw New ArgumentNullException(NameOf(complex))
            If complex.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(complex), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(complex, MyBase.Handle)
        End Function

        ''' <summary>
        ''' Copies real- and imaginary-part values to an existing FFTW-compatible array.  
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array with real-part values. </param>
        ''' <param name="imaginaries"> An array with imaginary-part values. </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal reals As Double(), ByVal imaginaries As Double()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(reals, imaginaries, MyBase.Handle)
        End Function

        ''' <summary>
        ''' Retrieves real- and imaginary-part values from <see cref="ComplexArrayF">complex</see> array.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals">       An array receiving the real-part values. </param>
        ''' <param name="imaginaries"> An array receiving the imaginary-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overloads Function CopyTo(ByVal reals As Double(), ByVal imaginaries As Double()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If imaginaries Is Nothing Then Throw New ArgumentNullException(NameOf(imaginaries))
            If reals.Length <> imaginaries.Length Then
                Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
            End If
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            Return MemoryArray.CopyTo(MyBase.Handle, reals, imaginaries)

        End Function

#End Region

    End Class

    ''' <summary> real-parts array suitable for FFTW memory management. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Class RealArrayR

        Inherits MemoryArray

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Creates an FFTW-compatible array of real-part values. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="length"> Logical length of the array (number of elements) </param>
        Public Sub New(ByVal length As Integer)
            MyBase.New(length, 8, 1)
        End Sub

        ''' <summary>
        ''' Creates an FFTW-compatible array from an array of real-part values by either pinning the
        ''' array or copying the array values to a pointer.
        ''' </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="reals">    Array of real-parts. </param>
        ''' <param name="isPinned"> True if using a pinned rather than allocated array. </param>
        Public Sub New(ByVal reals As Double(), ByVal isPinned As Boolean)
            MyBase.New(reals, 1, isPinned)
        End Sub

        ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        ''' <remarks>
        ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        ''' method has been called directly or indirectly by a user's code--managed and unmanaged
        ''' resources can be disposed. If disposing equals False, the method has been called by the
        ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
        ''' resources can be disposed.
        ''' </remarks>
        ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
        '''                          False if this method releases only unmanaged resources. </param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not Me.Disposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary> Copies the real-part values to an existing FFTW-compatible array.   </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals"> . </param>
        ''' <returns> A True if ok. </returns>
        Public Overloads Function CopyFrom(ByVal reals As Double()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If
            Return MemoryArray.CopyFrom(reals, MyBase.Handle)
        End Function

        ''' <summary> Retrieves real-part values from an existing FFTW-compatible array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reals"> An array receiving the real-part values. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overloads Function CopyTo(ByVal reals As Double()) As Boolean
            If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))
            If reals.Length < 2 Then
                Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
            End If

            Return MemoryArray.CopyTo(MyBase.Handle, reals)

        End Function

#End Region

    End Class

End Namespace

