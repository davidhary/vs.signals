Imports System.Numerics

''' <summary> Calculates the Discrete Fourier Transform. </summary>
''' <remarks>
''' Includes procedures for calculating the forward and inverse DFT.<p>
''' Benchmarks:</p><p>
''' CPU     Data    Speed</p><p>
''' Score   Points  (ms)</p><p>
''' 7.1     1000    1</p><p>
''' 7.1     1024    1</p><p>
''' 7.1     8192    10</p> <para>
''' David, 06/03/98. 1.0.00  </para><para>
''' David, 09/27/05. 1.0.2096. Create based on FFT pro. (c) 1998 Integrated Scientific Resources,
''' Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License.</para><para></para>
''' </remarks>
Public Class DiscreteFourierTransform
    Inherits FourierTransformBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New(FourierTransformType.Dft)
    End Sub

#End Region

#Region " COMPLEX "

    ''' <summary> Calculates the Forward Fourier Transform. </summary>
    ''' <remarks> Swap real and imaginary values to compute the inverse Fourier transform. </remarks>
    ''' <param name="values"> Takes the time series and returns the spectrum values. </param>
    Public Overrides Sub Forward(ByVal values() As Complex)
        MyBase.Forward(values)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the inputs and returns the outputs. </param>
    Protected Overrides Sub Initialize(ByVal values() As Complex)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        ' build the DFT table
        Me.BuildDftTable(values.Length)

        ' Set element count
        Me.TimeSeriesLength = values.Length

        ' save initial values to the cache
        Me.CopyToCache(values)

        Dim value As Complex
        Dim k As Integer
        For j As Integer = 0 To MyBase.TimeSeriesLength - 1
            value = MyBase.DftCache(0)
            k = 0
            For i As Integer = 1 To MyBase.TimeSeriesLength - 1
                k += j
                If k >= MyBase.TimeSeriesLength Then
                    k -= MyBase.TimeSeriesLength
                End If
                value += MyBase.DftCache(i) * MyBase.DftTable(k).Conjugate
            Next i
            values(j) = value
        Next j
    End Sub

#End Region

#Region " DOUBLE "

    ''' <summary> Calculates the Discrete Fourier Transform. </summary>
    ''' <remarks>
    ''' Computes the DFT directly without using any of the special rotation and permutation
    ''' algorithms that are part of the Fast Fourier Transform (FFT)
    ''' literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals() As Double, ByVal imaginaries() As Double)
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)
        If reals Is Nothing Then Throw New System.ArgumentNullException(NameOf(reals))
        If imaginaries Is Nothing Then Throw New System.ArgumentNullException(NameOf(imaginaries))
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException(NameOf(reals), reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException(NameOf(reals), "Array must be longer than 1")
        End If

        ' build the DFT tables
        MyBase.BuildDftTables(reals.Length)

        ' Set element count
        MyBase.TimeSeriesLength = reals.Length

        ' save initial values to the cache
        MyBase.CopyToCache(reals, imaginaries)

        Dim realValue As Double
        Dim imaginaryValue As Double
        Dim k As Integer
        For j As Integer = 0 To MyBase.TimeSeriesLength - 1
            realValue = MyBase.RealCache(0)
            imaginaryValue = MyBase.ImaginaryCache(0)
            k = 0
            For i As Integer = 1 To MyBase.TimeSeriesLength - 1
                k += j
                If k >= MyBase.TimeSeriesLength Then
                    k -= MyBase.TimeSeriesLength
                End If
                realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) _
                          + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) _
                          - MyBase.SineTable(k) * MyBase.RealCache(i)
            Next i
            reals(j) = realValue
            imaginaries(j) = imaginaryValue
        Next j
    End Sub

#End Region

#Region " SINGLE "

    ''' <summary> Calculates the Discrete Fourier Transform. </summary>
    ''' <remarks>
    ''' Computes the DFT directly without using any of the special rotation and permutation
    ''' algorithms that are part of the Fast Fourier Transform (FFT)
    ''' literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Public Overrides Sub Forward(ByVal reals() As Single, ByVal imaginaries() As Single)
        MyBase.Forward(reals, imaginaries)
    End Sub

    ''' <summary> Initializes the transform. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">       Holds the real values. </param>
    ''' <param name="imaginaries"> Holds the imaginary values. </param>
    Protected Overrides Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)
        If reals Is Nothing Then Throw New ArgumentNullException(NameOf(reals))

        ' build the DFT tables
        MyBase.BuildDftTables(reals.Length)

        ' Set element count
        MyBase.TimeSeriesLength = reals.Length

        ' save initial values to the cache
        MyBase.CopyToCache(reals, imaginaries)

        Dim realValue As Double
        Dim imaginaryValue As Double
        Dim k As Integer
        For j As Integer = 0 To MyBase.TimeSeriesLength - 1
            realValue = MyBase.RealCache(0)
            imaginaryValue = MyBase.ImaginaryCache(0)
            k = 0
            For i As Integer = 1 To MyBase.TimeSeriesLength - 1
                k += j
                If k >= MyBase.TimeSeriesLength Then
                    k -= MyBase.TimeSeriesLength
                End If
                realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) - MyBase.SineTable(k) * MyBase.RealCache(i)
            Next i
            reals(j) = CSng(realValue)
            imaginaries(j) = CSng(imaginaryValue)
        Next j

    End Sub

#End Region

End Class
