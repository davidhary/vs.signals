Imports isr.Algorithms.Signals.ExceptionExtensions
Imports isr.Core.EnumExtensions

''' <summary>
''' Includes test program for calculating the spectrum using DFT, FFTW, and sliding Fourier
''' transform algorithms.
''' </summary>
''' <remarks>
''' David, 10/11/05, 1.0.2110. Convert to .NET <para>
''' Launch this form by calling its Show or ShowDialog method from its default instance.
''' </para><para>
''' (c) 2005 Integrated Scientific Resources, Inc.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class WisdomSpectrumView
    Inherits isr.Core.Forma.ModelViewTalkerBase

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        Me.InitializingComponents = False

        Me._NotesMessageList.CommenceUpdates()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.InitializingComponents = True
            If disposing Then
                Me._NotesMessageList.SuspendUpdatesReleaseIndicators()

                ' Free managed resources when explicitly called
                If Me._SignalChartPane IsNot Nothing Then
                    Me._SignalChartPane.Dispose()
                    Me._SignalChartPane = Nothing
                End If

                If Me._SpectrumChartPan IsNot Nothing Then
                    Me._SpectrumChartPan.Dispose()
                    Me._SpectrumChartPan = Nothing
                End If

                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If

            End If
            ' Free shared unmanaged resources
            'onDisposeUnManagedResources()

        Finally
            Me.InitializingComponents = False
            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region

    #Region " PROPERTIES "

    ''' <summary> Gets the selected example. </summary>
    ''' <value> The selected example. </value>
    Private ReadOnly Property SelectedExample() As Example
        Get
            Return CType(CType(Me._ExampleComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, Example)
        End Get
    End Property

    ''' <summary> Gets or sets the status message. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
    Private _StatusMessage As String = String.Empty

    #End Region

    #Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown.  This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                               <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Initialize and set the user interface
            Me.InitializeUserInterface()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    #End Region

    #Region " USER INTERFACE "

    ''' <summary> Shows the status. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="message"> The message. </param>
    Private Sub ShowStatus(ByVal message As String)

        Me._StatusMessage = message
        Me._NotesMessageList.AddMessage(message)
        Me._StatusStatusBarPanel.Text = message

    End Sub

    ''' <summary>Gets or sets the data format</summary>
    Private Const _ListFormat As String = "{0:0.000000000000000}{1}{2:0.000000000000000}"

    ''' <summary> Enumerates the action options. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Enum Example

        ''' <summary> An enum constant representing the discrete fourier transform option. </summary>
        <System.ComponentModel.Description("Discrete Fourier Transform")> DiscreteFourierTransform

        ''' <summary> An enum constant representing the sliding FFT option. </summary>
        <System.ComponentModel.Description("Sliding FFT")> SlidingFFT

        ''' <summary> An enum constant representing the wisdom FFT option. </summary>
        <System.ComponentModel.Description("Wisdom FFT")> WisdomFFT
    End Enum

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub InitializeUserInterface()

        ' tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        ' tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

        ' Set initial values defining the signal
        Me._PointsTextBox.Text = "16" ' "1000"
        Me._CyclesTextBox.Text = "1" ' "11.5"
        Me._PhaseTextBox.Text = "0" ' "45"
        Me._PointsToDisplayTextBox.Text = "100"
        Me._SignalDurationTextBox.Text = "1"

        ' set the default sample
        Me.PopulateExampleComboBox()
        Me._ExampleComboBox.SelectedIndex = 0

        ' create the two charts.
        Me.CreateSpectrumChart()
        Me.CreateSignalChart()

        ' plot the default sign wave
        Me.SineWaveDouble()

    End Sub

    #End Region

    #Region " SIGNALS "

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequency"> . </param>
    ''' <param name="phase">     . </param>
    ''' <param name="elements">  . </param>
    ''' <returns> A Double() </returns>
    Private Overloads Function SineWave(ByVal frequency As Single, ByVal phase As Single, ByVal elements As Integer) As Single()

        ' get the signal
        Dim signal() As Single = isr.Algorithms.Signals.Signal.Sine(frequency, phase, elements)

        ' Plot the Signal 
        Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture) / elements, elements), signal)

        Return signal

    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequency"> . </param>
    ''' <param name="phase">     . </param>
    ''' <param name="elements">  . </param>
    ''' <returns> A Double() </returns>
    Private Overloads Function SineWave(ByVal frequency As Double, ByVal phase As Double, ByVal elements As Integer) As Double()

        ' get the signal
        Dim signal() As Double = isr.Algorithms.Signals.Signal.Sine(frequency, phase, elements)

        ' Plot the Signal 
        Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture) / elements, elements), signal)

        Return signal

    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A Double() </returns>
    Private Function SineWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Double = Double.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        Return Me.SineWave(signalCycles, signalPhase, signalPoints)

    End Function

    ''' <summary> Calculates the sine wave. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A Single() </returns>
    Private Function SineWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Single = Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Single = Convert.ToSingle(isr.Algorithms.Signals.Signal.ToRadians(
            Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
        Return Me.SineWave(signalCycles, signalPhase, signalPoints)

    End Function

    ''' <summary> The copy signal format. </summary>
    Private Const _CopySigFormat As String = "    Copy Signal:  {0:0.###} ms "

    ''' <summary> The FFT initialize format. </summary>
    Private Const _FftInitFormat As String = " FFT Initialize:  {0:0} ms "

    ''' <summary> The FFT calculate format. </summary>
    Private Const _FftCalcFormat As String = "  FFT Calculate:  {0:0.###} ms "

    ''' <summary> The frq calculate format. </summary>
    Private Const _FrqCalcFormat As String = "FFT Frequencies:  {0:0.###} ms "

    ''' <summary> The magnitude calculate format. </summary>
    Private Const _MagCalcFormat As String = "  FFT Magnitude:  {0:0.###} ms "

    ''' <summary> The inverse calculate format. </summary>
    Private Const _InvCalcFormat As String = "    Inverse FFT:  {0:0.###} ms "

    ''' <summary> The charting format. </summary>
    Private Const _ChartingFormat As String = "  Charting time:  {0:0} ms "

    #End Region

    #Region " SPECTRUM: DOUBLE "

    ''' <summary>
    ''' Calculates the Spectrum based on the selected algorithm and displays the results.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub CalculateSpectrumDouble(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Dim timingBuilder As New System.Text.StringBuilder()
        timingBuilder.AppendLine(algorithm.Description)

        Me.ShowStatus($"Calculating double-precision {algorithm.Description} FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Double
        ReDim real(fftPoints - 1)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Double
        ReDim imaginary(fftPoints - 1)

        ' ------------ SELECT PROPERTIES ---------------

        ' select the spectrum type
        Dim spectrum As isr.Algorithms.Signals.Spectrum
        Dim fft As isr.Algorithms.Signals.FourierTransformBase
        Select Case algorithm
            Case Example.DiscreteFourierTransform
                fft = New isr.Algorithms.Signals.DiscreteFourierTransform
            Case Example.WisdomFFT
                fft = New isr.Algorithms.Signals.Wisdom.Dft
            Case Else
                Return
        End Select

        ' scale the FFT by the Window power and data points
        ' Remove mean before calculating the fft
        spectrum = New isr.Algorithms.Signals.Spectrum(fft) With {
            .IsScaleFft = True,
            .IsRemoveMean = Me._RemoveMeanCheckBox.Checked
        }

        ' Use Taper Window as selected
        spectrum.TaperWindow = If(Me._TaperWindowCheckBox.Checked, New isr.Algorithms.Signals.BlackmanTaperWindow, Nothing)

        ' Initialize the FFT.
        timeKeeper.Start()
        spectrum.Initialize(real, imaginary)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_FftInitFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' ------------ CREATE TEH SIGNAL ---------------

        timeKeeper.Start()
        signal.CopyTo(real, 0)
        Array.Clear(imaginary, 0, imaginary.Length)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_CopySigFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Compute the FFT.
        timeKeeper.Start()
        spectrum.Calculate(real, imaginary)
        duration = timeKeeper.Elapsed
        Me._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms"
        timingBuilder.AppendFormat(_FftCalcFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' ------- Calculate FFT outcomes ---------

        ' You must re-map the FFT utilities if using more than one time series at a time
        ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

        ' Get the FFT magnitudes
        Dim magnitudes() As Double
        isr.Algorithms.Signals.DoubleExtensions.Magnitudes(real, imaginary)

        timeKeeper.Start()
        magnitudes = isr.Algorithms.Signals.DoubleExtensions.Magnitudes(real, imaginary)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_MagCalcFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' Get the Frequency
        Dim frequencies() As Double

        timeKeeper.Start()
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_FrqCalcFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' Compute the inverse transform.
        timeKeeper.Start()
        fft.Inverse(real, imaginary)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_InvCalcFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' Display the forward and inverse data.
        Me.DisplayCalculationAccuracy(signal, real)

        ' ------------ Plot FFT outcome ---------------
        timeKeeper.Start()
        Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_ChartingFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()
        Me._TimingTextBox.Text = timingBuilder.ToString

    End Sub

    ''' <summary>
    ''' Calculates the Spectrum based on the selected algorithm and displays the results.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="algorithm"> The algorithm. </param>
    Private Sub CalculateSpectrumSingle(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Dim timingBuilder As New System.Text.StringBuilder()
        timingBuilder.AppendLine(algorithm.Description)

        Me.ShowStatus($"Calculating single-precision {algorithm.Description} FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the sine wave.
        Dim signal() As Single = Me.SineWaveSingle()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Single
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Single
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim spectrum As isr.Algorithms.Signals.Spectrum
        Dim fft As isr.Algorithms.Signals.FourierTransformBase
        Select Case algorithm
            Case Example.DiscreteFourierTransform
                fft = New isr.Algorithms.Signals.DiscreteFourierTransform
            Case Example.WisdomFFT
                fft = New isr.Algorithms.Signals.Wisdom.Dft
            Case Else
                Return
        End Select
        ' scale the FFT by the Window power and data points
        ' Remove mean before calculating the fft
        spectrum = New isr.Algorithms.Signals.Spectrum(fft) With {
            .IsScaleFft = True,
            .IsRemoveMean = Me._RemoveMeanCheckBox.Checked
        }

        ' Use Taper Window as selected
        spectrum.TaperWindow = If(Me._TaperWindowCheckBox.Checked, New isr.Algorithms.Signals.BlackmanTaperWindow, Nothing)

        ' Initialize the FFT.
        timeKeeper.Start()
        spectrum.Initialize(real, imaginary)
        duration = timeKeeper.Elapsed
        timingBuilder.AppendFormat(_FftInitFormat, duration.TotalMilliseconds)
        timingBuilder.AppendLine()

        ' Compute the FFT.
        timeKeeper.Start()
        spectrum.Calculate(real, imaginary)
        duration = timeKeeper.Elapsed

        ' Display time
        Me._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms "

        ' ------- Calculate FFT outcomes ---------

        ' You must re-map the FFT utilities if using more than one time series at a time
        ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

        ' Get the FFT magnitudes
        Dim magnitudes() As Single
        magnitudes = isr.Algorithms.Signals.SingleExtensions.Magnitudes(real, imaginary)

        ' Get the Frequency
        Dim frequencies() As Single
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0F, fftPoints)

        ' Compute the inverse transform.
        fft.Inverse(real, imaginary)

        ' Display the forward and inverse data.
        Me.DisplayCalculationAccuracy(signal, real)

        ' ------------ Plot FFT outcome ---------------
        Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

    End Sub

    ''' <summary>
    ''' Executes a single-precision FFT using the selected algorithm and displays the results.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="algorithm"> The algorithm. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub CalculateSpectrumSingle2(ByVal algorithm As Example)

        Me.ShowStatus($"Calculating single-precision {algorithm.Description} FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Single = Me.SineWaveSingle()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Single
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Single
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the FFTW class
        Dim fft As isr.Algorithms.Signals.Wisdom.Dft = New isr.Algorithms.Signals.Wisdom.Dft
        ' scale the FFT by the Window power and data points
        ' Remove mean before calculating the fft
        Dim spectrum As isr.Algorithms.Signals.Spectrum = New isr.Algorithms.Signals.Spectrum(fft) With {
            .IsScaleFft = True,
            .IsRemoveMean = Me._RemoveMeanCheckBox.Checked
        }

        ' Use Window as selected
        spectrum.TaperWindow = If(Me._TaperWindowCheckBox.Checked, New isr.Algorithms.Signals.BlackmanTaperWindow, Nothing)

        ' Compute the FFT.
        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        timeKeeper.Start()
        spectrum.Calculate(real, imaginary)
        Dim duration As TimeSpan = timeKeeper.Elapsed

        ' Display time
        Me._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms"

        ' ------- Calculate FFT outcomes ---------

        ' You must re-map the FFT utilities if using more than one time series at a time
        ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

        ' Get the FFT magnitudes
        Dim magnitudes() As Single
        magnitudes = isr.Algorithms.Signals.SingleExtensions.Magnitudes(real, imaginary)

        ' Get the Frequency
        Dim frequencies() As Single
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0F, fftPoints)

        ' Compute the inverse transform.
        fft.Inverse(real, imaginary)

        ' Display the forward and inverse data.
        Me.DisplayCalculationAccuracy(signal, real)

        ' ------------ Plot FFT outcome ---------------

        Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="signal"> . </param>
    ''' <param name="real">   . </param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Double, ByVal real() As Double)

        If signal Is Nothing Then
            Exit Sub
        End If
        If real Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(real.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._FftListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - real(i)
            totalError += value * value
            Me._FftListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture, _ListFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, real(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}"
        Me._MainStatusBar.Invalidate()

    End Sub

    ''' <summary> Displays the test results. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="signal"> . </param>
    ''' <param name="real">   . </param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Single, ByVal real() As Single)

        If signal Is Nothing Then
            Exit Sub
        End If
        If real Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(real.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._FftListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - real(i)
            totalError += value * value
            Me._FftListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture, _ListFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, real(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}"
        Me._MainStatusBar.Invalidate()

    End Sub

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks>
    ''' David, 11/8/2004. Created <para>
    ''' It seems that out enumerated list does not work very well with this list. </para>
    ''' </remarks>
    Private Sub PopulateExampleComboBox()

        ' set the action list
        Me._ExampleComboBox.Items.Clear()
        Me._ExampleComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(Example)).ToList
        Me._ExampleComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._ExampleComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)

    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    '''                            observing the changes in the spectrum. </param>
    Private Sub SlidingFft(ByVal noiseFigure As Double)

        Me.ShowStatus("Calculating double-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Double
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Double
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the FFTW class
        Dim fft As isr.Algorithms.Signals.Wisdom.Dft = New isr.Algorithms.Signals.Wisdom.Dft

        ' Compute the FFT.
        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        timeKeeper.Start()
        fft.Forward(real, imaginary)
        Dim duration As TimeSpan = timeKeeper.Elapsed

        ' Display time
        Me._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms"

        ' Clear the data
        Me._FftListBox.Items.Clear()

        ' Clear the error value
        Me._ErrorStatusBarPanel.Text = String.Empty

        ' Get the FFT magnitudes
        Dim magnitudes() As Double
        magnitudes = isr.Algorithms.Signals.DoubleExtensions.Magnitudes(real, imaginary)

        ' Get the Frequency
        Dim frequencies() As Double
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

        ' Initialize the last point of the signal to the last point.
        Dim lastSignalPoint As Integer = fftPoints - 1

        ' Initialize the first point of the signal to the last point.
        Dim firstSignalPoint As Integer = 0

        ' Calculate the phase of the last point of the sine wave
        Dim deltaPhase As Double = 2 * Math.PI * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / Convert.ToSingle(fftPoints)

        ' get the signal phase of the last point
        Dim signalPhase As Double = isr.Algorithms.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        signalPhase += deltaPhase * lastSignalPoint

        ' Use a new frequency for the sine wave to see how
        ' the old is phased out and the new gets in
        deltaPhase *= 2

        ' First element in the previous Real part of the time series
        Dim oldReal As Double

        ' First element in the previous imaginary part of the time series.
        Dim oldImaginary As Double

        ' New Real part of Signal
        Dim newReal As Double

        ' New imaginary part of Signal
        Dim newImaginary As Double

        ' Create a new instance of the sliding fft class
        Dim slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

        ' Initialize sliding FFT coefficients.
        slidingFft.Forward(real, imaginary)

        ' Recalculate the sliding FFT
        Do While Me._StartStopCheckBox.Checked

            ' Allow other events to occur
            Windows.Forms.Application.DoEvents()

            ' ------- Calculate FFT outcomes ---------

            ' You must re-map the FFT utilities if using more than one time series at a time
            ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

            ' Get the FFT magnitudes
            magnitudes = isr.Algorithms.Signals.DoubleExtensions.Magnitudes(real, imaginary)

            ' Get the Frequency
            frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0R, fftPoints)

            ' ------------ Plot the Signal ---------------

            Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

            ' ------------ Plot FFT outcome ---------------

            Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

            ' Update the previous values of the signal
            oldReal = signal(firstSignalPoint)
            oldImaginary = 0

            ' Get new signal values.
            signalPhase += deltaPhase
            newReal = System.Math.Sin(signalPhase)
            newImaginary = 0

            ' Add some random noise to make it interesting.
            newReal += noiseFigure * 2 * (Microsoft.VisualBasic.Rnd() - 0.5)

            ' Update the signal itself.

            ' Update the last point.
            lastSignalPoint += 1
            If lastSignalPoint >= fftPoints Then
                lastSignalPoint = 0
            End If

            ' Update the first point.
            firstSignalPoint += 1
            If firstSignalPoint >= fftPoints Then
                firstSignalPoint = 0
            End If

            ' Place new data in the signal itself.
            signal(lastSignalPoint) = newReal

            ' You must re-map the FFT utilities if using more than one time series at a time
            ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

            ' Calculate the sliding FFT coefficients.
            slidingFft.Update(newReal, newImaginary, oldReal, oldImaginary)

        Loop

    End Sub

    ''' <summary> Evaluates sliding FFT until stopped. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
    '''                            observing the changes in the spectrum. </param>
    Private Sub SlidingFft(ByVal noiseFigure As Single)

        Me.ShowStatus("Calculating single-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Single = Me.SineWaveSingle()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Single
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Single
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the FFTW class
        Dim fft As isr.Algorithms.Signals.Wisdom.Dft = New isr.Algorithms.Signals.Wisdom.Dft

        ' Compute the FFT.
        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        timeKeeper.Start()
        fft.Forward(real, imaginary)
        Dim duration As TimeSpan = timeKeeper.Elapsed

        ' Display time
        Me._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms"

        ' Clear the data
        Me._FftListBox.Items.Clear()
        Me._ErrorStatusBarPanel.Text = String.Empty

        ' Get the FFT magnitudes
        Dim magnitudes() As Single
        magnitudes = isr.Algorithms.Signals.SingleExtensions.Magnitudes(real, imaginary)

        ' Get the Frequency
        Dim frequencies() As Single
        frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0F, fftPoints)

        ' Init the last point of the signal to the last point.
        Dim lastSignalPoint As Integer = fftPoints - 1

        ' Init the first point of the signal to the last point.
        Dim firstSignalPoint As Integer = 0

        ' Calculate the phase of the last point of the sine wave
        Dim deltaPhase As Single = 2.0F * Convert.ToSingle(Math.PI) _
        * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints

        ' get the signal phase of the last point
        Dim signalPhase As Single = Convert.ToSingle(isr.Algorithms.Signals.Signal.ToRadians(
                        Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
        signalPhase += deltaPhase * lastSignalPoint

        ' Use a new frequency for the sine wave to see how
        ' the old is phased out and the new gets in
        deltaPhase *= 2

        Dim oldReal As Single ' First element in the previous
        ' real part of the time series.

        Dim oldImaginary As Single ' First element in the previous
        ' imaginary time series.

        Dim newReal As Single ' New Real part of Signal

        Dim newImaginary As Single ' New imaginary part of Signal

        ' Create a new instance of the sliding fft class
        Dim slidingFft As isr.Algorithms.Signals.SlidingFourierTransform = New isr.Algorithms.Signals.SlidingFourierTransform

        ' Initialize sliding FFT coefficients.
        slidingFft.Forward(real, imaginary)

        ' Recalculate the sliding FFT
        Do While Me._StartStopCheckBox.Checked

            ' Allow other events to occur
            Windows.Forms.Application.DoEvents()

            ' ------- Calculate FFT outcomes ---------

            ' You must re-map the FFT utilities if using more than one time series at a time
            ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

            ' Get the FFT magnitudes
            magnitudes = isr.Algorithms.Signals.SingleExtensions.Magnitudes(real, imaginary)

            ' Get the Frequency
            frequencies = isr.Algorithms.Signals.Signal.Frequencies(1.0F, fftPoints)

            ' ------------ Plot the Signal ---------------

            Me.ChartSignal(isr.Algorithms.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

            ' ------------ Plot FFT outcome ---------------

            Me.ChartAmplitudeSpectrum(frequencies, magnitudes, 1)

            ' Update the previous values of the signal
            oldReal = signal(firstSignalPoint)
            oldImaginary = 0

            ' Get new signal values.
            signalPhase += deltaPhase
            newReal = Convert.ToSingle(System.Math.Sin(signalPhase))
            newImaginary = 0

            ' Add some random noise to make it interesting.
            newReal += noiseFigure * 2.0F * (Microsoft.VisualBasic.Rnd() - 0.5F)

            ' Update the last point.
            lastSignalPoint += 1
            If lastSignalPoint >= fftPoints Then
                lastSignalPoint = 0
            End If

            ' Update the first point.
            firstSignalPoint += 1
            If firstSignalPoint >= fftPoints Then
                firstSignalPoint = 0
            End If

            ' Place new data in the signal itself.
            signal(lastSignalPoint) = newReal

            ' You must re-map the FFT utilities if using more than one time series at a time
            ' isr.Algorithms.Signals.Helper.Map(real, imaginary)

            ' Calculate the sliding FFT coefficients.
            slidingFft.Update(newReal, newImaginary, oldReal, oldImaginary)

        Loop

    End Sub

    #End Region

    #Region " CHARTING "

    #Region " AMPLITUDE SPECTRUM CHART "

    ''' <summary>Gets or sets reference to the amplitude spectrum curve.</summary>
    Private _AmplitudeSpectrumCurve As isr.Visuals.Curve

    ''' <summary>Gets or sets reference to the spectrum amplitude axis.</summary>
    Private _AmplitudeSpectrumAxis As isr.Visuals.Axis

    ''' <summary>Gets or sets reference to the spectrum frequency axis.</summary>
    Private _FrequencyAxis As isr.Visuals.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _SpectrumChartPan As isr.Visuals.ChartPane

    ''' <summary> Creates the chart for displaying the amplitude spectrum. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub CreateSpectrumChart()

        ' set chart area and titles.
        Me._SpectrumChartPan = New isr.Visuals.ChartPane With {
            .PaneArea = New RectangleF(10, 10, 10, 10)
        }
        Me._SpectrumChartPan.Title.Caption = "Spectrum"
        Me._SpectrumChartPan.Legend.Visible = False

        Me._FrequencyAxis = Me._SpectrumChartPan.AddAxis("Frequency, Hz", isr.Visuals.AxisType.X)
        Me._FrequencyAxis.Max = New isr.Visuals.AutoValueR(100, False)
        Me._FrequencyAxis.Min = New isr.Visuals.AutoValueR(1, False)

        Me._FrequencyAxis.Grid.Visible = True
        Me._FrequencyAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._FrequencyAxis.TickLabels.Visible = True
        Me._FrequencyAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(1, True)
        ' frequencyAxis.TickLabels.Appearance.Angle = 60.0F

        Me._AmplitudeSpectrumAxis = Me._SpectrumChartPan.AddAxis("Amplitude, Volts", isr.Visuals.AxisType.Y)
        Me._AmplitudeSpectrumAxis.CoordinateScale = New isr.Visuals.CoordinateScale(isr.Visuals.CoordinateScaleType.Linear)

        Me._AmplitudeSpectrumAxis.Title.Visible = True

        Me._AmplitudeSpectrumAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._AmplitudeSpectrumAxis.Min = New isr.Visuals.AutoValueR(0, True)

        Me._AmplitudeSpectrumAxis.Grid.Visible = True
        Me._AmplitudeSpectrumAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._AmplitudeSpectrumAxis.TickLabels.Visible = True
        Me._AmplitudeSpectrumAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(0, True)

        Me._SpectrumChartPan.AxisFrame.FillColor = Color.WhiteSmoke 'color.FromArgb(232, 236, 245) '  Color.LightGoldenrodYellow

        Me._SpectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)

        Me._AmplitudeSpectrumCurve = Me._SpectrumChartPan.AddCurve(isr.Visuals.CurveType.XY, "Amplitude Spectrum", Me._FrequencyAxis, Me._AmplitudeSpectrumAxis)
        Me._AmplitudeSpectrumCurve.Cord.LineColor = Color.Red
        Me._AmplitudeSpectrumCurve.Cord.CordType = isr.Visuals.CordType.Linear
        Me._AmplitudeSpectrumCurve.Symbol.Visible = False
        Me._AmplitudeSpectrumCurve.Cord.LineWidth = 1.0F

        Me._SpectrumChartPan.Rescale()

    End Sub

    ''' <summary>
    ''' Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequencies"> Holds the spectrum frequencies. </param>
    ''' <param name="magnitudes">  Holds the spectrum amplitudes. </param>
    ''' <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
    '''                            plotting data that was not scaled. </param>
    Private Sub ChartAmplitudeSpectrum(ByVal frequencies() As Single, ByVal magnitudes() As Single,
      ByVal scaleFactor As Single)

        ' copy the amplitudes to a double array
        Dim amplitudes(frequencies.Length - 1) As Double
        magnitudes.CopyTo(amplitudes, 0)

        ' copy the frequencies to a double array.
        Dim freq(frequencies.Length - 1) As Double
        frequencies.CopyTo(freq, 0)
        Me.ChartAmplitudeSpectrum(freq, amplitudes, scaleFactor)

    End Sub

    ''' <summary>
    ''' Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="frequencies"> Holds the spectrum frequencies. </param>
    ''' <param name="magnitudes">  Holds the spectrum amplitudes. </param>
    ''' <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
    '''                            plotting data that was not scaled. </param>
    Private Sub ChartAmplitudeSpectrum(ByVal frequencies() As Double, ByVal magnitudes() As Double,
      ByVal scaleFactor As Double)

        ' adjust abscissa scale.
        Me._FrequencyAxis.Min = New isr.Visuals.AutoValueR(frequencies(frequencies.GetLowerBound(0)), False)
        Me._FrequencyAxis.Max = New isr.Visuals.AutoValueR(frequencies(frequencies.GetUpperBound(0)), False)

        ' adjust ordinate scale.
        Me._AmplitudeSpectrumAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._AmplitudeSpectrumAxis.Min = New isr.Visuals.AutoValueR(0, True)

        ' Set initial value for frequency
        '    Dim currentFrequency As Double = Gopher.AmplitudeSectrumChart.AbscissaMin.Value
        '   Dim deltaFrequency As Double = 1.0# / Gopher.Test.EpochDuration

        ' plot all but last, which was plotted above

        ' scale the magnitudes
        Dim amplitudes(frequencies.Length - 1) As Double
        For i As Integer = frequencies.GetLowerBound(0) To frequencies.GetUpperBound(0)

            amplitudes(i) = scaleFactor * magnitudes(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        Me._AmplitudeSpectrumCurve.UpdateData(frequencies, amplitudes)
        Me._SpectrumChartPan.Rescale()
        Me._SpectrumChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the amplitude spectrum Chart. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub SpectrumChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SpectrumChartPanel.Paint
        If Not Me._SpectrumChartPan Is Nothing Then
            e.Graphics.FillRectangle(New SolidBrush(Color.Gray), Me.ClientRectangle)
            Me._SpectrumChartPan.Draw(e.Graphics)
        End If
    End Sub

    ''' <summary> Redraws the amplitude spectrum chart when the form is resized. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the
    '''                                               chart panel
    '''                                               <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub SpectrumChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SpectrumChartPanel.Resize
        If Not Me._SpectrumChartPan Is Nothing Then
            Me._SpectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)
        End If
    End Sub

    #End Region

    #Region " SIGNAL CHART "

    ''' <summary>Gets or sets reference to the instantaneous DP curve.</summary>
    Private _SignalCurve As isr.Visuals.Curve

    ''' <summary>Gets or sets reference to the instantaneous DP amplitude axis.</summary>
    Private _SignalAxis As isr.Visuals.Axis

    ''' <summary>Gets or sets reference to the instantaneous DP time axis.</summary>
    Private _TimeAxis As isr.Visuals.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _SignalChartPane As isr.Visuals.ChartPane

    ''' <summary> Creates the Scope for display of voltages during the epoch. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub CreateSignalChart()

        ' set chart area and titles.
        Me._SignalChartPane = New isr.Visuals.ChartPane With {
            .PaneArea = New RectangleF(10, 10, 10, 10)
        }
        Me._SignalChartPane.Title.Caption = "Voltage versus Time"
        Me._SignalChartPane.Legend.Visible = False

        Me._TimeAxis = Me._SignalChartPane.AddAxis("Time, Seconds", isr.Visuals.AxisType.X)
        Me._TimeAxis.Max = New isr.Visuals.AutoValueR(1, False)
        Me._TimeAxis.Min = New isr.Visuals.AutoValueR(0, False)

        Me._TimeAxis.Grid.Visible = True
        Me._TimeAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._TimeAxis.TickLabels.Visible = True
        Me._TimeAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(1, True)
        ' timeAxis.TickLabels.Appearance.Angle = 60.0F

        Me._SignalAxis = Me._SignalChartPane.AddAxis("Volts", isr.Visuals.AxisType.Y)
        Me._SignalAxis.CoordinateScale = New isr.Visuals.CoordinateScale(isr.Visuals.CoordinateScaleType.Linear)

        Me._SignalAxis.Title.Visible = True

        Me._SignalAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._SignalAxis.Min = New isr.Visuals.AutoValueR(-1, True)

        Me._SignalAxis.Grid.Visible = True
        Me._SignalAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        Me._SignalAxis.TickLabels.Visible = True
        Me._SignalAxis.TickLabels.DecimalPlaces = New isr.Visuals.AutoValue(0, True)

        Me._SignalChartPane.AxisFrame.FillColor = Color.WhiteSmoke '  Color.FromArgb(232, 236, 245) ' Color.LightGoldenrodYellow
        Me._SignalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)

        Me._SignalCurve = Me._SignalChartPane.AddCurve(isr.Visuals.CurveType.XY, "TimeSeries", Me._TimeAxis, Me._SignalAxis)
        Me._SignalCurve.Cord.LineColor = Color.Red
        Me._SignalCurve.Cord.CordType = isr.Visuals.CordType.Linear
        Me._SignalCurve.Symbol.Visible = False
        Me._SignalCurve.Cord.LineWidth = 1.0F

        Me._SignalChartPane.Rescale()

    End Sub

    ''' <summary>
    ''' Plot the DP signal from the entire epoch data simulating a strip chart effect.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub ChartSignal(ByVal times() As Single, ByVal amplitudes() As Single)

        ' plot all but last, which was plotted above
        Dim newApms(times.Length - 1) As Double
        Dim newTimes(times.Length - 1) As Double
        For i As Integer = times.GetLowerBound(0) To times.GetUpperBound(0)

            newApms(i) = amplitudes(i)
            newTimes(i) = times(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        Me.ChartSignal(newTimes, newApms)

    End Sub

    ''' <summary>
    ''' Plot the DP signal from the entire epoch data simulating a strip chart effect.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="times">      The times. </param>
    ''' <param name="amplitudes"> The amplitudes. </param>
    Private Sub ChartSignal(ByVal times() As Double, ByVal amplitudes() As Double)

        Me._TimeAxis.Max = New isr.Visuals.AutoValueR(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture), False) ' times(UBound(times)), False)
        Me._TimeAxis.Min = New isr.Visuals.AutoValueR(0, False)  ' times(LBound(times)), False)

        Me._SignalAxis.Max = New isr.Visuals.AutoValueR(1, True)
        Me._SignalAxis.Min = New isr.Visuals.AutoValueR(-1, True)

        Me._SignalCurve.UpdateData(times, amplitudes)
        Me._SignalChartPane.Rescale()

        Me._SignalChartPanel.Invalidate()

    End Sub

    ''' <summary> Redraws the Scope Chart. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub ScopeChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SignalChartPanel.Paint
        If Not Me._SignalChartPane Is Nothing Then
            e.Graphics.FillRectangle(New SolidBrush(Color.Gray), Me.ClientRectangle)
            Me._SignalChartPane.Draw(e.Graphics)
        End If
    End Sub

    ''' <summary> Redraws the Scope chart when the form is resized. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of the
    '''                                               chart panel
    '''                                               <see cref="System.Windows.Forms.Panel"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub ScopeChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChartPanel.Resize
        If Not Me._SignalChartPane Is Nothing Then
            Me._SignalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)
        End If
    End Sub

    ''' <summary>
    ''' Includes sample code for special chart elements.  We can use the arrow and label to point to
    ''' a potential resonance.  This could be turned on or off from the chart context menu.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="signalChartPane"> Charts spectrum. </param>
    ''' <param name="timeAxis">        Gets or sets reference to the instantaneous DP time axis. </param>
    ''' <param name="signalAxis">      Gets or sets reference to the instantaneous DP amplitude
    '''                                axis. </param>
    Private Sub AddSpecialChartElements(signalChartPane As Visuals.ChartPane, ByVal timeAxis As Visuals.Axis, ByVal signalAxis As Visuals.Axis)

        Dim curve As isr.Visuals.Curve

        Dim x As Double() = {72, 200, 300, 400, 500, 600, 700, 800, 900, 1000}
        Dim y As Double() = {20, 10, 50, 40, 35, 60, 90, 25, 48, 75}
        curve = signalChartPane.AddCurve("Larry", x, y, Color.Red, Visuals.ShapeType.Circle, timeAxis, signalAxis)
        curve.Symbol.Size = 14
        curve.Cord.LineWidth = 2.0F

        Dim x2 As Double() = {300, 400, 500, 600, 700, 800, 900}
        Dim y2 As Double() = {75, 43, 27, 62, 89, 73, 12}
        curve = signalChartPane.AddCurve("Moe", x2, y2, Color.Blue, isr.Visuals.ShapeType.Diamond, timeAxis, signalAxis)
        curve.Cord.LineWidth = 2.0F
        curve.Cord.Visible = False
        curve.Symbol.Filled = True
        curve.Symbol.Size = 14

        Dim x3 As Double() = {150, 250, 400, 520, 780, 940}
        Dim y3 As Double() = {5.2, 49.0, 33.8, 88.57, 99.9, 36.8}
        curve = signalChartPane.AddCurve("Curly", x3, y3, Color.Green, isr.Visuals.ShapeType.Triangle, timeAxis, signalAxis)
        curve.Symbol.Size = 14
        curve.Cord.LineWidth = 2.0F
        curve.Symbol.Filled = True

        Dim [text] As isr.Visuals.TextBox = signalChartPane.AddTextBox($"First Prod {Environment.NewLine} 21-Oct-99", 100.0F, 50.0F)
        [text].Appearance.Italic = True
        [text].Alignment = New isr.Visuals.Alignment(isr.Visuals.HorizontalAlignment.Center, isr.Visuals.VerticalAlignment.Bottom)
        [text].Appearance.Frame.FillColor = Color.LightBlue

        Dim arrow As isr.Visuals.Arrow = signalChartPane.AddArrow(Color.Black, 12.0F, 100.0F, 47.0F, 72.0F, 25.0F)
        arrow.CoordinateFrame = isr.Visuals.CoordinateFrameType.AxisXYScale

        [text] = signalChartPane.AddTextBox("Upgrade", 700.0F, 50.0F)
        [text].CoordinateFrame = isr.Visuals.CoordinateFrameType.AxisXYScale
        [text].Appearance.Angle = 90
        [text].Appearance.FontColor = Color.Black
        [text].Appearance.Frame.Filled = True
        [text].Appearance.Frame.FillColor = Color.WhiteSmoke ' Color.FromArgb(232, 236, 245) ' Color.LightGoldenrodYellow
        [text].Appearance.Frame.Visible = False
        [text].Alignment = New isr.Visuals.Alignment(isr.Visuals.HorizontalAlignment.Right, isr.Visuals.VerticalAlignment.Center)

        arrow = signalChartPane.AddArrow(Color.Black, 15, 700, 53, 700, 80)
        arrow.CoordinateFrame = isr.Visuals.CoordinateFrameType.AxisXYScale
        arrow.LineWidth = 2.0F

        [text] = signalChartPane.AddTextBox("Confidential", 0.8F, -0.03F)
        [text].CoordinateFrame = isr.Visuals.CoordinateFrameType.AxisFraction
        [text].Appearance.Angle = 15.0F
        [text].Appearance.FontColor = Color.Red
        [text].Appearance.Bold = True
        [text].Appearance.FontSize = 16
        [text].Appearance.Frame.Visible = True
        [text].Appearance.Frame.LineColor = Color.Red
        [text].Alignment = New isr.Visuals.Alignment(isr.Visuals.HorizontalAlignment.Left, isr.Visuals.VerticalAlignment.Bottom)
    End Sub

    #End Region

    #End Region

    #Region " CONTROL EVENT HANDLERS "

    ''' <summary> Cycles text box validating. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub CyclesTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _CyclesTextBox.Validating

        Me._ErrorErrorProvider.SetError(Me._CyclesTextBox, String.Empty)
        Dim value As Integer = 0
        If Integer.TryParse(Me._CyclesTextBox.Text, value) Then
            If value < 1 Then
                e.Cancel = True
                Me._ErrorErrorProvider.SetError(Me._CyclesTextBox, "Must exceed 1")
            Else
                Me.SineWaveDouble()
            End If
        Else
            e.Cancel = True
            Me._ErrorErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Phase text box validating. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PhaseTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PhaseTextBox.Validating

        Me._ErrorErrorProvider.SetError(Me._PhaseTextBox, String.Empty)
        Dim value As Integer
        If Integer.TryParse(Me._PhaseTextBox.Text, value) Then
            Me.SineWaveDouble()
        Else
            e.Cancel = True
            Me._ErrorErrorProvider.SetError(Me._PhaseTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Points text box validating. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PointsTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsTextBox.Validating

        Me._ErrorErrorProvider.SetError(Me._PointsTextBox, String.Empty)
        Dim value As Integer
        If Integer.TryParse(Me._PointsTextBox.Text, value) Then
            If value < 2 Then
                e.Cancel = True
                Me._ErrorErrorProvider.SetError(Me._PointsTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If value < Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
                Me.SineWaveDouble()
            End If
        Else
            e.Cancel = True
            Me._ErrorErrorProvider.SetError(Me._PointsTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Points to display text box validating. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub PointsToDisplayTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsToDisplayTextBox.Validating

        Me._ErrorErrorProvider.SetError(Me._PointsToDisplayTextBox, String.Empty)
        Dim value As Integer
        If Integer.TryParse(Me._PointsToDisplayTextBox.Text, value) Then
            If value < 2 Then
                e.Cancel = True
                Me._ErrorErrorProvider.SetError(Me._PointsToDisplayTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) < value Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
            End If
        Else
            e.Cancel = True
            Me._ErrorErrorProvider.SetError(Me._PointsToDisplayTextBox, "Enter numeric value")
        End If

    End Sub

    ''' <summary> Starts stop check box checked changed. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartStopCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopCheckBox.CheckedChanged

        Me._StartStopCheckBox.Text = If(Me._StartStopCheckBox.Checked, "&Stop", "&Start")

        If Me._StartStopCheckBox.Enabled AndAlso Me._StartStopCheckBox.Checked Then

            Dim algorithm As Example = Me.SelectedExample

            Select Case algorithm

                Case Example.DiscreteFourierTransform, Example.WisdomFFT

                    Me._CountStatusBarPanel.Text = "0"
                    Do
                        If Me._DoubleRadioButton.Checked Then
                            Me.CalculateSpectrumDouble(algorithm)
                        Else
                            Me.CalculateSpectrumSingle(algorithm)
                        End If
                        Windows.Forms.Application.DoEvents()
                        Dim count As Integer = Integer.Parse(Me._CountStatusBarPanel.Text, Globalization.CultureInfo.CurrentCulture) + 1
                        Me._CountStatusBarPanel.Text = count.ToString(Globalization.CultureInfo.CurrentCulture)
                    Loop While Me._StartStopCheckBox.Checked And False

                Case Example.SlidingFFT

                    If Me._DoubleRadioButton.Checked Then
                        Me.SlidingFft(0.5R)
                    Else
                        Me.SlidingFft(0.5F)
                    End If

                Case Else

            End Select

            If Me._StartStopCheckBox.Checked Then
                Me._StartStopCheckBox.Enabled = False
                Me._StartStopCheckBox.Checked = False
                Me._StartStopCheckBox.Enabled = True
            End If

        Else

            Me._CountStatusBarPanel.Text = "0"

        End If

    End Sub

    #End Region

    #Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    #End Region

End Class
