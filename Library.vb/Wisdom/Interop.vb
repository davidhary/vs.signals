Imports System.Runtime.InteropServices
Imports System.Diagnostics.CodeAnalysis

Namespace Wisdom

#Region " ENUMERATIONS USED BY FFTW "

    ''' <summary>
    ''' FFTW planner options.
    ''' <para>
    ''' All of the planner routines in FFTW accept an integer options argument, which is a bitwise OR
    ''' of zero or more of the flag constants. These options control the rigor (and time) of the
    ''' planning process, and can also impose (or lift) restrictions on the type of transform
    ''' algorithm that is employed.
    ''' </para>
    ''' <para>
    ''' The options argument is usually either <see cref="PlannerOptions.Measure">measure</see> or
    ''' <see cref="PlannerOptions.Estimate">estimate</see>.
    ''' <see cref="PlannerOptions.Measure">measure</see>
    ''' instructs FFTW to run and measure the execution time of several FFTs in order to find the
    ''' best way to compute the transform of size n. This process takes some time (usually a few
    ''' seconds), depending on your machine and on the size of the transform.
    ''' <see cref="PlannerOptions.Estimate">estimate</see>, on the contrary, does not run any
    ''' computation
    ''' and just builds a reasonable plan that is probably sub-optimal. In short, if your program
    ''' performs many transforms of the same size and initialization time is not important, use
    ''' <see cref="PlannerOptions.Measure">measure</see>; otherwise use the estimate. The
    ''' data in the in/out arrays is overwritten during
    ''' <see cref="PlannerOptions.Measure">measure</see> planning, so such planning
    ''' should be done before the input is initialized by the user.
    ''' </para>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue"), Flags()>
    Public Enum PlannerOptions As Integer

        ''' <summary>
        ''' Tells FFTW to find an optimized plan by actually computing several FFTs and measuring their execution time. 
        ''' Depending on your machine, this can take some time (often a few seconds). Default (0x0). 
        ''' </summary>
        <System.ComponentModel.Description("Measure")> Measure = 0

        ''' <summary>
        ''' Specifies that an out-of-place transform is allowed to overwrite its 
        ''' input array with arbitrary data; this can sometimes allow more efficient algorithms to be employed.
        ''' </summary>
        <System.ComponentModel.Description("Destroy Input")> DestroyInput = 1

        ''' <summary>
        ''' Rarely used. Specifies that the algorithm may not impose any unusual alignment requirements on the input/output 
        ''' arrays (i.e. no SIMD). This flag is normally not necessary, since the planner automatically detects 
        ''' misaligned arrays. The only use for this flag is if you want to use the guru interface to execute a given 
        ''' plan on a different array that may not be aligned like the original. 
        ''' </summary>
        <System.ComponentModel.Description("Unaligned")> Unaligned = 2

        ''' <summary>
        ''' Not used.
        ''' </summary>
        <System.ComponentModel.Description("Conserve Memory")> ConserveMemory = 4

        ''' <summary>
        ''' Like Patient, but considers an even wider range of algorithms, including many that we think are 
        ''' unlikely to be fast, to produce the most optimal plan but with a substantially increased planning time. 
        ''' </summary>
        <System.ComponentModel.Description("Exhaustive")> Exhaustive = 8

        ''' <summary>
        ''' Specifies that an out-of-place transform must not change its input array. 
        ''' </summary>
        ''' <remarks>
        ''' This is ordinarily the default, 
        ''' except for c2r and hc2r (i.e. complex-to-real) transforms for which DestroyInput is the default. 
        ''' In the latter cases, passing PreserveInput will attempt to use algorithms that do not destroy the 
        ''' input, at the expense of worse performance; for multi-dimensional c2r transforms, however, no 
        ''' input-preserving algorithms are implemented and the planner will return null if one is requested.
        ''' </remarks>
        <System.ComponentModel.Description("Preserve Input")> PreserveInput = 16

        ''' <summary>
        ''' Like Measure, but considers a wider range of algorithms and often produces a “more optimal” plan 
        ''' (especially for large transforms), but at the expense of several times longer planning time 
        ''' (especially for large transforms).
        ''' </summary>
        <System.ComponentModel.Description("Patient")> Patient = 32

        ''' <summary>
        ''' Specifies that, instead of actual measurements of different algorithms, a simple heuristic is 
        ''' used to pick a (probably sub-optimal) plan quickly. With this flag, the input/output arrays 
        ''' are not overwritten during planning. 
        ''' </summary>
        <System.ComponentModel.Description("Estimate")> Estimate = 64
    End Enum

    ''' <summary>
    ''' Defines direction of operation. This can be either
    ''' <see cref="TransformDirection.Forward">forward</see> (-1) or
    ''' <see cref="TransformDirection.Backward">inverse</see> (+1), and indicates the direction of
    ''' the transform.
    ''' Technically, it is the sign of the exponent in the transform.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")>
    Public Enum TransformDirection As Integer

        ''' <summary>
        ''' Computes a regular DFT
        ''' </summary>
        <System.ComponentModel.Description("Forward DFT")> Forward = -1

        ''' <summary>
        ''' Computes the inverse DFT
        ''' </summary>
        <System.ComponentModel.Description("Inverse DFT")> Backward = 1
    End Enum

    ''' <summary> Kinds of real-to-real transforms. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    <SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")>
    Public Enum TransformKind As Integer

        ''' <summary>
        ''' R2HC Computes a real-input DFT with output in half-complex format, i.e. real and imaginary-parts 
        ''' for a transform of size n stored as: r0, r1, r2, . . . , r(n over 2), i(n+1) over 2−1, . . . , i2, i1
        ''' </summary>
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
      System.ComponentModel.Description("Real-input DFT with output in half-complex format")> R2HC = 0 ' R2HC
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("Reverse of real-input DFT with input in half-complex format")> HC2R = 1 ' HC2R 
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("Discrete Hartley Transform.")> DHT = 2 ' DHT
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DCT-I: Even around 0 and n-1")> REDFT00 = 3 ' REDFT00
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DCT-III: The IDCT: even around 0 and odd n")> REDFT01 = 4 ' REDFT01
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DCT-II: The DCT: even around −0.5 and n − 0.5")> REDFT10 = 5 ' REDFT10
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DCT-IV: Even around −0.5 and odd around n − 0.5")> REDFT11 = 6 ' REDFT11
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DST-I): odd around −1 and n.")> RODFT00 = 7 ' RODFT00
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("DST-III: odd around -1 and even around n − 1.")> RODFT01 = 8 ' RODFT01
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("(DST-II): odd around j −0.5 and n − 0.5.")> RODFT10 = 9 ' RODFT10
        <SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", MessageId:="Member"),
        System.ComponentModel.Description("(DST-IV): odd around j −0.5 and even around n − 0.5.")> RODFT11 = 10 ' RODFT11
    End Enum

#End Region

    Namespace FftwF

#Region " FftwF.SafeNativeMethods: SINGLE PRECISION INTEROP CLASS "

        ''' <summary>
        ''' Contains the Basic Interface FFTW functions for single-precision (float) operations. Contains
        ''' safe application programming interface calls suppressing stack walks for unmanaged code
        ''' permission.  Provides methods that are safe for anyone to call. Callers of these methods are
        ''' not required to do a full security review to ensure that the usage is secure because the
        ''' methods are harmless for any caller.
        ''' </summary>
        ''' <remarks>
        ''' David, 09/01/07" by="Tamas Szalay" revision="2.0.2800.x <para>
        ''' From http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html.
        ''' </para>
        ''' </remarks>
        Public NotInheritable Class SafeNativeMethods

            ''' <summary> Prevents construction of this class. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            Private Sub New()
            End Sub

            ''' <summary> Allocates FFTW-optimized unmanaged memory. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="length"> Amount to allocate, in bytes. </param>
            ''' <returns> Pointer to allocated memory. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_malloc", ExactSpelling:=True)>
            Friend Shared Function Malloc(ByVal length As Integer) As IntPtr
            End Function

            ''' <summary> Deallocates memory allocated by FFTW malloc. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="pointer"> Pointer to memory to release. </param>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_free", ExactSpelling:=True)>
            Friend Shared Sub Free(ByVal pointer As IntPtr)
            End Sub

            ''' <summary> Deallocates an FFTW plan and all associated resources. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="plan"> Pointer to the plan to release. </param>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_destroy_plan", ExactSpelling:=True)>
            Friend Shared Sub Destroy_plan(ByVal plan As IntPtr)
            End Sub

            ''' <summary>
            ''' Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
            ''' free.
            ''' </summary>
            ''' <remarks>
            ''' After calling fftw_cleanup, all existing plans become undefined, and you should not attempt
            ''' to execute them nor to destroy them. You can however create and execute/destroy new plans, in
            ''' which case FFTW starts accumulating wisdom information again. fftw_cleanup does not
            ''' deallocate your plans; you should still call fftw_destroy_plan for this purpose.
            ''' </remarks>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_cleanup", ExactSpelling:=True)>
            Friend Shared Sub Cleanup()
            End Sub

            ''' <summary> Sets the maximum time that can be used by the planner. </summary>
            ''' <remarks>
            ''' This function instructs FFTW to spend at most seconds seconds (approximately) in the planner.
            ''' If seconds == -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW
            ''' plans with a progressively wider range of algorithms until the the given time limit is
            ''' reached or the given range of algorithms is explored, returning the best available plan. For
            ''' example, specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode,
            ''' then finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead,
            ''' the planner will further progress to Exhaustive mode.
            ''' </remarks>
            ''' <param name="seconds"> Maximum time, in seconds. </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_set_timelimit", ExactSpelling:=True)>
            Friend Shared Sub Set_timelimit(ByVal seconds As Double)
            End Sub

            ''' <summary>
            ''' Executes an FFTW plan, provided that the input and output arrays still exist.
            ''' </summary>
            ''' <remarks>
            ''' execute (and equivalents) is the only function in FFTW guaranteed to be thread-safe.
            ''' </remarks>
            ''' <param name="plan"> Pointer to the plan to execute. </param>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_execute", ExactSpelling:=True)>
            Friend Shared Sub Execute(ByVal plan As IntPtr)
            End Sub

            ''' <summary> Creates a plan for a 1-dimensional complex-to-complex DFT. </summary>
            ''' <remarks>
            ''' The <paramref>input</paramref> and <paramref>output</paramref> arrays of the transform may be
            ''' the same yielding an in-place transform. These arrays are overwritten during planning, unless
            ''' FFTW_ESTIMATE is used in the options. The arrays need not be initialized, but they must be
            ''' allocated. If in == out, the transform is in-place and the input array is overwritten.
            ''' Otherwise, the two arrays must not overlap. FFTW does not check for this condition.
            ''' </remarks>
            ''' <param name="n">         The logical size (number of elements) of the transform. </param>
            ''' <param name="input">     Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. This equals the sign of the
            '''                          exponent in the formula that defines the Fourier transform. It can be
            '''                          −1 <see cref="TransformDirection.Forward">formward</see> or
            '''                          +1 <see cref="TransformDirection.Backward">inverse (backward)</see>. 
            ''' </param>
            ''' <param name="options">   A bitwise OR of zero or more <see cref="PlannerOptions">planner
            '''                          options</see>
            '''                          specifing the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_1d(ByVal n As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional complex-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">        The logical size (number of elements) of the transform along the
            '''                          first dimension. </param>
            ''' <param name="ny">        The logical size (number of elements) of the transform along the
            '''                          second dimension. </param>
            ''' <param name="input">     Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_2d(ByVal nx As Integer, ByVal ny As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional complex-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">        The logical size (number of elements) of the transform along the
            '''                          first dimension. </param>
            ''' <param name="ny">        The logical size (number of elements) of the transform along the
            '''                          second dimension. </param>
            ''' <param name="nz">        The logical size (number of elements) of the transform along the
            '''                          third dimension. </param>
            ''' <param name="input">     Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional complex-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">      Number of dimensions. </param>
            ''' <param name="n">         Array containing the logical size (number of elements) along each
            '''                          dimension. </param>
            ''' <param name="input">     Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft", ExactSpelling:=True)>
            Friend Shared Function Dft(ByVal rank As Integer, ByVal n As Integer(),
                                       ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of REAL (input) elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_r2c_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_1d(ByVal n As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (input) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (input) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_r2c_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_2d(ByVal nx As Integer, ByVal ny As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (input) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (input) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="nz">      Number of REAL (input) elements in the transform along the third
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_r2c_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of REAL (input) elements along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_r2c", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c(ByVal rank As Integer, ByVal n As Integer(),
                                           ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of REAL (output) elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_c2r_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_1d(ByVal n As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (output) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (output) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_c2r_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_2d(ByVal nx As Integer, ByVal ny As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (output) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (output) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="nz">      Number of REAL (output) elements in the transform along the third
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_c2r_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of REAL (output) elements along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_dft_c2r", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r(ByVal rank As Integer, ByVal n As Integer(),
                                           ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="kind">    The kind of real-to-real transform to compute. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_r2r_1d", ExactSpelling:=True)>
            Friend Shared Function R2r_1d(ByVal n As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kind As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of elements in the transform along the first dimension. </param>
            ''' <param name="ny">      Number of elements in the transform along the second dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="kindx">   The kind of real-to-real transform to compute along the first
            '''                        dimension. </param>
            ''' <param name="kindy">   The kind of real-to-real transform to compute along the second
            '''                        dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_r2r_2d", ExactSpelling:=True)>
            Friend Shared Function R2r_2d(ByVal nx As Integer, ByVal ny As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kindx As TransformKind, ByVal kindy As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of elements in the transform along the first dimension. </param>
            ''' <param name="ny">      Number of elements in the transform along the second dimension. </param>
            ''' <param name="nz">      Number of elements in the transform along the third dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="kindx">   The kind of real-to-real transform to compute along the first
            '''                        dimension. </param>
            ''' <param name="kindy">   The kind of real-to-real transform to compute along the second
            '''                        dimension. </param>
            ''' <param name="kindz">   The kind of real-to-real transform to compute along the third
            '''                        dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_r2r_3d", ExactSpelling:=True)>
            Friend Shared Function R2r_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kindx As TransformKind, ByVal kindy As TransformKind, ByVal kindz As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of elements in the transform along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 4-byte real numbers. </param>
            ''' <param name="kind">    An array containing the kind of real-to-real transform to compute
            '''                        along each dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_plan_r2r", ExactSpelling:=True)>
            Friend Shared Function R2r(ByVal rank As Integer, ByVal n As Integer(),
                                       ByVal input As IntPtr, ByVal output As IntPtr, ByVal kind As TransformKind(), ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Returns (approximately) the number of flops used by a certain plan. </summary>
            ''' <remarks> Total flops ~= add+mul+2*fma or add+mul+fma if fma is supported. </remarks>
            ''' <param name="plan"> The plan to measure. </param>
            ''' <param name="add">  [in,out] Reference to double to hold number of adds. </param>
            ''' <param name="mul">  [in,out] Reference to double to hold number of muls. </param>
            ''' <param name="fma">  [in,out] Reference to double to hold number of fmas (fused multiply-add) </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_flops", ExactSpelling:=True)>
            Friend Shared Sub Flops(ByVal plan As IntPtr, ByRef add As Double, ByRef mul As Double, ByRef fma As Double)
            End Sub

            ''' <summary> Outputs a "nerd-readable" version of the specified plan to stdout. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="plan"> The plan to output. </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3f-3.dll", EntryPoint:="fftwf_print_plan", ExactSpelling:=True)>
            Friend Shared Sub Print_plan(ByVal plan As IntPtr)
            End Sub
        End Class

#End Region

    End Namespace

    Namespace FftwR

#Region " FftwR.SafeNativeMethods: DOUBLE PRECISION INTEROP CLASS "

        ''' <summary>
        ''' Contains the Basic Interface FFTW functions for double-precision (float) operations.
        ''' Contains safe application programming interface calls suppressing stack walks for 
        ''' unmanaged code permission.  Provides methods that are safe for anyone to call. Callers 
        ''' of these methods are not required to do a full security review to ensure that the 
        ''' usage is secure because the methods are harmless for any caller.</summary>
        ''' <remarks> David, 09/01/07 <para>
        ''' (c) Tamas Szalay </para><para> 
        ''' From http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html. </para></remarks>
        <AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface),
       SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
       Scope:="type", Justification:="Exposes the entire API.")>
        Public NotInheritable Class SafeNativeMethods

            ''' <summary> Prevents construction of this class. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            Private Sub New()
            End Sub

            ''' <summary> Allocates FFTW-optimized unmanaged memory. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="length"> Amount to allocate, in bytes. </param>
            ''' <returns> Pointer to allocated memory. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_malloc", ExactSpelling:=True)>
            Friend Shared Function Malloc(ByVal length As Integer) As IntPtr
            End Function

            ''' <summary> Deallocates memory allocated by FFTW malloc. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="pointer"> Pointer to memory to release. </param>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_free", ExactSpelling:=True)>
            Friend Shared Sub Free(ByVal pointer As IntPtr)
            End Sub

            ''' <summary> Deallocates an FFTW plan and all associated resources. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="plan"> Pointer to the plan to release. </param>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_destroy_plan", ExactSpelling:=True)>
            Friend Shared Sub Destroy_plan(ByVal plan As IntPtr)
            End Sub

            ''' <summary>
            ''' Clears all memory used by FFTW, resets it to initial state. Does not replace destroy_plan and
            ''' free.
            ''' </summary>
            ''' <remarks>
            ''' After calling fftw_cleanup, all existing plans become undefined, and you should not attempt
            ''' to execute them nor to destroy them. You can however create and execute/destroy new plans, in
            ''' which case FFTW starts accumulating wisdom information again. fftw_cleanup does not
            ''' deallocate your plans; you should still call fftw_destroy_plan for this purpose.
            ''' </remarks>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3-3.dll", EntryPoint:="fftw_cleanup", ExactSpelling:=True)>
            Friend Shared Sub Cleanup()
            End Sub

            ''' <summary> Sets the maximum time that can be used by the planner. </summary>
            ''' <remarks>
            ''' This function instructs FFTW to spend at most seconds (approximately) in the planner. If
            ''' seconds == -1.0 (the default value), then planning time is unbounded. Otherwise, FFTW plans
            ''' with a progressively wider range of algorithms until the the given time limit is reached or
            ''' the given range of algorithms is explored, returning the best available plan. For example,
            ''' specifying PlannerOptions.Patient first plans in Estimate mode, then in Measure mode, then
            ''' finally (time permitting) in Patient. If PlannerOptions.Exhaustive is specified instead, the
            ''' planner will further progress to Exhaustive mode.
            ''' </remarks>
            ''' <param name="seconds"> Maximum time, in seconds. </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3-3.dll", EntryPoint:="fftw_set_timelimit", ExactSpelling:=True)>
            Friend Shared Sub Set_timelimit(ByVal seconds As Double)
            End Sub

            ''' <summary>
            ''' Executes an FFTW plan, provided that the input and output arrays still exist.
            ''' </summary>
            ''' <remarks>
            ''' execute (and equivalents) is the only function in FFTW guaranteed to be thread-safe.
            ''' </remarks>
            ''' <param name="plan"> Pointer to the plan to execute. </param>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_execute", ExactSpelling:=True)>
            Friend Shared Sub Execute(ByVal plan As IntPtr)
            End Sub

            ''' <summary> Creates a plan for a 1-dimensional complex-to-complex DFT. </summary>
            ''' <remarks>
            ''' The <paramref>input</paramref> and <paramref>output</paramref> arrays of the transform may be
            ''' the same yielding an in-place transform. These arrays are overwritten during planning, unless
            ''' FFTW_ESTIMATE is used in the options. The arrays need not be initialized, but they must be
            ''' allocated. If in == out, the transform is in-place and the input array is overwritten.
            ''' Otherwise, the two arrays must not overlap. FFTW does not check for this condition.
            ''' </remarks>
            ''' <param name="n">         The logical size (number of elements) of the transform. </param>
            ''' <param name="input">     Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. This equals the sign of the
            '''                          exponent in the formula that defines the Fourier transform. It can be
            '''                          −1 <see cref="TransformDirection.Forward">formward</see> or
            '''                          +1 <see cref="TransformDirection.Backward">inverse (backward)</see>. 
            ''' </param>
            ''' <param name="options">   A bitwise OR of zero or more <see cref="PlannerOptions">planner
            '''                          options</see>
            '''                          specifying the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_1d(ByVal n As Integer, ByVal input As IntPtr, ByVal output As IntPtr,
                                          ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional complex-to-complex DFT. </summary>
            ''' <remarks>
            ''' Multi-dimensional arrays are stored in row-major order with dimensions: nx x ny; or nx x ny x
            ''' nz;
            ''' or n[0] x n[1] x ... x n[rank-1].
            ''' </remarks>
            ''' <param name="nx">        The logical size (number of elements) of the transform along the
            '''                          first dimension. </param>
            ''' <param name="ny">        The logical size (number of elements) of the transform along the
            '''                          second dimension. </param>
            ''' <param name="input">     Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_2d(ByVal nx As Integer, ByVal ny As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional complex-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">        The logical size (number of elements) of the transform along the
            '''                          first dimension. </param>
            ''' <param name="ny">        The logical size (number of elements) of the transform along the
            '''                          second dimension. </param>
            ''' <param name="nz">        The logical size (number of elements) of the transform along the
            '''                          third dimension. </param>
            ''' <param name="input">     Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional complex-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">      Number of dimensions of the transform euqaling the length of the
            '''                          array <paramref>n</paramref>. It can be any non-negative integer. The
            '''                          ‘_1d’, ‘_2d’, and ‘_3d’ planners correspond to ranks of 1, 2, and 3,
            '''                          respectively. A rank of zero is equivalent to a transform of size 1,
            '''                          i.e. a copy of one number from input to output. </param>
            ''' <param name="n">         Array containing the logical size (number of elements) along each
            '''                          dimension. </param>
            ''' <param name="input">     Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">    Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="direction"> Specifies the direction of the transform. </param>
            ''' <param name="options">   Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft", ExactSpelling:=True)>
            Friend Shared Function Dft(ByVal rank As Integer, ByVal n As Integer(),
                                       ByVal input As IntPtr, ByVal output As IntPtr, ByVal direction As Integer, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of REAL (input) elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_r2c_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_1d(ByVal n As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (input) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (input) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_r2c_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_2d(ByVal nx As Integer, ByVal ny As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (input) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (input) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="nz">      Number of REAL (input) elements in the transform along the third
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_r2c_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional real-to-complex DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of REAL (input) elements along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_r2c", ExactSpelling:=True)>
            Friend Shared Function Dft_r2c(ByVal rank As Integer, ByVal n As Integer(),
                                           ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of REAL (output) elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_c2r_1d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_1d(ByVal n As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (output) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (output) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_c2r_2d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_2d(ByVal nx As Integer, ByVal ny As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of REAL (output) elements in the transform along the first
            '''                        dimension. </param>
            ''' <param name="ny">      Number of REAL (output) elements in the transform along the second
            '''                        dimension. </param>
            ''' <param name="nz">      Number of REAL (output) elements in the transform along the third
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_c2r_3d", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                              ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional complex-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of REAL (output) elements along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 16-byte complex numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_dft_c2r", ExactSpelling:=True)>
            Friend Shared Function Dft_c2r(ByVal rank As Integer, ByVal n As Integer(),
                                           ByVal input As IntPtr, ByVal output As IntPtr, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 1-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="n">       Number of elements in the transform. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="kind">    The kind of real-to-real transform to compute. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_r2r_1d", ExactSpelling:=True)>
            Friend Shared Function R2r_1d(ByVal n As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kind As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 2-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of elements in the transform along the first dimension. </param>
            ''' <param name="ny">      Number of elements in the transform along the second dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="kindx">   The kind of real-to-real transform to compute along the first
            '''                        dimension. </param>
            ''' <param name="kindy">   The kind of real-to-real transform to compute along the second
            '''                        dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_r2r_2d", ExactSpelling:=True)>
            Friend Shared Function R2r_2d(ByVal nx As Integer, ByVal ny As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kindx As TransformKind, ByVal kindy As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for a 3-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="nx">      Number of elements in the transform along the first dimension. </param>
            ''' <param name="ny">      Number of elements in the transform along the second dimension. </param>
            ''' <param name="nz">      Number of elements in the transform along the third dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="kindx">   The kind of real-to-real transform to compute along the first
            '''                        dimension. </param>
            ''' <param name="kindy">   The kind of real-to-real transform to compute along the second
            '''                        dimension. </param>
            ''' <param name="kindz">   The kind of real-to-real transform to compute along the third
            '''                        dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_r2r_3d", ExactSpelling:=True)>
            Friend Shared Function R2r_3d(ByVal nx As Integer, ByVal ny As Integer, ByVal nz As Integer,
                                          ByVal input As IntPtr, ByVal output As IntPtr, ByVal kindx As TransformKind, ByVal kindy As TransformKind, ByVal kindz As TransformKind, ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Creates a plan for an n-dimensional real-to-real DFT. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="rank">    Number of dimensions. </param>
            ''' <param name="n">       Array containing the number of elements in the transform along each
            '''                        dimension. </param>
            ''' <param name="input">   Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="output">  Pointer to an array of 8-byte real numbers. </param>
            ''' <param name="kind">    An array containing the kind of real-to-real transform to compute
            '''                        along each dimension. </param>
            ''' <param name="options"> Flags that specify the behavior of the planner. </param>
            ''' <returns> An IntPtr. </returns>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3-3.dll", EntryPoint:="fftw_plan_r2r", ExactSpelling:=True)>
            Friend Shared Function R2r(ByVal rank As Integer, ByVal n As Integer(),
                                       ByVal input As IntPtr, ByVal output As IntPtr, ByVal kind As TransformKind(), ByVal options As UInteger) As IntPtr
            End Function

            ''' <summary> Returns (approximately) the number of flops used by a certain plan. </summary>
            ''' <remarks> Total flops ~= add+mul+2*fma or add+mul+fma if fma is supported. </remarks>
            ''' <param name="plan"> The plan to measure. </param>
            ''' <param name="add">  [in,out] Reference to double to hold number of adds. </param>
            ''' <param name="mul">  [in,out] Reference to double to hold number of muls. </param>
            ''' <param name="fma">  [in,out] Reference to double to hold number of fmas (fused multiply-add) </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3-3.dll", EntryPoint:="fftw_flops", ExactSpelling:=True)>
            Friend Shared Sub Flops(ByVal plan As IntPtr, ByRef add As Double, ByRef mul As Double, ByRef fma As Double)
            End Sub

            ''' <summary> Outputs a "nerd-readable" version of the specified plan to stdout. </summary>
            ''' <remarks> David, 2020-10-26. </remarks>
            ''' <param name="plan"> The plan to output. </param>
            <SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"),
                DllImport("libfftw3-3.dll", EntryPoint:="fftw_print_plan", ExactSpelling:=True)>
            Friend Shared Sub Print_plan(ByVal plan As IntPtr)
            End Sub

        End Class

#End Region

    End Namespace

End Namespace
